#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <cstdint>

#include "hellmouth/data/unsigned_integer.h"
#include "hellmouth/data/signed_integer.h"

using namespace hellmouth;
using data::unsigned_integer;
using data::signed_integer;

TEST_CASE("data::signed_integer::signed_integer(unsigned_integer)") {
	bool xs = GENERATE(false, true);
	uint64_t xm = GENERATE(0, 1, 0x7fffffffUL, 0x80000000ULL, 0xffffffffffffffffULL);
	if (xm == 0) {
		xs = 0;
	}

	signed_integer x(xs, unsigned_integer(xm));
	REQUIRE(x.sign() == xs);
	unsigned_integer y;
	y = std::move(x);
	REQUIRE(uint64_t(y) == xm);
}

TEST_CASE("data::signed_integer::operator+=(signed_chunk_type)") {
	int64_t x = GENERATE(0, 1, 2, 0x7fffffffL, 0x80000000LL, -1, -2, -0x7fffffffL, -0x80000000L, -0x80000001LL);
	int32_t y = GENERATE(0, 1, 2, 0x7fffffffL, -1, -2, -0x7fffffffL, -0x80000000L);

	uint64_t xm = x;
	bool xs = (x < 0);
	if (xs) {
		xm = -xm;
	}

	signed_integer z(xs, unsigned_integer(xm));
	z += y;
	bool zs = z.sign();
	unsigned_integer zm;
	zm = std::move(z);

	if (x + y < 0) {
		REQUIRE(zs == true);
		REQUIRE(zm == uint64_t(-(x + y)));
	} else {
		REQUIRE(zs == false);
		REQUIRE(zm == uint64_t(x + y));
	}
}

TEST_CASE("data::signed_integer::operator+-=(signed_chunk_type)") {
	int64_t x = GENERATE(0, 1, 2, 0x7fffffffL, 0x80000000LL, -1, -2, -0x7fffffffL, -0x80000000L, -0x80000001LL);
	int32_t y = GENERATE(0, 1, 2, 0x7fffffffL, -1, -2, -0x7fffffffL, -0x80000000L);

	uint64_t xm = x;
	bool xs = (x < 0);
	if (xs) {
		xm = -xm;
	}

	signed_integer z(xs, unsigned_integer(xm));
	z -= y;
	bool zs = z.sign();
	unsigned_integer zm;
	zm = std::move(z);

	if (x - y < 0) {
		REQUIRE(zs == true);
		REQUIRE(zm == uint64_t(-(x - y)));
	} else {
		REQUIRE(zs == false);
		REQUIRE(zm == uint64_t(x - y));
	}
}
