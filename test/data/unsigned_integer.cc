#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <cstdint>

#include "hellmouth/data/unsigned_integer.h"

using namespace hellmouth;
using data::unsigned_integer;

TEST_CASE("data::unsigned_integer::unsigned_integer(uint16_t)") {
	uint16_t x = GENERATE(0, 1, 0xff, 0x100, 0xffffU);
	unsigned_integer y(x);
	const unsigned_integer::content_type& yc = y;
	if (x == 0) {
		REQUIRE(yc.size() == 0);
	} else {
		REQUIRE(yc.size() == 1);
		REQUIRE(yc[0] == x);
	}
}

TEST_CASE("data::unsigned_integer::unsigned_integer(uint32_t)") {
	uint32_t x = GENERATE(0, 1, 0xffffU, 0x10000UL, 0xffffffffUL);
	unsigned_integer y(x);
	const unsigned_integer::content_type& yc = y;
	if (x == 0) {
		REQUIRE(yc.size() == 0);
	} else {
		REQUIRE(yc.size() == 1);
		REQUIRE(yc[0] == x);
	}
}

TEST_CASE("data::unsigned_integer::unsigned_integer(uint64_t)") {
	uint64_t x = GENERATE(0, 1, 0xffffffffUL, 0x100000000ULL, 0xffffffffffffffffULL);
	unsigned_integer y(x);
	const unsigned_integer::content_type& yc = y;
	if (x == 0) {
		REQUIRE(yc.size() == 0);
	} else if (x <= 0xffffffffUL) {
		REQUIRE(yc.size() == 1);
		REQUIRE(yc[0] == x);
	} else {
		REQUIRE(yc.size() == 2);
		REQUIRE(yc[0] == uint32_t(x));
		REQUIRE(yc[1] == uint32_t(x >> 32));
	}
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [empty]") {
        unsigned_integer x;
        REQUIRE_THROWS(x = unsigned_integer(""));
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [decimal]") {
	std::string x = "314159265358979323";
	uint64_t y = 314159265358979323ULL;

	while (!x.empty()) {
		unsigned_integer z(x);
		REQUIRE(uint64_t(z) == y);
		x.pop_back();
		y /= 10;
	}
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [hex prefix]") {
        unsigned_integer x("0x2a");
        REQUIRE(uint32_t(x) == 42);
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [octal prefix]") {
        unsigned_integer x("0755");
        REQUIRE(uint32_t(x) == 0755);
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [invalid base]") {
        unsigned_integer x;
        REQUIRE_THROWS(x = unsigned_integer("0", 1));
        REQUIRE_THROWS(x = unsigned_integer("0", 37));
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [not a digit]") {
        unsigned_integer x;
        REQUIRE_THROWS(x = unsigned_integer("@", 16));
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [digit out of range]") {
        unsigned_integer x;
        REQUIRE_THROWS(x = unsigned_integer("FF00", 15));
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [upper case digits]") {
        unsigned_integer x("FF00", 16);
        REQUIRE(x == 65280U);
}

TEST_CASE("data::unsigned_integer::unsigned_integer(std::string_view) [lower case digits]") {
        unsigned_integer x("ff00", 16);
        REQUIRE(x == 65280U);
}

TEST_CASE("data::unsigned_integer::operator uint16_t()") {
	uint16_t x = GENERATE(0, 1, 0xff, 0x100, 0xffffU);
	unsigned_integer y(x);
	REQUIRE(uint16_t(y) == x);
}

TEST_CASE("data::unsigned_integer::operator uint16_t() [error]") {
	unsigned_integer y(0x10000U);
	REQUIRE_THROWS(uint16_t(y));
}

TEST_CASE("data::unsigned_integer::operator uint32_t()") {
	uint32_t x = GENERATE(0, 1, 0xffffU, 0x10000U, 0xffffffffUL);
	unsigned_integer y(x);
	REQUIRE(uint32_t(y) == x);
}

TEST_CASE("data::unsigned_integer::operator uint32_t() [error]") {
	unsigned_integer y;
	y.push_back(0);
	y.push_back(1);
	REQUIRE_THROWS(uint32_t(y));
}
TEST_CASE("data::unsigned_integer::operator uint64_t()") {
	uint64_t x = GENERATE(0, 1, 0xffffffffUL, 0x100000000UL, 0xffffffffffffffffULL);
	unsigned_integer y(x);
	REQUIRE(uint64_t(y) == x);
}

TEST_CASE("data::unsigned_integer::operator uint64_t() [error]") {
	unsigned_integer y;
	y.push_back(0);
	y.push_back(0);
	y.push_back(1);
	REQUIRE_THROWS(uint64_t(y));
}

TEST_CASE("data::unsigned_integer::empty()") {
	unsigned_integer x(0U);
	REQUIRE(x.empty());
	x = 1U;
	REQUIRE(!x.empty());
	x = 0U;
	REQUIRE(x.empty());
}

TEST_CASE("data::unsigned_integer::operator+(chunk_type)") {
	uint64_t x = GENERATE(0, 1, 0xffffffffUL, 0x100000000ULL, 0xffffffffffffffffULL);
	uint32_t y = GENERATE(0, 1, 0xffffffffUL);
	uint64_t z = x + y;

	unsigned_integer xx(x);
	unsigned_integer zz = xx + y;
	const unsigned_integer::content_type& zc = zz;

	if (z < x) {
		REQUIRE(zc.size() == 3);
	} else if ((z >> 32) != 0) {
		REQUIRE(zc.size() == 2);
	} else if (z != 0) {
		REQUIRE(zc.size() == 1);
	} else {
		REQUIRE(zc.size() == 0);
	}
	if (zc.size() >= 1) {
		REQUIRE(zc[0] == uint32_t(z));
	}
	if (zc.size() >= 2) {
		REQUIRE(zc[1] == uint32_t(z >> 32));
	}
	if (zc.size() == 3) {
		REQUIRE(zc[2] == 1);
	}
}

TEST_CASE("data::unsigned_integer::operator*(chunk_type)") {
	uint64_t x = GENERATE(0, 1, 0x10000UL, 0xffffffffUL, 0x1000000000000ULL, 0xffffffffffffffffULL);
	uint32_t y = GENERATE(0, 1, 2, 0x10000UL, 0xffffffffUL);
	uint64_t z0 = uint64_t(uint32_t(x)) * y;
	uint64_t z1 = (x >> 32) * y;
	z1 += z0 >> 32;

	unsigned_integer xx(x);
	unsigned_integer zz = xx * y;
	const unsigned_integer::content_type& zc = zz;

	if (z1 >> 32) {
		REQUIRE(zc.size() == 3);
	} else if (z1 != 0) {
		REQUIRE(zc.size() == 2);
	} else if (z0 != 0) {
		REQUIRE(zc.size() == 1);
	} else {
		REQUIRE(zc.size() == 0);
	}
	if (zc.size() >= 1) {
		REQUIRE(zc[0] == uint32_t(z0));
	}
	if (zc.size() >= 2) {
		REQUIRE(zc[1] == uint32_t(z1));
	}
	if (zc.size() == 3) {
		REQUIRE(zc[2] == uint32_t(z1 >> 32));
	}
}

TEST_CASE("data::unsigned_integer::operator/(0)") {
	REQUIRE_THROWS(unsigned_integer(1U) / 0U);
}

TEST_CASE("data::unsigned_integer::operator/(chunk_type)") {
	uint64_t x = GENERATE(0, 1, 0x10000UL, 0xffffffffUL, 0x1000000000000ULL, 0xffffffffffffffffULL);
	uint32_t y = GENERATE(1, 2, 7, 0xffffU, 0x10000UL, 0xffffffffUL);

	unsigned_integer xx(x);
	unsigned_integer ww = xx * y;
	unsigned_integer zz0 = ww / y;
	unsigned_integer zz1 = (ww + (y - 1)) / y;
	unsigned_integer zz2 = (ww + y) / y;

	REQUIRE(zz0 == xx);
	REQUIRE(zz1 == xx);
	REQUIRE(zz2 == (xx + 1));
}

TEST_CASE("data::unsigned_integer::operator%(0)") {
	REQUIRE_THROWS(unsigned_integer(1U) % 0U);
}

TEST_CASE("data::unsigned_integer::operator%(chunk_type)") {
	uint64_t x = GENERATE(0, 1, 0x10000UL, 0xffffffffUL, 0x1000000000000ULL, 0xffffffffffffffffULL);
	uint32_t y = GENERATE(1, 2, 7, 0xffffU, 0x10000UL, 0xffffffffUL);

	unsigned_integer xx(x);
	unsigned_integer ww = xx * y;
	auto zz0 = ww % y;
	auto zz1 = (ww + (y - 1)) % y;
	auto zz2 = (ww + y) % y;

	REQUIRE(zz0 == 0);
	REQUIRE(zz1 == (y - 1));
	REQUIRE(zz2 == 0);
}

TEST_CASE("data::unsigned_integer::operator==") {
	uint64_t x = GENERATE(0, 1, 7, 0xffffffffUL, 0x100000000ULL, 0xffffffffffffffffULL);
	uint64_t y = GENERATE(0, 1, 7, 0xffffffffUL, 0x100000000ULL, 0xffffffffffffffffULL);

	unsigned_integer xx(x);
	unsigned_integer yy(y);
	REQUIRE((xx == yy) == (x == y));
}

TEST_CASE("data::unsigned_integer::operator<=>") {
	uint64_t x = GENERATE(0, 1, 7, 0xffffffffUL, 0x100000000ULL, 0xffffffffffffffffULL);
	uint64_t y = GENERATE(0, 1, 7, 0xffffffffUL, 0x100000000ULL, 0xffffffffffffffffULL);

	unsigned_integer xx(x);
	unsigned_integer yy(y);
	REQUIRE((xx <=> yy) == (x <=> y));
}

TEST_CASE("to_string(data::unsigned_integer) [invalid base]") {
        unsigned_integer x;
        REQUIRE_THROWS(to_string(x, 0));
        REQUIRE_THROWS(to_string(x, 1));
        REQUIRE_THROWS(to_string(x, 37));
}

TEST_CASE("to_string(data::unsigned_integer) [zero]") {
        unsigned_integer x;
        REQUIRE(to_string(x) == "0");
}

TEST_CASE("to_string(data::unsigned_integer) [decimal]") {
	std::string x = "314159265358979323";
	uint64_t y = 314159265358979323ULL;

	while (!x.empty()) {
		unsigned_integer z(y);
		REQUIRE(to_string(z) == x);
		x.pop_back();
		y /= 10;
	}
}

TEST_CASE("to_string(data::unsigned_integer) [hex]") {
	std::string x = "fedcba9876543210";
	uint64_t y = 0xFEDCBA9876543210ULL;

	while (!x.empty()) {
		unsigned_integer z(y);
		REQUIRE(to_string(z, 16) == x);
		x.pop_back();
		y /= 16;
	}
}

TEST_CASE("to_string(data::unsigned_integer) [binary]") {
	unsigned_integer x(42U);
	REQUIRE(to_string(x, 2) == "101010");
}

TEST_CASE("to_string(data::unsigned_integer) [base-36]") {
	unsigned_integer x(2628U);
	REQUIRE(to_string(x, 36) == "210");
}
