#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include <utility>

#include "hellmouth/data/integer.h"

using namespace hellmouth;

TEST_CASE("data::integer::operator+=(signed_chunk_type)") {
	int64_t x = GENERATE(0, 1, 2, 0x7fffffffL, 0x80000000LL, -1, -2, -0x7fffffffL, -0x80000000L, -0x80000001LL);
	int32_t y = GENERATE(0, 1, 2, 0x7fffffffL, -1, -2, -0x7fffffffL, -0x80000000L);
	data::integer xx(x);
	data::integer zz = xx + y;
	int64_t z = zz;
	REQUIRE(z == x + y);
}

TEST_CASE("data::integer::operator-=(signed_chunk_type)") {
	int64_t x = GENERATE(0, 1, 2, 0x7fffffffL, 0x80000000LL, -1, -2, -0x7fffffffL, -0x80000000L, -0x80000001LL);
	int32_t y = GENERATE(0, 1, 2, 0x7fffffffL, -1, -2, -0x7fffffffL, -0x80000000L);
	data::integer xx(x);
	data::integer zz = xx - y;
	int64_t z = zz;
	REQUIRE(z == x - y);
}

TEST_CASE("data::integer::operator*(signed_chunk_type)") {
	int64_t x = GENERATE(
		-0x100000000LL, -0xffffffffLL, -0x80000000L, -0x7fffffffL, -6, -1,
		0, 1, 6, 0x7fffffffL, 0x80000000LL, 0xffffffffLL, 0x100000000LL);
	int32_t y = GENERATE(-0x80000000L, -0x7fffffffL, -9, -1, 0, 1, 9, 0x7fffffffL);

	if (std::in_range<int32_t>(x) || std::in_range<int16_t>(y)) {
		data::integer xx(x);
		data::integer zz = xx * y;
		int64_t z = zz;
		REQUIRE(z == x * y);
	}
}

TEST_CASE("data::integer::operator/(0)") {
	REQUIRE_THROWS(data::integer(1) / 0);
}

TEST_CASE("data::integer::operator/(signed_chunk_type)") {
	int64_t x = GENERATE(
		-0x100000000LL, -0xffffffffLL, -0x80000000L, -0x7fffffffL, -6, -1,
		1, 6, 0x7fffffffL, 0x80000000LL, 0xffffffffLL, 0x100000000LL);
	int32_t y = GENERATE(-0x80000000L, -0x7fffffffL, -9, -1, 1, 9, 0x7fffffffL);

	data::integer xx(x);
	data::integer ww = xx * y;
	data::integer zz0 = ww / y;
	if ((ww.negative()) ^ (y < 0)) {
		ww -= y;
	} else {
		ww += y;
	}
	data::integer zz2 = ww / y;
	if (ww.negative()) {
		ww += 1;
	} else {
		ww -= 1;
	}
	data::integer zz1 = ww / y;

	REQUIRE(zz0 == xx);
	REQUIRE(zz1 == xx);
	if (xx.negative()) {
		REQUIRE(zz2 == (xx - 1));
	} else {
		REQUIRE(zz2 == (xx + 1));
	}
}

TEST_CASE("data::integer::operator%(0)") {
        REQUIRE_THROWS(data::integer(1) % 0);
}

TEST_CASE("data::integer::operator%(signed_chunk_type)") {
	int64_t x = GENERATE(
		-0x100000000LL, -0xffffffffLL, -0x80000000L, -0x7fffffffL, -6, -1,
		1, 6, 0x7fffffffL, 0x80000000LL, 0xffffffffLL, 0x100000000LL);
	int32_t y = GENERATE(-0x80000000L, -0x7fffffffL, -9, -1, 1, 9, 0x7fffffffL);

	data::integer xx(x);
	data::integer ww = xx * y;
	data::integer zz0 = ww % y;
	if ((ww.negative()) ^ (y < 0)) {
		ww -= y;
	} else {
		ww += y;
	}
	data::integer zz2 = ww % y;
	if (ww.negative()) {
		ww += 1;
	} else {
		ww -= 1;
	}
	data::integer zz1 = ww % y;

	int64_t ym = y;
	if (ym < 0) {
		ym = -ym;
	}

	REQUIRE(zz0 == data::integer(0));
	if (ww.negative()) {
		REQUIRE(zz1 == data::integer(1 - ym));
	} else {
		REQUIRE(zz1 == data::integer(ym - 1));
	}
        REQUIRE(zz2 == data::integer(0));
}

TEST_CASE("data::integer::operator==") {
	int64_t x = GENERATE(
		-0x8000000000000000LL, -0x7fffffffffffffffLL, -0x100000000LL, -0xffffffffLL, -7, -1,
		0, 1, 7, 0xffffffffLL, 0x100000000LL, 0x7fffffffffffffffLL);
	int64_t y = GENERATE(
		-0x8000000000000000LL, -0x7fffffffffffffffLL, -0x100000000LL, -0xffffffffLL, -7, -1,
		0, 1, 7, 0xffffffffLL, 0x100000000LL, 0x7fffffffffffffffLL);

        data::integer xx(x);
        data::integer yy(y);
        REQUIRE((xx == yy) == (x == y));
}

TEST_CASE("data::integer::operator<=>") {
	int64_t x = GENERATE(
		-0x8000000000000000LL, -0x7fffffffffffffffLL, -0x100000000LL, -0xffffffffLL, -7, -1,
		0, 1, 7, 0xffffffffLL, 0x100000000LL, 0x7fffffffffffffffLL);
	int64_t y = GENERATE(
		-0x8000000000000000LL, -0x7fffffffffffffffLL, -0x100000000LL, -0xffffffffLL, -7, -1,
		0, 1, 7, 0xffffffffLL, 0x100000000LL, 0x7fffffffffffffffLL);

        data::integer xx(x);
        data::integer yy(y);
        REQUIRE((xx <=> yy) == (x <=> y));
}
