#include <catch2/catch_test_macros.hpp>

#include "hellmouth/data/object.h"
#include "hellmouth/data/any.h"

using namespace hellmouth;

TEST_CASE("data::any::merge") {
	data::any root;
	root["foo"] = 1;
	root["bar"] = 2;
	root["baz"]["spam"] = 3;
	root["baz"]["ham"] = 4;
	root["qux"] = 6;

	data::any patch;
	patch["foo"] = data::object();
	patch["bar"] = data::any();
	patch["baz"]["ham"] = data::any();
	patch["baz"]["eggs"] = 7;
	patch["quux"] = 8;

	root.merge(patch);

	REQUIRE(root["foo"].cobject().empty());
	REQUIRE(!root.contains("bar"));
	REQUIRE(int(root["baz"]["spam"]) == 3);
	REQUIRE(!root["baz"].contains("ham"));
	REQUIRE(int(root["baz"]["eggs"]) == 7);
	REQUIRE(int(root["qux"]) == 6);
	REQUIRE(int(root["quux"]) == 8);
}
