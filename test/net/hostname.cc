// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/hostname.h"

using namespace hellmouth;

TEST_CASE("net::hostname::hostname(string_view) [valid]") {
	net::hostname host("www.example.com");
	REQUIRE(std::string(host) == "www.example.com");
}

TEST_CASE("net::hostname::hostname(string_view) [empty]") {
	REQUIRE_THROWS(net::hostname(""));
}

TEST_CASE("net::hostname::hostname(string_view) [empty-label]") {
	REQUIRE_THROWS(net::hostname(".example.com"));
}

TEST_CASE("net::hostname::hostname(string_view) [leading-hyphen]") {
	REQUIRE_THROWS(net::hostname("www.-example.com"));
}

TEST_CASE("net::hostname::hostname(string_view) [trailing-hyphen]") {
	REQUIRE_THROWS(net::hostname("www.example-.com"));
}

TEST_CASE("net::hostname::hostname(string_view) [internal-hyphen]") {
	net::hostname host("www.ex-ample.com");
	REQUIRE(std::string(host) == "www.ex-ample.com");
}
