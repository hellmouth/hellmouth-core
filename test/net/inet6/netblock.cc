// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/inet6/netblock.h"

using namespace hellmouth;

TEST_CASE("net::inet6::netblock::netblock(string_view) [strictly-valid]") {
        uint8_t expected[16] = {
                0xfd, 0x34, 0xae, 0x96, 0xc6, 0xfc, 0xed, 0x4a,
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        net::inet6::netblock nb("fd34:ae96:c6fc:ed4a::/64", false);
        REQUIRE(memcmp(nb.address(), expected, 16) == 0);
	REQUIRE(nb.prefix_length() == 64);
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [strictly invalid]") {
        REQUIRE_THROWS(net::inet6::netblock("fd34:ae96:c6fc:ed4a:8000::/64"));
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [non-strict]") {
        uint8_t expected[16] = {
                0xfd, 0x34, 0xae, 0x96, 0xc6, 0xfc, 0xed, 0x4a,
                0x57, 0xba, 0xaf, 0x56, 0xce, 0xe3, 0x8f, 0x7c};
        net::inet6::netblock nb("fd34:ae96:c6fc:ed4a:57ba:af56:cee3:8f7c/64", false);
        REQUIRE(memcmp(nb.address(), expected, 16) == 0);
	REQUIRE(nb.prefix_length() == 64);
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [default route]") {
	uint8_t expected[16] = {0};
	net::inet6::netblock nb("::/0");
	REQUIRE(memcmp(nb.address(), expected, 16) == 0);
	REQUIRE(nb.prefix_length() == 0);
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [single host]") {
        uint8_t expected[16] = {
                0xfd, 0x34, 0xae, 0x96, 0xc6, 0xfc, 0xed, 0x4a,
                0x57, 0xba, 0xaf, 0x56, 0xce, 0xe3, 0x8f, 0x7c};
        net::inet6::netblock nb("fd34:ae96:c6fc:ed4a:57ba:af56:cee3:8f7c/128");
        REQUIRE(memcmp(nb.address(), expected, 16) == 0);
	REQUIRE(nb.prefix_length() == 128);
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [no prefix length]") {
        uint8_t expected[16] = {
                0xfd, 0x34, 0xae, 0x96, 0xc6, 0xfc, 0xed, 0x4a,
                0x57, 0xba, 0xaf, 0x56, 0xce, 0xe3, 0x8f, 0x7c};
        net::inet6::netblock nb("fd34:ae96:c6fc:ed4a:57ba:af56:cee3:8f7c");
        REQUIRE(memcmp(nb.address(), expected, 16) == 0);
	REQUIRE(nb.prefix_length() == 128);
}

TEST_CASE("inet::inet6::netblock::netblock(string_view) [empty prefix length]") {
        REQUIRE_THROWS(net::inet6::netblock("::/"));
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [invalid prefix length]") {
        REQUIRE_THROWS(net::inet6::netblock("::/a"));
}

TEST_CASE("net::inet6::netblock::netblock(string_view) [high prefix length]") {
        REQUIRE_THROWS(net::inet6::netblock("::/129"));
}
