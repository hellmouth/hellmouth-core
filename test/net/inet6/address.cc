// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/inet6/address.h"

using namespace hellmouth;

TEST_CASE("net::inet6::address::address() [any]") {
	uint8_t expected[16] = {0};
	net::inet6::address addr("::");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [loopback]") {
	uint8_t expected[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01};
	net::inet6::address addr("::1");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [full]") {
	uint8_t expected[16] = {
		0xfd, 0x34, 0xae, 0x96, 0xc6, 0xfc, 0xed, 0x4a,
		0x57, 0xba, 0xaf, 0x56, 0xce, 0xe3, 0x8f, 0x7c};

	net::inet6::address addr("fd34:ae96:c6fc:ed4a:57ba:af56:cee3:8f7c");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [compress-1]") {
	uint8_t expected[16] = {
		0xfd, 0x34, 0xae, 0x96, 0x00, 0x00, 0xed, 0x4a,
		0x57, 0xba, 0xaf, 0x56, 0xce, 0xe3, 0x8f, 0x7c};

	net::inet6::address addr("fd34:ae96::ed4a:57ba:af56:cee3:8f7c");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [compress-2]") {
	uint8_t expected[16] = {
		0xfd, 0x34, 0xae, 0x96, 0xc6, 0xfc, 0xed, 0x4a,
		0x00, 0x00, 0x00, 0x00, 0xce, 0xe3, 0x8f, 0x7c};

	net::inet6::address addr("fd34:ae96:c6fc:ed4a::cee3:8f7c");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [compress-6]") {
	uint8_t expected[16] = {
		0xfd, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x8f, 0x7c};

	net::inet6::address addr("fd34::8f7c");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [trailing]") {
	uint8_t expected[16] = {
		0xfd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	net::inet6::address addr("fd00::");
	REQUIRE(memcmp(addr, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [ipv4-compatibility]") {
	uint8_t expected[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0xc0, 0xa8, 0x00, 0x01};
	net::inet6::address addr("::192.168.0.1");
	const void* addrraw = addr;
	REQUIRE(memcmp(addrraw, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [ipv4-mapped]") {
	uint8_t expected[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0xff, 0xff, 0xc0, 0xa8, 0x00, 0x01};
	net::inet6::address addr("::ffff:192.168.0.1");
	const void* addrraw = addr;
	REQUIRE(memcmp(addrraw, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::address() [ipv4-translated]") {
	uint8_t expected[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xff, 0xff, 0x00, 0x00, 0xc0, 0xa8, 0x00, 0x01};
	net::inet6::address addr("::ffff:0:192.168.0.1");
	const void* addrraw = addr;
	REQUIRE(memcmp(addrraw, expected, 16) == 0);
}

TEST_CASE("net::inet6::address::operator std::string() [any]") {
	net::inet6::address addr;
	std::string addrstr = addr;
	REQUIRE(addrstr == "::");
}

TEST_CASE("net::inet6::address::operator std::string() [leading zeros]") {
	uint8_t addrraw[16] = {0};
	addrraw[15] = 1;
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "::1");
}

TEST_CASE("net::inet6::address::operator std::string() [trailing zeros]") {
	uint8_t addrraw[16] = {0};
	addrraw[0] = 0xfd;
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "fd00::");
}

TEST_CASE("net::inet6::address::operator std::string() [single zero]") {
	uint8_t addrraw[16] = {
		0xfd, 0xc2, 0x2d, 0x70, 0x00, 0x00, 0xbd, 0xa8,
		0x97, 0xf1, 0x36, 0x19, 0x44, 0x28, 0x48, 0x93};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "fdc2:2d70:0:bda8:97f1:3619:4428:4893");
}

TEST_CASE("net::inet6::address::operator std::string() [double zero]") {
	uint8_t addrraw[16] = {
		0xfd, 0x16, 0xa3, 0x2a, 0x00, 0x00, 0x00, 0x00,
		0xd4, 0x40, 0x70, 0xa7, 0x63, 0x17, 0x31, 0x33};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "fd16:a32a::d440:70a7:6317:3133");
}

TEST_CASE("net::inet6::address::operator std::string() [longest wins]") {
	uint8_t addrraw[16] = {
		0xfd, 0x5c, 0x00, 0x00, 0x00, 0x00, 0xd9, 0x63,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x8f, 0xe1};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "fd5c:0:0:d963::8fe1");
}

TEST_CASE("net::inet6::address::operator std::string() [first wins]") {
	uint8_t addrraw[16] = {
		0xfd, 0x1d, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x9e,
		0x41, 0x08, 0x00, 0x00, 0x00, 0x00, 0x26, 0x21};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "fd1d::c09e:4108:0:0:2621");
}

TEST_CASE("net::inet6::address::operator std::string() [IPv4-compatible]") {
	uint8_t addrraw[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0xc0, 0xa8, 0x00, 0x01};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "::192.168.0.1");
}

TEST_CASE("net::inet6::address::operator std::string() [IPv4-mapped]") {
	uint8_t addrraw[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0xff, 0xff, 0xc0, 0xa8, 0x00, 0x01};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "::ffff:192.168.0.1");
}

TEST_CASE("net::inet6::address::operator std::string() [IPv4-translated]") {
	uint8_t addrraw[16] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0xff, 0xff, 0x00, 0x00, 0xc0, 0xa8, 0x00, 0x01};
	net::inet6::address addr(addrraw);
	std::string addrstr = addr;
	REQUIRE(addrstr == "::ffff:0:192.168.0.1");
}
