// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/version.h"

using namespace hellmouth;

TEST_CASE("net::http::version::version(unsigned long, unsigned long) [valid]") {
	net::http::version ver(2, 4);
	REQUIRE(ver.major == 2);
	REQUIRE(ver.minor == 4);
}

TEST_CASE("net::http::version::version(string_view) [valid]") {
	net::http::version ver("HTTP/3.6");
	REQUIRE(ver.major == 3);
	REQUIRE(ver.minor == 6);
}

TEST_CASE("net::http::version::version(string_view) [large]") {
	net::http::version ver("HTTP/12.345");
	REQUIRE(ver.major == 12);
	REQUIRE(ver.minor == 345);
}

TEST_CASE("net::http::version::version(string_view) [leading-zeros]") {
	net::http::version ver("HTTP/0023.000456");
	REQUIRE(ver.major == 23);
	REQUIRE(ver.minor == 456);
}

TEST_CASE("net::http::version::version(string_view) [trailing-non-digit]") {
	REQUIRE_THROWS(net::http::version("HTTP/1.1a"));
}

TEST_CASE("net::http::version::version(string_view) [wrong-prefix]") {
	REQUIRE_THROWS(net::http::version("HTTP:1,1"));
}

TEST_CASE("net::http::version::version(string_view) [wrong-separator]") {
	REQUIRE_THROWS(net::http::version("HTTP/1,1"));
}

TEST_CASE("net::http::version::version(string_view) [empty-major]") {
	REQUIRE_THROWS(net::http::version("HTTP/.1"));
}

TEST_CASE("net::http::version::version(string_view) [empty-minor]") {
	REQUIRE_THROWS(net::http::version("HTTP/1."));
}
