// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/header_name.h"

using namespace hellmouth;

TEST_CASE("net::http::header_name::header_name(const char*) [valid]") {
	net::http::header_name tk("Foo");
	REQUIRE(std::string(tk) == "Foo");
}

TEST_CASE("net::http::header_name::header_name(const char*) [invalid]") {
	REQUIRE_THROWS(net::http::header_name("foo,bar"));
}

TEST_CASE("net::http::header_name::operator== [equivalent]") {
	net::http::header_name tk1a("foobar");
	net::http::header_name tk1b("FooBar");
	net::http::header_name tk1c("FOOBAR");
	REQUIRE(tk1a == tk1b);
	REQUIRE(tk1b == tk1c);
	REQUIRE(tk1c == tk1a);
	REQUIRE(tk1b == tk1a);
	REQUIRE(tk1c == tk1b);
	REQUIRE(tk1a == tk1c);
}

TEST_CASE("net::http::header_name::operator<=> [equivalent]") {
	net::http::header_name tk1a("foo");
	net::http::header_name tk1b("Foo");
	net::http::header_name tk2a("bar");
	net::http::header_name tk2b("Bar");
	net::http::header_name tk3a("qux");
	net::http::header_name tk3b("Qux");
	REQUIRE((tk1a <=> tk1b) == 0);
	REQUIRE((tk1a <=> tk2a) > 0);
	REQUIRE((tk1a <=> tk2b) > 0);
	REQUIRE((tk1a <=> tk3a) < 0);
	REQUIRE((tk1a <=> tk3b) < 0);
	REQUIRE((tk1b <=> tk2a) > 0);
	REQUIRE((tk1b <=> tk2b) > 0);
	REQUIRE((tk1b <=> tk3a) < 0);
	REQUIRE((tk1b <=> tk3b) < 0);
}
