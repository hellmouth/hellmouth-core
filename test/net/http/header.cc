// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/header.h"

using namespace hellmouth;

TEST_CASE("operator<<(std::ostream, const header&") {
	net::http::header hdr("Foo", "Bar");
	std::ostringstream out;
	out << hdr;
	REQUIRE(out.str() == "Foo: Bar\r\n");
}
