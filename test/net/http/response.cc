// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/body.h"
#include "hellmouth/net/http/response.h"

using namespace hellmouth;

TEST_CASE("response::response(std::istream)") {
	std::string body = R"({"foo": "bar"})";
	auto raw = std::make_unique<std::stringstream>();
	*raw << "HTTP/1.414 200 OK\r\n"
		<< "Content-Type: text/plain\r\n"
		<< "\r\n"
		<< body;
	net::http::response resp(std::move(raw));

	REQUIRE(resp.status_code == 200);
	REQUIRE(resp.reason_phrase == "OK");
	REQUIRE(to_string(resp.version) == "HTTP/1.414");
	REQUIRE(resp.headers.begin()->name == "Content-Type");
	REQUIRE(resp.headers.begin()->value == "text/plain");
	REQUIRE(std::next(resp.headers.begin()) == resp.headers.end());
	std::ostringstream out;
	out << resp.body.content().rdbuf();
	REQUIRE(out.str() == body);
}

TEST_CASE("operator<<(std::ostream, const response&") {
	std::string body = R"({"foo": "bar"})";
	net::http::response resp(200, "OK", net::http::version("HTTP/1.414"));
	resp.headers.push_back("Content-Type", "text/plain");
	resp.body = net::http::body(body);
	std::ostringstream out;
	out << resp;

	std::ostringstream expect;
	expect << "HTTP/1.414 200 OK\r\n"
		<< "Content-Type: text/plain\r\n"
		<< "\r\n"
		<< body;

	REQUIRE(out.str() == expect.str());
}
