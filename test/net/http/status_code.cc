// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/status_code.h"

using namespace hellmouth;

TEST_CASE("net::http::status_code::status_code(unsigned int) [valid]") {
	net::http::status_code status(200);
	REQUIRE(status == 200);
}

TEST_CASE("net::http::status_code::status_code(unsigned int) [minimum]") {
	net::http::status_code status(100);
	REQUIRE(status == 100);
}

TEST_CASE("net::http::status_code::status_code(unsigned int) [maximum]") {
	net::http::status_code status(599);
	REQUIRE(status == 599);
}

TEST_CASE("net::http::status_code::status_code(unsigned int) [too-low]") {
	REQUIRE_THROWS(net::http::status_code(99));
}

TEST_CASE("net::http::status_code::status_code(unsigned int) [too-high]") {
	REQUIRE_THROWS(net::http::status_code(600));
}

TEST_CASE("net::http::status_code::status_code(string_view) [valid]") {
	net::http::status_code status("200");
	REQUIRE(status == 200);
}

TEST_CASE("net::http::status_code::status_code(string_view) [minimum]") {
	net::http::status_code status("100");
	REQUIRE(status == 100);
}

TEST_CASE("net::http::status_code::status_code(string_view) [maximum]") {
	net::http::status_code status("599");
	REQUIRE(status == 599);
}

TEST_CASE("net::http::status_code::status_code(string_view) [too-low]") {
	REQUIRE_THROWS(net::http::status_code("099"));
}

TEST_CASE("net::http::status_code::status_code(string_view) [too-high]") {
	REQUIRE_THROWS(net::http::status_code("600"));
}

TEST_CASE("net::http::status_code::status_code(string_view) [too-long]") {
	REQUIRE_THROWS(net::http::status_code("0200"));
}

TEST_CASE("net::http::status_code::status_code(string_view) [too-short]") {
	REQUIRE_THROWS(net::http::status_code("20"));
}

TEST_CASE("net::http::status_code::status_code(string_view) [non-numeric]") {
	REQUIRE_THROWS(net::http::status_code("abc"));
}

TEST_CASE("net::http::status_code::status_code(string_view) [extra-non-digit]") {
	REQUIRE_THROWS(net::http::status_code("200a"));
}

TEST_CASE("net::http::make_reason_phrase(status_code) [OK]") {
	// Additional tests here would likely be brittle and add little value,
	// (since there have been minor historical changes to the recommended
	// reason phrases), however 200 is a relatively safe choice for
	// providing a minimal level of testing.
	net::http::status_code status(200);
	REQUIRE(make_reason_phrase(status) == "OK");
}
