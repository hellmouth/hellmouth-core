// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <string>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/body.h"

using namespace hellmouth;

TEST_CASE("net::http::body::body(string) [valid]") {
	std::string content = "Foo\r\nBar\r\nBaz\r\n";
	net::http::body body(content);
	std::string result(std::istreambuf_iterator<char>(body.content()), {});
	REQUIRE(result == content);
}
