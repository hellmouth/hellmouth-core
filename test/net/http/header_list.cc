// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iterator>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/header_list.h"

using namespace hellmouth;

TEST_CASE("net::http::header_list::header_list()") {
	net::http::header_list headers;
	REQUIRE(headers.begin() == headers.end());
}

TEST_CASE("net::http::header_list::empty()") {
	net::http::header_list headers;
	REQUIRE(headers.empty() == true);
	headers.push_back("Foo", "1");
	REQUIRE(headers.empty() == false);
}

TEST_CASE("net::http::header_list::push_back(header_name, std::string)") {
	net::http::header_list headers;
	headers.push_back("Foo", "1");
	headers.push_back("Foo", "2");
	headers.push_back("Bar", "3");
	REQUIRE(headers.begin()->name == "Foo");
	REQUIRE(headers.begin()->value == "1");
	REQUIRE(std::next(headers.begin(), 1)->name == "Foo");
	REQUIRE(std::next(headers.begin(), 1)->value == "2");
	REQUIRE(std::next(headers.begin(), 2)->name == "Bar");
	REQUIRE(std::next(headers.begin(), 2)->value == "3");
	REQUIRE(std::next(headers.begin(), 3) == headers.end());
}

TEST_CASE("net::http::header_list::contains(header_name)") {
	net::http::header_list headers;
	headers.push_back("Foo", "1");
	headers.push_back("Foo", "2");
	headers.push_back("Bar", "3");
	REQUIRE(headers.contains("Foo") == true);
	REQUIRE(headers.contains("Bar") == true);
	REQUIRE(headers.contains("Baz") == false);
}

TEST_CASE("net::http::header_list::get(header_name)") {
	net::http::header_list headers;
	headers.push_back("Foo", "1");
	headers.push_back("Foo", "2");
	headers.push_back("Bar", "3");
	REQUIRE(bool(headers.get("Foo")) == true);
	REQUIRE(*headers.get("Foo") == "1, 2");
	REQUIRE(bool(headers.get("Bar")) == true);
	REQUIRE(*headers.get("Bar") == "3");
	REQUIRE(bool(headers.get("Baz")) == false);
}

TEST_CASE("operator<<(std::ostream&, const header_list&") {
	net::http::header_list headers;
	headers.push_back("Foo", "1");
	headers.push_back("Foo", "2");
	headers.push_back("Bar", "3");
	std::ostringstream out;
	out << headers;
	REQUIRE(out.str() == "Foo: 1\r\nFoo: 2\r\nBar: 3\r\n");
}

TEST_CASE("operator>>(std::istream&, const header_list&") {
	// Make body look like another header, check not parsed.
	std::istringstream in("Foo:  1 \r\nBar: 2\r\nFoo:3\r\n\r\nFoo: Bar\r\n");
	net::http::header_list headers;
	in >> headers;
	REQUIRE(headers.begin()->name == "Foo");
	REQUIRE(headers.begin()->value == "1");
	REQUIRE(std::next(headers.begin(), 1)->name == "Bar");
	REQUIRE(std::next(headers.begin(), 1)->value == "2");
	REQUIRE(std::next(headers.begin(), 2)->name == "Foo");
	REQUIRE(std::next(headers.begin(), 2)->value == "3");
	REQUIRE(std::next(headers.begin(), 3) == headers.end());
}
