// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "hellmouth/net/http/token.h"

using namespace hellmouth;

TEST_CASE("net::http::status_code::token(string_view) [valid]") {
	net::http::token tk("Foo");
	REQUIRE(std::string(tk) == "Foo");
}

TEST_CASE("net::http::status_code::token(string_view) [empty]") {
	REQUIRE_THROWS(net::http::token(std::string()));
}

TEST_CASE("net::http::status_code::token(string_view) [control]") {
	auto control = GENERATE('\b', '\f', '\r', '\n', '\x7f');
	std::string strtk = std::string("foo") + control + "bar";
	REQUIRE_THROWS(net::http::token(strtk));
}

TEST_CASE("net::http::status_code::token(string_view) [separator]") {
	auto separator = GENERATE(
		'(', ')', '<', '>', '@',
		',', ';', ':', '\\', '"',
		'/', '[', ']', '?', '=',
		'{', '}', ' ', '\t');
	std::string strtk = std::string("foo") + separator + "bar";
	REQUIRE_THROWS(net::http::token(strtk));
}

TEST_CASE("net::http::status_code::token(string_view) [non-ASCII]") {
	std::string strtk = std::string("foo") + char(0x80) + "bar";
	REQUIRE_THROWS(net::http::token(strtk));
}
