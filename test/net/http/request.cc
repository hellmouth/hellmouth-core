// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/http/body.h"
#include "hellmouth/net/http/request.h"

using namespace hellmouth;

TEST_CASE("request::request(std::istream)") {
	std::string body = R"({"foo": "bar"})";
	auto raw = std::make_unique<std::stringstream>();
	*raw << "POST /foo?bar HTTP/1.732\r\n"
		<< "Host: www.example.com:8000\r\n"
		<< "Connection: keep-alive\r\n"
		<< "\r\n"
		<< body;
	net::http::request req(std::move(raw));

	REQUIRE(req.method == "POST");
	REQUIRE(to_string(req.target) == "/foo?bar");
	REQUIRE(to_string(req.version) == "HTTP/1.732");
	REQUIRE(req.headers.begin()->name == "Host");
	REQUIRE(req.headers.begin()->value == "www.example.com:8000");
	REQUIRE(std::next(req.headers.begin())->name == "Connection");
	REQUIRE(std::next(req.headers.begin())->value == "keep-alive");
	REQUIRE(std::next(req.headers.begin(), 2) == req.headers.end());
	std::ostringstream out;
	out << req.body.content().rdbuf();
	REQUIRE(out.str() == body);
}

TEST_CASE("operator<<(std::ostream, const request&") {
	std::string target = "http://user@www.example.org:8001/foo?bar#baz";
	std::string body = R"({"foo": "baz"})";
	net::http::request req("POST", target);
	req.body = net::http::body(body);
	std::ostringstream out;
	out << req;

	std::ostringstream expect;
	expect << "POST /foo?bar HTTP/1.1\r\n"
		<< "Host: www.example.org:8001\r\n"
		<< "Connection: close\r\n"
		<< "\r\n"
		<< body;

	REQUIRE(out.str() == expect.str());
}
