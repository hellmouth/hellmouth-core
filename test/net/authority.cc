// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/authority.h"

using namespace hellmouth;

TEST_CASE("net::authority::authority(string) [full]") {
	net::authority authority("username:password@www.example.com:8000");
	REQUIRE(bool(authority.userinfo) == true);
	REQUIRE(*authority.userinfo == "username:password");
	REQUIRE(authority.host == "www.example.com");
	REQUIRE(bool(authority.port) == true);
	REQUIRE(*authority.port == 8000);
}

TEST_CASE("net::authority::authority(string) [host-only]") {
	net::authority authority("www.example.com");
	REQUIRE(bool(authority.userinfo) == false);
	REQUIRE(authority.host == "www.example.com");
	REQUIRE(bool(authority.port) == false);
}

TEST_CASE("net::authority::authority(string) [max-port]") {
	net::authority authority("www.example.com:65535");
	REQUIRE(bool(authority.port) == true);
	REQUIRE(*authority.port == 65535);
}

TEST_CASE("net::authority::authority(string) [port-too-high]") {
	REQUIRE_THROWS(net::authority("www.example.com:65536"));
}

TEST_CASE("net::authority::authority(string) [port-negative]") {
	REQUIRE_THROWS(net::authority("www.example.com:-1"));
}

TEST_CASE("net::authority::authority(string) [empty-port]") {
	net::authority authority("www.example.com:");
	REQUIRE(authority.host == "www.example.com");
	REQUIRE(bool(authority.port) == false);
}

TEST_CASE("operator<<(std::istream&, const authority&) [full]") {
	net::authority authority("www.example.com");
	authority.userinfo = "username:password";
	authority.port = 8000;

	std::ostringstream out;
	out << authority;
	REQUIRE(out.str() == "username:password@www.example.com:8000");
}

TEST_CASE("operator<<(std::istream&, const authority&) [host-only]") {
	net::authority authority("www.example.com");

	std::ostringstream out;
	out << authority;
	REQUIRE(out.str() == "www.example.com");
}
