// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/inet4/address.h"

using namespace hellmouth;

TEST_CASE("net::inet4::address::address(string_view) [valid]") {
	in_addr addr = net::inet4::address("192.168.0.1");
	REQUIRE(addr.s_addr == htonl(0xc0a80001UL));
}

TEST_CASE("net::inet4::address::address(string_view) [short]") {
	REQUIRE_THROWS(net::inet4::address("192.168.0."));
}

TEST_CASE("net::inet4::address::address(string_view) [long]") {
	REQUIRE_THROWS(net::inet4::address("192.168.0.1."));
}

TEST_CASE("net::inet4::address::address(string_view) [empty]") {
	REQUIRE_THROWS(net::inet4::address("192.168..1"));
}

TEST_CASE("net::inet4::address::address(string_view) [range]") {
	REQUIRE_THROWS(net::inet4::address("256.168.0.1"));
}

TEST_CASE("net::inet4::address::operator std::string()") {
	net::inet4::address addr("192.168.000.001");
	std::string addrstr = addr;
	REQUIRE(addrstr == "192.168.0.1");
}
