// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/inet4/netblock.h"

using namespace hellmouth;

TEST_CASE("net::inet4::netblock::netblock(string_view) [strictly valid]") {
	net::inet4::netblock nb("192.168.0.0/24");
	in_addr addr = nb.address();
	REQUIRE(addr.s_addr == htonl(0xc0a80000UL));
	REQUIRE(nb.prefix_length() == 24);
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [strictly invalid]") {
	REQUIRE_THROWS(net::inet4::netblock("192.168.0.80/24"));
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [non-strict]") {
	net::inet4::netblock nb("192.168.0.1/24", false);
	in_addr addr = nb.address();
	REQUIRE(addr.s_addr == htonl(0xc0a80001UL));
	REQUIRE(nb.prefix_length() == 24);
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [default route]") {
	net::inet4::netblock nb("0.0.0.0/0");
	in_addr addr = nb.address();
	REQUIRE(addr.s_addr == htonl(INADDR_ANY));
	REQUIRE(nb.prefix_length() == 0);
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [single host]") {
	net::inet4::netblock nb("192.168.0.42/32", false);
	in_addr addr = nb.address();
	REQUIRE(addr.s_addr == htonl(0xc0a8002aUL));
	REQUIRE(nb.prefix_length() == 32);
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [no prefix length]") {
	net::inet4::netblock nb("192.168.0.42", false);
	in_addr addr = nb.address();
	REQUIRE(addr.s_addr == htonl(0xc0a8002aUL));
	REQUIRE(nb.prefix_length() == 32);
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [empty prefix length]") {
	REQUIRE_THROWS(net::inet4::netblock("192.168.0.0/"));
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [invalid prefix length]") {
	REQUIRE_THROWS(net::inet4::netblock("192.168.0.0/a"));
}

TEST_CASE("net::inet4::netblock::netblock(string_view) [high prefix length]") {
	REQUIRE_THROWS(net::inet4::netblock("192.168.0.0/33"));
}
