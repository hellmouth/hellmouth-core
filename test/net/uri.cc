// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/uri.h"

using namespace hellmouth;

TEST_CASE("net::uri::uri(string) [full]") {
	net::uri uri("http://username:password@www.example.com/foo/bar?baz=qux#frag1");
	REQUIRE(bool(uri.scheme) == true);
	REQUIRE(*uri.scheme == "http");
	REQUIRE(bool(uri.authority) == true);
	REQUIRE(uri.authority == "username:password@www.example.com");
	REQUIRE(uri.path == "/foo/bar");
	REQUIRE(bool(uri.query) == true);
	REQUIRE(*uri.query == "baz=qux");
	REQUIRE(bool(uri.fragment) == true);
	REQUIRE(*uri.fragment == "frag1");
}

TEST_CASE("net::uri::uri(string) [empty]") {
	net::uri uri("");
	REQUIRE(bool(uri.scheme) == false);
	REQUIRE(bool(uri.authority) == false);
	REQUIRE(uri.path == "");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [scheme-only]") {
	net::uri uri("data:");
	REQUIRE(bool(uri.scheme) == true);
	REQUIRE(*uri.scheme == "data");
	REQUIRE(bool(uri.authority) == false);
	REQUIRE(uri.path == "");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [bad-scheme]") {
	net::uri uri("da/ta:");
	REQUIRE(bool(uri.scheme) == false);
	REQUIRE(bool(uri.authority) == false);
	REQUIRE(uri.path == "da/ta:");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [authority-only]") {
	net::uri uri("//www.example.com");
	REQUIRE(bool(uri.scheme) == false);
	REQUIRE(bool(uri.authority) == true);
	REQUIRE(uri.authority == "www.example.com");
	REQUIRE(uri.path == "");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [empty-authority]") {
	net::uri uri("file:///home/user/test.txt");
	REQUIRE(bool(uri.scheme) == true);
	REQUIRE(*uri.scheme == "file");
	REQUIRE(bool(uri.authority) == true);
	REQUIRE(uri.authority == "");
	REQUIRE(uri.path == "/home/user/test.txt");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [path-only]") {
	net::uri uri("/foo");
	REQUIRE(bool(uri.scheme) == false);
	REQUIRE(bool(uri.authority) == false);
	REQUIRE(uri.path == "/foo");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [query-only]") {
	net::uri uri("?foo=bar");
	REQUIRE(bool(uri.scheme) == false);
	REQUIRE(bool(uri.authority) == false);
	REQUIRE(uri.path == "");
	REQUIRE(bool(uri.query) == true);
	REQUIRE(*uri.query == "foo=bar");
	REQUIRE(bool(uri.fragment) == false);
}

TEST_CASE("net::uri::uri(string) [fragment-only]") {
	net::uri uri("#frag2");
	REQUIRE(bool(uri.scheme) == false);
	REQUIRE(bool(uri.authority) == false);
	REQUIRE(uri.path == "");
	REQUIRE(bool(uri.query) == false);
	REQUIRE(bool(uri.fragment) == true);
	REQUIRE(*uri.fragment == "frag2");
}
