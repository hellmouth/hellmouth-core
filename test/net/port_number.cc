// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/net/port_number.h"

using namespace hellmouth;

TEST_CASE("net::port_number::port_number(uint16_t) [valid]") {
	net::port_number port(8000);
	REQUIRE(port == 8000);
}

TEST_CASE("net::port_number::port_number(uint16_t) [zero]") {
	REQUIRE_THROWS(net::port_number(0));
}

TEST_CASE("net::port_number::port_number(string_view) [valid]") {
	net::port_number port("8080");
	REQUIRE(port == 8080);
}

TEST_CASE("net::port_number::port_number(string_view) [empty]") {
	REQUIRE_THROWS(net::port_number(""));
}

TEST_CASE("net::port_number::port_number(string_view) [non-digit]") {
	REQUIRE_THROWS(net::port_number("a"));
}

TEST_CASE("net::port_number::port_number(string_view) [zero]") {
	REQUIRE_THROWS(net::port_number("0"));
}

TEST_CASE("net::port_number::port_number(string_view) [minimum]") {
	net::port_number port("1");
	REQUIRE(port == 1);
}

TEST_CASE("net::port_number::port_number(string_view) [maximum]") {
	net::port_number port("65535");
	REQUIRE(port == 65535);
}

TEST_CASE("net::port_number::port_number(string_view) [out-of-range]") {
	REQUIRE_THROWS(net::port_number("65536"));
}
