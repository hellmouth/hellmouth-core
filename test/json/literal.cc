#include <catch2/catch_test_macros.hpp>

#include "hellmouth/json/literal.h"

using namespace hellmouth;

TEST_CASE("hellmouth::operator\"\"_json") {
	auto x = "{\"foo\": 1, \"bar\": 2, \"baz\": 3}"_json;
	REQUIRE(int(x.at("foo")) == 1);
	REQUIRE(int(x.at("bar")) == 2);
	REQUIRE(int(x.at("baz")) == 3);
}
