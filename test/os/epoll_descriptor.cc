// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/os/pipe.h"
#include "hellmouth/os/epoll_descriptor.h"

using namespace hellmouth;

TEST_CASE("hellmouth::os::epoll_descriptor::add [normal]") {
	os::pipe testpipe;
	os::epoll_descriptor efd;
	epoll_event events[1];

	REQUIRE(efd.wait(events, 1, 0) == 0);

	epoll_event epev;
	epev.events = EPOLLOUT;
	epev.data.u32 = 42;
	REQUIRE_NOTHROW(efd.add(testpipe.writefd, epev));

	REQUIRE(efd.wait(events, 1, 0) == 1);
	REQUIRE(events[0].events == EPOLLOUT);
	REQUIRE(events[0].data.u32 == 42);
}

TEST_CASE("hellmouth::os::epoll_descriptor::add [double]") {
	os::pipe testpipe;
	os::epoll_descriptor efd;

	epoll_event epev;
	epev.events = EPOLLOUT;
	epev.data.u32 = 42;
	REQUIRE_NOTHROW(efd.add(testpipe.writefd, epev));
	REQUIRE_THROWS_AS(efd.add(testpipe.writefd, epev), std::system_error);
}

TEST_CASE("hellmouth::os::epoll_descriptor::mod [normal]") {
	os::pipe testpipe;
	os::epoll_descriptor efd;
	epoll_event events[1];

	epoll_event epev;
	epev.events = EPOLLOUT;
	epev.data.u32 = 42;
	REQUIRE_NOTHROW(efd.add(testpipe.writefd, epev));

	REQUIRE(efd.wait(events, 1, 0) == 1);
	REQUIRE(events[0].events == EPOLLOUT);
	REQUIRE(events[0].data.u32 == 42);

	epev.data.u32 += 1;
	REQUIRE_NOTHROW(efd.mod(testpipe.writefd, epev));

	REQUIRE(efd.wait(events, 1, 0) == 1);
	REQUIRE(events[0].events == EPOLLOUT);
	REQUIRE(events[0].data.u32 == 43);
}

TEST_CASE("hellmouth::os::epoll_descriptor::mod [noexist]") {
	os::pipe testpipe;
	os::epoll_descriptor efd;

	epoll_event epev;
	epev.events = EPOLLOUT;
	epev.data.u32 = 42;
	REQUIRE_THROWS_AS(efd.mod(testpipe.writefd, epev), std::system_error);
}

TEST_CASE("hellmouth::os::epoll_descriptor::del [normal]") {
	os::pipe testpipe;
	os::epoll_descriptor efd;
	epoll_event events[1];

	epoll_event epev;
	epev.events = EPOLLOUT;
	epev.data.u32 = 42;
	REQUIRE_NOTHROW(efd.add(testpipe.writefd, epev));

	REQUIRE(efd.wait(events, 1, 0) == 1);
	REQUIRE(events[0].events == EPOLLOUT);
	REQUIRE(events[0].data.u32 == 42);

	REQUIRE_NOTHROW(efd.del(testpipe.writefd));

	REQUIRE(efd.wait(events, 1, 0) == 0);
}

TEST_CASE("hellmouth::os::epoll_descriptor::del [double]") {
	os::pipe testpipe;
	os::epoll_descriptor efd;

	epoll_event epev;
	epev.events = EPOLLOUT;
	epev.data.u32 = 42;
	REQUIRE_NOTHROW(efd.add(testpipe.writefd, epev));
	REQUIRE_NOTHROW(efd.del(testpipe.writefd));
	REQUIRE_THROWS_AS(efd.del(testpipe.writefd), std::system_error);
}

TEST_CASE("hellmouth::os::epoll_descriptor::wait") {
	os::pipe testpipe;
	os::epoll_descriptor efd;
	epoll_event events[4];

	epoll_event epev0;
	epev0.events = EPOLLOUT;
	epev0.data.u32 = 42;
	REQUIRE_NOTHROW(efd.add(testpipe.writefd, epev0));

	epoll_event epev1;
	epev1.events = EPOLLIN;
	epev1.data.u32 = 84;
	REQUIRE_NOTHROW(efd.add(testpipe.readfd, epev1));

	REQUIRE(efd.wait(events, 4, 0) == 1);
	REQUIRE(events[0].events == EPOLLOUT);
	REQUIRE(events[0].data.u32 == 42);

	testpipe.writefd.write("x", 1);

	REQUIRE(efd.wait(events, 4, 0) == 2);
	size_t index0 = (events[0].data.u32 == 42) ? 0 : 1;
	size_t index1 = 1 - index0;
	REQUIRE(events[index0].events == EPOLLOUT);
	REQUIRE(events[index0].data.u32 == 42);
	REQUIRE(events[index1].events == EPOLLIN);
	REQUIRE(events[index1].data.u32 == 84);
}
