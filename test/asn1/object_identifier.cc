// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/object_identifier.h"
#include "hellmouth/asn1/ber/encoder.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;
using decoder = asn1::ber::decoder;

TEST_CASE("operator<<(encoder&, const object_identifier&)") {
	asn1::object_identifier oid{2, 999, 3};
	encoder enc;
	enc << oid;
	REQUIRE(enc.data() == octet::string{0x06, 0x03, 0x88, 0x37, 0x03});
}

TEST_CASE("operator>>(decoder&, object_identifier&)") {
	octet::string data{0x06, 0x03, 0x88, 0x37, 0x03};
	decoder dec(data);
	asn1::object_identifier oid;
	dec >> oid;
	REQUIRE(oid == asn1::object_identifier{2, 999, 3});
}
