// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using decoder = ber::decoder;

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [empty]") {
	octet::string data{0x03, 0x01, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [single]") {
	octet::string data{0x03, 0x02, 0x07, 0x80};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{1});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [full]") {
	octet::string data{0x03, 0x03, 0x00, 0xaa, 0x55};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [one-short]") {
	octet::string data{0x03, 0x03, 0x01, 0xaa, 0x54};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [nonzero-padding-ber]") {
	octet::string data{0x03, 0x03, 0x07, 0xaa, 0xff};
	decoder dec(data);
	dec.encoding(encoding_ber);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 1});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [nonzero-padding-cer]") {
	octet::string data{0x03, 0x03, 0x07, 0xaa, 0xff};
	decoder dec(data);
	dec.encoding(encoding_cer);
	std::vector<bool> value{0, 1, 0, 1, 0};
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [nonzero-padding-der]") {
	octet::string data{0x03, 0x03, 0x07, 0xaa, 0xff};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [constructed-definite]") {
	octet::string data =
		octet::string{0x23, 0x0a} +
		octet::string{0x03, 0x03, 0x00, 0xaa, 0xaa} +
		octet::string{0x03, 0x03, 0x06, 0x55, 0x40};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [constructed-indefinite-cer]") {
	octet::string data =
		octet::string{0x23, 0x80} +
		octet::string{0x03, 0x03, 0x00, 0xaa, 0xaa} +
		octet::string{0x03, 0x03, 0x06, 0x55, 0x40} +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_cer);
	std::vector<bool> value{0, 1, 0, 1, 0};
	dec >> value;
	REQUIRE(value == std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::vector<bool>&) [constructed-indefinite-der]") {
	octet::string data =
		octet::string{0x23, 0x80} +
		octet::string{0x03, 0x03, 0x00, 0xaa, 0xaa} +
		octet::string{0x03, 0x03, 0x06, 0x55, 0x40} +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::vector<bool> value{0, 1, 0, 1, 0};
	REQUIRE_THROWS(dec >> value);
}
