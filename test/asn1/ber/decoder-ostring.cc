// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using decoder = ber::decoder;

TEST_CASE("operator>>(decoder&, octet::string&) [empty]") {
	octet::string data{0x04, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == octet::string());
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [single]") {
	octet::string data{0x04, 0x01, 0x2a};
	decoder dec(data);
	dec.encoding(encoding_der);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == octet::string{0x2a});
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [max-short]") {
	octet::string payload(127, 0x2a);
	octet::string data = octet::string{0x04, 0x7f} + payload;
	decoder dec(data);
	dec.encoding(encoding_der);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == payload);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [long]") {
	octet::string payload(128, 0x2a);
	octet::string data = octet::string{0x04, 0x81, 0x80} + payload;
	decoder dec(data);
	dec.encoding(encoding_der);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == payload);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [vlong]") {
	octet::string payload(0x12345, 0x2a);
	octet::string data = octet::string{0x04, 0x83, 0x01, 0x23, 0x45} + payload;
	decoder dec(data);
	dec.encoding(encoding_der);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == payload);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [constructed-definite]") {
	octet::string payload1(1023, 0x55);
	octet::string payload2(2047, 0xaa);
	octet::string data =
		octet::string{0x24, 0x82, 0x0c, 0x06} +
		octet::string{0x04, 0x82, 0x03, 0xff} + payload1 +
		octet::string{0x04, 0x82, 0x07, 0xff} + payload2;
	decoder dec(data);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == payload1 + payload2);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [constructed-indefinite]") {
	octet::string payload1(1023, 0x55);
	octet::string payload2(2047, 0xaa);
	octet::string data =
		octet::string{0x24, 0x80} +
		octet::string{0x04, 0x82, 0x03, 0xff} + payload1 +
		octet::string{0x04, 0x82, 0x07, 0xff} + payload2 +
		octet::string{0x00, 0x00};
	decoder dec(data);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	dec >> value;
	REQUIRE(value == payload1 + payload2);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, octet::string&) [constructed-der]") {
	octet::string payload1(1023, 0x55);
	octet::string payload2(2047, 0xaa);
	octet::string data =
		octet::string{0x24, 0x80} +
		octet::string{0x04, 0x82, 0x03, 0xff} + payload1 +
		octet::string{0x04, 0x82, 0x07, 0xff} + payload2 +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	octet::string value{0xde, 0xad, 0xbe, 0xef};
	REQUIRE_THROWS(dec >> value);
}
