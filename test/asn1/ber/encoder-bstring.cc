// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using encoder = asn1::ber::encoder;

TEST_CASE("operator<<(encoder&, const std::vector<bool>&) [empty]") {
    encoder enc;
	enc.encoding(asn1::encoding_der);
    enc << std::vector<bool>{};
    REQUIRE(enc.data() == octet::string{0x03, 0x01, 0x00});
}

TEST_CASE("operator<<(encoder&, const std::vector<bool>&) [single]") {
    encoder enc;
	enc.encoding(asn1::encoding_der);
    enc << std::vector<bool>{1};
    REQUIRE(enc.data() == octet::string{0x03, 0x02, 0x07, 0x80});
}

TEST_CASE("operator<<(encoder&, const std::vector<bool>&) [full]") {
    encoder enc;
	enc.encoding(asn1::encoding_der);
    enc << std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1};
    REQUIRE(enc.data() == octet::string{0x03, 0x03, 0x00, 0xaa, 0x55});
}

TEST_CASE("operator<<(encoder&, const std::vector<bool>&) [one-short]") {
    encoder enc;
	enc.encoding(asn1::encoding_der);
    enc << std::vector<bool>{1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0};
    REQUIRE(enc.data() == octet::string{0x03, 0x03, 0x01, 0xaa, 0x54});
}

TEST_CASE("operator<<(encoder&, const std::vector<bool>&) [cer-max-single]") {
    encoder enc;
	enc.encoding(asn1::encoding_cer);
    enc << std::vector<bool>(7992, 1);
	octet::string expected = octet::string{0x03, 0x82, 0x03, 0xe8, 0x00} + octet::string(999, 0xff);
    REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, const std::vector<bool>&) [cer-min-double]") {
    encoder enc;
	enc.encoding(asn1::encoding_cer);
    enc << std::vector<bool>(7993, 1);
	octet::string expected =
		octet::string{0x23, 0x80} +
		octet::string{0x03, 0x82, 0x03, 0xe8, 0x00} + octet::string(999, 0xff) +
		octet::string{0x03, 0x02, 0x07, 0x80} +
		octet::string{0x00, 0x00};
    REQUIRE(enc.data() == expected);
}
