// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using decoder = ber::decoder;

TEST_CASE("asn1::ber::decoder::read_base128() [zero]") {
	octet::string data{0x00};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [one]") {
	octet::string data{0x01};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 1);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [max-single]") {
	octet::string data{0x7f};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0x7f);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [min-double]") {
	octet::string data{0x81, 0x00};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0x80);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [max-double]") {
	octet::string data{0xff, 0x7f};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0x3fff);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [min-triple]") {
	octet::string data{0x81, 0x80, 0x00};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0x4000);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [max-triple]") {
	octet::string data{0xff, 0xff, 0x7f};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0x1fffff);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_base128() [min-quad]") {
	octet::string data{0x81, 0x80, 0x80, 0x00};
	decoder dec(data);
	auto value = dec.read_base128();
	REQUIRE(value == 0x200000);
	REQUIRE(dec.empty() == true);
}

TEST_CASE("asn1::ber::decoder::read_identifier() [integer]") {
	octet::string data{0x02};
	decoder dec(data);
	auto [tag, cons] = dec.read_identifier();
	REQUIRE(tag == asn1::univ(2));
	REQUIRE(cons == false);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_identifier() [sequence]") {
	octet::string data{0x30};
	decoder dec(data);
	auto [tag, cons] = dec.read_identifier();
	REQUIRE(tag == asn1::univ(16));
	REQUIRE(cons == true);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_identifier() [app-3]") {
	octet::string data{0x63};
	decoder dec(data);
	auto [tag, cons] = dec.read_identifier();
	REQUIRE(tag == asn1::app(3));
	REQUIRE(cons == true);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_identifier() [ctx-7]") {
	octet::string data{0xa7};
	decoder dec(data);
	auto [tag, cons] = dec.read_identifier();
	REQUIRE(tag == asn1::ctx(7));
	REQUIRE(cons == true);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_identifier() [priv-2]") {
	octet::string data{0xe2};
	decoder dec(data);
	auto [tag, cons] = dec.read_identifier();
	REQUIRE(tag == asn1::priv(2));
	REQUIRE(cons == true);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_content() [empty]") {
	octet::string data{0x00};
	decoder dec(data);
	auto content = dec.read_content();
	REQUIRE(content.has_value() == true);
	REQUIRE(content->empty());
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_content() [definite, short]") {
	octet::string refcontent{0x01, 0x02, 0x03, 0x04, 0x05};
	octet::string data = octet::string{0x05} + refcontent;
	decoder dec(data);
	auto content = dec.read_content();
	REQUIRE(content.has_value() == true);
	REQUIRE(*content == refcontent);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_content() [definite, long]") {
	octet::string refcontent(0x10002, 0xaa);
	octet::string data = octet::string{0x83, 0x01, 0x00, 0x02} + refcontent;
	decoder dec(data);
	auto content = dec.read_content();
	REQUIRE(content.has_value() == true);
	REQUIRE(*content == refcontent);
	REQUIRE(dec.empty());
}

TEST_CASE("asn1::ber::decoder::read_content() [indefinite]") {
	octet::string data{0x80};
	decoder dec(data);
	auto content = dec.read_content();
	REQUIRE(content.has_value() == false);
	REQUIRE(dec.empty());
}
