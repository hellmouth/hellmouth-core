// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;

TEST_CASE("operator<<(encoder&, octet::string_view) [empty]") {
    encoder enc;
	enc.encoding(asn1::encoding_der);
    enc << octet::string();
    REQUIRE(enc.data() == octet::string{0x04, 0x00});
}

TEST_CASE("operator<<(encoder&, octet::string_view) [single]") {
    encoder enc;
	enc.encoding(asn1::encoding_der);
    enc << octet::string{0x2a};
    REQUIRE(enc.data() == octet::string{0x04, 0x01, 0x2a});
}

TEST_CASE("operator<<(encoder&, octet::string_view) [max-short]") {
    octet::string payload(127, 0x2a);
    encoder enc;
    enc.encoding(asn1::encoding_der);
    enc << payload;
    REQUIRE(enc.data() == octet::string{0x04, 0x7f} + payload);
}

TEST_CASE("operator<<(encoder&, octet::string_view) [long]") {
    octet::string payload(128, 0x2a);
    encoder enc;
    enc.encoding(asn1::encoding_der);
    enc << payload;
    REQUIRE(enc.data() == octet::string{0x04, 0x81, 0x80} + payload);
}

TEST_CASE("operator<<(encoder&, octet::string_view) [vlong]") {
    octet::string payload(0x12345, 0x2a);
    encoder enc;
    enc.encoding(asn1::encoding_der);
    enc << payload;
    REQUIRE(enc.data() == octet::string{0x04, 0x83, 0x01, 0x23, 0x45} + payload);
}

TEST_CASE("operator<<(encoder&, octet::string_view) [cer-max-single]") {
    octet::string payload(1000, 0x2a);
    encoder enc;
    enc.encoding(asn1::encoding_cer);
    enc << payload;
    REQUIRE(enc.data() == octet::string{0x04, 0x82, 0x03, 0xe8} + payload);
}

TEST_CASE("operator<<(encoder&, octet::string_view) [cer-min-double]") {
    octet::string payload(1001, 0x2a);
    encoder enc;
    enc.encoding(asn1::encoding_cer);
    enc << payload;
	octet::string expected =
		octet::string{0x24, 0x80} +
		octet::string{0x04, 0x82, 0x03, 0xe8} + payload.substr(0, 1000) +
		octet::string{0x04, 0x01} + payload.substr(1000) +
		octet::string{0x00, 0x00};
    REQUIRE(enc.data() == expected);
}
