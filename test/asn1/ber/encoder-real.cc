// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cmath>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;

TEST_CASE("operator<<(encoder&, double) [plus-zero]") {
	encoder enc;
	enc << 0.0;
	REQUIRE(enc.data() == octet::string{0x09, 0x00});
}

TEST_CASE("operator<<(encoder&, double) [minus-zero]") {
	encoder enc;
	enc << -0.0;
	REQUIRE(enc.data() == octet::string{0x09, 0x01, 0x43});
}

TEST_CASE("operator<<(encoder&, double) [one]") {
	encoder enc;
	enc << 1.0;
	REQUIRE(enc.data() == octet::string{0x09, 0x03, 0x80, 0x00, 0x01});
}

TEST_CASE("operator<<(encoder&, double) [two]") {
	encoder enc;
	enc << 2.0;
	REQUIRE(enc.data() == octet::string{0x09, 0x03, 0x80, 0x01, 0x01});
}

TEST_CASE("operator<<(encoder&, double) [three]") {
	encoder enc;
	enc << 3.0;
	REQUIRE(enc.data() == octet::string{0x09, 0x03, 0x80, 0x00, 0x03});
}

TEST_CASE("operator<<(encoder&, double) [max-exact]") {
	encoder enc;
	enc << double(0x1fffffffffffff);
	REQUIRE(enc.data() == octet::string{0x09, 0x09, 0x80, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, double) [max]") {
	encoder enc;
	enc << std::numeric_limits<double>::max();
	REQUIRE(enc.data() == octet::string{0x09, 0x0a, 0x81, 0x03, 0xcb, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, double) [min]") {
	encoder enc;
	enc << std::numeric_limits<double>::min();
	REQUIRE(enc.data() == octet::string{0x09, 0x04, 0x81, 0xfc, 0x02, 0x01});
}

TEST_CASE("operator<<(encoder&, double) [minus-one]") {
	encoder enc;
	enc << -1.0;
	REQUIRE(enc.data() == octet::string{0x09, 0x03, 0xc0, 0x00, 0x01});
}

TEST_CASE("operator<<(encoder&, double) [plus-infinity]") {
	encoder enc;
	enc << INFINITY;
	REQUIRE(enc.data() == octet::string{0x09, 0x01, 0x40});
}

TEST_CASE("operator<<(encoder&, double) [minus-infinity]") {
	encoder enc;
	enc << -INFINITY;
	REQUIRE(enc.data() == octet::string{0x09, 0x01, 0x41});
}

TEST_CASE("operator<<(encoder&, double) [not-a-number]") {
	encoder enc;
	enc << NAN;
	REQUIRE(enc.data() == octet::string{0x09, 0x01, 0x42});
}
