// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <vector>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using decoder = asn1::ber::decoder;

TEST_CASE("operator>>(decoder&, std::vector<uint16_t>&) [cer]") {
	octet::string data =
		octet::string{0x30, 0x80} +
		octet::string{0x02, 0x01, 0x01} +
		octet::string{0x02, 0x01, 0x01} +
		octet::string{0x02, 0x01, 0x02} +
		octet::string{0x02, 0x01, 0x03} +
		octet::string{0x02, 0x01, 0x05} +
		octet::string{0x02, 0x01, 0x08} +
		octet::string{0x02, 0x01, 0x0d} +
		octet::string{0x02, 0x01, 0x15} +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(asn1::encoding_cer);
	std::vector<uint16_t> value;
	dec >> value;
	std::vector<uint16_t> expected{ 1, 1, 2, 3, 5, 8, 13, 21};
	REQUIRE(value == expected);
}

TEST_CASE("operator>>(decoder&, std::vector<uint16_t>&) [der]") {
	octet::string data =
		octet::string{0x30, 0x18} +
		octet::string{0x02, 0x01, 0x01} +
		octet::string{0x02, 0x01, 0x01} +
		octet::string{0x02, 0x01, 0x02} +
		octet::string{0x02, 0x01, 0x03} +
		octet::string{0x02, 0x01, 0x05} +
		octet::string{0x02, 0x01, 0x08} +
		octet::string{0x02, 0x01, 0x0d} +
		octet::string{0x02, 0x01, 0x15};
	decoder dec(data);
	dec.encoding(asn1::encoding_der);
	std::vector<uint16_t> value;
	dec >> value;
	std::vector<uint16_t> expected{ 1, 1, 2, 3, 5, 8, 13, 21};
	REQUIRE(value == expected);
}
