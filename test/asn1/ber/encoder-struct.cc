// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;

struct untagged_struct {
	uint16_t x;
	uint16_t y;
	template<class V>
	void visit(V&& visitor) {
		visitor("x", x);
		visitor("y", y);
	}
};

struct tagged_struct {
	uint16_t x;
	uint16_t y;
	template<class V>
	void visit(V&& visitor) {
		visitor("x", x, asn1::ctx(1));
		visitor("y", y, asn1::ctx(2));
	}
};

struct optional_struct {
	std::optional<uint16_t> w;
	std::optional<uint16_t> x;
	std::optional<uint16_t> y;
	uint16_t z;
	template<class V>
	void visit(V&& visitor) {
		visitor("w", w, asn1::ctx(1));
		visitor("x", x, asn1::ctx(2));
		visitor("y", y, asn1::ctx(3));
		visitor("z", z, asn1::ctx(4));
	}
};

TEST_CASE("operator<<(encoder&, const visitable&) [cer, untagged]") {
	untagged_struct foo;
	foo.x = 4;
	foo.y = 7;
	encoder enc;
	enc.encoding(asn1::encoding_cer);
	enc << foo;
	octet::string expected =
		octet::string{0x30, 0x80} +
		octet::string{0x02, 0x01, 0x04} +
		octet::string{0x02, 0x01, 0x07} +
		octet::string{0x00, 0x00};
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, const visitable&) [cer, tagged]") {
	tagged_struct foo;
	foo.x = 5;
	foo.y = 9;
	encoder enc;
	enc.encoding(asn1::encoding_cer);
	enc << foo;
	octet::string expected =
		octet::string{0x30, 0x80} +
		octet::string{0xa1, 0x80, 0x02, 0x01, 0x05, 0x00, 0x00} +
		octet::string{0xa2, 0x80, 0x02, 0x01, 0x09, 0x00, 0x00} +
		octet::string{0x00, 0x00};
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, const visitable&) [cer, optional]") {
	optional_struct foo;
	foo.x = 6;
	foo.z = 11;
	encoder enc;
	enc.encoding(asn1::encoding_cer);
	enc << foo;
	octet::string expected =
		octet::string{0x30, 0x80} +
		octet::string{0xa2, 0x80, 0x02, 0x01, 0x06, 0x00, 0x00} +
		octet::string{0xa4, 0x80, 0x02, 0x01, 0x0b, 0x00, 0x00} +
		octet::string{0x00, 0x00};
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, const visitable&) [der, untagged]") {
	untagged_struct foo;
	foo.x = 4;
	foo.y = 7;
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << foo;
	octet::string expected =
		octet::string{0x30, 0x06} +
		octet::string{0x02, 0x01, 0x04} +
		octet::string{0x02, 0x01, 0x07};
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, const visitable&) [der, tagged]") {
	tagged_struct foo;
	foo.x = 5;
	foo.y = 9;
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << foo;
	octet::string expected =
		octet::string{0x30, 0x0a} +
		octet::string{0xa1, 0x03, 0x02, 0x01, 0x05} +
		octet::string{0xa2, 0x03, 0x02, 0x01, 0x09};
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, const visitable&) [der, optional]") {
	optional_struct foo;
	foo.x = 6;
	foo.z = 11;
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << foo;
	octet::string expected =
		octet::string{0x30, 0x0a} +
		octet::string{0xa2, 0x03, 0x02, 0x01, 0x06} +
		octet::string{0xa4, 0x03, 0x02, 0x01, 0x0b};
	REQUIRE(enc.data() == expected);
}
