// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using decoder = ber::decoder;

TEST_CASE("operator>>(decoder&, std::string&) [empty]") {
	octet::string data{0x0c, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == "");
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [single]") {
	octet::string data{0x0c, 0x01, 'X'};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == "X");
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [max-short]") {
	octet::string payload(127, 'X');
	octet::string data = octet::string{0x0c, 0x7f} + payload;
	decoder dec(data);
	dec.encoding(encoding_der);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == std::string(127, 'X'));
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [long]") {
	octet::string payload(128, 'X');
	octet::string data = octet::string{0x0c, 0x81, 0x80} + payload;
	decoder dec(data);
	dec.encoding(encoding_der);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == std::string(128, 'X'));
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [vlong]") {
	octet::string payload(0x12345, 'X');
	octet::string data = octet::string{0x0c, 0x83, 0x01, 0x23, 0x45} + payload;
	decoder dec(data);
	dec.encoding(encoding_der);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == std::string(0x12345, 'X'));
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [constructed-definite]") {
	octet::string payload1(1023, 'X');
	octet::string payload2(2047, 'Y');
	octet::string data =
		octet::string{0x2c, 0x82, 0x0c, 0x06} +
		octet::string{0x0c, 0x82, 0x03, 0xff} + payload1 +
		octet::string{0x0c, 0x82, 0x07, 0xff} + payload2;
	decoder dec(data);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == std::string(1023, 'X') + std::string(2047, 'Y'));
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [constructed-indefinite]") {
	octet::string payload1(1023, 'X');
	octet::string payload2(2047, 'Y');
	octet::string data =
		octet::string{0x2c, 0x80} +
		octet::string{0x0c, 0x82, 0x03, 0xff} + payload1 +
		octet::string{0x0c, 0x82, 0x07, 0xff} + payload2 +
		octet::string{0x00, 0x00};
	decoder dec(data);
	std::string value="invalid";
	dec >> value;
	REQUIRE(value == std::string(1023, 'X') + std::string(2047, 'Y'));
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, std::string&) [constructed-der]") {
	octet::string payload1(1023, 'X');
	octet::string payload2(2047, 'Y');
	octet::string data =
		octet::string{0x2c, 0x80} +
		octet::string{0x0c, 0x82, 0x03, 0xff} + payload1 +
		octet::string{0x0c, 0x82, 0x07, 0xff} + payload2 +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	std::string value="invalid";
	REQUIRE_THROWS(dec >> value);
}
