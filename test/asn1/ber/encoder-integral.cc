// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;

TEST_CASE("operator<<(encoder&, bool) [true]") {
	encoder enc;
	enc << true;
	REQUIRE(enc.data() == octet::string{0x01, 0x01, 0xff});
}

TEST_CASE("operator<<(encoder&, bool) [false]") {
	encoder enc;
	enc << false;
	REQUIRE(enc.data() == octet::string{0x01, 0x01, 0x00});
}

TEST_CASE("operator<<(encoder&, unsigned_integral) [0]") {
	encoder enc;
	enc << 0U;
	REQUIRE(enc.data() == octet::string{0x02, 0x01, 0x00});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [1]") {
	encoder enc;
	enc << 1U;
	REQUIRE(enc.data() == octet::string{0x02, 0x01, 0x01});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x7f]") {
	encoder enc;
	enc << 0x7fU;
	REQUIRE(enc.data() == octet::string{0x02, 0x01, 0x7f});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x80]") {
	encoder enc;
	enc << 0x80U;
	REQUIRE(enc.data() == octet::string{0x02, 0x02, 0x00, 0x80});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x7fff]") {
	encoder enc;
	enc << 0x7fffU;
	REQUIRE(enc.data() == octet::string{0x02, 0x02, 0x7f, 0xff});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x8000]") {
	encoder enc;
	enc << 0x8000U;
	REQUIRE(enc.data() == octet::string{0x02, 0x03, 0x00, 0x80, 0x00});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x7fffff]") {
	encoder enc;
	enc << 0x7fffffUL;
	REQUIRE(enc.data() == octet::string{0x02, 0x03, 0x7f, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x800000]") {
	encoder enc;
	enc << 0x800000UL;
	REQUIRE(enc.data() == octet::string{0x02, 0x04, 0x00, 0x80, 0x00, 0x00});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x7fffffff]") {
	encoder enc;
	enc << 0x7fffffffUL;
	REQUIRE(enc.data() == octet::string{0x02, 0x04, 0x7f, 0xff, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x80000000]") {
	encoder enc;
	enc << 0x80000000UL;
	REQUIRE(enc.data() == octet::string{0x02, 0x05, 0x00, 0x80, 0x00, 0x00, 0x00});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0x7fffffffffffffff]") {
	encoder enc;
	enc << 0x7fffffffffffffffULL;
	REQUIRE(enc.data() == octet::string{0x02, 0x08, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, unsigned_integeral) [0xffffffffffffffff]") {
	encoder enc;
	enc << 0xffffffffffffffffULL;
	REQUIRE(enc.data() == octet::string{0x02, 0x09, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x80]") {
	encoder enc;
	enc << -0x80;
	REQUIRE(enc.data() == octet::string{0x02, 0x01, 0x80});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x81]") {
	encoder enc;
	enc << -0x81;
	REQUIRE(enc.data() == octet::string{0x02, 0x02, 0xff, 0x7f});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x8000]") {
	encoder enc;
	enc << -0x8000;
	REQUIRE(enc.data() == octet::string{0x02, 0x02, 0x80, 0x00});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x8001]") {
	encoder enc;
	enc << -0x8001;
	REQUIRE(enc.data() == octet::string{0x02, 0x03, 0xff, 0x7f, 0xff});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x800000]") {
	encoder enc;
	enc << -0x800000;
	REQUIRE(enc.data() == octet::string{0x02, 0x03, 0x80, 0x00, 0x00});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x800001]") {
	encoder enc;
	enc << -0x800001;
	REQUIRE(enc.data() == octet::string{0x02, 0x04, 0xff, 0x7f, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x80000000]") {
	encoder enc;
	enc << -0x80000000LL;
	REQUIRE(enc.data() == octet::string{0x02, 0x04, 0x80, 0x00, 0x00, 0x00});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x80000001]") {
	encoder enc;
	enc << -0x80000001LL;
	REQUIRE(enc.data() == octet::string{0x02, 0x05, 0xff, 0x7f, 0xff, 0xff, 0xff});
}

TEST_CASE("operator<<(encoder&, signed_integeral) [-0x8000000000000000]") {
	encoder enc;
	enc << (-0x7fffffffffffffffLL - 1);
	REQUIRE(enc.data() == octet::string{0x02, 0x08, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
}
