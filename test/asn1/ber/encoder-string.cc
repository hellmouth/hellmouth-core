// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;

TEST_CASE("operator<<(encoder&, std::string_view) [empty]") {
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << std::string();
	REQUIRE(enc.data() == octet::string{0x0c, 0x00});
}

TEST_CASE("operator<<(encoder&, std::string_view) [single]") {
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << std::string("X");
	REQUIRE(enc.data() == octet::string{0x0c, 0x01, 0x58});
}

TEST_CASE("operator<<(encoder&, std::string_view) [max-short]") {
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << std::string(127, 'X');
	octet::string expected =
		octet::string{0x0c, 0x7f} +
		octet::string(127, 'X');
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, std::string_view) [long]") {
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << std::string(128, 'X');
	octet::string expected =
		octet::string{0x0c, 0x81, 0x80} +
		octet::string(128, 'X');
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, std::string_view) [vlong]") {
	encoder enc;
	enc.encoding(asn1::encoding_der);
	enc << std::string(0x12345, 'X');
	octet::string expected =
		octet::string{0x0c, 0x83, 0x01, 0x23, 0x45} +
		octet::string(0x12345, 'X');
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, std::string_view) [cer-max-single]") {
	encoder enc;
	enc.encoding(asn1::encoding_cer);
	enc << std::string(1000, 'X');
	octet::string expected =
		octet::string{0x0c, 0x82, 0x03, 0xe8} +
		octet::string(1000, 'X');
	REQUIRE(enc.data() == expected);
}

TEST_CASE("operator<<(encoder&, std::string_view) [cer-min-double]") {
	encoder enc;
	enc.encoding(asn1::encoding_cer);
	enc << std::string(1001, 'X');
	octet::string expected =
		octet::string{0x2c, 0x80} +
		octet::string{0x0c, 0x82, 0x03, 0xe8} +
		octet::string(1000, 'X') +
		octet::string{0x0c, 0x01, 'X'} +
		octet::string{0x00, 0x00};
	REQUIRE(enc.data() == expected);
}
