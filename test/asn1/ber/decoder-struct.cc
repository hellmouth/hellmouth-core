// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using decoder = ber::decoder;

struct untagged_struct {
	uint16_t x = 0;
	uint16_t y = 0;
	template<class V>
	void visit(this auto&& self, V&& visitor) {
		visitor("x", self.x);
		visitor("y", self.y);
	}
};

struct tagged_struct {
	uint16_t x = 0;
	uint16_t y = 0;
	template<class V>
	void visit(this auto&& self, V&& visitor) {
		visitor("x", self.x, asn1::ctx(1));
		visitor("y", self.y, asn1::ctx(2));
	}
};

struct optional_struct {
	std::optional<uint16_t> w;
	std::optional<uint16_t> x;
	std::optional<uint16_t> y;
	uint16_t z = 0;
	template<class V>
	void visit(this auto&& self, V&& visitor) {
		visitor("w", self.w, asn1::ctx(1));
		visitor("x", self.x, asn1::ctx(2));
		visitor("y", self.y, asn1::ctx(3));
		visitor("z", self.z, asn1::ctx(4));
	}
};

TEST_CASE("operator>>(decoder&, visitable&) [cer, untagged]") {
	octet::string data =
		octet::string{0x30, 0x80} +
		octet::string{0x02, 0x01, 0x04} +
		octet::string{0x02, 0x01, 0x07} +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_cer);
	untagged_struct foo;
	dec >> foo;
	REQUIRE(foo.x == 4);
	REQUIRE(foo.y == 7);
	REQUIRE(dec.empty());

	decoder dec2(data);
	dec2.encoding(encoding_der);
	REQUIRE_THROWS(dec2 >> foo);
}

TEST_CASE("operator>>(decoder&, visitable&) [cer, tagged]") {
	octet::string data =
		octet::string{0x30, 0x80} +
		octet::string{0xa1, 0x80, 0x02, 0x01, 0x05, 0x00, 0x00} +
		octet::string{0xa2, 0x80, 0x02, 0x01, 0x09, 0x00, 0x00} +
		octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_cer);
	tagged_struct foo;
	dec >> foo;
	REQUIRE(foo.x == 5);
	REQUIRE(foo.y == 9);
	REQUIRE(dec.empty());

	decoder dec2(data);
	dec2.encoding(encoding_der);
	REQUIRE_THROWS(dec2 >> foo);
}

TEST_CASE("operator>>(decoder&, visitable&) [cer, optional]") {
    octet::string data =
        octet::string{0x30, 0x80} +
        octet::string{0xa2, 0x80, 0x02, 0x01, 0x06, 0x00, 0x00} +
        octet::string{0xa4, 0x80, 0x02, 0x01, 0x0b, 0x00, 0x00} +
        octet::string{0x00, 0x00};
	decoder dec(data);
	dec.encoding(encoding_cer);
	optional_struct foo;
	dec >> foo;
	REQUIRE(foo.w.has_value() == false);
	REQUIRE(foo.x == 6);
	REQUIRE(foo.y.has_value() == false);
	REQUIRE(foo.z == 11);
	REQUIRE(dec.empty());

	decoder dec2(data);
	dec2.encoding(encoding_der);
	REQUIRE_THROWS(dec2 >> foo);
}

TEST_CASE("operator>>(decoder&, visitable&) [der, untagged]") {
	octet::string data =
		octet::string{0x30, 0x06} +
		octet::string{0x02, 0x01, 0x04} +
		octet::string{0x02, 0x01, 0x07};
	decoder dec(data);
	dec.encoding(encoding_der);
	untagged_struct foo;
	dec >> foo;
	REQUIRE(foo.x == 4);
	REQUIRE(foo.y == 7);
	REQUIRE(dec.empty());

	decoder dec2(data);
	dec2.encoding(encoding_cer);
	REQUIRE_THROWS(dec2 >> foo);
}

TEST_CASE("operator>>(decoder&, visitable&) [der, tagged]") {
	octet::string data =
		octet::string{0x30, 0x0a} +
		octet::string{0xa1, 0x03, 0x02, 0x01, 0x05} +
		octet::string{0xa2, 0x03, 0x02, 0x01, 0x09};
	decoder dec(data);
	dec.encoding(encoding_der);
	tagged_struct foo;
	dec >> foo;
	REQUIRE(foo.x == 5);
	REQUIRE(foo.y == 9);
	REQUIRE(dec.empty());

	decoder dec2(data);
	dec2.encoding(encoding_cer);
	REQUIRE_THROWS(dec2 >> foo);
}

TEST_CASE("operator>>(decoder&, visitable&) [der, optional]") {
    octet::string data =
        octet::string{0x30, 0x0a} +
        octet::string{0xa2, 0x03, 0x02, 0x01, 0x06} +
        octet::string{0xa4, 0x03, 0x02, 0x01, 0x0b};
	decoder dec(data);
	dec.encoding(encoding_der);
	optional_struct foo;
	dec >> foo;
	REQUIRE(foo.w.has_value() == false);
	REQUIRE(foo.x == 6);
	REQUIRE(foo.y.has_value() == false);
	REQUIRE(foo.z == 11);
	REQUIRE(dec.empty());

	decoder dec2(data);
	dec2.encoding(encoding_cer);
	REQUIRE_THROWS(dec2 >> foo);
}
