// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/encoder.h"

using namespace hellmouth;
using encoder = asn1::ber::encoder;

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [zero]") {
    encoder enc;
	enc.write_base128(0);
	REQUIRE(enc.data() == octet::string{0x00});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [one]") {
    encoder enc;
	enc.write_base128(1);
	REQUIRE(enc.data() == octet::string{0x01});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [max-single]") {
    encoder enc;
	enc.write_base128(0x7f);
	REQUIRE(enc.data() == octet::string{0x7f});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [min-double]") {
    encoder enc;
	enc.write_base128(0x80);
	REQUIRE(enc.data() == octet::string{0x81, 0x00});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [max-double]") {
    encoder enc;
	enc.write_base128(0x3fff);
	REQUIRE(enc.data() == octet::string{0xff, 0x7f});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [min-triple]") {
    encoder enc;
	enc.write_base128(0x4000);
	REQUIRE(enc.data() == octet::string{0x81, 0x80, 0x00});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [max-triple]") {
    encoder enc;
	enc.write_base128(0x1fffff);
	REQUIRE(enc.data() == octet::string{0xff, 0xff, 0x7f});
}

TEST_CASE("asn1::ber::decoder::write_base128(uint64_t) [min-quad]") {
    encoder enc;
	enc.write_base128(0x200000);
	REQUIRE(enc.data() == octet::string{0x81, 0x80, 0x80, 0x00});
}

TEST_CASE("asn1::ber::encoder::write_identifier() [integer]") {
	encoder enc;
	enc.write_identifier(asn1::univ(2), false);
	REQUIRE(enc.data() == octet::string{0x02});
}

TEST_CASE("asn1::ber::encoder::write_identifier() [sequence]") {
	encoder enc;
	enc.write_identifier(asn1::univ(16), true);
	REQUIRE(enc.data() == octet::string{0x30});
}

TEST_CASE("asn1::ber::encoder::write_identifier() [app-3]") {
	encoder enc;
	enc.write_identifier(asn1::app(3), true);
	REQUIRE(enc.data() == octet::string{0x63});
}

TEST_CASE("asn1::ber::encoder::write_identifier() [ctx-7]") {
	encoder enc;
	enc.write_identifier(asn1::ctx(7), true);
	REQUIRE(enc.data() == octet::string{0xa7});
}

TEST_CASE("asn1::ber::encoder::write_identifier() [priv-2]") {
	encoder enc;
	enc.write_identifier(asn1::priv(2), true);
	REQUIRE(enc.data() == octet::string{0xe2});
}

TEST_CASE("asn1::ber::encoder::write_identifier() [high]") {
	encoder enc;
	enc.write_identifier(asn1::app(0xcdef), true);
	REQUIRE(enc.data() == octet::string{0x7f, 0x83, 0x9b, 0x6f});
}

TEST_CASE("asn1::ber::encoder::write_content() [empty]") {
	octet::string content;
	encoder enc;
	enc.write_content(content);
	REQUIRE(enc.data() == octet::string{0x00});
}

TEST_CASE("asn1::ber::encoder::write_content() [definite, short]") {
	octet::string content{0x01, 0x02, 0x03, 0x04, 0x05};
	encoder enc;
	enc.write_content(content);
	REQUIRE(enc.data() == octet::string{0x05} + content);
}

TEST_CASE("asn1::ber::encoder::write_content() [definite, long]") {
	octet::string content(0x10002, 0xaa);
	encoder enc;
	enc.write_content(content);
	REQUIRE(enc.data() == octet::string{0x83, 0x01, 0x00, 0x02} + content);
}

TEST_CASE("asn1::ber::encoder::begin_content()") {
	encoder enc;
	enc.begin_content();
	REQUIRE(enc.data() == octet::string{0x80});
}

TEST_CASE("asn1::ber::encoder::end_content()") {
	encoder enc;
	enc.end_content();
	REQUIRE(enc.data() == octet::string{0x00, 0x00});
}
