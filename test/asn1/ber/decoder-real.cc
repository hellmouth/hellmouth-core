// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cmath>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using decoder = asn1::ber::decoder;

TEST_CASE("operator>>(decoder&, double&) [plus-zero]") {
	octet::string data{0x09, 0x00};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 0.0);
	REQUIRE(std::signbit(value) == 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [minus-zero]") {
	octet::string data{0x09, 0x01, 0x43};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 0.0);
	REQUIRE(std::signbit(value) != 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [one]") {
	octet::string data{0x09, 0x03, 0x80, 0x00, 0x01};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 1.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [two]") {
	octet::string data{0x09, 0x03, 0x80, 0x01, 0x01};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 2.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [three]") {
	octet::string data{0x09, 0x03, 0x80, 0x00, 0x03};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 3.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [max-exact]") {
	octet::string data{0x09, 0x09, 0x80, 0x00, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 0x1fffffffffffff);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [max]") {
	octet::string data{0x09, 0x0a, 0x81, 0x03, 0xcb, 0x1f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == std::numeric_limits<double>::max());
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [min]") {
	octet::string data{0x09, 0x04, 0x81, 0xfc, 0x02, 0x01};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == std::numeric_limits<double>::min());
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [minus-one]") {
	octet::string data{0x09, 0x03, 0xc0, 0x00, 0x01};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == -1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [plus-infinity]") {
	octet::string data{0x09, 0x01, 0x40};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(std::isinf(value) != 0);
	REQUIRE(std::signbit(value) == 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [minus-infinity]") {
	octet::string data{0x09, 0x01, 0x41};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(std::isinf(value) != 0);
	REQUIRE(std::signbit(value) != 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [not-a-number]") {
	octet::string data{0x09, 0x01, 0x42};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(std::isnan(value) != 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr1-unsigned]") {
	octet::string data{0x09, 0x03, 0x01, '4', '2'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 42.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr1-padded]") {
	octet::string data{0x09, 0x05, 0x01, ' ', ' ', '4', '2'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 42.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr1-positive]") {
	octet::string data{0x09, 0x04, 0x01, '+', '4', '2'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 42.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr1-negative]") {
	octet::string data{0x09, 0x04, 0x01, '-', '4', '2'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == -42.0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-unsigned]") {
	octet::string data{0x09, 0x05, 0x02, '1', '2', '.', '5'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 12.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-padded]") {
	octet::string data{0x09, 0x07, 0x02, ' ', ' ', '1', '2', '.', '5'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 12.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-positive]") {
	octet::string data{0x09, 0x06, 0x02, '+', '1', '2', '.', '5'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 12.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-negative]") {
	octet::string data{0x09, 0x06, 0x02, '-', '1', '2', '.', '5'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == -12.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-whole]") {
	octet::string data{0x09, 0x04, 0x02, '1', '2', '.'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 12);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-frac]") {
	octet::string data{0x09, 0x03, 0x02, '.', '5'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 0.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr2-comma]") {
	octet::string data{0x09, 0x05, 0x02, '1', '2', ',', '5'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 12.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-positive]") {
	octet::string data{0x09, 0x09, 0x03, '+', '6', '.', '2', '5', 'E', '+', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 62.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-negative]") {
	octet::string data{0x09, 0x09, 0x03, '-', '6', '.', '2', '5', 'E', '+', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == -62.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-negexp]") {
	octet::string data{0x09, 0x09, 0x03, '+', '6', '2', '.', '5', 'E', '-', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 6.25);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-padded]") {
	octet::string data{0x09, 0x09, 0x03, ' ', '6', '.', '2', '5', 'E', '+', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 62.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-comma]") {
	octet::string data{0x09, 0x09, 0x03, ' ', '6', ',', '2', '5', 'E', '+', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 62.5);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-whole]") {
	octet::string data{0x09, 0x07, 0x03, '+', '6', '.', 'E', '+', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 60);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, double&) [nr3-frac]") {
	octet::string data{0x09, 0x08, 0x03, '+', '.', '2', '5', 'E', '+', '1'};
	decoder dec(data);
	double value = 3.14;
	dec >> value;
	REQUIRE(value == 2.5);
	REQUIRE(dec.empty());
}
