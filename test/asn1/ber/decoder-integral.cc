// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/asn1/ber/decoder.h"

using namespace hellmouth;
using namespace hellmouth::asn1;
using decoder = ber::decoder;

TEST_CASE("operator>>(decoder&, bool&) [true]") {
	octet::string data{0x01, 0x01, 0xff};
	decoder dec(data);
	dec.encoding(encoding_der);
	bool value = false;
	dec >> value;
	REQUIRE(value == true);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, bool&) [true,ber]") {
	octet::string data{0x01, 0x01, 0x01};
	decoder dec(data);
	bool value = false;
	dec >> value;
	REQUIRE(value == true);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, bool&) [false]") {
	octet::string data{0x01, 0x01, 0x00};
	decoder dec(data);
	dec.encoding(encoding_der);
	bool value = true;
	dec >> value;
	REQUIRE(value == false);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, bool&) [empty]") {
	octet::string data{0x01, 0x00};
	decoder dec(data);
	bool value;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, bool&) [long]") {
	octet::string data{0x01, 0x02, 0x00, 0x00};
	decoder dec(data);
	bool value;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, bool&) [invalid]") {
	octet::string data{0x01, 0x01, 0x01};
	decoder dec(data);
	dec.encoding(encoding_der);
	bool value;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [empty]") {
	octet::string data{0x02, 0x00};
	decoder dec(data);
	unsigned value = 1;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0]") {
	octet::string data{0x02, 0x01, 0x00};
	decoder dec(data);
	unsigned value = 1;
	dec >> value;
	REQUIRE(value == 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [1]") {
	octet::string data{0x02, 0x01, 0x01};
	decoder dec(data);
	unsigned value = 0;
	dec >> value;
	REQUIRE(value == 1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x7f]") {
	octet::string data{0x02, 0x01, 0x7f};
	decoder dec(data);
	unsigned value = 0;
	dec >> value;
	REQUIRE(value == 0x7f);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x80]") {
	octet::string data{0x02, 0x02, 0x00, 0x80};
	decoder dec(data);
	unsigned value = 0;
	dec >> value;
	REQUIRE(value == 0x80);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x80-nolz]") {
	octet::string data{0x02, 0x01, 0x80};
	decoder dec(data);
	unsigned value = 0;
	REQUIRE_THROWS(dec >> value);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x100-range]") {
	octet::string data{0x02, 0x02, 0x01, 0x00};
	decoder dec(data);
	uint8_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x7fff]") {
	octet::string data{0x02, 0x02, 0x7f, 0xff};
	decoder dec(data);
	unsigned value = 0;
	dec >> value;
	REQUIRE(value == 0x7fff);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x8000]") {
	octet::string data{0x02, 0x03, 0x00, 0x80, 0x00};
	decoder dec(data);
	unsigned value = 0;
	dec >> value;
	REQUIRE(value == 0x8000U);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x8000-nolz]") {
	octet::string data{0x02, 0x02, 0x80, 0x00};
	decoder dec(data);
	unsigned value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x10000-range]") {
	octet::string data{0x02, 0x03, 0x01, 0x00, 0x00};
	decoder dec(data);
	uint16_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x7fffffff]") {
	octet::string data{0x02, 0x04, 0x7f, 0xff, 0xff, 0xff};
	decoder dec(data);
	unsigned long value = 0;
	dec >> value;
	REQUIRE(value == 0x7fffffffUL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x80000000]") {
	octet::string data{0x02, 0x05, 0x00, 0x80, 0x00, 0x00, 0x00};
	decoder dec(data);
	unsigned long value = 0;
	dec >> value;
	REQUIRE(value == 0x80000000UL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x80000000-nolz]") {
	octet::string data{0x02, 0x04, 0x80, 0x00, 0x00, 0x00};
	decoder dec(data);
	unsigned long value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x100000000-range]") {
	octet::string data{0x02, 0x05, 0x01, 0x00, 0x00, 0x00, 0x00};
	decoder dec(data);
	uint32_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x7fffffffffffffff]") {
	octet::string data{0x02, 0x08, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	decoder dec(data);
	unsigned long long value = 0;
	dec >> value;
	REQUIRE(value == 0x7fffffffffffffffULL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x8000000000000000]") {
	octet::string data{0x02, 0x09, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	decoder dec(data);
	unsigned long long value = 0;
	dec >> value;
	REQUIRE(value == 0x8000000000000000ULL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x8000000000000000-nolz]") {
	octet::string data{0x02, 0x08, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	decoder dec(data);
	unsigned long long value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0xffffffffffffffff]") {
	octet::string data{0x02, 0x09, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	decoder dec(data);
	uint64_t value = 0;
	dec >> value;
	REQUIRE(value == 0xffffffffffffffffULL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, unsigned_integral&) [0x10000000000000000-range]") {
	octet::string data{0x02, 0x09, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	decoder dec(data);
	uint64_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [empty]") {
	octet::string data{0x02, 0x00};
	decoder dec(data);
	int value = 1;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0]") {
	octet::string data{0x02, 0x01, 0x00};
	decoder dec(data);
	int value = 1;
	dec >> value;
	REQUIRE(value == 0);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [1]") {
	octet::string data{0x02, 0x01, 0x01};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == 1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x7f]") {
	octet::string data{0x02, 0x01, 0x7f};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == 0x7f);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80]") {
	octet::string data{0x02, 0x02, 0x00, 0x80};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == 0x80);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80-range]") {
	octet::string data{0x02, 0x02, 0x00, 0x80};
	decoder dec(data);
	int8_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x7fff]") {
	octet::string data{0x02, 0x02, 0x7f, 0xff};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == 0x7fff);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x8000]") {
	octet::string data{0x02, 0x03, 0x00, 0x80, 0x00};
	decoder dec(data);
	long value = 0;
	dec >> value;
	REQUIRE(value == 0x8000L);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x8000-range]") {
	octet::string data{0x02, 0x03, 0x00, 0x80, 0x00};
	decoder dec(data);
	int16_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x7fffffff]") {
	octet::string data{0x02, 0x04, 0x7f, 0xff, 0xff, 0xff};
	decoder dec(data);
	long value = 0;
	dec >> value;
	REQUIRE(value == 0x7fffffffL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80000000]") {
	octet::string data{0x02, 0x05, 0x00, 0x80, 0x00, 0x00, 0x00};
	decoder dec(data);
	long long value = 0;
	dec >> value;
	REQUIRE(value == 0x80000000LL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80000000-range]") {
	octet::string data{0x02, 0x05, 0x00, 0x80, 0x00, 0x00, 0x00};
	decoder dec(data);
	int32_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x7fffffffffffffff]") {
	octet::string data{0x02, 0x08, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	decoder dec(data);
	long long value = 0;
	dec >> value;
	REQUIRE(value == 0x7fffffffffffffffLL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80000000000000000-range]") {
	octet::string data{0x02, 0x09, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	decoder dec(data);
	int64_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-1]") {
	octet::string data{0x02, 0x01, 0xff};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == -1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x80]") {
	octet::string data{0x02, 0x01, 0x80};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == -0x80);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x81]") {
	octet::string data{0x02, 0x02, 0xff, 0x7f};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == -0x81);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x81-range]") {
	octet::string data{0x02, 0x02, 0xff, 0x7f};
	decoder dec(data);
	int8_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x8000]") {
	octet::string data{0x02, 0x02, 0x80, 0x00};
	decoder dec(data);
	int value = 0;
	dec >> value;
	REQUIRE(value == -0x7fff - 1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x8001]") {
	octet::string data{0x02, 0x03, 0xff, 0x7f, 0xff};
	decoder dec(data);
	long value = 0;
	dec >> value;
	REQUIRE(value == -0x8001L);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x8001-range]") {
	octet::string data{0x02, 0x03, 0xff, 0x7f, 0xff};
	decoder dec(data);
	int16_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x80000000]") {
	octet::string data{0x02, 0x04, 0x80, 0x00, 0x00, 0x00};
	decoder dec(data);
	long value = 0;
	dec >> value;
	REQUIRE(value == -0x7fffffffL - 1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [-0x80000001]") {
	octet::string data{0x02, 0x05, 0xff, 0x7f, 0xff, 0xff, 0xff};
	decoder dec(data);
	long long value = 0;
	dec >> value;
	REQUIRE(value == -0x80000001LL);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80000001-range]") {
	octet::string data{0x02, 0x05, 0xff, 0x7f, 0xff, 0xff, 0xff};
	decoder dec(data);
	int32_t value = 0;
	REQUIRE_THROWS(dec >> value);
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x8000000000000000]") {
	octet::string data{0x02, 0x08, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	decoder dec(data);
	long long value = 0;
	dec >> value;
	REQUIRE(value == -0x7fffffffffffffffLL - 1);
	REQUIRE(dec.empty());
}

TEST_CASE("operator>>(decoder&, signed_integral&) [0x80000000000000001-range]") {
	octet::string data{0x02, 0x09, 0xff, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};
	decoder dec(data);
	int64_t value = 0;
	REQUIRE_THROWS(dec >> value);
}
