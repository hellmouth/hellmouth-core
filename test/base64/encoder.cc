// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/base64/encoder.h"

using namespace hellmouth;

// Test values match the examples given in section 7 of RFC 3548.

TEST_CASE("encoder::operator() [no-padding]") {
	octet::string data = {
		0x14, 0xfb, 0x9c, 0x03, 0xd9, 0x7e};
	base64::encoder enc;
	REQUIRE(enc(data) == "FPucA9l+");
}

TEST_CASE("encoder::operator() [1-padding]") {
	octet::string data = {
		0x14, 0xfb, 0x9c, 0x03, 0xd9};
	base64::encoder enc;
	REQUIRE(enc(data) == "FPucA9k=");
}

TEST_CASE("encoder::operator() [2-padding]") {
	octet::string data = {
		0x14, 0xfb, 0x9c, 0x03};
	base64::encoder enc;
	REQUIRE(enc(data) == "FPucAw==");
}

TEST_CASE("encoder::operator() [split]") {
	octet::string data1 = { 0x14, 0xfb, 0x9c, 0x03, 0xd9 };
	octet::string data2 = { 0x7e };
	base64::encoder enc;
	REQUIRE(enc(data1, false) == "FPuc");
	REQUIRE(enc(data2) == "A9l+");
}
