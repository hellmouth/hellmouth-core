// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/octet/string.h"
#include "hellmouth/base64/decoder.h"

using namespace hellmouth;

// Test values match the examples given in section 7 of RFC 3548.

TEST_CASE("decoder::operator() [no-padding]") {
	octet::string data = {
		0x14, 0xfb, 0x9c, 0x03, 0xd9, 0x7e};
	base64::decoder dec;
	REQUIRE(dec("FPucA9l+") == data);
}

TEST_CASE("decoder::operator() [1-padding]") {
	octet::string data = {
		0x14, 0xfb, 0x9c, 0x03, 0xd9};
	base64::decoder dec;
	REQUIRE(dec("FPucA9k=") == data);
}

TEST_CASE("decoder::operator() [2-padding]") {
	octet::string data = {
		0x14, 0xfb, 0x9c, 0x03};
	base64::decoder dec;
	REQUIRE(dec("FPucAw==") == data);
}

TEST_CASE("decoder::operator() [internal-padding]") {
	base64::decoder dec;
	REQUIRE_THROWS(dec("FPucA9=+"));
}

TEST_CASE("decoder::operator() [excess-padding]") {
	base64::decoder dec;
	REQUIRE_THROWS(dec("FPucA==="));
}

TEST_CASE("decoder::operator() [split0]") {
	octet::string data1 = {0x14, 0xfb, 0x9c};
	octet::string data2 = {0x03, 0xd9, 0x7e};
	base64::decoder dec;
	REQUIRE(dec("FPuc", false) == data1);
	REQUIRE(dec("A9l+") == data2);
}

TEST_CASE("decoder::operator() [split1]") {
	octet::string data1 = {0x14, 0xfb, 0x9c};
	octet::string data2 = {0x03, 0xd9, 0x7e};
	base64::decoder dec;
	REQUIRE(dec("FPucA", false) == data1);
	REQUIRE(dec("9l+") == data2);
}

TEST_CASE("decoder::operator() [split2]") {
	octet::string data1 = {0x14, 0xfb, 0x9c};
	octet::string data2 = {0x03, 0xd9, 0x7e};
	base64::decoder dec;
	REQUIRE(dec("FPucA9", false) == data1);
	REQUIRE(dec("l+") == data2);
}

TEST_CASE("decoder::operator() [split3]") {
	octet::string data1 = {0x14, 0xfb, 0x9c};
	octet::string data2 = {0x03, 0xd9, 0x7e};
	base64::decoder dec;
	REQUIRE(dec("FPucA9l", false) == data1);
	REQUIRE(dec("+") == data2);
}

TEST_CASE("decoder::operator() [incomplete]") {
	base64::decoder dec;
	REQUIRE_THROWS(dec("FPucA9l"));
}
