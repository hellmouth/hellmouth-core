// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/os/pipe.h"
#include "hellmouth/event/timeout.h"
#include "hellmouth/event/file_descriptor.h"
#include "hellmouth/event/manager.h"

using namespace hellmouth;

TEST_CASE("hellmouth::event::manager") {
    os::pipe testpipe;
	event::manager em;

	bool tmflag = false;
	bool fdflag = false;
	bool hupflag = false;
	event::timeout tmev(em, [&]{ tmflag = true; }, 0);
	event::file_descriptor fdev1(em, [&]{ fdflag = true; }, testpipe.readfd, EPOLLIN);
	event::file_descriptor fdev2(em, [&]{ hupflag = true; }, testpipe.writefd, EPOLLERR);
	REQUIRE(fdev1.fd() == int(testpipe.readfd));
	REQUIRE(fdev2.fd() == int(testpipe.writefd));

	em.poll();
	REQUIRE(tmflag == true);
	REQUIRE(fdflag == false);
	REQUIRE(hupflag == false);

	tmev.reset(0);
	testpipe.writefd.write("x", 1);
	em.poll();
	REQUIRE(tmflag == true);
	REQUIRE(fdflag == true);
	REQUIRE(hupflag == false);
	REQUIRE(fdev1.revents() == EPOLLIN);

	tmev.reset(0);
	fdev1 = event::file_descriptor();
	testpipe.readfd.close();
	em.poll();
	REQUIRE(tmflag == true);
	REQUIRE(fdflag == true);
	REQUIRE(hupflag == true);
	REQUIRE(fdev2.revents() == EPOLLERR);
}
