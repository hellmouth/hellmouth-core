// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <time.h>

#include <catch2/catch_test_macros.hpp>

#include "hellmouth/event/timeout.h"
#include "hellmouth/event/timeout_queue.h"
#include "hellmouth/event/manager.h"

using namespace hellmouth;

static bool approx_equal(int lhs, int rhs, int tol) {
	return (lhs <= rhs) && (lhs + tol >= rhs);
}

static timespec make_timespec(int tv_sec, int tv_nsec) {
	timespec result{0};
	result.tv_sec = tv_sec;
	result.tv_nsec = tv_nsec;
	return result;
}

TEST_CASE("hellmouth::event::timeout_queue::remaining") {
	event::manager em;
	event::timeout_queue queue;

	time_t time0 = time(0);
	event::timeout tmev0(em, []{}, 200000);
	event::timeout tmev1(em, []{}, 300000);
	event::timeout tmev2(em, []{}, 400000);
	queue.insert(tmev1);
	int remaining0 = queue.remaining();
	queue.insert(tmev2);
	int remaining1 = queue.remaining();
	queue.insert(tmev0);
	int remaining2 = queue.remaining();
	queue.remove(tmev0);
	int remaining3 = queue.remaining();
	queue.remove(tmev1);
	int remaining4 = queue.remaining();
	queue.remove(tmev2);
	int remaining5 = queue.remaining();
	time_t time1 = time(0);
	time_t tol = (time1 - time0 + 1) * 1000;

	REQUIRE(approx_equal(remaining0, 300000, tol));
	REQUIRE(approx_equal(remaining1, 300000, tol));
	REQUIRE(approx_equal(remaining2, 200000, tol));
	REQUIRE(approx_equal(remaining3, 300000, tol));
	REQUIRE(approx_equal(remaining4, 400000, tol));
	REQUIRE(remaining5 == -1);
}

TEST_CASE("hellmouth::event::timeout_queue::operator()") {
	event::manager em;
	event::timeout_queue queue;

	bool triggered0 = false;
	bool triggered1 = false;
	bool triggered2 = false;
	event::timeout tmev0(em, [&]{ triggered0 = true; }, make_timespec(3, 0));
	event::timeout tmev1(em, [&]{ triggered1 = true; }, make_timespec(2, 0));
	event::timeout tmev2(em, [&]{ triggered2 = true; }, make_timespec(4, 0));
	queue.insert(tmev0);
	queue.insert(tmev1);
	queue.insert(tmev2);

	queue();
	REQUIRE(triggered0 == false);
	REQUIRE(triggered1 == true);
	REQUIRE(triggered2 == false);
	queue.remove(tmev1);

	queue();
	REQUIRE(triggered0 == true);
	REQUIRE(triggered1 == true);
	REQUIRE(triggered2 == false);
	queue.remove(tmev0);

	queue();
	REQUIRE(triggered0 == true);
	REQUIRE(triggered1 == true);
	REQUIRE(triggered2 == true);
}
