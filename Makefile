# This file is part of the HELLMOUTH cluster system.
# Copyright 2024-25 Graham Shaw.
# Distribution and modification are permitted within the terms of the
# GNU General Public License (version 3 or any later version).

prefix = /usr/local
datarootdir = $(prefix)/share
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
libdir = $(exec_prefix)/lib
libexecdir = $(libdir)
includedir = $(exec_prefix)/include
mandir = $(datarootdir)/man
man8dir = $(mandir)/man8
rpath = $(libdir)

pkgname = hellmouth
version = 0.0.0~pre1
distdir = hellmouth-core-$(version)
distfile = $(distdir).tar.gz

solinkname = lib$(pkgname).so
soname = lib$(pkgname).so.0
sofullname = lib$(pkgname).so.0.0.0

CPPFLAGS = -MD -MP -I. '-DLIBEXECDIR="$(libexecdir)"' '-DPKGNAME="$(pkgname)"'
CXXFLAGS = -fPIC -O2 -g --std=c++2b -Wall -Wpedantic
LDLIBS = -latomic -lsodium -lssl -lcrypto
TESTLIBS = -lCatch2Main -lCatch2
RPATH = -Wl,-rpath $(libdir)
CONDRPATH = $(if $(rpath), $(RPATH))

SRC = $(wildcard src/*.cc)
BIN = $(SRC:src/%.cc=bin/%)
MAINBIN = bin/$(pkgname)
AUXBIN = $(filter-out $(MAINBIN),$(BIN))

HELLMOUTH = $(wildcard hellmouth/*.cc) $(wildcard hellmouth/*/*.cc) $(wildcard hellmouth/*/*/*.cc)
TEST = $(wildcard test/*.cc) $(wildcard test/*/*.cc) $(wildcard test/*/*/*.cc)
INCLUDE = $(wildcard hellmouth/*.h) $(wildcard hellmouth/*/*.h) $(wildcard hellmouth/*/*/*.h)
DISTFILES = $(wildcard *.md) Makefile $(INCLUDE) $(HELLMOUTH) $(SRC) $(TEST)

MAN8 = $(wildcard man/man8/*.8)
MAN8GZ = $(MAN8:man/%=man/%.gz)
MANGZ = $(MAN8GZ)

.PHONY: all
all: $(sofullname) $(BIN) $(MANGZ)

.PHONY: test
test: bin/test
	bin/test

$(BIN): bin/%: src/%.o $(sofullname)
	@mkdir -p bin
	g++ -rdynamic $(CONDRPATH) -o $@ $^ $(LDLIBS)

bin/test: $(TEST:%.cc=%.o) $(HELLMOUTH:%.cc=%.o)
	@mkdir -p bin
	g++ -rdynamic $(CONDRPATH) -o $@ $^ $(LDLIBS) $(TESTLIBS)

$(sofullname): $(HELLMOUTH:%.cc=%.o)
	gcc -shared -Wl,-soname,$(soname) -o $@ $^ $(LDLIBS)

.PHONY: clean
clean:
	rm -f hellmouth/*.[do]
	rm -f hellmouth/*/*.[do]
	rm -f hellmouth/*/*/*.[do]
	rm -f src/*.[do]
	rm -f test/*.[do]
	rm -f test/*/*.[do]
	rm -f test/*/*/*.[do]
	rm -f *.so
	rm -rf bin
	rm -f man/*/*.gz

man/%.gz: man/%
	gzip -k -f $<

.PHONY: dist
dist: $(distfile)

$(distfile): $(DISTFILES)
	rm -f $@
	rm -rf $(distdir)
	ln -s . $(distdir)
	tar zchf $@ $(DISTFILES:%=$(distdir)/%)
	rm $(distdir)

$(DESTDIR)$(includedir)/%.h: %.h
	@mkdir -p $(dir $@)
	cp $< $(dir $@)

.PHONY: install
install: $(INCLUDE:%.h=$(DESTDIR)$(includedir)/%.h) $(MANGZ)
	@mkdir -p $(DESTDIR)$(bindir)
	@mkdir -p $(DESTDIR)$(libexecdir)/$(pkgname)/bin
	@mkdir -p $(DESTDIR)$(libdir)
	@mkdir -p $(DESTDIR)$(man8dir)
	cp $(MAINBIN) $(DESTDIR)$(bindir)/
	cp $(AUXBIN) $(DESTDIR)$(libexecdir)/$(pkgname)/bin/
	cp $(sofullname) $(DESTDIR)$(libdir)/
	ln -s -f $(libdir)/$(sofullname) $(DESTDIR)$(libdir)/$(soname)
	ln -s -f $(libdir)/$(sofullname) $(DESTDIR)$(libdir)/$(solinkname)
	cp $(MAN8GZ) $(DESTDIR)$(man8dir)/

.PHONY: uninstall
uninstall:
	rm -f $(INCLUDE:%.h=$(DESTDIR)$(includedir)/%.h)
	find $(DESTDIR)$(includedir)/hellmouth -type d -empty -delete
	rm $(DESTDIR)$(libdir)/$(sofullname)
	rm $(DESTDIR)$(libdir)/$(solinkname)
	rm -f $(DESTDIR)$(bindir)/$(notdir $(MAINBIN))
	rm -rf $(DESTDIR)$(libexecdir)/$(pkgname)
	rm -f $(foreach MANFILE,$(MAN8GZ),$(DESTDIR)$(man8dir)/$(notdir $(MANFILE)))

.PHONY: always
always:

-include $(HELLMOUTH:%.cc=%.d)
-include $(SRC:%.cc=%.d)
-include $(TEST:%.cc=%.d)
