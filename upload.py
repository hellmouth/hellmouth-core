#!/usr/bin/python3

import os
import os.path
import re
import subprocess

CI_API_V4_URL = os.getenv('CI_API_V4_URL', default='https://gitlab.com/api/v4')
CI_PROJECT_ID = os.getenv('CI_PROJECT_ID')
CI_JOB_TOKEN = os.getenv('CI_JOB_TOKEN')

with open('debian/files') as files:
    for file in files:
        file = file.strip()
        filename = file.split()[0]
        match = re.match(r'^([^_]+)_([^_]+)*_([^_]+).deb$', filename)
        if (match):
            pkgname = match.group(1)
            version = match.group(2).replace('~', '-')
            pathname = os.path.join('..', filename)
            url = f'{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/packages/generic/{pkgname}/latest/{pkgname}.deb';
            args = [
                'curl',
                '--location',
                '--header',
                'JOB-TOKEN: ' + CI_JOB_TOKEN,
                '--upload-file',
                pathname,
                url
            ]
            print(url)
            subprocess.run(args)
            print()
