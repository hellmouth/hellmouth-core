# HELLMOUTH Cluster System: Core Library

## Purpose

A HELLMOUTH cluster will provide an efficient, container-based,
high-availability computing system.

This repository provides a library containing code which is common to
multiple components of the system.

## Current status

Incomplete and experimental.

## Copyright and licensing

The HELLMOUTH cluster system is copyright 2024-25 Graham Shaw.

Distribution and modification are permitted within the terms of the GNU
General Public License (version 3 or any later version).

Please refer to the file LICENCE.md for details.

Third-party contributions are not currently being accepted into this
project.
