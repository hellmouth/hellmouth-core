// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/error.h"
#include "hellmouth/db/connection.h"
#include "hellmouth/db/client.h"
#include "hellmouth/json/encoder.h"

using namespace hellmouth;

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth db status [<options>]"
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -U  the database UNIX domain socket" << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
}

int main(int argc, char* argv[]) {
	std::string sockpath = db::connection::default_pathname;

	int opt;
	while ((opt = getopt(argc, argv, "hU:")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		case 'U':
			sockpath = optarg;
			break;
		}
	}

	if (optind != argc) {
		write_help(std::cerr);
		return 1;
	}

	db::client client(sockpath);
	try {
		data::any result = client.status();
		json::encoder encode(std::cout);
		encode(result);
		std::cerr << "\n";
	} catch (db::error& ex) {
		std::cerr << ex.what() << std::endl;
	}
}
