// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iostream>

#include <getopt.h>

#include "hellmouth/cli/exec.h"

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth db [<options>] <command> [<args>]"
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
	out << std::endl;
	out << "Commands:" << std::endl;
	out << std::endl;
	out << "  daemon   run database node daemon" << std::endl;
	out << "  get      get data from path" << std::endl;
	out << "  delete   delete data at path" << std::endl;
	out << "  patch    apply patch" << std::endl;
	out << "  put      put data to path" << std::endl;
	out << "  status   get status of database node" << std::endl;
}

extern char** environ;

int main(int argc, char* argv[]) {
	// Parse command line options.
	int opt;
	while ((opt = getopt(argc, argv, "+h")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		}
	}
	hellmouth::cli::exec("hellmouth-db-", argv + optind, environ);
}
