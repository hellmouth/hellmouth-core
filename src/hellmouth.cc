// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <iostream>

#include <getopt.h>

#include "hellmouth/cli/exec.h"

/** Print help text.
 * @param out the ostream to which the help text should be written
 */
void write_help(std::ostream& out) {
	out << "Usage: hellmouth [<options>] <command> [<args>]"
		<< std::endl;
	out << std::endl;
	out << "Options:" << std::endl;
	out << std::endl;
	out << "  -h  display this help text then exit" << std::endl;
	out << std::endl;
	out << "Commands:" << std::endl;
	out << std::endl;
	out << "  db       manage config database" << std::endl;
}

extern char** environ;

int main(int argc, char* argv[]) {
	// Parse command line options.
	int opt;
	while ((opt = getopt(argc, argv, "+h")) != -1) {
		switch (opt) {
		case 'h':
			write_help(std::cout);
			return 0;
		}
	}
	hellmouth::cli::exec("hellmouth-", argv + optind, environ);
}
