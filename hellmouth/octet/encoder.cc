// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/os/utility.h"
#include "hellmouth/octet/encoder.h"

namespace hellmouth::octet {

encoder::encoder(size_t capacity) {
	_base = static_cast<pointer>(malloc(capacity));
	if (!_base) {
		os::throw_errno();
	}
	_ptr = _base;
	_eptr = _base + capacity;
}

encoder::~encoder() {
	free(_base);
}

void encoder::reserve(size_type capacity) {
	size_type index = tell();
	if (index > capacity) {
		index = capacity;
	}

	pointer base = static_cast<pointer>(realloc(_base, capacity));
	if (!base) {
		os::throw_errno();
	}

	_base = base;
	_ptr = _base + index;
	_eptr = _base + capacity;
}

void encoder::seek(size_type location) {
	if (location > capacity()) {
		reserve(location);
	}
	_ptr = _base + location;
}

}
