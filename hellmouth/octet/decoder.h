// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OCTET_DECODER
#define HELLMOUTH_OCTET_DECODER

#include <cstdint>
#include <cstring>
#include <bit>
#include <type_traits>
#include <concepts>

#include "hellmouth/octet/string_view.h"

namespace hellmouth::octet {

/** A base class for decoding a sequence of octets.
 * Operators are provided for decoding a selection of commonly-used data
 * types using the following conventions:
 *
 * - Network byte order is used throughout.
 * - Signed integers use twos-complement representation.
 *
 * This behaviour can be overridden in derived classes, but note that none
 * of the relevant functions are virtual. For this reason, the expectation
 * is that derived classes will always be used directly and never via a
 * pointer or reference to the base class.
 */
class decoder {
public:
	/** A type to represent an encoded octet. */
	typedef unsigned char value_type;

	/** A type for pointing to an immutable encoded octet. */
	typedef const value_type* const_pointer;

	/** A type to represent a number of octets. */
	typedef size_t size_type;
protected:
	/** A pointer to the next octet to be decoded. */
	const_pointer _ptr;

	/** A pointer to one past the end of the encoded data. */
	const_pointer _eptr;

	/** Report that the end of the data was reached unexpectedly. */
	void _end_of_data();
public:
	/** Construct decoder.
	 * The content passed to this function must remain valid until
	 * decoding has been completed.
	 *
	 * @param data the data to be decoded
	 * @param length the length of the data
	 */
	decoder(const_pointer data, size_type length):
		_ptr(data),
		_eptr(_ptr + length) {}

	/** Construct decoder.
	 * The content passed to this function must remain valid until
	 * decoding has been completed.
	 *
	 * @param data the data to be decoded
	 */
	decoder(octet::string_view data):
		_ptr(data.data()),
		_eptr(_ptr + data.length()) {}

	/** Check whether the end of the sequence has been reached.
	 * @return true if no octets remaining, otherwise false
	 */
	bool empty() const {
		return _ptr == _eptr;
	}

	/** Get the number of octets remaining.
	 * @return the number of octets
	 */
	size_type remaining() const {
		return _eptr - _ptr;
	}

	/** Read a given number of octets.
	 * @param count the number of octets to be read
	 * @return a pointer to the first octet
	 */
	const_pointer read(size_type count) {
		if (remaining() < count) {
			_end_of_data();
		}
		const_pointer ptr = _ptr;
		_ptr += count;
		return ptr;
	}

	/** Read a given number of octets into a supplied buffer.
	 * @param count the number of octets to be read
	 * @return a pointer to the first octet
	 */
	void read(void* buffer, size_type count) {
		memcpy(buffer, read(count), count);
	}

	template<typename T>
	T read() {
		if constexpr (std::is_constructible_v<T, decoder&>) {
			return T(*this);
		} else {
			T value;
			*this >> value;
			return value;
		}
	}
};

/** Decode any type which can be constructed from a decoder.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D, typename T>
requires std::derived_from<D, decoder> &&
	std::constructible_from<T, D&>
inline D& operator>>(D& dec, T& value) {
	value = T(dec);
	return dec;
}

/** Decode an 8-bit unsigned integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, uint8_t& value) {
	const auto* data = dec.read(1);
	value = data[0];
	return dec;
}

/** Decode a 16-bit unsigned integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, uint16_t& value) {
	const auto* data = dec.read(2);
	value = data[0];
	value <<= 8;
	value |= data[1];
	return dec;
}

/** Decode a 32-bit unsigned integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, uint32_t& value) {
	const auto* data = dec.read(4);
	value = data[0];
	value <<= 8;
	value |= data[1];
	value <<= 8;
	value |= data[2];
	value <<= 8;
	value |= data[3];
	return dec;
}

/** Decode a 64-bit unsigned integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, uint64_t& value) {
	const auto* data = dec.read(8);
	value = data[0];
	value <<= 8;
	value |= data[1];
	value <<= 8;
	value |= data[2];
	value <<= 8;
	value |= data[3];
	value <<= 8;
	value |= data[4];
	value <<= 8;
	value |= data[5];
	value <<= 8;
	value |= data[6];
	value <<= 8;
	value |= data[7];
	return dec;
}

/** Decode an 8-bit signed integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, int8_t& value) {
	const auto* data = dec.read(1);
	uint8_t uvalue = data[0];
	value = std::bit_cast<int8_t>(uvalue);
	return dec;
}

/** Decode a 16-bit signed integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, int16_t& value) {
	const auto* data = dec.read(2);
	uint16_t uvalue = data[0];
	uvalue <<= 8;
	uvalue |= data[1];
	value = std::bit_cast<int16_t>(uvalue);
	return dec;
}

/** Decode a 32-bit signed integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, int32_t& value) {
	const auto* data = dec.read(4);
	uint32_t uvalue = data[0];
	uvalue <<= 8;
	uvalue |= data[1];
	uvalue <<= 8;
	uvalue |= data[2];
	uvalue <<= 8;
	uvalue |= data[3];
	value = std::bit_cast<int32_t>(uvalue);
	return dec;
}

/** Decode a 64-bit signed integer.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 */
template<typename D>
requires std::derived_from<D, decoder>
inline D& operator>>(D& dec, int64_t& value) {
	const auto* data = dec.read(8);
	uint64_t uvalue = data[0];
	uvalue <<= 8;
	uvalue |= data[1];
	uvalue <<= 8;
	uvalue |= data[2];
	uvalue <<= 8;
	uvalue |= data[3];
	uvalue <<= 8;
	uvalue |= data[4];
	uvalue <<= 8;
	uvalue |= data[5];
	uvalue <<= 8;
	uvalue |= data[6];
	uvalue <<= 8;
	uvalue |= data[7];
	value = std::bit_cast<int64_t>(uvalue);
	return dec;
}

}

#endif
