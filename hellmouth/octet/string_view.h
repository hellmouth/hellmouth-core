// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OCTET_STRING_VIEW
#define HELLMOUTH_OCTET_STRING_VIEW

#include <string_view>

namespace hellmouth::octet {

typedef std::basic_string_view<unsigned char> string_view;

}

#endif
