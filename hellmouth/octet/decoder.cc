// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/octet/decoder.h"

namespace hellmouth::octet {

void decoder::_end_of_data() {
	throw std::out_of_range("unexpected end of data");
}

}
