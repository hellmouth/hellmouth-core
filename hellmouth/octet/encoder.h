// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OCTET_ENCODER
#define HELLMOUTH_OCTET_ENCODER

#include <cstdint>
#include <cstring>
#include <concepts>

#include "hellmouth/octet/string_view.h"

namespace hellmouth::octet {

/** A base class for encoding a sequence of octets.
 * Operators are provided for encoding a selection of commonly-used data
 * types using the following conventions:
 *
 * - Network byte order is used throughout.
 * - Signed integers use twos-complement representation.
 *
 * This behaviour can be overridden in derived classes, but note that none
 * of the relevant functions are virtual. For this reason, the expectation
 * is that derived classes will always be used directly and never via a
 * pointer or reference to the base class.
 */
class encoder {
public:
	/** A type to represent an encoded octet. */
	typedef unsigned char value_type;

	/** A type for pointing to a mutable encoded octet. */
	typedef value_type* pointer;

	/** A type to represent a number of octets. */
	typedef size_t size_type;
protected:
	/** A pointer to the start of the encoded data. */
	pointer _base;

	/** A pointer to where the next octet should be encoded. */
	pointer _ptr;

	/** A pointer to one past the end of the buffer. */
	pointer _eptr;

	/** Get the number of free octets remaining in the buffer.
	 * @return the number of free octets remaining
	 */
	size_type remaining() const {
		return _eptr - _ptr;
	}
public:
	/** Construct new encoder.
	 * @param capacity the required initial capacity, in octets
	 */
	encoder(size_t capacity = 0x100);

	/** Destroy encoder. */
	~encoder();

	encoder(const encoder&) = delete;
	encoder& operator=(const encoder&) = delete;

	/** Write a given number of octets.
	 * This function reserves space to receive the octets, but does not
	 * itself write them into the buffer.
	 *
	 * @param count the number of octets to write
	 * @return a pointer to where the first octet should be placed
	 */
	pointer write(size_type count) {
		if (count > remaining()) {
			reserve(tell() + count);
		}
		pointer ptr = _ptr;
		_ptr += count;
		return ptr;
    }

	/** Write a given number of octets.
	 * @param data a pointer to the octets to be written
	 * @param count the number of octets to write
	 */
	void write(const void* data, size_type count) {
		memcpy(write(count), data, count);
	}

	/** Get the total buffer capacity.
	 * @return the capacity, in octets
	 */
	size_type capacity() const {
		return _eptr - _base;
	}

	/** Reserve a given total buffer capacity.
	 * @param capacity the required capacity
	 */
	void reserve(size_type capacity);

	/** Get the current location within the output data.
	 * @return the current location
	 */
	size_type tell() {
		return _ptr - _base;
	}

	/** Seek to a given location within the output data.
	 * @param the required location
	 */
	void seek(size_type location);

	/** Get the encoded output.
 	 * @return the encoded output
	 */
	octet::string_view data() const {
		return octet::string_view(_base, _ptr - _base);
	}
};

/** Encode anything convertible to an octet string view.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E, typename T>
requires std::derived_from<E, encoder> &&
	std::convertible_to<T, octet::string_view>
inline E& operator<<(E& enc, const T& value) {
	octet::string_view data = value;
	auto* buffer = enc.write(data.size());
	data.copy(buffer, data.size());
	return enc;
}

/** Encode an 8-bit unsigned integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, uint8_t value) {
	auto* buffer = enc.write(1);
	buffer[0] = value;
	return enc;
}

/** Encode a 16-bit unsigned integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, uint16_t value) {
	auto* buffer = enc.write(2);
	buffer[0] = value >> 8;
	buffer[1] = value >> 0;
	return enc;
}

/** Encode a 32-bit unsigned integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, uint32_t value) {
	auto* buffer = enc.write(4);
	buffer[0] = value >> 24;
	buffer[1] = value >> 16;
	buffer[2] = value >> 8;
	buffer[3] = value >> 0;
	return enc;
}

/** Encode a 64-bit unsigned integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, uint64_t value) {
	auto* buffer = enc.write(8);
	buffer[0] = value >> 56;
	buffer[1] = value >> 48;
	buffer[2] = value >> 40;
	buffer[3] = value >> 32;
	buffer[4] = value >> 24;
	buffer[5] = value >> 16;
	buffer[6] = value >> 8;
	buffer[7] = value >> 0;
	return enc;
}

/** Encode an 8-bit signed integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, int8_t value) {
	auto* buffer = enc.write(1);
	buffer[0] = value;
	return enc;
}

/** Encode a 16-bit signed integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, int16_t value) {
	uint16_t uvalue = value;
	auto* buffer = enc.write(2);
	buffer[0] = uvalue >> 8;
	buffer[1] = uvalue >> 0;
	return enc;
}

/** Encode a 32-bit signed integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, int32_t value) {
	uint32_t uvalue = value;
	auto* buffer = enc.write(4);
	buffer[0] = uvalue >> 24;
	buffer[1] = uvalue >> 16;
	buffer[2] = uvalue >> 8;
	buffer[3] = uvalue >> 0;
	return enc;
}

/** Encode a 64-bit signed integer.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 */
template<typename E>
requires std::derived_from<E, encoder>
inline E& operator<<(E& enc, int64_t value) {
	uint64_t uvalue = value;
	auto* buffer = enc.write(8);
	buffer[0] = uvalue >> 56;
	buffer[1] = uvalue >> 48;
	buffer[2] = uvalue >> 40;
	buffer[3] = uvalue >> 32;
	buffer[4] = uvalue >> 24;
	buffer[5] = uvalue >> 16;
	buffer[6] = uvalue >> 8;
	buffer[7] = uvalue >> 0;
	return enc;
}

}

#endif
