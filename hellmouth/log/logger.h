// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_LOG_LOGGER
#define HELLMOUTH_LOG_LOGGER

#include <format>
#include <string_view>
#include <utility>

#include <syslog.h>

namespace hellmouth::log {

/** An abstract base class for recording log messages.
 * The available severity levels match those defined by RFC 5424. POSIX
 * does not technically guarantee that these match the constants defined
 * in <syslog.h>, however the LSB does, and it is is acceptable to make
 * use of those constants within this project.
 *
 * Where reference is made below to the relative magnitude of two
 * severities, this should be interpreted semantically not numerically
 * (such that LOG_ERR is higher than LOG_WARNING).
 */
class logger {
private:
	/** The lowest severity level to be recorded, or -1 for none. */
	int _severity;
protected:
	/** Write a pre-formatted log message to the logger back end.
	 * Note that, unlike the syslog function, there is no provision
	 * for specifying the facility when logging individual messages
	 * (hence the first argument is the severity, not the priority).
	 *
	 * @param severity the severity of the message
	 * @param msg the message to be written
	 */
	virtual void write(int severity, std::string_view msg) = 0;
public:
	/** Construct logger.
	 * @param severity the lowest severity level to be recorded,
	 *  or -1 for none
	 */
	logger(int severity):
		_severity(severity) {}

	/** Destroy logger. */
	virtual ~logger() = default;

	/** Get the lowest severity level accepted by this logger.
	 * Note that this value only reflects filtering performed by this
	 * base class before messages are passed to the back end, and
	 * does not exclude the possibility of additional filtering
	 * occurring downstream.
	 *
	 * For this reason, higher-severity messages should never be
	 * replaced by lower-severity messages, only supplemented.
	 *
	 * Sources of log messages should not normally need to perform any
	 * pre-emptive run-time filtering themselves, but this might be
	 * desirable if the data for a log message was particularly
	 * expensive to collect, and can be achieved by consulting the
	 * value returned by this function.
	 *
	 * @return the lowest severity, or -1 if none
	 */
	int severity() const {
		return _severity;
	}

	/** Log a pre-formatted message.
	 * If the severity is lower than the threshold for recording then
	 * the message will be filtered by this base class before being
	 * passed to the back end.
	 *
	 * @param severity the severity of the message
	 * @param msg the message to be logged
	 */
	virtual void log(int severity, std::string_view msg) {
		if (severity <= _severity) {
			write(severity, msg);
		}
	}

	/** Format then log a message.
	 * If the severity is lower than the threshold for recording then
	 * the message will be filtered by this base class before being
	 * passed to the back end.
	 *
	 * @param severity the severity of the message
	 * @param fmt a format string for structuring the message
	 * @param args an argument pack for use by the format string
	 */
	template<typename... Args>
	void format(int severity,
		std::format_string<Args...> fmt, Args&&... args) {

		if (severity <= _severity) {
			write(severity, std::format(
				fmt, std::forward<Args>(args)...));
		}
	}
};

/** Get the default logger.
 * This is currently the null logger.
 */
logger& default_logger();

}

#endif
