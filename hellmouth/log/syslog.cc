// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/log/syslog.h"

namespace hellmouth::log {

void syslog::write(int severity, std::string_view msg) {
	int length = msg.length();
	const char* data = msg.data();
	::syslog(severity, "%.*s", length, data);
}

syslog::syslog(int severity):
	logger(severity) {}

syslog::syslog(int severity, const char* ident, int logopt, int facility):
	logger(severity) {

	::openlog(ident, logopt, facility);
}

}
