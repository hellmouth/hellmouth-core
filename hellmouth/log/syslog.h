// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_LOG_SYSLOG
#define HELLMOUTH_LOG_SYSLOG

#include "hellmouth/log/logger.h"

namespace hellmouth::log {

/** A class for logging to syslog. */
class syslog:
	public logger {
protected:
	void write(int severity, std::string_view msg) override;
public:
	/** Construct syslog logger using defaults.
	 * Construction by this this method does not call openlog.
	 *
         * @param severity the lowest severity level to be recorded,
         *  or -1 for none
	 */
	syslog(int severity);

	/** Construct syslog logger.
         * @param severity the lowest severity level to be recorded,
         *  or -1 for none
	 * @param ident the ident string to be passed to openlog
	 * @param logopt optional log options to be passed to openlog
	 * @param facility an optional facility to be passed to openlog
	 */
	syslog(int severity, const char* ident, int logopt = 0,
		int facility = LOG_USER);
};

}

#endif
