// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <ctime>
#include <iostream>

#include "hellmouth/log/ostream.h"

namespace hellmouth::log {

void ostream::write(int, std::string_view msg) {
	auto t = time(0);
	const auto* tm = gmtime(&t);

	char buffer[32];
	size_t count = strftime(buffer, sizeof(buffer), "%FT%T", tm);
	(*_out) << std::string_view(buffer, count) << ": "
		<< msg << std::endl;
}

}
