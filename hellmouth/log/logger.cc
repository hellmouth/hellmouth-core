// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/log/null.h"
#include "hellmouth/log/logger.h"

namespace hellmouth::log {

logger& default_logger() {
	static log::null _logger;
	return _logger;
}

}
