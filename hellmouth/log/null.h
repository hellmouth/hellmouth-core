// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_LOG_NULL
#define HELLMOUTH_LOG_NULL

#include "hellmouth/log/logger.h"

namespace hellmouth::log {

/** A class for disregarding all log entries. */
class null:
	public logger {
protected:
	void write(int, std::string_view msg) override;
public:
	/** Construct null logger.
	 * @param severity the highest severity to be logged
	 */
	null(int severity = -1):
		logger(severity) {}
};

}

#endif
