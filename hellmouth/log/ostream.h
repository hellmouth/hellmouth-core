// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_LOG_OSTREAM
#define HELLMOUTH_LOG_OSTREAM

#include <ostream>

#include "hellmouth/log/logger.h"

namespace hellmouth::log {

/** A class for logging to a std::ostream. */
class ostream:
	public logger {
private:
	/** The output stream to which messages should be written. */
	std::ostream* _out;
protected:
	void write(int, std::string_view msg) override;
public:
	/** Construct output stream logger.
	 * The specified output stream must outlive the logger constructed.
	 *
         * @param severity the lowest severity level to be recorded,
         *  or -1 for none
	 * @param out the output stream to which messages should be written
	 */
	ostream(int severity, std::ostream& out):
		logger(severity),
		_out(&out) {}
};

}

#endif
