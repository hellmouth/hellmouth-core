// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <climits>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <unistd.h>

#include "hellmouth/cli/exec.h"

namespace hellmouth::cli {

static void handle_error(const char* msg, const char* prefix=0) {
	std::cerr << msg << '.';
	if (prefix) {
		std::cerr << " See ";
		while (const char* f = strchr(prefix, '-')) {
			std::cerr << std::string_view(prefix, f - prefix) << ' ';
			prefix = f + 1;
		}
		std::cerr << "-h.";
	}
	std::cerr << std::endl;
	exit(1);
}

void exec(const char* prefix, char* argv[], char* env[]) {
	// Error if no command given.
	if (!argv[0]) {
		handle_error("Command not specified", prefix);
	}

	// The command name will be interpolated into a pathname.
	// There is unlikely to be any requirement to support commands
	// containing slash characters, therefore they are forbidden
	// to avoid unexpected behaviour.
	const char* cmdname = argv[0];
	if (strchr(cmdname, '/')) {
		handle_error("Command not recognised", prefix);
	}

	// Construct pathname for executable corresponding to command.
	// Also need the filename, for use as what will become argv[0].
	// This is done by recording the index into the pathname at
	// which the filename begins.
	char pathname[PATH_MAX];
	int filename_idx = 0;
	ssize_t count = snprintf(pathname, sizeof(pathname), "%s/%s/bin/%n%s%s",
		LIBEXECDIR, PKGNAME, &filename_idx, prefix, cmdname);
	if (count < 0) {
		perror("Failed to construct command");
		exit(1);
	} else if (static_cast<size_t>(count) >= sizeof(pathname)) {
		handle_error("Command too long");
		exit(1);
	}

	// The arguments passed to the command will be argv[optind]
	// onwards. The first argument should be a filename string
	// associated with the command to be executed, but it currently
	// contains only the raw command name. Change it to point to
	// the filename component within the constructed pathname.
	argv[0] = pathname + filename_idx;

	// Attemt to invoke the executable at the constructed pathname.
	if (::execve(pathname, argv, env) == -1) {
		// Want to give user-friendly error message for the case
		// where there is no executable corresponding to the given
		// command name. This would result in errno being set to
		// ENOENT, but is not the only way that could happen.
		// Therefore, if execve fails for any reason, try testing
		// whether the executable exists before deciding on how
		// to report the error.
		int execve_errno = errno;
		if ((access(pathname, F_OK) == -1) && (errno == ENOENT)) {
			handle_error("Command not recognised", prefix);
		} else {
			errno = execve_errno;
			perror("Command execution failed");
			exit(1);
		}
	}
}

}
