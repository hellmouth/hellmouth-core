// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CLI_EXEC
#define HELLMOUTH_CLI_EXEC

#include <filesystem>

namespace hellmouth::cli {

/** Execute HELLMOUTH CLI command.
 * The filename of the executable will consist of a sequence of
 * dash-separated subcommands. The final subcommand should be passed in
 * argv[0]. The others, together with the separating dash characters,
 * should be passed in the prefix argument.
 *
 * For example, if executing the command "hellmouth db get" then the
 * executable filename will be "hellmouth-db-get". The prefix would then
 * be "hellmouth-db-" and argv[0] (on entry) would be "get".
 *
 * The argument list must be null-terminated, and begin with the unprefixed
 * name of the subcommand to be executed. Note that the argument list is
 * modified during execution of this function, therefore it must be
 * non-const, and in the event that this function returns its content should
 * be considered unspecified.
 *
 * @param cmdprefix the command prefix
 * @param argv the required arguments
 * @param environ the required environment variables
 */
void exec(const char* prefix, char* argv[], char* env[]);

}

#endif
