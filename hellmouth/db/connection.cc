// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sys/socket.h>
#include <sys/un.h>
#include <system_error>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/db/connection.h"

namespace hellmouth::db {

static os::socket_descriptor connect(const std::filesystem::path& pathname) {
	os::socket_descriptor sd(AF_UNIX, SOCK_STREAM, 0);

	struct sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	auto npathname = pathname.native();
	if (npathname.length() >= sizeof(addr.sun_path)) {
		throw std::system_error(make_error_code(
			std::errc::filename_too_long));
	}
	npathname.copy(addr.sun_path, npathname.length());
	addr.sun_path[npathname.length()] = 0;

	sd.connect(addr);
	return sd;
}

const std::filesystem::path connection::default_pathname("/var/run/hellmouth");

connection::connection(const std::filesystem::path& pathname):
	connection(connect(pathname)) {}

connection::connection(os::socket_descriptor&& sd):
	_stream(std::move(sd)),
	_encode(_stream),
	_decode(_stream) {

	_stream.exceptions(
		std::ios_base::badbit |
		std::ios_base::failbit |
		std::ios_base::eofbit);
}

void connection::write(const data::any& value) {
	_encode(value);
	_stream.flush();
}

data::any connection::read() {
	return _decode();
}

}
