// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_DATABASE
#define HELLMOUTH_DB_DATABASE

#include <string_view>

#include "hellmouth/data/fwd.h"
#include "hellmouth/data/any.h"

namespace hellmouth::db {

/** An abstract base class for accessing a HELLMOUTH database.
 * This class should be used as a base for both full implementations of the
 * database, and for proxies which provide remote and/or filtered access.
 */
class database {
public:
	/** Get the value corresponding to a given path.
	 * @param path the path to be queried
	 * @return the value corresponding to that path
	 */
	virtual data::any get(std::string_view path);

	/** Put a value to a given path.
	 * @param path the path to be modified
	 * @param value the value to be put to that path
	 */
	virtual void put(std::string_view path, const data::any& value);

	/** Delete a given path.
	 * @param path the path to be deleted
	 */
	virtual void del(std::string_view path);

	/** Apply a patch to a given path.
	 * Paths specified within the path are relative to the path
	 * specified as an argument to this function.
	 *
	 * @param path the path to be modified
	 * @param spec a specification for the patch to be applied
	 */
	virtual void patch(std::string_view path, const data::any& spec);

	/** Get the status of the database node.
	 * The result of this function is explicitly dependent on the
	 * node it is sent to.
	 *
	 * The node status is returned as structured data, the content of
	 * which is not currently specified.
	 *
	 * @return the node status
	 */
	virtual data::any status();
};

}

#endif
