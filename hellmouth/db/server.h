// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_SERVER
#define HELLMOUTH_DB_SERVER

#include <filesystem>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/data/any.h"
#include "hellmouth/db/connection.h"
#include "hellmouth/db/database.h"

namespace hellmouth::db {

/** A class for serving a HELLMOUTH database using CBOR.
 * Note that this class does not implement the database itself,
 * only the CBOR-based IPC protocl used to access it.
 */
class server {
private:
	/** The databse to be served. */
	db::database* _db;

	/** A socket descriptor for listening for connections. */
	os::socket_descriptor _fd;
public:
	/** Construct database server.
	 * @param db the database to be served
	 * @param pathname the pathname of the UNIX domain socket
	 */
	server(database& db, const std::filesystem::path& pathname =
		connection::default_pathname);

	/** Get the socket descriptor.
	 * @return the socket descriptor
	 */
	const os::socket_descriptor& fd() const {
		return _fd;
	}

	/** Handle a single request.
	 * @param request the decoded request
	 * @return the unencoded response
	 */
	data::any handle(const data::any& request);

	/** Accept and handle a single connection. */
	void accept();

	/** Run this server.
	 * This function does not return unless and until the server stops
	 * running for some reason.
	 */
	void run();
};

}

#endif
