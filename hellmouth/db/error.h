// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_ERROR
#define HELLMOUTH_DB_ERROR

#include <stdexcept>
#include <string>

namespace hellmouth::db {

/** A base class for reporting database-related errors. */
class error:
	public std::runtime_error {
public:
	/** Construct generic database error.
	 * @param errmsg a human-readable message describing the error
	 */
	error(const std::string& errmsg):
		std::runtime_error(errmsg) {}

	/** Get the name of this error, for use in a resposne message.
	 * @return the name
	 */
	virtual std::string name() const = 0;

	static void throw_named_error(const std::string& errname,
		const std::string& errmsg);
};

/** An error for reporting that a database request is well-formed, but
 * requests an operation which has not been implemented. */
class not_implemented_error:
	public error {
public:
	/** Construct not implemented  error.
	 * @param errmsg a human-readable message describing the error
	 */
	not_implemented_error(const std::string& errmsg):
		error(errmsg) {}

	std::string name() const override {
		return "not-implemented";
	}
};

/** An error class for reporting that a requested path does not exist
 * within the database. */
class not_found_error:
	public error {
public:
	/** Construct not found error.
	 * @param errmsg a human-readable message describing the error
	 */
	not_found_error(const std::string& errmsg):
		error(errmsg) {}

	std::string name() const override {
		return "not-found";
	}
};

/** An error class for reporting that a requested patch was incompatible
 * with the content of the database. */
class patch_failed_error:
	public error {
public:
	/** Construct patch failed error.
	 * @param errmsg a human-readable message describing the error
	 */
	patch_failed_error(const std::string& errmsg):
		error(errmsg) {}

	std::string name() const override {
		return "patch-failed";
	}
};

/** An error class for reporting that the database has malfunctioned. */
class internal_error:
	public error {
public:
	/** Construct internal database error.
	 * @param errmsg a human-readable message describing the error
	 */
	internal_error(const std::string& errmsg):
		error(errmsg) {}

	std::string name() const override {
		return "internal";
	}
};

}

#endif
