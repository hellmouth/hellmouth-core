// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstring>
#include <ios>
#include <iostream>
#include <string_view>
#include <ranges>
#include <system_error>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "hellmouth/data/any.h"
#include "hellmouth/data/patch.h"
#include "hellmouth/data/pointer.h"
#include "hellmouth/cbor/encoder.h"
#include "hellmouth/cbor/decoder.h"
#include "hellmouth/db/error.h"
#include "hellmouth/db/server.h"

namespace hellmouth::db {

server::server(database& db, const std::filesystem::path& pathname):
	_db(&db),
	_fd(AF_UNIX, SOCK_STREAM, 0) {

	struct sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	auto npathname = pathname.native();
	if (npathname.length() >= sizeof(addr.sun_path)) {
		throw std::system_error(make_error_code(
			std::errc::filename_too_long));
	}
	npathname.copy(addr.sun_path, npathname.length());
	addr.sun_path[npathname.length()] = 0;
	unlink(pathname.c_str());
	_fd.bind(addr);
	_fd.listen();
}

data::any server::handle(const data::any& request) {
	data::object response;
	response["success"] = false;
	data::string method = request.at("method");
	if (method == "GET") {
		data::string path = request.at("path");
		try {
			response["content"] = _db->get(path);
			response["success"] = true;
		} catch (db::error& ex) {
			response["error"] = ex.name();
		}
	} else if (method == "PUT") {
		data::string path = request.at("path");
		data::any value = request.at("value");
		try {
			_db->put(path, value);
			response["success"] = true;
		} catch (db::error& ex) {
			response["error"] = ex.name();
		}
	} else if (method == "DELETE") {
		data::string path = request.at("path");
		try {
			_db->del(path);
			response["success"] = true;
		} catch (db::error& ex) {
			response["error"] = ex.name();
		}
	} else if (method == "PATCH") {
		data::string path = request.at("path");
		data::any spec = request.at("spec");
		try {
			_db->patch(path, spec);
			response["success"] = true;
		} catch (db::error& ex) {
			response["error"] = ex.name();
		}
	} else if (method == "STATUS") {
		try {
			response["content"] = _db->status();
			response["success"] = true;
		} catch (db::error& ex) {
			response["error"] = ex.name();
		}
	} else {
		response["error"] = "not-implemented";
	}
	return response;
}

void server::accept() {
	connection conn(_fd.accept());
	while (conn) {
		data::any req = conn.read();
		data::object resp = handle(req);
		conn.write(resp);
	}
}

void server::run() {
	while (true) {
		try {
			accept();
		} catch (std::ios_base::failure&) {}
	}
}

}
