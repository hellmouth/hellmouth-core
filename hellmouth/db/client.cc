// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/db/error.h"
#include "hellmouth/db/client.h"

namespace hellmouth::db {

client::client(const std::filesystem::path& pathname):
	_conn(pathname) {}

data::object client::request(const data::object& req) {
	_conn.write(req);
	data::object resp = _conn.read();

	bool success = resp.at("success");
	if (!success) {
		std::string errname = resp.at("error");
		std::string errmsg = (resp.contains("message")) ?
			static_cast<std::string>(resp.at("message")) :
			errname;
		db::error::throw_named_error(errname, errmsg);
	}
	return resp;
}

data::any client::get(std::string_view path) {
	data::object req;
	req["method"] = "GET";
	req["path"] = data::string(path);

	data::object resp = request(req);
	return resp.at("content");
}

void client::put(std::string_view path, const data::any& value) {
	data::object req;
	req["method"] = "PUT";
	req["path"] = data::string(path);
	req["value"] = value;
	request(req);
}

void client::del(std::string_view path) {
	data::object req;
	req["method"] = "DELETE";
	req["path"] = data::string(path);
	request(req);
}

void client::patch(std::string_view path, const data::any& spec) {
	data::object req;
	req["method"] = "PATCH";
	req["path"] = data::string(path);
	req["spec"] = spec;
	request(req);
}

data::any client::status() {
	data::object req;
	req["method"] = "STATUS";

	data::object resp = request(req);
	return resp.at("content");
}

}
