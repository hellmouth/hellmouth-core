// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <functional>
#include <map>

#include "hellmouth/db/error.h"

namespace hellmouth::db {

void error::throw_named_error(const std::string& errname,
	const std::string& errmsg) {

	typedef std::function<void(const std::string& errmsg)> handler;
	static std::map<std::string, handler> errors = {
		{"not-implemented", [](const std::string& errmsg) { throw not_implemented_error(errmsg); }},
		{"not-found", [](const std::string& errmsg) { throw not_found_error(errmsg); }},
		{"patch-failed", [](const std::string& errmsg) { throw patch_failed_error(errmsg); }},
		{"internal", [](const std::string& errmsg) { throw internal_error(errmsg); }}};

	auto f = errors.find(errname);
	if (f != errors.end()) {
		f->second(errmsg);
	}
	throw internal_error(errmsg);
}

}
