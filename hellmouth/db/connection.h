// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CONNECTION
#define HELLMOUTH_DB_CONNECTION

#include <filesystem>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/os/fdstream.h"
#include "hellmouth/cbor/encoder.h"
#include "hellmouth/cbor/decoder.h"
#include "hellmouth/data/any.h"

namespace hellmouth::db {

/** A class to represent a HELLMOUTH database connection. */
class connection {
public:
	/** The default pathname for the UNIX domain socket. */
	static const std::filesystem::path default_pathname;
private:
	/** An input/output stream for communicating the database. */
	os::fdstream _stream;

	/** A CBOR encoder for writing structured data to the stream. */
	cbor::encoder _encode;

	/** A CBOR decoder for reading structured data from the stream. */
	cbor::decoder _decode;
public:
	/** Make a connection from an existing socket descriptor.
	 * @param sd the socket descriptor
	 */
	connection(os::socket_descriptor&& sd);

	/** Make a new connection to a server.
	 * @param pathname the pathname of the UNIX domain socket
	 */
	connection(const std::filesystem::path& pathname = default_pathname);

	/** Test whether an error has occurred on the underlying stream.
	 * @return false if badbit or failbit is set, otherwise true
	 */
	explicit operator bool() const {
		return !_stream.fail();
	}

	/** Test whether an error has occurred on the underlying stream.
	 * @return true if badbit or failbit is set, otherwise false
	 */
	bool operator!() const {
		return _stream.fail();
	}

	/** Write a value to the connection.
	 * @param value the value to be written
	 */
	void write(const data::any& value);

	/** Read a value from the connection.
	 * @return the value read
	 */
	data::any read();
};

}

#endif
