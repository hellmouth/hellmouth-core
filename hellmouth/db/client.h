// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DB_CLIENT
#define HELLMOUTH_DB_CLIENT

#include <string_view>
#include <filesystem>

#include "hellmouth/db/connection.h"
#include "hellmouth/db/database.h"

namespace hellmouth::db {

/** A class for accessing a HELLMOUTH database using CBOR. */
class client:
	public database {
private:
	/** A connection to the database. */
	connection _conn;

	/** Make a request to the database. */
	data::object request(const data::object& req);
public:
	/** Construct database client.
	 * @param pathname the pathname of the UNIX domain socket
	 */
	client(const std::filesystem::path& pathname =
		connection::default_pathname);

	data::any get(std::string_view path) override;
	void put(std::string_view path, const data::any& value) override;
	void del(std::string_view path) override;
	void patch(std::string_view path, const data::any& spec) override;
	data::any status() override;
};

}

#endif
