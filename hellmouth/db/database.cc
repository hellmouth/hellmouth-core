// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/string.h"
#include "hellmouth/data/array.h"
#include "hellmouth/data/object.h"
#include "hellmouth/db/error.h"
#include "hellmouth/db/database.h"

namespace hellmouth::db {

data::any database::get(std::string_view path) {
	throw not_implemented_error("database GET method not implemented");
}

void database::put(std::string_view path, const data::any& value) {
	// [TODO]: it would be preferable to achieve this using a single
	// patch by calculating the difference from the current state.
	patch(path, data::any());
	patch(path, value);
}

void database::del(std::string_view path) {
	patch(path, data::any());
}

void database::patch(std::string_view path, const data::any& spec) {
	throw not_implemented_error("database PATCH method not implemented");
}

data::any database::status() {
	throw not_implemented_error("database STATUS method not implemented");
}

}
