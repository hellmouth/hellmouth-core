// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_PORT_NUMBER
#define HELLMOUTH_PORT NUMBER

#include <cstdint>
#include <string>
#include <string_view>

namespace hellmouth::net {

/** A class to represent a validated port number.
 * A valid port number must be an integer in the range 1-65535 inclusive.
 *
 * As strings, port numbers are represented in decimal form. Leading zeros
 * are accepted during parsing, but are not preserved.
 */
class port_number {
private:
	/** The port number, as an integer. */
	uint16_t _value;
public:
	/** Construct port number from an unsigned 16-bit integer.
	 * @param value the value as a uint16_t
	 */
	explicit port_number(uint16_t value);

	/** Construct port number from a character string.
	 * @param value the value as a string_view
	 */
	explicit port_number(const std::string_view& value);

	/** Convert this port number to an unsigned 16-bit integer.
	 * The result will be in host byte order.
	 *
	 * @return the port number as a uint16_t
	 */
	operator uint16_t() const {
		return _value;
	}

	/** Convert this port number to a string.
	 * @return the port number as a string
	 */
	operator std::string() const {
		return std::to_string(_value);
	}
};

}

#endif
