// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_GETADDRINFO
#define HELLMOUTH_NET_GETADDRINFO

#include <stdexcept>
#include <netdb.h>

namespace hellmouth::net {

/** A class for resolving host and service names.
 * This class acts as wrapper around the getaddrinfo function, providing
 * an iterable and RAII-managed result list.
 *
 * To resolve a hostname:
 *
 * - Construct an instance of this class.
 * - Perform any required customisation of the hints.
 * - Call the resolve function to obtain a list of results
 * - Iterate over the list of results
 */
class getaddrinfo {
private:
	/** The hints to be passed to getaddrinfo. */
	struct addrinfo _hints{0};
public:
	/** A class to represent an error returned by getaddrinfo. */
	class error:
		public std::runtime_error {
	public:
		/** Construct error from error code.
		 * @param errcode the error code returned by getaddrinfo
		 */
		error(int errcode);
	};

	/** A forward iterator for a list of addrinfo structures. */
	class iterator {
	private:
		/** The node referred to by this iterator,
		 * or 0 to refer to the end of the list. */
		struct addrinfo* _node = 0;
	public:
		/** Construct iterator referring to the end of the list. */
		iterator() = default;

		/** Construct iterator referring to a specific list node.
		 * @param node the node to refer to
		 */
		explicit iterator(struct addrinfo* node):
			_node(node) {}

		/** Pre-increment this iterator.
		 * @return a reference to this
		 */
		iterator& operator++() {
			_node = _node->ai_next;
			return *this;
		}

		/** Post-increment this iterator.
		 * @return a reference to this
		 */
		iterator operator++(int) {
			struct addrinfo* before = _node;
			_node = _node->ai_next;
			return iterator(before);
		}

		/** Dereference this iterator.
		 * @return a reference to the underlying addrinfo structure
		 */
		const struct addrinfo& operator*() const {
			return *_node;
		}

		/** Access members of the addrinfo structure.
		 * @return a pointer to the underlying addrinfo structure
		 */
		const struct addrinfo* operator->() const {
			return _node;
		}

		/** Compare this iterator with another.
		 * @param that the iterator to compare against
		 * @return true if equal, otherwise false
		 */
		bool operator==(const iterator& that) const = default;
	};

	/** A class to represent a list of addrinfo structures.
	 * Iteration through the list is supported in the forward direction.
	 *
	 * Note that although is possible for instances of this class to be
	 * empty, this will not happen when used for their intended purpose
	 * of returning results from a resolver.
	 */
	class result {
	public:
		/** A forward iterator for a list of addrinfo structures. */
		typedef getaddrinfo::iterator iterator;
	private:
		/** A pointer to the head of the list. */
		struct addrinfo *_head;
	public:
		/** Construct result list.
		 * @param head the head of the list
		 */
		explicit result(struct addrinfo* head):
			_head(head) {}

		result(const result&) = delete;
		result(result&&) = default;

		/** Destroy result list. */
		~result();

		result& operator=(const result&) = delete;
		result& operator=(result&&) = default;

		/** Get iterator for tbe start of this list.
		 * @return the iterator
		 */
		iterator begin() const {
			return iterator(_head);
		}

		/** Get iterator for the end of this list.
		 * @return the iterator
		 */
		iterator end() const {
			return iterator();
		}

		/** Get the first addrinfo structure from this list.
		 * If the list is empty then the effect of this function
		 * is undefined.
		 *
		 * @return the first addrinfo structure
		 */
		const struct addrinfo& front() const {
			return *_head;
		}
	};

	/** Construct resolver.
	 * By default the address family is set to AF_UNSPEC, and the
	 * AI_V4MAPPED and AI_ADDRCONFIG flags are set.
	 */
	getaddrinfo();

	/** Set flags.
	 * @param flags the required flags
	 * @return a reference to this
	 */
	getaddrinfo& flags(int flags) {
		_hints.ai_flags = flags;
		return *this;
	}

	/** Set or clear individual flag.
	 * @param flag the flag to be set or cleared
	 * @param value true to set flag, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& flag(int flag, bool value) {
		if (value) {
			_hints.ai_flags |= flag;
		} else {
			_hints.ai_flags &= ~flag;
		}
		return *this;
	}

	/** Set or clear the AI_PASSIVE flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& passive(bool value = true) {
		return flag(AI_PASSIVE, value);
	}

	/** Set or clear the AI_CANONNAME flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& canonname(bool value = true) {
		return flag(AI_CANONNAME, value);
	}

	/** Set or clear the AI_NUMERICHOST flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& numerichost(bool value) {
		return flag(AI_NUMERICHOST, value);
	}

	/** Set or clear the AI_NUMERICSERV flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& numericserv(bool value) {
		return flag(AI_NUMERICSERV, value);
	}

	/** Set or clear the AI_V4MAPPED flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& v4mapped(bool value) {
		return flag(AI_V4MAPPED, value);
	}

	/** Set or clear the AI_ALL flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& all(bool value) {
		return flag(AI_ALL, value);
	}

	/** Set or clear the AI_ADDRCONFIG flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getaddrinfo& addrconfig(bool value) {
		return flag(AI_ADDRCONFIG, value);
	}

	/** Set the required address family.
	 * @param family the required address family
	 * @return a reference to this
	 */
	getaddrinfo& family(int family) {
		_hints.ai_family = family;
		return *this;
	}

	/** Set the required socket type.
	 * @param family the required socket type
	 * @return a reference to this
	 */
	getaddrinfo& socktype(int socktype) {
		_hints.ai_socktype = socktype;
		return *this;
	}

	/** Set the required socket type to SOCK_STREAM.
	 * @return a reference to this
	 */
	getaddrinfo& stream() {
		return socktype(SOCK_STREAM);
	}

	/** Set the required socket type to SOCK_DGRAM.
	 * @return a reference to this
	 */
	getaddrinfo& dgram() {
		return socktype(SOCK_DGRAM);
	}

	/** Set the required protocol.
	 * @param family the required protocol
	 * @return a reference to this
	 */
	getaddrinfo& protocol(int protocol) {
		_hints.ai_protocol = protocol;
		return *this;
	}

	/** Resolve a given host and service name.
	 * If this function returns successfully then the result list
	 * will contain at least one entry.
	 *
	 * @param hostname the host name to be resolved
	 * @param servname the service name to be resolved
	 */
	result resolve(const std::string& hostname,
		const std::string& servname) const;
};

}

#endif
