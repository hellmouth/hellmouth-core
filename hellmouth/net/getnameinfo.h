#// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_GETNAMEINFO
#define HELLMOUTH_NET_GETNAMEINFO

#include <stdexcept>
#include <netdb.h>

namespace hellmouth::net {

/** A class for resolving host and service names.
 * This class acts as wrapper around the getnameinfo function.
 *
 * To resolve a hostname:
 *
 * - Construct an instance of this class.
 * - Perform any required customisation of the flags.
 * - Call the resolve function to obtain the result
 */
class getnameinfo {
private:
	/** The flags to be passed to getnameinfo. */
	int _flags = 0;
public:
	/** A class to represent an error returned by getaddrinfo. */
	class error:
		public std::runtime_error {
	public:
		/** Construct error from error code.
		 * @param errcode the error code returned by getaddrinfo
		 */
		error(int errcode);
	};

	/** A structure to represent the result of resolving an address. */
	struct result {
		/** The resulting hostname. */
		std::string hostname;

		/** The resulting service name. */
		std::string servname;

		/** Construct result.
		 * @param hostname the null-terminated hostname
		 * @param servname the null-terminated service name
		 */
		result(const char* hostname, const char* servname):
			hostname(hostname), servname(servname) {}
	};

	/** Construct resolver.
	 * By default all flags are unset. */
	getnameinfo() = default;

	/** Set flags.
	 * @param flags the required flags
	 * @return a reference to this
	 */
	getnameinfo& flags(int flags) {
		_flags = flags;
		return *this;
	}

	/** Set or clear individual flag.
	 * @param flag the flag to be set or cleared
	 * @param value true to set flag, false to clear
	 * @return a reference to this
	 */
	getnameinfo& flag(int flag, bool value) {
		if (value) {
			_flags |= flag;
		} else {
			_flags &= ~flag;
		}
		return *this;
	}

	/** Set or clear the NI_NOFQDN flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getnameinfo& nofqdn(bool value = true) {
		return flag(NI_NOFQDN, value);
	}

	/** Set or clear the NI_NUMERICHOST flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getnameinfo& numerichost(bool value) {
		return flag(NI_NUMERICHOST, value);
	}

	/** Set or clear the NI_NAMEREQD flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getnameinfo& namereqd(bool value) {
		return flag(NI_NAMEREQD, value);
	}

	/** Set or clear the NI_NUMERICSERV flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getnameinfo& numericserv(bool value) {
		return flag(NI_NUMERICSERV, value);
	}

	/** Set or clear the NI_DGRAM flag.
	 * @param value true to set, false to clear
	 * @return a reference to this
	 */
	getnameinfo& dgram(bool value) {
		return flag(NI_DGRAM, value);
	}

	/** Resolve a given socket address.
	 * @param addr the socket address to be resolved
	 * @param addrlen the length of the socket address
	 * @return the corresponding host and service names
	 */
	result resolve(const struct sockaddr* addr, socklen_t addrlen) const;

	/** Resolve a given socket address.
	 * @param addr the socket address to be resolved
	 */
	template<typename T>
	result resolve(const T& addr) const {
		return resolve(reinterpret_cast<const sockaddr*>(&addr), sizeof(addr));
	}
};

}

#endif
