// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_STATUS_CODE
#define HELLMOUTH_NET_HTTP_STATUS_CODE

#include <string>

namespace hellmouth::net::http {

/** A class to represent an HTTP status code. */
class status_code {
private:
	/** The numerical value of the status code. */
	unsigned int _code;
public:
	/** Construct HTTP status code from numerical value.
	 * @param code the required code
	 */
	status_code(unsigned int code);

	/** Parse HTTP status code from character string.
	 * @param content the string to be parsed
	 */
	explicit status_code(std::string content);

	/** Assign a numerical value to this status code.
	 * @param code the required code
	 */
	status_code& operator=(unsigned int code) {
		*this = status_code(code);
		return *this;
	}

	/** Assign a character string to this status code.
	 * @param content the string to be parsed
	 */
	status_code& operator=(std::string content) {
		*this = status_code(content);
		return *this;
	}

	/** Get the numerical value of this status code.
	 * @return the numerical value
	 */
	operator unsigned int() const {
		return _code;
	}

	/** Convert this status code to a string
	 * @return the status code as a string
	 */
	operator std::string() const;
};

inline std::string to_string(status_code status) {
        return status;
}

inline std::ostream& operator<<(std::ostream& out, status_code status) {
	out << to_string(status);
	return out;
}

/** Make a reason phrase for a given status code.
 * This function is primarily intended for use by functions which accept a
 * status code and an optional reason phrase as arguments. It provides the
 * means to automatically generate a meaningful reason phrase, for most
 * commonly-used status codes, if one has not been supplied.
 *
 * The current implementation supports only the status codes defined by
 * RFC 9110, returning an empty string for any other values. Future
 * implementations can be expected to provide similar functionality, but
 * the set of supported status codes and the detailed wording of reason
 * phrases are subject to change.
 *
 * @param status the status code
 * @return a corresponding reason phrase, or the empty string if
 *  not available.
 */
std::string make_reason_phrase(status_code status);

}

#endif
