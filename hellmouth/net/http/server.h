// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_SERVER
#define HELLMOUTH_NET_HTTP_SERVER

#include <functional>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/net/http/handler.h"

namespace hellmouth::net::http {

class server {
private:
	/** A handler providing the content to be served. */
	std::reference_wrapper<http::handler> _content;

	/** A socket descriptor for listening for connections. */
	os::socket_descriptor _fd;
public:
	/** Construct HTTP server.
	 * @param content the content to be served
	 * @param port the port number on which to listen
	 */
	server(http::handler& content, uint16_t port = 80);

	/** Accept a single connection. */
	void accept();

	/** Run server. */
	void run();
};

}

#endif
