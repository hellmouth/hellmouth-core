// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_FILE_HANDLER
#define HELLMOUTH_NET_HTTP_FILE_HANDLER

#include <filesystem>

#include "hellmouth/net/http/handler.h"

namespace hellmouth::net::http {

/** A class for serving content from the filesystem.
 * Only GET requests are supported at present, therefore the question of
 * whether filesystem access is read-only or read-write is currently moot,
 * however read-only may be considered the default.
 *
 * Request targets are normalised to prevent path traversal.
 *
 * Symlinks are followed, even if they lead outside the base directory,
 * therefore care is needed to ensure that this does not expose more data
 * than intended.
 */
class file_handler:
	public handler {
public:
	/** The base pathname of the content to be served. */
	std::filesystem::path base;

	/** The default filename for use when path is slash-terminated. */
	std::string default_filename = "index.html";

	/** Construct file handler.
	 * @param base the base pathname of the content to be served
	 */
	explicit file_handler(std::filesystem::path base):
		base(std::move(base)) {}

	response handle(request& req) override;
};

}

#endif
