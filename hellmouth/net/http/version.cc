// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>
#include <format>
#include <utility>

#include "hellmouth/net/http/version.h"

namespace hellmouth::net::http {

version::version(std::string content) {
	static const std::regex version_pattern(
		R"(HTTP/([0-9]+)[.]([0-9]+))");

	std::smatch match;
	if (!std::regex_match(content, match, version_pattern)) {
		throw std::invalid_argument("invalid HTTP version");
	}

	major = std::stoul(match[1]);
	minor = std::stoul(match[2]);
}

version::operator std::string() const {
	return std::format("HTTP/{}.{}", major, minor);
}

}
