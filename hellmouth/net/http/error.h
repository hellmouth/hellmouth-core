// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_ERROR
#define HELLMOUTH_NET_HTTP_ERROR

#include <stdexcept>

#include "hellmouth/net/http/status_code.h"

namespace hellmouth::net::http {

/** A class to represent an HTTP error. */
class error:
	public std::runtime_error {
private:
	/** The HTTP status code. */
	http::status_code _status_code;
public:
	error(http::status_code status_code):
		std::runtime_error(make_reason_phrase(status_code)),
		_status_code(status_code) {}

	error(http::status_code status_code,
		const std::string& reason_phrase):
		std::runtime_error(reason_phrase),
		_status_code(status_code) {}

	/** Get the HTTP status code.
	 * @return the HTTP status code
	 */
	const http::status_code& status_code() const {
		return _status_code;
	}
};

class bad_request:
	public error {
public:
	bad_request():
		error(400) {}
};

}

#endif
