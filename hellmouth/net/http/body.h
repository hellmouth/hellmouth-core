// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_BODY
#define HELLMOUTH_NET_HTTP_BODY

#include <memory>
#include <string>
#include <istream>

namespace hellmouth::net::http {

/** A class to represent the body of an HTTP message.
 * This class is used to represent the bodies of both inbound and outbound
 * HTTP messages. It allows the unencoded content of the body to be read
 * using a std::stream. The source of the data will depend on whether the
 * message is inbound or outbound:
 *
 * - For inbound messages the data source would normally be a network
 *   connection, via any required decoding stages.
 * - For outbound messages the data source would typically be either
 *   string if the content is small enough to conveniently fit in memory,
 *   or file descriptor or similar if not.
 *
 * Aside from the very limited read-ahead facilities provided by a
 * std::istream (peek/unget), once the content has been read once it
 * cannot be read again.
 */
class body {
private:
	/** The underlying stream. */
	std::unique_ptr<std::istream> _stream;
public:
	/** Construct message body from character string.
	 * @param content the required content as a string
	 */
	body(std::string content = std::string());

	/** Construct message body from stream.
	 * @param in a stream for delivering the required content
	 */
	body(std::unique_ptr<std::istream> in);

	/** Access the content of this message body. */
	std::istream& content() {
		return *_stream;
	}
};

}

#endif
