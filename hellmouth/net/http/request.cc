// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <memory>
#include <map>

#include "hellmouth/net/authority.h"
#include "hellmouth/net/http/error.h"
#include "hellmouth/net/http/request.h"
#include "hellmouth/net/http/client.h"
#include "hellmouth/net/https/client.h"

namespace hellmouth::net::http {

static inline bool iscrlf(int ch) {
	return ch == '\r' || ch == '\n';
}

class request::parser {
public:
	std::string method;
	std::string target;
	std::string version;
	parser(std::istream& in);
};

request::parser::parser(std::istream& in) {
	// Ignore any leading CR or LF characters.
	while (iscrlf(in.peek())) {
		in.ignore();
	}

	// Read request line from stream.
	// Line endings should be CRLF, but tolerate LF.
	std::string reqline;
	std::getline(in, reqline);
	if (reqline.ends_with('\r')) {
		reqline.pop_back();
	}

	// The first separator must be a single space.
	auto spc1 = reqline.find(' ');
	if (spc1 == std::string::npos) {
		throw bad_request();
	}
	method = reqline.substr(0, spc1);

	// If there is a second separator then it too must be a
	// single space,
	auto spc2 = reqline.find(' ', spc1 + 1);
	if (spc2 != std::string::npos) {
		target = reqline.substr(spc1 + 1, spc2 - spc1 - 1);
		version = reqline.substr(spc2 + 1);
	} else {
		target = reqline.substr(spc1 + 1);
		version = std::string("HTTP/0.9");
	}
}

request::request(const parser& parsed):
	method(std::move(parsed.method)),
	target(std::move(parsed.target)),
	version(std::move(parsed.version)) {}

request::request(std::string method, std::string target, std::string version):
	method(std::move(method)),
	target(std::move(target)),
	version(std::move(version)) {

	uint32_t default_port = -1;
	if (this->target.scheme) {
		const auto& scheme = *this->target.scheme;
		if (scheme == "http") {
			default_port = 80;
		} else if (scheme == "https") {
			default_port = 443;
		}
	}

	if (this->target.authority) {
		net::authority authority(*this->target.authority);
		std::string host = authority.host;
		if (authority.port && *authority.port != default_port) {
			host += ':';
			host += std::to_string(*authority.port);
		}
		headers.push_back("Host", host);
	}
	headers.push_back("Connection", "close");
}

request::request(std::unique_ptr<std::istream> in):
	request(parser(*in)) {

	*in >> headers;
	body = http::body(std::move(in));
}

std::ostream& operator<<(std::ostream& out, request& req) {
	net::uri request_target;
	request_target.path = req.target.path;
	request_target.query = req.target.query;
	if (request_target.path.empty()) {
		request_target.path = "/";
	}

	out << req.method << ' '
		<< request_target << ' '
		<< req.version << "\r\n"
		<< req.headers
		<< "\r\n";

	// When copying from a streambuf to an ostream, no content is
	// unhelpfully treated as a special case which puts the output
	// stream into the failed state. We avoid this by first
	// checking that the body is non-empty.
	auto* buf = req.body.content().rdbuf();
	int ch = buf->sgetc();
	if (ch != std::istream::traits_type::eof()) {
		buf->sungetc();
		out << buf;
	}
	return out;
}

typedef std::map<std::string, std::unique_ptr<handler>> handler_table;

handler_table make_handler_table() {
	handler_table table;
	table["http"] = std::make_unique<http::client>();
	table["https"] = std::make_unique<https::client>();
	return table;
}

http::response fetch(http::request& req) {
	static handler_table table = make_handler_table();

	if (!req.target.scheme) {
		throw std::invalid_argument("missing URI scheme");
	}

	auto f = table.find(*req.target.scheme);
	if (f == table.end()) {
		throw std::invalid_argument("unrecognised URI scheme");
	}
	return f->second->handle(req);
}

}
