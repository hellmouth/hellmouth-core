// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sstream>

#include "hellmouth/net/http/body.h"

namespace hellmouth::net::http {

body::body(std::string content):
	_stream(std::make_unique<std::istringstream>(
		std::move(content))) {}

body::body(std::unique_ptr<std::istream> in):
	_stream(std::move(in)) {}

}
