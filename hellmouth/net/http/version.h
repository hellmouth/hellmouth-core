// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_VERSION
#define HELLMOUTH_NET_HTTP_VERSION

#include <string>
#include <ostream>

namespace hellmouth::net::http {

/** A class to represent an HTTP version number. */
class version {
public:
	/** The major version. */
	unsigned long major;

	/** The minor version. */
	unsigned long minor;

	/** Construct an HTTP version number from its components.
	 * @param major the major version
	 * @param minor the minor version
	 */
	version(unsigned long major, unsigned long minor):
		major(major), minor(minor) {}

	/** Parse an HTTP version number from a character string.
	 * @param content the string to be parsed
	 */
	explicit version(std::string content);

	/** Assign a parsed character string to this HTTP version.
	 * @param content the string to be parsed
	 */
	version& operator=(std::string content) {
		*this = version(content);
		return *this;
	}

	/** Convert this version number to a string
	 * @return the version number as a string
	 */
	operator std::string() const;
};

inline std::string to_string(const version& ver) {
	return ver;
}

inline std::ostream& operator<<(std::ostream& out, const version& ver) {
	out << to_string(ver);
	return out;
}

}

#endif
