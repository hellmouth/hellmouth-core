// This file is part of the HELLMOUTH cluster system.
// Copyright 2022 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_RESPONSE
#define HELLMOUTH_NET_HTTP_RESPONSE

#include <memory>
#include <ostream>

#include "hellmouth/net/http/version.h"
#include "hellmouth/net/http/status_code.h"
#include "hellmouth/net/http/header_list.h"
#include "hellmouth/net/http/body.h"

namespace hellmouth::net::http {

/** A data structure to represent an HTTP response. */
struct response {
public:
	/** The HTTP version. */
	http::version version;

	/** The HTTP status code. */
	http::status_code status_code;

	/** An optional reason phrase. */
	std::string reason_phrase;

	/** The response headers. */
	http::header_list headers;

	/** The response body. */
	http::body body;
private:
	/** A class for parsing HTTP responses. */
	class parser;

	/** Construct HTTP response from parser instance. */
	response(const parser& parsed);
public:
	/** Construct HTTP response from components.
	 * @param status_code the required status code
	 * @param reason_phrase the required reason phrase
	 * @param version the required HTTP version
	 */
	response(
		http::status_code status_code,
		std::string reason_phrase = std::string(),
		http::version version = http::version(1, 1)):
		version(version),
		status_code(status_code),
		reason_phrase(reason_phrase) {}

	/** Construct HTTP response from input stream.
	 * @param in the stream
	 */
	response(std::unique_ptr<std::istream> in);
};

std::ostream& operator<<(std::ostream& out, response& resp);

}

#endif
