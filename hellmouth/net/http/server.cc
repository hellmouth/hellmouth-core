// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sys/socket.h>
#include <arpa/inet.h>

#include "hellmouth/os/fdstream.h"
#include "hellmouth/net/http/request.h"
#include "hellmouth/net/http/server.h"

namespace hellmouth::net::http {

server::server(http::handler& content, uint16_t port):
	_content(content),
	_fd(AF_INET, SOCK_STREAM, 0) {

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(port);
	_fd.bind(addr);
	_fd.listen();
}

void server::accept() {
	auto fd = _fd.accept();
	auto in = std::make_unique<os::fdstream>(fd);
	request req(std::move(in));
	response resp = _content.get().handle(req);
	os::fdstream out(fd);
	out << resp;
	out.flush();
}

void server::run() {
	while (true) {
		try {
			accept();
		} catch (std::ios_base::failure&) {}
	}
}

}
