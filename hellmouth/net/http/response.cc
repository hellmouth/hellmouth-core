// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <format>

#include "hellmouth/net/http/response.h"

namespace hellmouth::net::http {

inline bool iscrlf(int ch) {
	return ch == '\r' || ch == '\n';
}

class response::parser {
public:
	std::string version;
	std::string status_code;
	std::string reason_phrase;
	parser(std::istream& in);
};

response::parser::parser(std::istream& in) {
	// Ignore any leading CR or LF characters.
	while (iscrlf(in.peek())) {
		in.ignore();
	}

	// Read response line from stream.
	// Line endings should be CRLF, but tolerate LF.
	std::string respline;
	std::getline(in, respline);
	if (respline.ends_with('\r')) {
		respline.pop_back();
	}

	// The first separator must be a single space.
	auto spc1 = respline.find(' ');
	if (spc1 == std::string::npos) {
		throw std::invalid_argument("invalid HTTP response");
	}
	version = respline.substr(0, spc1);

	// If there is a second separator then it too must be a
	// single space,
	auto spc2 = respline.find(' ', spc1 + 1);
	if (spc2 != std::string::npos) {
		status_code = respline.substr(spc1 + 1, spc2 - spc1 - 1);
		reason_phrase = respline.substr(spc2 + 1);
	} else {
		status_code = respline.substr(spc1 + 1);
	}
}

response::response(const parser& parsed):
	version(parsed.version),
	status_code(parsed.status_code),
	reason_phrase(parsed.reason_phrase) {}

response::response(std::unique_ptr<std::istream> in):
	response(parser(*in)) {

	*in >> headers;
	body = http::body(std::move(in));
}

std::ostream& operator<<(std::ostream& out, response& resp) {
	std::string status_line = std::format("{} {} {}",
		std::string(resp.version),
		std::string(resp.status_code),
		resp.reason_phrase);
	out << status_line << "\r\n";
	out << resp.headers;
	out << "\r\n";

	// When copying from a streambuf to an ostream, no content is
	// unhelpfully treated as a special case which puts the output
	// stream into the failed state. We avoid this by first
	// checking that the body is non-empty.
	auto* buf = resp.body.content().rdbuf();
	int ch = buf->sgetc();
	if (ch != std::istream::traits_type::eof()) {
		buf->sungetc();
		out << buf;
	}
	return out;
}

}
