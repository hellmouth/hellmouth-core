// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_TOKEN
#define HELLMOUTH_NET_HTTP_TOKEN

#include <cctype>
#include <string>
#include <ostream>

namespace hellmouth::net::http {

extern const unsigned char separators[];

/** Test whether a character is a separator.
 * @param the character to be tested
 * @return true if a separator, otherwise false
 */
inline bool isseparator(int ch) {
	return isascii(ch) && separators[ch] != 0;
}

/** A class to represent an HTTP token.
 * A token is a non-empty sequence of characters in the ASCII range
 * which does not include any control characters or separators.
 */
class token {
private:
	/** The content of this token. */
	std::string _content;
public:
	/** Construct an HTTP token from a character string.
	 * @param content the required content
	 */
	token(std::string content);

	/** Construct an HTTP token from a C-style character string.
	 * @param content the required content
	 */
	token(const char* content):
		token(std::string(content)) {}

	/** Assign a character string to this token.
	 * @param content the required content
	 */
	token& operator=(std::string content) {
		*this = token(content);
		return *this;
	}

	/** Get the content of this token.
	 * @return the content as a string
	 */
	operator const std::string&() const {
		return _content;
	}

	bool operator==(const token&) const = default;
	auto operator<=>(const token&) const = default;
};

inline const std::string& to_string(const token& tk) {
	return tk;
}

inline std::ostream& operator<<(std::ostream& out, const token& tk) {
	out << to_string(tk);
	return out;
}

}

#endif
