// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <memory>
#include <vector>
#include <fstream>

#include "hellmouth/net/http/body.h"
#include "hellmouth/net/http/file_handler.h"

namespace hellmouth::net::http {

static inline std::vector<std::string> split(const std::string& path) {
	std::vector<std::string> segments;
	std::string::size_type i = 0;
	std::string::size_type j = path.find('/');
	while (j != std::string::npos) {
		segments.push_back(path.substr(i, j - i));
		i = j + 1;
		j = path.find('/', i);
	}
	segments.push_back(path.substr(i));
	return segments;
}

response file_handler::handle(request& req) {
	if (req.method != "GET") {
		return response(405);
	}

	std::vector<std::string> segments = split(req.target.path);

	// Separate filename component (which may be empty).
	std::string filename = segments.back();
	segments.pop_back();

	// Normalise remaining path:
	// - Segments are processed from left to right.
	// - Empty segments, and segments containing a single or double dot,
	//   are removed.
	// - Segments containing a double dot additionally cause the
	//   preceding segment to be removed, if there is one.
	for (std::string::size_type i = 0; i < segments.size(); ++i) {
		if (segments[i] == "") {
			segments.erase(segments.begin() + i);
		} else if (segments[i] == ".") {
			segments.erase(segments.begin() + i);
		} else if (segments[i] == "..") {
			if (i == 0) {
				segments.erase(segments.begin() + i);
			} else {
				segments.erase(
					segments.begin() + (i - 1),
					segments.begin() + (i + 1));
			}
		}
	}

	// Append segments to base to form pathname of directory.
	std::filesystem::path dirname = base;
	for (const auto& segment : segments) {
		dirname /= segment;
	}

	// Append either the filename, or if empty the default filename,
	// to form the complete pathname.
	std::filesystem::path pathname = dirname;
	if (!filename.empty()) {
		pathname /= filename;
	} else {
		pathname /= default_filename;
	}

	if (is_regular_file(pathname)) {
		auto content = std::make_unique<std::ifstream>(pathname);
		if (content->bad()) {
			return http::response(404);
		}
		response resp(200);
		resp.body = http::body(std::move(content));
		return resp;
	} else if (is_directory(pathname)) {
		net::uri location = req.target;
		location.path += "/";
		response resp(301);
		resp.headers.push_back("Location", location);
		return resp;
	} else {
		return response(404);
	}
};

}
