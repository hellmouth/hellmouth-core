// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_HEADER
#define HELLMOUTH_NET_HTTP_HEADER

#include <string>
#include <ostream>

#include "hellmouth/net/http/header_name.h"

namespace hellmouth::net::http {

/** A class to represent an HTTP header. */
struct header {
	/** The name of this HTTP header. */
	http::header_name name;

	/** The value of this HTTP header. */
	std::string value;
public:
	/** Construct HTTP header.
	 * @param name the header name
	 * @param value the header value
	 */
	header(http::header_name name, std::string value):
		name(std::move(name)),
		value(std::move(value)) {}
};

std::ostream& operator<<(std::ostream& out, const header& hdr);

}

#endif
