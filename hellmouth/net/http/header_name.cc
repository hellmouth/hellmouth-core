// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <algorithm>

#include "hellmouth/net/http/header_name.h"

namespace hellmouth::net::http {

header_name::header_name(http::token name):
	_content(std::move(name)),
	_lower(_content) {

	std::transform(_lower.begin(), _lower.end(), _lower.begin(),
		[](unsigned char c){ return std::tolower(c); });
}

}
