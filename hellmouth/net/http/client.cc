// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <sys/socket.h>
#include <arpa/inet.h>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/os/fdstream.h"
#include "hellmouth/net/authority.h"
#include "hellmouth/net/getaddrinfo.h"
#include "hellmouth/net/http/request.h"
#include "hellmouth/net/http/response.h"
#include "hellmouth/net/http/client.h"

namespace hellmouth::net::http {

response client::handle(request& req) {
	if (!req.target.scheme || *req.target.scheme != "http") {
		throw std::invalid_argument("scheme component in URL is not 'http'");
	}
	if (!req.target.authority) {
		throw std::runtime_error("missing authority component in URL");
	}
	net::authority aty(*req.target.authority);

	uint16_t port = 80;
	if (aty.port) {
		port = *aty.port;
	}

	const auto addresses = net::getaddrinfo().stream()
		.resolve(aty.host, std::to_string(port));
	const auto& ainfo = addresses.front();
	os::socket_descriptor fd(ainfo.ai_family, ainfo.ai_socktype, ainfo.ai_protocol);
	fd.connect(ainfo.ai_addr, ainfo.ai_addrlen);

	auto conn = std::make_unique<os::fdstream>(fd);
	*conn << req;
	conn->flush();

	response resp(std::move(conn));
	return resp;
}

}
