// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_HEADER_LIST
#define HELLMOUTH_NET_HTTP_HEADER_LIST

#include <optional>
#include <vector>
#include <map>
#include <string>
#include <istream>
#include <ostream>

#include "hellmouth/net/http/header_name.h"
#include "hellmouth/net/http/header.h"

namespace hellmouth::net::http {

/** A class to represent a list of HTTP headers.
 * Comparisons between header names are case-insensitive.
 *
 * Unless otherwise specified, this class preserves:
 *
 * - the case of header names;
 * - the order of headers; and
 * - separation of headers with matching names.
 *
 * However, be aware that this is a higher level of fidelity than that
 * promised by the HTTP protocol, and it is inadvisable for clients or
 * servers to rely on any of the above unless compelled by external
 * constraints. It is safe to rely upon:
 *
 * - the order of headers with the matching names (or of their content if
 *   they have been concatenated); and
 * - separation of Set-Cookie headers.
 */
class header_list {
private:
	/** The headers, in transmission order. */
	std::vector<http::header> _headers;
public:
	/** Get an iterator for the start of this list.
 	 * @return the resulting iterator
	 */
	auto begin() const {
		return _headers.begin();
	}

	/** Get an iterator for the end of this list.
 	 * @return the resulting iterator
	 */
	auto end() const {
		return _headers.end();
	}

	/** Test whether this header list is empty.
	 * @return true if empty, otherwise false
	 */
	bool empty() const {
		return _headers.empty();
	}

	/** Append a header to this list.
	 * @param name the name of the header to be appended
	 * @param value the value of the header to be appended
	 */
	void push_back(http::header_name name, std::string value);

	/** Test whether this list contains any headers matching a given name.
	 * @param name the header name to be matched
	 * @return true if one or more matching header names, otherwise false
	 */
	bool contains(http::header_name name) const;

	/** Get the value of a single matching header.
	 * It is an error if the number of matching headers is not exactly
	 * equal to one.
	 *
	 * @param name the header name to be matched
	 * @return the content of the matching header
	 */
	std::string at(http::header_name name) const;

	/** Get the combined content of all headers matching a given name.
	 * If there are multiple matching headers, they are concatenated in
	 * transmission order separated by a comma followed by a space.
	 *
	 * @param name the header name to be matched
	 * @return the combined content
	 */
	std::optional<std::string> get(http::header_name name) const;
};

std::istream& operator>>(std::istream& in, header_list& hdrs);
std::ostream& operator<<(std::ostream& out, const header_list& hdrs);

}

#endif
