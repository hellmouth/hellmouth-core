// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/net/http/header_list.h"

namespace hellmouth::net::http {

/** Test whether a character is optional white space (OWS).
 * This function can also be used to test for LWS characters
 * following the initial CRLF.
 *
 * @param ch the character to be tested
 * @return true if OWS, otherwise false
 */
static inline bool isows(int ch) {
	return ch == ' ' || ch == '\t';
}

void header_list::push_back(http::header_name name, std::string value) {
	_headers.push_back(header(name, value));
}

bool header_list::contains(http::header_name name) const {
	for (const auto& hdr: _headers) {
		if (hdr.name == name) {
			return true;
		}
	}
	return false;
}

std::string header_list::at(http::header_name name) const {
	std::string value;
	size_t count = 0;
	for (const auto& hdr: _headers) {
		if (hdr.name == name) {
			value = hdr.value;
			count += 1;
		}
	}
	if (count == 0) {
		throw std::out_of_range("header not found");
	}
	if (count > 1) {
		throw std::out_of_range("multiple matching headers");
	}
	return value;
}

std::optional<std::string> header_list::get(http::header_name name) const {
	std::optional<std::string> value;
	for (const auto& hdr: _headers) {
		if (hdr.name == name) {
			if (value) {
				value->append(", ");
				value->append(hdr.value);
			} else {
				value = hdr.value;
			}
		}
	}
	return value;
}

std::istream& operator>>(std::istream& in, header_list& hdrs) {
	while (true) {
		// Read the first line of a header.
		// Line ending should be CRLF, but tolerate LF.
		std::string hdrline;
		std::getline(in, hdrline);
		if (hdrline.ends_with('\r')) {
			hdrline.pop_back();
		}

		// An empty line terminates the header list.
		if (hdrline.empty()) {
			break;
		}

		// Strip any trailing OWS.
		auto f = hdrline.find_last_not_of("\t ");
		if (f != std::string::npos) {
			hdrline.resize(f + 1);
		}

		// A colon separating the name from the value is always required,
		// and it must be in the first line (not a continuation line).
		auto f1 = hdrline.find(':');
		if (f1 == std::string::npos) {
			throw std::invalid_argument(
				"expected colon in HTTP header");
		}
		http::token name = hdrline.substr(0, f1);

		// Read and concatenate continuation lines.
		while (isows(in.peek())) {
			// Read continuation line.
			// Line ending should be CRLF, but tolerate LF.
			std::string contline;
			std::getline(in, contline);
			if (contline.ends_with('\r')) {
				contline.pop_back();
			}

			// Strip leading and trailing OWS from continuation line.
			// If the remainder is non-empty, append to existing
			// header value with single space as separator.
			auto f2 = contline.find_first_not_of("\t ");
			auto f3 = contline.find_last_not_of("\t ");
			if (f2 != std::string::npos) {
				hdrline.push_back(' ');
				hdrline += contline.substr(f2, f3 - f2 + 1);
			}
		}

		// Strip leading OWS from full header value.
		auto f4 = hdrline.find_first_not_of("\t ", f1 + 1);
		if (f4 == std::string::npos) {
			f4 = hdrline.length();
		}
		std::string value = hdrline.substr(f4);
		hdrs.push_back(name, value);
	}
	return in;
}

std::ostream& operator<<(std::ostream& out, const header_list& hdrs) {
	for (const auto& hdr: hdrs) {
		out << hdr.name << ": " << hdr.value << "\r\n";
	}
	return out;
}

}
