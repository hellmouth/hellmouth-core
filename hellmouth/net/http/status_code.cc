// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>
#include <format>
#include <utility>

#include "hellmouth/net/http/status_code.h"

namespace hellmouth::net::http {

status_code::status_code(unsigned int code):
	_code(code) {

	if (_code < 100 ||_code > 599) {
		throw std::domain_error("invalid HTTP status code");
	}
}

status_code::status_code(std::string content) {
	static const std::regex status_code_pattern(R"([0-9]{3})");
	if (!std::regex_match(content, status_code_pattern)) {
		throw std::invalid_argument("invalid HTTP status code");
	}

	// Narrowing conversion, but if the code matches the regex
	// then it must be in the range 000-999.
	_code = std::stoul(content);

	if (_code < 100 ||_code > 599) {
		throw std::domain_error("invalid HTTP status code");
	}
}

status_code::operator std::string() const {
	return std::format("{:03}", _code);
}

std::string make_reason_phrase(status_code status) {
	switch (status) {
	// Informational
	case 100: return "Continue";
	case 101: return "Switching Protocols";
	// Successful
	case 200: return "OK";
	case 201: return "Created";
	case 202: return "Accepted";
	case 203: return "Non-Authoritative Information";
	case 204: return "No Content";
	case 205: return "Reset Content";
	case 206: return "Partial Content";
	// Redirection
	case 300: return "Multiple Choices";
	case 301: return "Moved Permanently";
	case 302: return "Found";
	case 303: return "See Other";
	case 304: return "Not Modified";
	case 305: return "Use Proxy";
	case 307: return "Temporary Redirect";
	case 308: return "Permanent Redirect";
	// Client Error
	case 400: return "Bad Request";
	case 401: return "Unauthorized";
	case 402: return "Payment Required";
	case 403: return "Forbidden";
	case 404: return "Not Found";
	case 405: return "Method Not Allowed";
	case 406: return "Not Acceptable";
	case 407: return "Proxy Authentication Required";
	case 408: return "Request Timeout";
	case 409: return "Conflict";
	case 410: return "Gone";
	case 411: return "Length Required";
	case 412: return "Precondition Failed";
	case 413: return "Content Too Large";
	case 414: return "URI Too Long";
	case 415: return "Unsupported Media Type";
	case 416: return "Range Not Satisfiable";
	case 417: return "Expectation Failed";
	case 421: return "Misdirected Request";
	case 422: return "Unprocessable Content";
	case 426: return "Upgrade Required";
	// Server Error
	case 500: return "Internal Server Error";
	case 501: return "Not Implemented";
	case 502: return "Bad Gateway";
	case 503: return "Service Unavailable";
	case 504: return "Gateway Timeout";
	case 505: return "HTTP Version Not Supported";
	// Reason phrases are optional, so if a meaningful value is
	// unavailable then the next best option is to return nothing.
	default:
		return "";
	}
}

}
