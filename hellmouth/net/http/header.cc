// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <exception>

#include "hellmouth/net/http/header.h"

namespace hellmouth::net::http {

/** Test whether a character is optional white space (OWS).
 * This function can also be used to test for LWS characters
 * following the initial CRLF.
 *
 * @param ch the character to be tested
 * @return true if OWS, otherwise false
 */
static inline bool isows(int ch) {
	return ch == ' ' || ch == '\t';
}

#if 0
class header::parser {
public:
	std::string name;
	std::string value;
	parser(std::istream& in);
};

header::parser::parser(std::istream& in) {
	// Read first line of header.
	// Line ending should be CRLF, but tolerate LF.
	std::string hdrline;
	std::getline(in, hdrline);
	if (hdrline.ends_with('\r')) {
		hdrline.pop_back();
	}

	// An empty line terminates the header list.
	if (hdrline.empty()) {
		throw end_of_headers();
	}

	// Strip any trailing OWS.
	auto f = hdrline.find_last_not_of("\t ");
	if (f != std::string::npos) {
		hdrline.resize(f + 1);
	}

	// A colon separating the name from the value is always required,
	// and it must be in the first line (not a continuation line).
	auto f1 = hdrline.find(':');
	if (f1 == std::string::npos) {
		throw std::invalid_argument(
			"expected colon in HTTP header");
	}
	name = hdrline.substr(0, f1);

	// Read and concatenate continuation lines.
	while (isows(in.peek())) {
		// Read continuation line.
		// Line ending should be CRLF, but tolerate LF.
		std::string contline;
		std::getline(in, contline);
		if (contline.ends_with('\r')) {
			contline.pop_back();
		}

		// Strip leading and trailing OWS from continuation line.
		// If the remainder is non-empty, append to existing
		// header value with single space as separator.
		auto f2 = contline.find_first_not_of("\t ");
		auto f3 = contline.find_last_not_of("\t ");
		if (f2 != std::string::npos) {
			hdrline.push_back(' ');
			hdrline += contline.substr(f2, f3 - f2 + 1);
		}
	}

	// Strip leading OWS from full header value.
	auto f4 = hdrline.find_first_not_of("\t ", f1 + 1);
	if (f4 != std::string::npos) {
		value = hdrline.substr(f4);
	}
}

header::header(const parser& parsed):
	_name(parsed.name),
	_value(parsed.value) {}

header::header(std::istream& in):
	header(parser(in)) {}
#endif

std::ostream& operator<<(std::ostream& out, const header& hdr) {
	out << hdr.name << ": " << hdr.value << "\r\n";
	return out;
}

}
