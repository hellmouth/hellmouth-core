// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_CLIENT
#define HELLMOUTH_NET_HTTP_CLIENT

#include "hellmouth/net/http/handler.h"

namespace hellmouth::net::http {

/** A class to represent an HTTP client.
 * Note that this class is limited to making unencrypted HTTP requests
 * over TCP, corresponding to the 'http' URL scheme. It is not able to
 * handle other schemes such as 'https' or 'file'.
 *
 * [TODO]: only tries the first address returned by getaddrinfo, with no
 * retries to that or any other address.
*/
class client:
	public handler {
public:
	/** Perform HTTP transaction.
	 * @param req the HTTP request
	 * @return the corresponding HTTP response
	 */
	response handle(request& req) override;
};

}

#endif
