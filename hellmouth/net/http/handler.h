// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_HANDLER
#define HELLMOUTH_NET_HTTP_HANDLER

#include "hellmouth/net/http/request.h"
#include "hellmouth/net/http/response.h"

namespace hellmouth::net::http {

/** An abstract base class for handling HTTP requests. */
class handler {
public:
	virtual ~handler() = default;

	/** Handle an HTTP request.
	 * @param req the request to be handled
	 */
	virtual response handle(request& req) = 0;
};

}

#endif
