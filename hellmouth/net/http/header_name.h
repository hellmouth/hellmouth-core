// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_HEADER_NAME
#define HELLMOUTH_NET_HTTP_HEADER_NAME

#include "hellmouth/net/http/token.h"

namespace hellmouth::net::http {

/** A class to represent the name of an HTTP header. */
class header_name {
	/** The raw content of this header name. */
	http::token _content;

	/** The normalised content of this header name. */
	std::string _lower;
public:
	/** Construct HTTP header name from token.
	 * @param name the name as a token
	 */
	header_name(http::token content);

	/** Construct HTTP header name from standard string.
	 * @param content the required content
	 */
	header_name(std::string content):
		header_name(http::token(content)) {}

	/** Construct HTTP header name from C-style character string.
	 * @param content the required content
	 */
	header_name(const char* content):
		header_name(http::token(content)) {}

	operator const http::token&() const {
		return _content;
	}

	operator const std::string&() const {
		return _content;
	}

	bool operator==(const header_name& rhs) const {
		return _lower == rhs._lower;
	}

	auto operator<=>(const header_name& rhs) const {
		return _lower <=> rhs._lower;
	}
};

inline const std::string& to_string(const header_name& name) {
	return name;
}

inline std::ostream& operator<<(std::ostream& out, const header_name& name) {
	out << to_string(name);
	return out;
}

}

#endif
