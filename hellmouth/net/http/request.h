// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTP_REQUEST
#define HELLMOUTH_NET_HTTP_REQUEST

#include <string>
#include <istream>
#include <ostream>

#include "hellmouth/net/uri.h"
#include "hellmouth/net/http/token.h"
#include "hellmouth/net/http/version.h"
#include "hellmouth/net/http/header_list.h"
#include "hellmouth/net/http/body.h"
#include "hellmouth/net/http/response.h"

namespace hellmouth::net::http {

/** A data structure to represent an HTTP request. */
struct request {
public:
	/** The HTTP request method. */
	http::token method;

	/** The target URL to be requested. */
	net::uri target;

	/** The HTTP version. */
	http::version version;

	/** The HTTP headers. */
	http::header_list headers;

	/** The request body. */
	http::body body;
private:
	/** A class for parsing HTTP requests. */
	class parser;

	/** Construct HTTP request from parser instance. */
	request(const parser& parsed);
public:
	/** Construct HTTP request from components.
	 * The header list is initialised with a Host field derived from
	 * the specified target, and a Close field which requests that the
	 * connection be closed when the body has been set. Either or both
	 * of these may be overridden before the request is sent.
	 *
	 * @param method the HTTP request method
	 * @param target the target URL to be requested
	 * @param version the HTTP version to be used
	 */
	request(std::string method, std::string target,
		std::string version = "HTTP/1.1");

	/** Construct HTTP request from input stream.
	 * This constructor takes ownership of the input stream, in order
	 * to deliver the body.
	 *
	 * @param in the stream
	 */
	request(std::unique_ptr<std::istream> in);
};

std::ostream& operator<<(std::ostream& out, request& req);

/** Handle an HTTP request.
 * Although the input to this function must be framed as an HTTP request
 * and the output as an HTTP response, the URL scheme need not be one which
 * results in use of the HTTP network protocol: analogous behaviour is
 * sufficient provided that a suitable handler is available.
 *
 * For example, a handler for the "file:" URL scheme is not required to
 * serialise the request for transmission, and may instead translate it
 * directly into POSIX filesystem calls. However, it must then synthesise a
 * suitable HTTP response code to represent the outcome of the operation,
 * and return the file content as an HTTP response body where appropriate.
 *
 * @param req the request to be performed
 * @return the resulting response
 */
response fetch(request& req);

}

#endif
