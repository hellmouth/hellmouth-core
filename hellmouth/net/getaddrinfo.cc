// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/net/getaddrinfo.h"

namespace hellmouth::net {

getaddrinfo::error::error(int errcode):
        std::runtime_error(gai_strerror(errcode)) {}

getaddrinfo::result::~result() {
	::freeaddrinfo(_head);
}

getaddrinfo::getaddrinfo() {
	_hints.ai_family = AF_UNSPEC;
	_hints.ai_flags = AI_V4MAPPED | AI_ADDRCONFIG;
}

getaddrinfo::result getaddrinfo::resolve(
	const std::string& hostname, const std::string& servname) const {

	struct addrinfo* head = 0;
	int errcode = ::getaddrinfo(hostname.c_str(), servname.c_str(), &_hints, &head);
	if (errcode != 0) {
		throw error(errcode);
	}
	return result(head);
}

}
