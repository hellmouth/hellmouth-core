// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstring>
#include <stdexcept>
#include <sstream>

#include <arpa/inet.h>

#include "hellmouth/net/inet6/address.h"
#include "hellmouth/net/inet4/address.h"

namespace hellmouth::net::inet6 {

address::address(octet::decoder& dec) {
    dec.read(&_address, sizeof(_address));
}

address::address(std::string_view addrstr) {
	uint16_t fields[8];
	size_t idx = 0;
	size_t expand_idx = 8;
	bool colon_expected = false;
	bool inet4_expected = false;

	// Loop repeats until:
	//
	// - No more hex fields are available, or
	// - A full set of 8 hex fields have been parsed, or
	// - The next field is a decimal field.
	//
	while (!addrstr.empty() && idx < 8) {
		// A double-colon allowed either at the start of the address,
		// or anywhere that a colon-separator is expected, provided
		// that one has not been seen before.
		if (addrstr.starts_with("::")) {
			if (expand_idx != 8) {
				throw std::invalid_argument(
					"multiply-compressed IPv6 address");
			}
			expand_idx = idx;
			fields[idx++] = 0;
			addrstr.remove_prefix(2);
			colon_expected = false;

			// A double colon can also end the address.
			if (addrstr.empty()) {
				break;
			}
		}

		if (colon_expected) {
			if (!addrstr.starts_with(':')) {
				throw std::invalid_argument(
					"colon expected in IPv6 address");
			}
			addrstr.remove_prefix(1);
		}

		size_t i = 0;
		while (!addrstr.empty() && isxdigit(addrstr[i])) {
			i += 1;
		}

		bool inet4_permitted = (i == 6) || (i < 6 && expand_idx != 8);
		if (inet4_permitted && !addrstr.empty() && addrstr[i] == '.') {
			inet4_expected = true;
			break;
		}
		if (i == 0) {
			throw std::invalid_argument(
				"hex digit expected in IPv6 address");
		}
		if (i > 4) {
			throw std::invalid_argument(
				"too many hex digits in IPv6 address");
		}
		uint16_t field = 0;
		for (size_t j = 0; j != i; ++j) {
			field *= 16;
			char ch = addrstr[j];
			if (isdigit(ch)) {
				field += ch - '0';
			} else {
				field += toupper(ch) - 'A' + 10;
			}
		}
		addrstr.remove_prefix(i);

		fields[idx++] = field;
		colon_expected = true;
	}

	if (inet4_expected) {
		inet4::address in4addr(addrstr);
		const void* in4_raw = in4addr;
		uint16_t in4_fields[2];
		memcpy(in4_fields, in4_raw, sizeof(in4_fields));
		fields[idx++] = ntohs(in4_fields[0]);
		fields[idx++] = ntohs(in4_fields[1]);
		addrstr.remove_prefix(addrstr.length());
	}

	if (idx < 8 && expand_idx != 8) {
		size_t extra = 8 - idx;
		for (size_t i = 8; i != expand_idx + extra; --i) {
			fields[i - 1] = fields[i - 1 - extra];
		}
		for (size_t i = expand_idx + extra; i != expand_idx; --i) {
			fields[i] = 0;
		}
		idx += extra;
	}

	if (idx < 8) {
		throw std::invalid_argument(
			"unexpected end of IPv6 address");
	}

	if (!addrstr.empty()) {
		throw std::invalid_argument(
			"expected end of IPv6 address");
	}

	for (size_t i = 0; i != 8; ++i) {
		_address.s6_addr[i * 2] = fields[i] >> 8;
		_address.s6_addr[i * 2 + 1] = fields[i];
	}
}

address::operator std::string() const {
	// Convert from octets to 16-bit fields.
	uint16_t fields[8];
	for (size_t i = 0; i != 8; ++i) {
		size_t j = i * 2;
		uint16_t field = _address.s6_addr[j];
		field <<= 8;
		field |= _address.s6_addr[j + 1];
		fields[i] = field;
	}

	// Find longest run of zeros, choosing the first
	// in the event of a tie.
	size_t best_run_idx = 8;
	size_t best_run_len = 0;
	size_t run_len = 0;
	for (size_t i = 0; i != 8; ++i) {
		if (fields[i] == 0) {
			run_len += 1;
			if (run_len > best_run_len) {
				best_run_idx = i - (run_len - 1);
				best_run_len = run_len;
			}
		} else {
			run_len = 0;
		}
	}

	// RFC 4291 allows a single field to be compressed,
	// but RFC 5952 does not.
	if (best_run_len == 1) {
		best_run_idx = 8;
		best_run_len = 0;
	}

	// Detect addresses with an embedded IPv4 suffix.
	// Current behaviour is to detect IPv4-compatible, IPv4-mapped
	// and IPv4-translated addresses (but taking care to exclude the
	// unspecified and loopback addresses).
	size_t end_hex_idx = 8;
	if (best_run_idx == 0) {
		switch (best_run_len) {
		case 4:
			if (fields[4] == 0xffff && fields[5] == 0) {
				// IPv4-translated address
				end_hex_idx = 6;
			}
			break;
		case 5:
			if (fields[5] == 0xffff) {
				// IPv4-mapped address
				end_hex_idx = 6;
			}
			break;
		case 6:
			// IPv4-compatible address
			end_hex_idx = 6;
			break;
		case 7:
			if (fields[7] != 1) {
				// IPv4-compatible address
				end_hex_idx = 6;
			}
			break;
		}
	}

	// Convert to hexadecimal.
	std::ostringstream out;
	out << std::hex;
	bool suppress_colon = true;
	for (size_t i = 0; i != end_hex_idx; ++i) {
		if (i == best_run_idx) {
			out << "::";
			suppress_colon = true;
			i += best_run_len - 1;
		} else {
			if (suppress_colon) {
				suppress_colon = false;
			} else {
				out << ':';
			}
			out << fields[i];
		}
	}

	// Append IPv4 suffix if required.
	if (end_hex_idx < 8) {
		if (!suppress_colon) {
			out << ':';
		}
		out << std::dec;
		bool suppress_dot = true;
		for (size_t i = 12; i != 16; ++i) {
			if (suppress_dot) {
				suppress_dot = false;
			} else {
				out << '.';
			}
			out << (unsigned int)_address.s6_addr[i];
		}
	}

	// Return human-readable result.
	return out.str();
}

address& address::operator&=(const address& that) {
	for (size_t i = 0; i != 16; ++i) {
		_address.s6_addr[i] &= that._address.s6_addr[i];
	}
	return *this;
}

address& address::operator|=(const address& that) {
	for (size_t i = 0; i != 16; ++i) {
		_address.s6_addr[i] |= that._address.s6_addr[i];
	}
	return *this;
}

address address::operator~() const {
	address result;
	for (size_t i = 0; i != 16; ++i) {
		result._address.s6_addr[i] = ~_address.s6_addr[i];
	}
	return result;
}

address address::make_netmask(unsigned int prefixlen) {
	if (prefixlen > 128) {
		throw std::invalid_argument("invalid IPv6 prefix length");
	}
	unsigned int div = prefixlen / 8;
	unsigned int mod = prefixlen % 8;

	in6_addr in6addr = {0};
	memset(in6addr.s6_addr, 0xff, div);
	if (div < 8) {
		in6addr.s6_addr[div] = -1 << (8 - mod);
	}
	return address(in6addr);
}

}
