// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_INET6_ADDRESS
#define HELLMOUTH_NET_INET6_ADDRESS

#include <cstring>
#include <string_view>
#include <string>
#include <ostream>

#include <netinet/in.h>

#include "hellmouth/octet/string_view.h"
#include "hellmouth/octet/decoder.h"

namespace hellmouth::net::inet6 {

/** A class to represent an IPv6 address. */
class address {
private:
	/** The address, in network byte order. */
	struct in6_addr _address;
public:
	/** Construct address with default value of ::. */
	address():
		_address{0} {}

	/** Construct address from a struct in6_addr.
	 * @param addr the address
	 */
	explicit address(const struct in6_addr& addr):
		_address(addr) {}

	/** Construct address from an array of uint8_t.
	 * @param addr the address
	 */
	explicit address(const uint8_t* addr) {
		memcpy(_address.s6_addr, addr, sizeof(_address.s6_addr));
	}

	/** Decode address from raw data.
	 * @dec a decoder to supply the content
	 */
	explicit address(octet::decoder& dec);

	/** Construct address from character sequence.
	 * @param addrstr the address, using any of the permitted
	 *  human-readable forms specified by RFC 4291.
	 */
	explicit address(std::string_view addrstr);

	/** Get the underlying address.
	 * @return the address as an in6_addr
	 */
	operator const struct in6_addr&() const {
		return _address;
	}

	/** Get the underlying address.
	 * @return the address as an array of uint8_t.
	 */
	operator const uint8_t*() const {
		return _address.s6_addr;
	}

	/** Get the address as raw data.
	 * @return the address as raw data
	 */
	operator const void*() const {
		return &_address;
	}

	/** Get the address as raw data.
	 * @return the raw data
	 */
	operator octet::string_view() const {
		auto ptr = reinterpret_cast<const unsigned char*>(&_address);
		return octet::string_view(ptr, sizeof(_address));
	}

	/** Get the address as a character string.
	 * This function uses the canonical human-readable form specified
	 * by RFC 5952. Current behaviour is to use an embedded IPv4 suffix
	 * when the address is IPv4-compatible, -mapped or -translated.
	 *
	 * @return the address as a string
	 */
	operator std::string() const;

	/** Perform bitwise-AND with another address.
	 * @param that the other address
	 * @return a reference to this
	 */
	address& operator&=(const address& that);

	/** Perform bitwise-OR with another address.
	 * @param that the other address
	 * @return a reference to this
	 */
	address& operator|=(const address& that);

	/** Calculate bitwise-NOT of this address.
	 * @return the resulting address
	 */
	address operator~() const;

	/** Calculate bitwise-AND with another address.
	 * @param that the other address
	 * @return the resulting address
	 */
	address operator&(const address& that) const {
		return address(*this) &= that;
	}

	/** Calculate bitwise-OR with another address.
	 * @param that the other address
	 * @return the resulting address
	 */
	address operator|(const address& that) const {
		return address(*this) |= that;
	}

	/** The width of this address type, in bits. */
	static const unsigned int width = 128;

	/** Make a netmask for this address type.
	 * @param prefixlen the number of leading ones in the netmask
	 * @return the resulting netmask
	 */
	static address make_netmask(unsigned int prefixlen);
};

/** Test whether an address is the unspecified address.
 * @param addr the address to be tested
 * @return true if unspecified, otherwise false
 */
inline bool is_unspecified(address addr) {
	static in6_addr unspecified = {0};
	const in6_addr& in6addr = addr;
	return memcmp(in6addr.s6_addr, unspecified.s6_addr, 16) == 0;
}

inline std::string to_string(const address& addr) {
	return addr;
}

inline std::ostream& operator<<(std::ostream& out, const address& addr) {
	out << std::string(addr);
	return out;
}

}

#endif
