// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_INET6_NETBLOCK
#define HELLMOUTH_NET_INET6_NETBLOCK

#include "hellmouth/net/netblock.h"
#include "hellmouth/net/inet6/address.h"

namespace hellmouth::net::inet6 {

typedef basic_netblock<address> netblock;

}

#endif
