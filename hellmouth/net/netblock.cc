#// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <arpa/inet.h>

namespace hellmouth::net {

unsigned int parse_prefix_length(std::string_view& netblockstr,
	unsigned int maxlen) {

	auto f = netblockstr.find('/');
	if (f == std::string::npos) {
		return maxlen;
	}

	std::string_view prefixstr = netblockstr.substr(f + 1);
	netblockstr.remove_suffix(netblockstr.length() - f);
	if (prefixstr.length() == 0) {
		throw std::invalid_argument("invalid prefix length");
	}

	unsigned int prefixlen = 0;
	for (char ch: prefixstr) {
		if (!isdigit(ch)) {
			throw std::invalid_argument("invalid prefix length");
		}
		prefixlen *= 10;
		prefixlen += ch - '0';
		if (prefixlen > maxlen) {
			throw std::invalid_argument("invalid prefix length");
		}
	}
	return prefixlen;
}

}
