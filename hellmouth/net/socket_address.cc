// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstring>
#include <stdexcept>
#include <typeinfo>

#include "hellmouth/net/socket_address.h"

namespace hellmouth::net {

void socket_address::_ensure(sa_family_t family) const {
	if (this->family() != family) {
		throw std::bad_cast();
	}
}

socket_address::socket_address(const sockaddr* addr, socklen_t addrlen) {
	// If the address family is recognised then perform a type-aware copy.
	// This will be safe provided that the address family matches the
	// actual type of the structure referred to, and allows it to be
	// read back in that form without violation of strict aliasing rules.
	// Otherwise, it is copied as opaque data.
	switch (addr->sa_family) {
	case AF_UNIX:
		_unix = *reinterpret_cast<const sockaddr_un*>(addr);
		_addrlen = sizeof(sockaddr_un);
		break;
	case AF_INET:
		_inet = *reinterpret_cast<const sockaddr_in*>(addr);
		_addrlen = sizeof(sockaddr_in);
		break;
	case AF_INET6:
		_inet6 = *reinterpret_cast<const sockaddr_in6*>(addr);
		_addrlen = sizeof(sockaddr_in6);
		break;
	default:
		if (addrlen > sizeof(sockaddr_storage)) {
			throw std::invalid_argument(
				"length of sockaddr exceeds sockaddr_storage");
		}
		memcpy(&_storage, addr, addrlen);
		_addrlen = addrlen;
		break;
	}
}

}
