// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/net/getnameinfo.h"

namespace hellmouth::net {

getnameinfo::error::error(int errcode):
	std::runtime_error(gai_strerror(errcode)) {}

getnameinfo::result getnameinfo::resolve(
	const struct sockaddr* addr, socklen_t addrlen) const {

	char hostname[256];
	char servname[64];
	int errcode = ::getnameinfo(addr, addrlen,
		hostname, sizeof(hostname),
		servname, sizeof(servname),
		_flags);
	if (errcode != 0) {
		throw error(errcode);
	}
	return result(hostname, servname);
}

}
