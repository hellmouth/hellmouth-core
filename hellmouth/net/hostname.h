// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HOSTNAME
#define HELLMOUTH_NET_HOSTNAME

#include <string>
#include <string_view>

namespace hellmouth::net {

/** A class to represent a validated hostname.
 * A hostname is valid if it matches the following syntax:
 *
 * hostname ::= label ("." label)*
 * label ::= [A-Za-z0-9]([A-Za-z0-9-]*[A-Za-z0-9])?
 *
 * (Based on the syntax for hostnames specified in RFC 952, as modified
 * by RFC 1123.)
 */
class hostname {
private:
	/** A class for validating hostnames. */
	class validator;

	/** The hostname, as a string. */
	std::string _value;
public:
	/** Construct and validate hostname.
	 * @param value the hostname
	 */
	explicit hostname(const std::string_view& value);

	/** Convert this hostname to a string.
	 * @return the hostname as a string
	 */
	operator const std::string&() const {
		return _value;
	}

	/** Convert this hostname to a C-style string.
	 * @return the hostname as a C-style string
	 */
	const char* c_str() const {
		return _value.c_str();
	}

	bool operator==(const hostname&) const = default;
	auto operator<=>(const hostname&) const = default;
};

inline const std::string& to_string(const hostname& name) {
	return name;
}

}

#endif
