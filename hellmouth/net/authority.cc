// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>
#include <utility>
#include <sstream>

#include "hellmouth/net/authority.h"

namespace hellmouth::net {

authority::authority(std::string_view content) {
	auto f1 = content.find('@');
	if (f1 != content.npos) {
		userinfo = content.substr(0, f1);
		content.remove_prefix(f1 + 1);
	}

	auto f2 = content.rfind(':');
	if (f2 != content.npos) {
		unsigned long acc = 0;
		for (char ch : content.substr(f2 + 1)) {
			if (!isdigit(ch)) {
				throw std::invalid_argument("invalid port number");
			}
			acc *= 10;
			acc += ch - '0';
			if (!std::in_range<uint16_t>(acc)) {
				throw std::invalid_argument("port number out of range");
			}
		}

		// An empty port number normalises to an absent one,
		// so only set the port number if non-empty.
		if (f2 != content.length() - 1) {
			port = acc;
		}

		content.remove_suffix(content.length() - f2);
	}

	host = content;
}

authority::operator std::string() const {
	std::ostringstream out;
	if (userinfo) {
		out << *userinfo << '@';
	}
	out << host;
	if (port) {
		out << ':' << *port;
	}
	return out.str();
}

}
