// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/net/getaddrinfo.h"
#include "hellmouth/net/ssl/client.h"
#include "hellmouth/net/ssl/sslstream.h"

namespace hellmouth::net::ssl {

client::client(context& ctx, const std::string& host, uint16_t port):
	ssl::session(ctx),
	_fd(AF_INET, SOCK_STREAM, 0) {

	auto addresses = net::getaddrinfo().resolve(host, std::to_string(port));
	auto& ainfo = *addresses.begin();
	_fd.connect(ainfo.ai_addr, ainfo.ai_addrlen);
	set_hostname(host);
	attach(_fd);
	connect();
}

}
