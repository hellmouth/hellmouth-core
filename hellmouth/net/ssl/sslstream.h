// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SSL_SSLSTREAM
#define HELLMOUTH_NET_SSL_SSLSTREAM

#include <iostream>

#include "hellmouth/net/ssl/sslbuf.h"

namespace hellmouth::net::ssl {

/** A stream class for accessing an OpenSSL client. */
template<class CharT, class Traits = std::char_traits<CharT>>
class basic_sslstream:
	public std::basic_iostream<CharT, Traits> {
private:
	using std::basic_iostream<CharT, Traits>::rdbuf;

	/** The underlying OpenSSL client. */
	basic_sslbuf<CharT, Traits> _sslbuf;
public:
	/** Construct stream from OpenSSL client (moved).
	 * @param client the underlying OpenSSL client
	 */
	basic_sslstream(ssl::client&& client):
		_sslbuf(std::move(client)) {

		rdbuf(&_sslbuf);
	}
};

/** A stream class for accessing a file or socket descriptor. */
typedef basic_sslstream<char> sslstream;

}

#endif
