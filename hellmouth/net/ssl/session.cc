// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <limits>
#include <utility>

#include <openssl/err.h>
#include <openssl/x509v3.h>

#include "hellmouth/net/ssl/error.h"
#include "hellmouth/net/ssl/session.h"

namespace hellmouth::net::ssl {

session::session(context& ctx):
	_ssl(SSL_new(ctx)) {

	if (!_ssl) {
		throw ssl::error();
	}
}

session::session(const session& that):
	_ssl(that._ssl) {

	SSL_up_ref(_ssl);
}

session::session(session&& that):
	_ssl(that._ssl) {

	that._ssl = 0;
}

session::~session() {
	if (_ssl) {
		SSL_free(_ssl);
	}
}

session& session::operator=(const session& that) {
	if (this != &that) {
		SSL_free(_ssl);
		_ssl = that._ssl;
		SSL_up_ref(_ssl);
	}
	return *this;
}

session& session::operator=(session&& that) {
	if (this != &that) {
		_ssl = that._ssl;
		that._ssl = 0;
	}
	return *this;
}

uint64_t session::set_options(uint64_t options) {
	return SSL_set_options(_ssl, options);
}

uint64_t session::clear_options(uint64_t options) {
	return SSL_clear_options(_ssl, options);
}

uint64_t session::get_options() const {
	return SSL_get_options(_ssl);
}

void session::set_verify(int mode,
	int (*callback)(int, X509_STORE_CTX *)) {

	SSL_set_verify(_ssl, mode, callback);
}

void session::set_hostname(const std::string& hostname) {
	SSL_set_tlsext_host_name(_ssl, hostname.c_str());

	X509_VERIFY_PARAM *vpm = SSL_get0_param(_ssl);
	X509_VERIFY_PARAM_set1_host(vpm,
		hostname.data(), hostname.length());
	X509_VERIFY_PARAM_set_purpose(vpm, X509_PURPOSE_SSL_SERVER);
}

void session::attach(const os::file_descriptor& fd) {
	ERR_clear_error();
	if (SSL_set_fd(_ssl, fd) != 1) {
		throw ssl::error();
	}
}

void session::connect() {
	ERR_clear_error();
	if (SSL_connect(_ssl) != 1) {
		throw ssl::error();
	}
}

size_t session::read(void* buf, size_t nbyte) {
	if (!std::in_range<int>(nbyte)) {
		nbyte = std::numeric_limits<int>::max();
	}
	while (true) {
		ERR_clear_error();
		int ret = SSL_read(_ssl, buf, nbyte);
		if (ret > 0) {
			return ret;
		} else {
			throw ssl::error();
		}
	}
}

size_t session::write(const void* buf, size_t nbyte) {
	if (!std::in_range<int>(nbyte)) {
		nbyte = std::numeric_limits<int>::max();
	}
	while (true) {
		ERR_clear_error();
		int ret = SSL_write(_ssl, buf, nbyte);
		if (ret > 0) {
			return ret;
		} else {
			throw ssl::error();
		}
	}
}

}
