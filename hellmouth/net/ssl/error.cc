// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <string>

#include <openssl/err.h>

#include "hellmouth/net/ssl/error.h"

namespace hellmouth::net::ssl {

static std::string make_error_string() {
	char buffer[120];
	ERR_error_string(ERR_get_error(), buffer);
	return buffer;
}

error::error():
	std::runtime_error(make_error_string()) {}

}
