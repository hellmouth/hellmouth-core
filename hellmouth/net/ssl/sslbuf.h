// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SSL_SSLBUF
#define HELLMOUTH_NET_SSL_SSLBUF

#include <string>
#include <streambuf>

#include "hellmouth/net/ssl/client.h"

namespace hellmouth::net::ssl {

/** A class for providing the buffers for a basic_sslbuf. */
template<class CharT>
struct basic_sslbuf_buffers {
	/** The inbound buffer. */
	CharT _gbuf[0x1000];

	/** The outbound buffer. */
	CharT _pbuf[0x1000];
};

/** A stream buffer template class for reading from and writing to
 * an OpenSSL client. */
template<class CharT, class Traits = std::char_traits<CharT>>
class basic_sslbuf:
	public basic_sslbuf_buffers<CharT>,
	public std::basic_streambuf<CharT, Traits> {
private:
	using basic_sslbuf_buffers<CharT>::_gbuf;
	using basic_sslbuf_buffers<CharT>::_pbuf;
	using std::basic_streambuf<CharT, Traits>::pbase;
	using std::basic_streambuf<CharT, Traits>::pptr;
	using std::basic_streambuf<CharT, Traits>::epptr;
	using std::basic_streambuf<CharT, Traits>::pbump;
	using std::basic_streambuf<CharT, Traits>::setp;
	using std::basic_streambuf<CharT, Traits>::setg;

	/** The underlying OpenSSL client. */
	ssl::client _client;
public:
	/** Construct buffer from OpenSSL client (moved).
	 * @param client the underlying OpenSSL client
	 */
	basic_sslbuf(ssl::client&& client):
		_client(std::move(client)) {}
protected:
	Traits::int_type underflow() override {
		size_t count = _client.read(_gbuf, sizeof(_gbuf));
		setg(_gbuf, _gbuf, _gbuf + count);
		return (count != 0) ? _gbuf[0] : Traits::eof();
	}

	Traits::int_type overflow(Traits::int_type ch) override {
		if (sync() == -1) {
			return Traits::eof();
		}
		setp(_pbuf, _pbuf + sizeof(_pbuf));
		if (ch != Traits::eof()) {
			*pptr() = ch;
			pbump(1);
		}
		return 0;
	}

	int sync() override {
		if (!pptr()) {
			return 0;
		}
		try {
			size_t nbyte = pptr() - pbase();
			CharT* ptr = pbase();
			while (nbyte != 0) {
				size_t count = _client.write(ptr, nbyte);
				ptr += count;
				nbyte -= count;
			}
			setp(pbase(), epptr());
			return 0;
		} catch (std::system_error&) {
			setp(0, 0);
			return -1;
		}
	}
};

/** A stream buffer class for reading and writing chars using
 * an OpenSSL client. */
typedef basic_sslbuf<char> sslbuf;

}

#endif
