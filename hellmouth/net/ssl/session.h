// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SSL_SESSION
#define HELLMOUTH_NET_SSL_SESSION

#include <openssl/ssl.h>

#include "hellmouth/os/file_descriptor.h"
#include "hellmouth/net/ssl/context.h"

namespace hellmouth::net::ssl {

/** A class to represent an OpenSSL session handle.
 * This class is copyable, but note that it is the handle which is copied
 * as opposed to the underlying session state.
 */
class session {
private:
	/** The session handle. */
	SSL* _ssl;
public:
	/** Construct new OpenSSL session.
	 * @param ctx the required context
	 */
	session(context& ctx);

	/** Copy-construct session handle.
	 * @param that the handle to be copied
	 */
	session(const session& that);

	/** Move-construct session handle.
	 * @param that the handle to be moved
	 */
	session(session&& that);

	/** Destroy session handle. */
	~session();

	/** Copy-assign session handle.
	 * @param that the handle to be copied
	 */
	session& operator=(const session& that);

	/** Move-assign session handle.
	 * @param that the handle to be moved
	 */
	session& operator=(session&& that);

	/** Get the underlying OpenSLL session handle.
	 * @return the handle
	 */
	operator SSL*() {
		return _ssl;
	}

	/** Set options.
	 * Any previously-set options will remain set
	 *
	 * @param options the options to be set
	 * @return the new options bit-mask
	 */
	uint64_t set_options(uint64_t options);

	/** Clear options.
	 * @param options the options to be cleared
	 * @return the new options bit-mask
	 */
	uint64_t clear_options(uint64_t options);

	/** Get options.
	 * @return the options bit-mask
	 */
	uint64_t get_options() const;

	/** Set verification mode.
	 * @param mode the verification mode
	 * @param callback the verification callback function
	 */
	void set_verify(int mode,
		int (*callback)(int, X509_STORE_CTX *) = 0);

	/** Set the hostname to be requested.
	 * @param hostname the required hostname
	 */
	void set_hostname(const std::string& hostname);

	/** Attach to a file descriptor.
	 * @param fd the file descriptor to be attached
	 */
	void attach(const os::file_descriptor& fd);

	/** Connect to server. */
	void connect();

	/** Read data from connection.
	 * @param buf the buffer to read into
	 * @param nbyte the maximum number of bytes to be read
	 * @return the number of bytes actually read
	 */
	size_t read(void* buf, size_t nbyte);

	/** Write data to connection.
	 * @param buf the buffer to write from
	 * @param nbyte the maximum number of bytes to be written
	 * @return the number of bytes actually written
	 */
	size_t write(const void* buf, size_t nbyte);
};

}

#endif
