// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SSL_ERROR
#define HELLMOUTH_NET_SSL_ERROR

#include <stdexcept>

namespace hellmouth::net::ssl {

/** A class to represent an OpenSSL error. */
class error:
	public std::runtime_error {
public:
	error();
};

}

#endif
