// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SSL_CLIENT
#define HELLMOUTH_NET_SSL_CLIENT

#include <cstdint>

#include <openssl/ssl.h>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/net/ssl/context.h"
#include "hellmouth/net/ssl/session.h"

namespace hellmouth::net::ssl {

/** A class to represent an SSL client.
 * Although the constituent components of this class are individually
 * copyable, doing so would cause the underlying file descriptor number
 * to change and the link between them to be broken. For that reason,
 * this class is moveable but not copyable.
 */
class client:
	public session {
private:
	/** The socket descriptor with which to connect. */
	os::socket_descriptor _fd;
public:
	/** Construct new SSL client.
	 * @param ctx the context on which this session is based
	 * @param host the remote hostname
	 * @param port the remote port number
	 */
	client(context& ctx, const std::string& host, uint16_t port = 443);

	client(const client&) = delete;
	client(client&&) = default;

	client& operator=(const client&) = delete;
	client& operator=(client&&) = default;
};

}

#endif
