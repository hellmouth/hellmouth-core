// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SSL_CONTEXT
#define HELLMOUTH_NET_SSL_CONTEXT

#include <openssl/ssl.h>

namespace hellmouth::net::ssl {

/** A class to represent an OpenSSL context handle.
 * This class is copyable, but note that it is the handle which is copied
 * as opposed to the underlying context.
 */
class context {
private:
	/** The context handle. */
	SSL_CTX* _ctx;
public:
	/** Construct new OpenSSL context.
	 * @param method an optional connection method,
	 *  or null for TLS_method.
	 */
	context(const SSL_METHOD* method = 0);

	/** Copy-construct context handle.
	 * @param that the handle to be copied
	 */
	context(const context& that);

	/** Move-construct context handle.
	 * @param that the handle to be moved
	 */
	context(context&& that);

	/** Destroy context handle. */
	~context();

	/** Copy-assign context handle.
	 * @param that the handle to be copied
	 */
	context& operator=(const context& that);

	/** Move-assign context handle.
	 * @param that the handle to be moved
	 */
	context& operator=(context&& that);

	/** Get the underlying OpenSLL context handle.
	 * @return the handle
	 */
	operator SSL_CTX*() {
		return _ctx;
	}

	/** Set options.
	 * Any previously-set options will remain set
	 *
	 * @param options the options to be set
	 * @return the new options bit-mask
	 */
	uint64_t set_options(uint64_t options);

	/** Clear options.
	 * @param options the options to be cleared
	 * @return the new options bit-mask
	 */
	uint64_t clear_options(uint64_t options);

	/** Get options.
	 * @return the options bit-mask
	 */
	uint64_t get_options() const;
};

}

#endif
