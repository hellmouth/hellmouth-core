// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/net/ssl/error.h"
#include "hellmouth/net/ssl/context.h"

namespace hellmouth::net::ssl {

context::context(const SSL_METHOD* method) {
	if (!method) {
		method = TLS_method();
	}
	_ctx = SSL_CTX_new(method);
	if (!_ctx) {
		throw ssl::error();
	}
}

context::context(const context& that):
	_ctx(that._ctx) {

	SSL_CTX_up_ref(_ctx);
}

context::context(context&& that):
	_ctx(that._ctx) {

	that._ctx = 0;
}

context::~context() {
	if (_ctx) {
		SSL_CTX_free(_ctx);
	}
}

context& context::operator=(const context& that) {
	if (this != &that) {
		SSL_CTX_free(_ctx);
		_ctx = that._ctx;
		SSL_CTX_up_ref(_ctx);
	}
	return *this;
}

context& context::operator=(context&& that) {
	if (this != &that) {
		_ctx = that._ctx;
		that._ctx = 0;
	}
	return *this;
}

uint64_t context::set_options(uint64_t options) {
	return SSL_CTX_set_options(_ctx, options);
}

uint64_t context::clear_options(uint64_t options) {
	return SSL_CTX_clear_options(_ctx, options);
}

uint64_t context::get_options() const {
	return SSL_CTX_get_options(_ctx);
}

}
