// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>
#include <regex>
#include <sstream>

#include "hellmouth/net/uri.h"

namespace hellmouth::net {

uri::uri(const std::string& content) {
	static const std::regex uri_pattern(
		R"((([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?)");

	std::string content2(content);

	std::smatch match;
	if (!std::regex_match(content, match, uri_pattern)) {
		// This line ought to be unreachable, since uri_pattern
		// ought to match any input, however we check before
		// inspecting the result out of an abundance of caution.
		throw std::invalid_argument("invalid URI");
	}

	if (match[2].matched) {
		scheme = match[2];
	}
	if (match[4].matched) {
		authority = match[4];
	}
	path = match[5];
	if (match[7].matched) {
		query = match[7];
	}
	if (match[9].matched) {
		fragment = match[9];
	}
}

uri::operator std::string() const {
	std::ostringstream result;
	if (scheme) {
		result << *scheme << ':';
	}
	if (authority) {
		result << "//" << *authority;
	}
	result << path;
	if (query) {
		result << '?' << *query;
	}
	if (fragment) {
		result << '#' << *fragment;
	}
	return result.str();
}

}
