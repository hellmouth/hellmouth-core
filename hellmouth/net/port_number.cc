// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>

#include "hellmouth/net/port_number.h"

namespace hellmouth::net {

port_number::port_number(uint16_t value):
	_value(value) {

	if (_value == 0) {
		throw std::invalid_argument("invalid");
	}
}

port_number::port_number(const std::string_view& value) {
	uint32_t acc = 0;
	for (char ch: value) {
		if (!isdigit(ch)) {
			throw std::invalid_argument("invalid port number");
		}
		acc *= 10;
		acc += ch - '0';
		if (acc > 65535) {
			throw std::invalid_argument("invalid port number");
		}
	}
	if (acc == 0) {
		// In addition to testing for an explicit numeric value of zero,
		// this also handles the case of an empty string.
		throw std::invalid_argument("invalid port number");
	}
	_value = acc;
}

}
