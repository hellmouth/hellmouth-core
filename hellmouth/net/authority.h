// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_AUTHORITY
#define HELLMOUTH_NET_AUTHORITY

#include <cstdint>
#include <optional>
#include <string>
#include <string_view>

namespace hellmouth::net {

/** A structure to represent the authority component of a URI. */
struct authority {
	/** An optional userinfo component. */
	std::optional<std::string> userinfo;

	/** The host component (unvalidated). */
	std::string host;

	/** An optional port number, in host byte order. */
	std::optional<uint16_t> port;

	/** Convert this authority component to a string.
	 * @return the authority as a string
	 */
	operator std::string() const;

	/** Parse authority component from character string.
	 * @param content the string to be parsed
	 */
	authority(std::string_view content);
};

inline std::string to_string(const net::authority& authority) {
	return authority;
}

inline std::ostream& operator<<(std::ostream& out,
	const net::authority& authority) {

	out << to_string(authority);
	return out;
}

}

#endif
