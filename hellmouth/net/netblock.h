// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_NETBLOCK
#define HELLMOUTH_NET_NETBLOCK

#include <string_view>

namespace hellmouth::net {

/** A function for parsing a prefix length.
 * If the supplied string contains a slash character then it and
 * anything following is removed, and interpreted as a prefix length.
 * If there is no slash then the specified default is used, and no
 * characters are removed.
 *
 * @param netblockstr the netblock, using CIDR notation
 * @param maxlen the maximum prefix length for the relevant address type,
 *  which is also the default if not specified in netblockstr
 */
unsigned int parse_prefix_length(std::string_view& netblockstr,
		unsigned int maxlen);

/** A template class to represent a CIDR netblock.
 * When strict validation is disabled, this can also be used to represent
 * the address of a network interface.
 */
template<class T>
class basic_netblock {
private:
	/** The prefix length, in bits. */
	unsigned int _prefixlen;

	/** The address component. */
	T _address;
public:
	/** Construct netblock from string.
	 * @param netblockstr the netblock, using CIDR notation
	 * @param strict true to validate that the address is compatible with
	 *  the prefix, otherwise false
	 */
	basic_netblock(std::string_view netblockstr, bool strict = true):
		_prefixlen(parse_prefix_length(netblockstr, T::width)),
		_address(netblockstr) {

		if (strict) {
			if (!is_unspecified(_address & ~netmask())) {
				throw std::invalid_argument("invalid netblock");
			}
		}
	}

	/** Get full address.
	 * This may include bits corresponding to both the network and host.
	 * @return the full address
	 */
	const T& address() const {
		return _address;
	}

	/** Get netmask.
	 * @return the netmask
	 */
	T netmask() const {
		return T::make_netmask(_prefixlen);
	}

	/** Get network address.
	 * This may include bits corresponding to the network only.
	 * @return the network address
	 */
	T network() const {
		return _address & netmask();
	}

	/** Get broadcast address.
	 * @return the broadcast address
	 */
	T broadcast() const {
		return _address | ~netmask();
	}

	/** Get the prefix length.
	 * @return the prefix length, in bits
	 */
	unsigned int prefix_length() const {
		return _prefixlen;
	}
};

}

#endif
