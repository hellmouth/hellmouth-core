// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_SOCKET_ADDRESS
#define HELLMOUTH_NET_SOCKET_ADDRESS

#include <concepts>

#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>

namespace hellmouth::net {

template<class T>
concept sockaddr_type = (
	std::same_as<T, sockaddr_un> ||
	std::same_as<T, sockaddr_in> ||
	std::same_as<T, sockaddr_in6> ||
	std::same_as<T, sockaddr_storage>);

/** A class to represent any type of socket address.
 * Currently this class is able to:
 *
 * - Hold any type of socket address in opaque form, and
 * - Safely interpret the socket address if the address family
 *   is AF_UNIX, AF_INET or AF_INET6.
 */
class socket_address {
private:
	/** The underlying socket address.
	 * All members of this union must share a common initial sequence
	 * composed of an sa_family_t.
	 */
	union {
		sockaddr _any;
		sockaddr_un _unix;
		sockaddr_in _inet;
		sockaddr_in6 _inet6;
		sockaddr_storage _storage;
	};

	/** The length of this socket address. */
	socklen_t _addrlen;

	/** Ensure that the address family has a given value.
	 * @param family the required address family
	 */
	void _ensure(sa_family_t family) const;
public:
	/** Construct empty socket address.
	 * The address family is set to zero (AF_UNSPEC).
	 */
	socket_address():
		_any{0},
		_addrlen(sizeof(sockaddr)) {}

	/** Construct a socket address from underlying value.
	 * @param addr the address
	 * @param addrlen the address length in bytes
	 */
	socket_address(const sockaddr* addr, socklen_t addrlen);

	/** Construct a socket address from underlying value.
	 * @param addr the address
	 */
	template<sockaddr_type T>
	socket_address(const T& addr):
		socket_address(reinterpret_cast<const sockaddr*>(&addr), sizeof(T)) {}

	/** Get the address family of this socket address.
	 * @return the address family
	 */
	sa_family_t family() const {
		return _any.sa_family;
	}

	/** Get the length of this socket address.
	 * @return the length, in bytes
	 */
	socklen_t length() const {
		return _addrlen;
	}

	/** Change the length of the underlying content.
	 * @param length the new length
	 */
	void resize(socklen_t length) {
		_addrlen = length;
	}

	/** Get the capacity of this socket address.
	 * This is equal to sizeof(sockaddr_storage).
	 */
	socklen_t capacity() const {
		return sizeof(_storage);
	}

	/** Get the address as a sockaddr.
	 * This function may be called regardless of the address family.
	 *
	 * @return the sockaddr
	 */
	operator sockaddr&() {
		return _any;
	}

	/** Get the address as a sockaddr.
	 * This function may be called regardless of the address family.
	 *
	 * @return the sockaddr
	 */
	operator const sockaddr&() const {
		return _any;
	}

	/** Get the address as a sockaddr_un.
	 * It is an error to call this function unless the address family is
	 * equal to AF_UNIX (or equivalently AF_LOCAL).
	 *
	 * @return the sockaddr_un
	 */
	operator sockaddr_un&() {
		_ensure(AF_UNIX);
		return _unix;
	}

	/** Get the address as a sockaddr_un.
	 * It is an error to call this function unless the address family is
	 * equal to AF_UNIX (or equivalently AF_LOCAL).
	 *
	 * @return the sockaddr_un
	 */
	operator const sockaddr_un&() const {
		_ensure(AF_UNIX);
		return _unix;
	}

	/** Get the address as a sockaddr_in.
	 * It is an error to call this function unless the address family is
	 * equal to AF_INET.
	 *
	 * @return the sockaddr_in
	 */
	operator sockaddr_in&() {
		_ensure(AF_INET);
		return _inet;
	}

	/** Get the address as a sockaddr_in.
	 * It is an error to call this function unless the address family is
	 * equal to AF_INET.
	 *
	 * @return the sockaddr_in
	 */
	operator const sockaddr_in&() const {
		_ensure(AF_INET);
		return _inet;
	}

	/** Get the address as a sockaddr_in6.
	 * It is an error to call this function unless the address family is
	 * equal to AF_INET.
	 *
	 * @return the sockaddr_in6
	 */
	operator sockaddr_in6&() {
		_ensure(AF_INET6);
		return _inet6;
	}

	/** Get the address as a sockaddr_in6.
	 * It is an error to call this function unless the address family is
	 * equal to AF_INET.
	 *
	 * @return the sockaddr_in6
	 */
	operator const sockaddr_in6&() const {
		_ensure(AF_INET6);
		return _inet6;
	}

};

}

#endif
