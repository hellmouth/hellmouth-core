// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_HTTPS_CLIENT
#define HELLMOUTH_NET_HTTPS_CLIENT

#include "hellmouth/net/http/handler.h"

namespace hellmouth::net::https {

/** A class to represent an HTTPS (HTTP+TLS) client.
 * [TODO]: only tries the first address returned by getaddrinfo, with no
 * retries to that or any other address.
*/
class client:
	public http::handler {
public:
	/** Perform HTTPS transaction.
	 * @param req the HTTP request
	 * @return the corresponding HTTP response
	 */
	http::response handle(http::request& req) override;
};

}

#endif
