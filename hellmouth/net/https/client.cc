// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <sys/socket.h>
#include <arpa/inet.h>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/os/fdstream.h"
#include "hellmouth/net/authority.h"
#include "hellmouth/net/getaddrinfo.h"
#include "hellmouth/net/http/request.h"
#include "hellmouth/net/http/response.h"
#include "hellmouth/net/ssl/context.h"
#include "hellmouth/net/ssl/client.h"
#include "hellmouth/net/ssl/sslstream.h"
#include "hellmouth/net/https/client.h"

namespace hellmouth::net::https {

http::response client::handle(http::request& req) {
	if (!req.target.scheme || *req.target.scheme != "https") {
		throw std::invalid_argument("scheme component in URL is not 'https'");
	}
	if (!req.target.authority) {
		throw std::runtime_error("missing authority component in URL");
	}
	net::authority aty(*req.target.authority);

	uint16_t port = 443;
	if (aty.port) {
		port = *aty.port;
	}

	ssl::context ctx;
	ssl::client sslclient(ctx, aty.host, port);
	auto conn = std::make_unique<ssl::sslstream>(std::move(sslclient));
	*conn << req;
	conn->flush();

	http::response resp(std::move(conn));
	return resp;
}

}
