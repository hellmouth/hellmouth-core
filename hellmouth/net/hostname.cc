// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>

#include "hellmouth/net/hostname.h"

namespace hellmouth::net {

/** A class for validating hostnames. */
class hostname::validator {
private:
	/** The allowed states of this validator. */
	enum state_type {
		/** Awaiting the first character of a label. */
		state_before_label,
		/** Following an alphanumeric character within a label. */
		state_after_alnum,
		/** Following a hypen within a label. */
		state_after_hyphen
	};

	/** The current state of this validator. */
	state_type _state = state_before_label;
public:
	/** Validate character.
	 * @param ch the character to be validated
	 */
	void operator()(char ch);

	/** Finalise validation. */
	void operator()();
};

void hostname::validator::operator()(char ch) {
	switch (_state) {
	case state_before_label:
		if (isalnum(ch)) {
			_state = state_after_alnum;
		} else {
			throw std::invalid_argument(
				"invalid character in hostname");
		}
		break;
	case state_after_alnum:
		if (ch == '.') {
			_state = state_before_label;
		} else if (ch == '-') {
			_state = state_after_hyphen;
		} else if (isalnum(ch)) {
			// No action
		} else {
			throw std::invalid_argument(
				"invalid character in hostname");
		}
		break;
	case state_after_hyphen:
		if (ch == '-') {
			// No action
		} else if (isalnum(ch)) {
			_state = state_after_alnum;
		} else {
			throw std::invalid_argument(
				"invalid character in hostname");
		}
		break;
	}
}

void hostname::validator::operator()() {
	(*this)('.');
}

hostname::hostname(const std::string_view& value):
	_value(value) {

	validator validate;
	for (char ch: _value) {
		validate(ch);
	}
	validate();
}

}
