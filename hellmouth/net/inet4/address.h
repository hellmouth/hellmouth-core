// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_INET4_ADDRESS
#define HELLMOUTH_NET_INET4_ADDRESS

#include <string_view>
#include <string>
#include <ostream>

#include <netinet/in.h>

#include "hellmouth/octet/string_view.h"
#include "hellmouth/octet/decoder.h"

namespace hellmouth::net::inet4 {

/** A class to represent an IPv4 address. */
class address {
private:
	/** The address, in network byte order. */
	struct in_addr _address;
public:
	/** Construct address with default value of 0.0.0.0. */
	address():
		_address{0} {}

	/** Construct address from a struct in_addr.
	 * @param addr the address
	 */
	explicit address(const struct in_addr& addr):
		_address(addr) {}

	/** Construct address from an in_addr_t.
	 * @param addr the address
	 */
	explicit address(const in_addr_t& addr) {
		_address.s_addr = addr;
	}

	/** Decode address from raw data.
	 * @dec a decoder to supply the content
	 */
	explicit address(octet::decoder& dec);

	/** Construct address from character sequence.
	 * @param addrstr the address, in dotted quad format
	 */
	explicit address(std::string_view addrstr);

	/** Get the underlying address.
	 * @return the address as an in_addr, in network byte order
	 */
	operator const struct in_addr&() const {
		return _address;
	}

	/** Get the underlying address.
	 * @return the address as an in_addr_t, in network byte order
	 */
	operator const in_addr_t&() const {
		return _address.s_addr;
	}

	/** Get the address as raw data.
	 * @return the address, in network byte order
	 */
	operator const void*() const {
		return &_address;
	}

	/** Get the address as raw data.
	 * @return the address, in network byte order
	 */
	operator octet::string_view() const {
		auto ptr = reinterpret_cast<const unsigned char*>(&_address);
		return octet::string_view(ptr, sizeof(_address));
	}

	/** Get the address as a character string.
	 * @return the address as a string, in dotted quad format
	 */
	operator std::string() const;

	/** Perform bitwise-AND with another address.
	 * @param that the other address
	 * @return a reference to this
	 */
	address& operator&=(const address& that) {
		_address.s_addr &= that._address.s_addr;
		return *this;
	}

	/** Perform bitwise-OR with another address.
	 * @param that the other address
	 * @return a reference to this
	 */
	address& operator|=(const address& that) {
		_address.s_addr |= that._address.s_addr;
		return *this;
	}

	/** Calculate bitwise-NOT of this address.
	 * @return the resulting address
	 */
	address operator~() const {
		return address(~_address.s_addr);
	}

	/** Calculate bitwise-AND with another address.
	 * @param that the other address
	 * @return the resulting address
	 */
	address operator&(const address& that) const {
		return address(*this) &= that;
	}

	/** Calculate bitwise-OR with another address.
	 * @param that the other address
	 * @return the resulting address
	 */
	address operator|(const address& that) const {
		return address(*this) |= that;
	}

	/** The width of this address type, in bits. */
	static const unsigned int width = 32;

	/** Make a netmask for this address type.
	 * @param prefixlen the number of leading ones in the netmask
	 * @return the resulting netmask
	 */
	static address make_netmask(unsigned int prefixlen);
};

/** Test whether an address is the unspecified address.
 * @param addr the address to be tested
 * @return true if unspecified, otherwise false
 */
inline bool is_unspecified(address addr) {
	in_addr inaddr = addr;
	return inaddr.s_addr == 0;
}

inline std::string to_string(const address& addr) {
	return addr;
}

inline std::ostream& operator<<(std::ostream& out, const address& addr) {
	out << std::string(addr);
	return out;
}

}

#endif
