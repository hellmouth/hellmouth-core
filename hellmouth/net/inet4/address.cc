// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstring>
#include <stdexcept>

#include <arpa/inet.h>

#include "hellmouth/net/inet4/address.h"

namespace hellmouth::net::inet4 {

address::address(octet::decoder& dec) {
	dec.read(&_address, sizeof(_address));
}

address::address(std::string_view addrstr):
	_address{0} {

	uint32_t s_addr = 0;
	for (unsigned int i = 0; i != 4; ++i) {
		if (i != 0) {
			if (!addrstr.starts_with('.')) {
				throw std::invalid_argument(
					"dot expected in IPv4 address");
			}
			addrstr.remove_prefix(1);
		}

		if (addrstr.empty() || !isdigit(addrstr.front())) {
			throw std::invalid_argument(
				"digit expected in IPv4 address");
		}

		unsigned int acc = 0;
		while (!addrstr.empty() && isdigit(addrstr.front())) {
			acc *= 10;
			acc += addrstr.front() - '0';
			if (acc > 255) {
				throw std::invalid_argument(
					"octet value out of range in IPv4 address");
			}
			addrstr.remove_prefix(1);
		}

		s_addr <<= 8;
		s_addr += acc;
	}

	if (!addrstr.empty()) {
		throw std::invalid_argument(
			"unexpected characters at end of IPv4 address");
	}
	_address.s_addr = htonl(s_addr);
}

address::operator std::string() const {
	char buffer[16];
	uint32_t s_addr = ntohl(_address.s_addr);
	sprintf(buffer, "%d.%d.%d.%d",
		uint8_t(s_addr >> 24),
		uint8_t(s_addr >> 16),
		uint8_t(s_addr >> 8),
		uint8_t(s_addr));
	return buffer;
}

address address::make_netmask(unsigned int prefixlen) {
	in_addr_t mask = -(1 << (32 - prefixlen));
	return address(htonl(mask));
}

}
