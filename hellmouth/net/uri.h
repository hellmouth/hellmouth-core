// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_NET_URI
#define HELLMOUTH_NET_URI

#include <optional>
#include <string>

namespace hellmouth::net {

/** A structure to represent a validated uniform resource identifier. */
struct uri {
	/** An optional scheme component. */
	std::optional<std::string> scheme;

	/** An optional authority component. */
	std::optional<std::string> authority;

	/** The path component. */
	std::string path;

	/** An optional query component. */
	std::optional<std::string> query;

	/** An optional fragment component. */
	std::optional<std::string> fragment;

	/** Construct empty URI. */
	uri() = default;

	/** Parse URI from character string.
	 * @param content the string to be parsed
	 */
	uri(const std::string& content);

	/** Convert this URI to a string.
	 * @return the URI as a string
	 */
	operator std::string() const;
};

inline std::string to_string(const net::uri& uri) {
	return uri;
}

inline std::ostream& operator<<(std::ostream& out, const net::uri& uri) {
	out << to_string(uri);
	return out;
}

}

#endif
