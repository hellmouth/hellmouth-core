// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/crypto/octet_buffer.h"

namespace hellmouth::crypto {

void parse_hex(std::string_view hexstr, octet_buffer_ref buf) {
	if (hexstr.length() != buf.size() * 2) {
		throw std::invalid_argument("hex value has wrong length");
	}

	size_t index = 0;
	unsigned int acc = 1;
	for (char ch: hexstr) {
		acc <<= 4;
		if (isdigit(ch)) {
			acc |= ch - '0';
		} else if (isupper(ch)) {
			acc |= ch - 'A' + 10;
		} else if (islower(ch)) {
			acc |= ch - 'a' + 10;
		} else {
			throw std::invalid_argument("invalid hex string");
		}
		if (acc & 0x100) {
			buf[index++] = acc & 0xff;
			acc = 1;
		}
	}
}

std::string to_string(const_octet_buffer_ref buf) {
	std::string result;
	result.reserve(2 * buf.size());
	for (unsigned char byte: buf) {
		char buffer[3];
		snprintf(buffer, sizeof(buffer), "%02x", byte);
		result.append(buffer);
	}
	return result;
}

}
