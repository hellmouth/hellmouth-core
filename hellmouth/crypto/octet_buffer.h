// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CRYPTO_OCTET_BUFFER
#define HELLMOUTH_CRYPTO_OCTET_BUFFER

#include <vector>
#include <span>
#include <string_view>
#include <string>

#include "hellmouth/crypto/secure_allocator.h"

namespace hellmouth::crypto {

typedef std::span<unsigned char> octet_buffer_ref;
typedef std::span<const unsigned char> const_octet_buffer_ref;

void parse_hex(std::string_view hexstr, octet_buffer_ref buf);

/** A class template for holding an opaque block of data. */
template<class Allocator = std::allocator<unsigned char>>
class basic_octet_buffer:
	public std::vector<unsigned char, Allocator> {
public:
	/** Construct basic octet buffer.
	 * @param size the required size, in octets
	 */
	basic_octet_buffer(size_t size):
		std::vector<unsigned char, Allocator>(size) {}

	/** Construct basic octet buffer and initialise from hexadecimal string
	 * The string may use upper case, lower case, or a mixture,
	 * however its length must be a multiple of two.
	 *
	 * @param hexstr a string containing the initial value in hex
	 * @param size the required size, in octets
	 */
	basic_octet_buffer(std::string_view hexstr, size_t size):
		basic_octet_buffer(size) {

		parse_hex(hexstr, octet_buffer_ref(*this));
	}

	operator std::basic_string_view<unsigned char>() const {
		return std::basic_string_view<unsigned char>(
			this->data(), this->size());
	}
};

/** Convert octet buffer to hexadecimal string.
 * @return the octet buffer as hex
 */
std::string to_string(const_octet_buffer_ref buf);

/** Convert octet buffer to hexadecimal string.
 * @return the octet buffer as hex
 */
template<class Allocator>
std::string to_string(const basic_octet_buffer<Allocator>& buf) {
	const_octet_buffer_ref ref(buf);
	return to_string(ref);
}

typedef basic_octet_buffer<std::allocator<unsigned char>> octet_buffer;
typedef basic_octet_buffer<secure_allocator<unsigned char>> secure_octet_buffer;

}

#endif
