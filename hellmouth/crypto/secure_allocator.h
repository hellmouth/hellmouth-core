// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CRYPTO_SECURE_ALLOCATOR
#define HELLMOUTH_CRYPTO_SECURE_ALLOCATOR

namespace hellmouth::crypto {

#include <new>

#include <sodium.h>

/** A class for allocating memory for sensitive data.
 * Current behaviour is to allocate memory for the data using the libsodium
 * function sodium_mallocarray, which amongst other precautions:
 *
 * - Attempts to lock the content into volatile memory.
 * - Zeros the content on destruction.
 *
 * Use of this class should be reserved for data which warrants a very high
 * level of protection, such as non-public encryption keys, since allocation
 * is expected to be considerably slower than std:allocator.
 */
template<class T>
struct secure_allocator {
	/** The type allocated by this allocator. */
	typedef T value_type;

	/** Construct secure allocator.
	 * No action required because allocator is stateless.
	 */
	secure_allocator() = default;

	/** Copy-construct secure allocator.
	 * No action required because allocator is stateless.
	 */
	template<class U>
	constexpr secure_allocator(const secure_allocator <U>&) noexcept {}

	/** Allocate memory.
	 * @param count the number of instances of type T to be allocated
	 */
	T* allocate(size_t count) {
		if (void* ptr = sodium_allocarray(count, sizeof(T))) {
			return static_cast<T*>(ptr);
		}
		throw std::bad_alloc();
	}

	/** Deallocate memory.
	 * @param ptr the memory to be deallocated
	 * @param count the number of instances of T to be deallocated
	 */
	void deallocate(T* ptr, size_t count) noexcept {
		sodium_free(ptr);
	}
};

/** Compare two secure allocators.
 * @return true, because secure allocator instances are stateless.
 */
template<class T, class U>
bool operator==(const secure_allocator<T>&, const secure_allocator<U>&) {
	return true;
}

/** Compare two secure allocators.
 * @return false, because secure allocator instances are stateless.
 */
template<class T, class U>
bool operator!=(const secure_allocator<T>&, const secure_allocator<U>&) {
	return false;
}

}

#endif
