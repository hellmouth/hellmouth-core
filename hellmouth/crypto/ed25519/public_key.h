// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CRYPTO_ED25519_PUBLIC_KEY
#define HELLMOUTH_CRYPTO_ED25519_PUBLIC_KEY

#include "hellmouth/crypto/octet_buffer.h"
#include "hellmouth/crypto/ed25519/signature.h"

namespace hellmouth::crypto::ed25519 {

/** A class to represent an Ed25519 public key. */
class public_key:
	public octet_buffer {
public:
	/** Construct empty public key. */
	public_key();

	/** Construct public key from hexadecimal string.
	 * @param hexstr the hexadecimal string
	 */
	public_key(const std::string& hexstr);

	/** Verify a digital signature.
	 * @param sig the signature to be verified
	 * @param data the message
	 * @param size the size of the message, in octets
	 */
	void verify(const signature& sig, const void* data, size_t size) const;
};

}

#endif
