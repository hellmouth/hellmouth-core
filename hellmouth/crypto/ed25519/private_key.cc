// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sodium.h>

#include "hellmouth/crypto/ed25519/private_key.h"

namespace hellmouth::crypto::ed25519 {

private_key::private_key():
	secure_octet_buffer(crypto_sign_SECRETKEYBYTES) {}

private_key::private_key(const std::string& hexstr):
	secure_octet_buffer(hexstr, crypto_sign_SECRETKEYBYTES) {}

signature private_key::sign(const void* data, size_t size) const {
	ed25519::signature sig;
	crypto_sign_detached(sig.data(), 0,
		static_cast<const unsigned char*>(data), size, this->data());
	return sig;
}

}
