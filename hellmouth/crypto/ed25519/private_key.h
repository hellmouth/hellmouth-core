// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CRYPTO_ED25519_PRIVATE_KEY
#define HELLMOUTH_CRYPTO_ED25519_PRIVATE_KEY

#include "hellmouth/crypto/octet_buffer.h"
#include "hellmouth/crypto/ed25519/signature.h"

namespace hellmouth::crypto::ed25519 {

/** A class to represent an Ed25519 private key. */
class private_key:
	public secure_octet_buffer {
public:
	/** Construct empty private key. */
	private_key();

	/** Construct private key from hexadecimal string.
	 * @param hexstr the hexadecimal string
	 */
	private_key(const std::string& hexstr);

	/** Sign a message using this key.
	 * @param data the message to be signed
	 * @param size the size of the message, in octets
	 */
	signature sign(const void* data, size_t size) const;
};

}

#endif
