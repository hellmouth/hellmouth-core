// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sodium.h>

#include "hellmouth/crypto/ed25519/key_pair.h"

namespace hellmouth::crypto::ed25519 {

key_pair::key_pair() {
	crypto_sign_keypair(_pubkey.data(), _privkey.data());
}

}
