// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CRYPTO_ED25519_SIGNATURE
#define HELLMOUTH_CRYPTO_ED25519_SIGNATURE

#include "hellmouth/crypto/octet_buffer.h"

namespace hellmouth::crypto::ed25519 {

/** A class to represent an Ed25519 digital signature. */
class signature:
	public octet_buffer {
public:
	/** Construct empty signature. */
	signature();

	/** Construct signature from hexadecimal string.
	 * @param hexstr the hexadecimal string
	 */
	signature(const std::string& hexstr);
};

}

#endif
