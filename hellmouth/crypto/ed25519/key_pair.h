// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CRYPTO_ED25519_KEY_PAIR
#define HELLMOUTH_CRYPTO_ED25519_KEY_PAIR

#include "hellmouth/crypto/ed25519/private_key.h"
#include "hellmouth/crypto/ed25519/public_key.h"

namespace hellmouth::crypto::ed25519 {

/** A class to represent a public-private key pair for Ed25519. */
class key_pair {
private:
	/** The private key. */
	ed25519::private_key _privkey;

	/** The public key. */
	ed25519::public_key _pubkey;
public:
	/** Construct new randomly-generated key pair. */
	key_pair();

	/** Get the private key.
	 * @return the private key
	 */
	const ed25519::private_key& private_key() const {
		return _privkey;
	}

	/** Get the public key.
	 * @return the public key
	 */
	const ed25519::public_key& public_key() const {
		return _pubkey;
	}
};

}

#endif
