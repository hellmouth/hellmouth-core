// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <sodium.h>

#include "hellmouth/crypto/ed25519/public_key.h"

namespace hellmouth::crypto::ed25519 {

public_key::public_key():
	octet_buffer(crypto_sign_PUBLICKEYBYTES) {}

public_key::public_key(const std::string& hexstr):
	octet_buffer(hexstr, crypto_sign_PUBLICKEYBYTES) {}

void public_key::verify(const signature& sig, const void* data, size_t size) const {
	if (crypto_sign_verify_detached(sig.data(),
		static_cast<const unsigned char*>(data), size, this->data()) != 0) {

		throw std::invalid_argument("digital signature could not be verified");
	}
}

}
