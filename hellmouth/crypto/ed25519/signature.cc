// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sodium.h>

#include "hellmouth/crypto/ed25519/signature.h"

namespace hellmouth::crypto::ed25519 {

signature::signature():
	octet_buffer(crypto_sign_BYTES) {}

signature::signature(const std::string& hexstr):
	octet_buffer(hexstr, crypto_sign_BYTES) {}

}
