// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_C2_SESSION
#define HELLMOUTH_C2_SESSION

#include <thread>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/c2/connection.h"
#include "hellmouth/c2/agent.h"

namespace hellmouth::c2 {

/** A class to represent a HELLMOUTH C2 server session.
 * [TODO]: there is currently no provision for a session to
 * be ended by the server, nor does it end immediately on
 * disconnection.
 */
class session {
private:
	/** The agent controlled by this server. */
	c2::agent* _agent;

	/** The underlying connection. */
	c2::connection _conn;

	/** The latest observed status revision. */
	uint64_t _revision = 0;

	/** A thread for receiving configuration requests. */
	std::jthread _receiver;

	/** A thread for sending status updates. */
	std::jthread _sender;

	/** Receive configuration requests from the connection. */
	void _receive();

	/** Send status updates to the connection. */
	void _send();
public:
	/** Make a session for an existing socket descriptor.
	 * @param sd the socket descriptor
	 */
	session(c2::agent& agent, os::socket_descriptor&& sd);
};

}

#endif
