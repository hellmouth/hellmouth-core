// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_C2_AGENT
#define HELLMOUTH_C2_AGENT

#include <mutex>
#include <condition_variable>
#include <atomic>

#include "hellmouth/data/any.h"

namespace hellmouth::c2 {

/** A mixin class to represent a HELLMOUTH C2 agent.
 * The agent is incorporated into the entity which is to be controlled
 * using the C2 protocol. It includes provision for:
 *
 * - configuring or reconfiguring the controlled entity; and
 * - reporting the status of the controlled entity.
 */
class agent {
private:
	/** A mutex to control access to _status. */
	mutable std::mutex _mutex;

	/** A condition variable to control access to _status. */
	mutable std::condition_variable _cv;

	/** The current status. */
	data::any _status;

	/** The revision number of the current status. */
	uint64_t _revision = 0;
public:
	/** Destroy this agent. */
	virtual ~agent() = default;

	/** Configure or reconfigure.
	 * The default behaviour is to take no action.
	 *
	 * @param config the new configuration
	 */
	virtual void configure(const data::any& config);

	/** Report new status.
	 * @param status the new status
	 */
	void report(const data::any& status);

	/** Watch for status changes.
	 * This function will block unless and until a status update
	 * is available with a revision number greater than the one
	 * supplied.
	 *
	 * @param revision the current revision number (updated)
	 * @return the updated status
	 */
	data::any watch(uint64_t& revision) const;
};

}

#endif
