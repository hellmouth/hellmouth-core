// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_C2_SERVER
#define HELLMOUTH_C2_SERVER

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/data/any.h"
#include "hellmouth/c2/agent.h"

namespace hellmouth::c2 {

/** A class for serving the HELLMOUTH C2 protocol. */
class server {
private:
	/** The agent controlled by this server. */
	c2::agent* _agent;

	/** A socket descriptor for listening for connections. */
	os::socket_descriptor _fd;
public:
	/** Construct C2 server.
	 * @param agent the agent to be controlled by this server
	 * @param pathname the pathname of the UNIX domain socket
	 */
	server(c2::agent& agent, const std::filesystem::path& pathname);

	/** Get the socket descriptor.
	 * @return the socket descriptor
	 */
	const os::socket_descriptor& fd() const {
		return _fd;
	}

	/** Handle a single request.
	 * @param request the decoded request
	 * @return the unencoded response
	 */
	data::any handle(const data::any& request);

	/** Accept and handle a single connection. */
	void accept();

	/** Run this server.
	 * This function does not return unless and until the server stops
	 * running for some reason.
	 */
	void run();
};

}

#endif
