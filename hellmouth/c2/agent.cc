// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/c2/agent.h"

namespace hellmouth::c2 {

void agent::configure(const data::any& config) {}

void agent::report(const data::any& status) {
	std::lock_guard lock(_mutex);
	_status = status;
	_revision += 1;
	_cv.notify_all();
}

data::any agent::watch(uint64_t& revision) const {
	std::unique_lock<std::mutex> lock(_mutex);
	_cv.wait(lock, [this,revision]{ return _revision > revision; });
	revision = _revision;
	return _status;
}

}
