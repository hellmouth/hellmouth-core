// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <ios>
#include <system_error>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "hellmouth/c2/session.h"
#include "hellmouth/c2/server.h"

namespace hellmouth::c2 {

server::server(c2::agent& agent, const std::filesystem::path& pathname):
	_agent(&agent),
	_fd(AF_UNIX, SOCK_STREAM, 0) {

	struct sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	auto npathname = pathname.native();
	if (npathname.length() >= sizeof(addr.sun_path)) {
		throw std::system_error(make_error_code(
			std::errc::filename_too_long));
	}
	npathname.copy(addr.sun_path, npathname.length());
	addr.sun_path[npathname.length()] = 0;
	unlink(pathname.c_str());
	_fd.bind(addr);
	_fd.listen();
}

void server::accept() {
	session sess(*_agent, _fd.accept());
}

void server::run() {
	while (true) {
		try {
			accept();
		} catch (std::ios_base::failure&) {}
	}
}

}
