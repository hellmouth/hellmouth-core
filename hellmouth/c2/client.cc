// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/c2/client.h"

namespace hellmouth::c2 {

client::client(const std::filesystem::path& pathname):
	_conn(pathname) {}

void client::configure(const data::any& config) {
	_conn.write(config);
}

data::any client::watch() {
	return _conn.read();
}

}
