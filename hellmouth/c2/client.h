// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_C2_CLIENT
#define HELLMOUTH_C2_CLIENT

#include <filesystem>

#include "hellmouth/c2/connection.h"

namespace hellmouth::c2 {

/** A class to represent a HELLMOUTH C2 client. */
class client {
private:
	/** A connection to the server. */
	connection _conn;

	/** Make a request to the server. */
	data::object request(const data::object& req);
public:
	/** Construct C2 client.
	 * @param pathname the pathname of the UNIX domain socket
	 */
	client(const std::filesystem::path& pathname);

	void configure(const data::any& config);
	data::any watch();
};

}

#endif
