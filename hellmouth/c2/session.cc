// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/c2/session.h"

namespace hellmouth::c2 {

session::session(c2::agent& agent, os::socket_descriptor&& sd):
	_agent(&agent),
	_conn(std::move(sd)),
	_receiver([this]{ _receive(); }),
	_sender([this]{ _send(); }) {}

void session::_receive() {
	try {
		while (true) {
			data::any config = _conn.read();
			_agent->configure(config);
		}
	} catch (std::ios_base::failure&) {}
}

void session::_send() {
	try {
		while (true) {
			data::any status = _agent->watch(_revision);
			_conn.write(status);
		}
	} catch (std::ios_base::failure&) {}
}

}
