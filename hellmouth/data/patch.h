// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_PATCH
#define HELLMOUTH_DATA_PATCH

#include <memory>
#include <vector>

#include "hellmouth/data/operation.h"

namespace hellmouth::data {

/** A class for patching structured data values.
 * Syntax and behaviour are analogous to a JSON patch, as defined by
 * RFC 6902.
 *
 * This class has been implemented as a sublass of data::operation,
 * since it is capable of providing the required functionality,
 * however it differs from other operations in that it is not
 * constructible via the usual factory method.
 */
class patch:
	public operation {
private:
	/** The constituent operations to be applied. */
	std::vector<std::unique_ptr<operation>> _operations;
public:
	/** Construct empty patch. */
	patch() = default;

	/** Construct patch from structured data.
	 * @param the patch specification
	 */
	explicit patch(const data::array& spec);

	/** Append an operation to this patch.
	 * @param spec a specification for the operation to be performed
	 */
	void push_back(const data::object& spec) {
		_operations.push_back(operation::make_unique(spec)); }

	void operator()(data::any& root) const override;
};

}

#endif
