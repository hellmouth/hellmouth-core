// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/any.h"

namespace hellmouth::data {

void any::merge(const data::any& patch) {
	if (is_object() && patch.is_object()) {
		object().merge(patch.cobject());
	} else {
		(*this) = patch;
	}
}

}
