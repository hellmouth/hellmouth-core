// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_FWD
#define HELLMOUTH_DATA_FWD

#include <cstdint>

namespace hellmouth::data {

class null_t;
typedef bool boolean;
typedef int32_t small_integer;
class integer;
class string;
class array;
class object;
class any;
class segment;
class pointer;
class operation;
class patch;

}

#endif
