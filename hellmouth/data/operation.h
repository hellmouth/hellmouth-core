// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_OPERATION
#define HELLMOUTH_DATA_OPERATION

#include <memory>

#include "hellmouth/data/pointer.h"
#include "hellmouth/data/any.h"

namespace hellmouth::data {

class object;

/** A abstract base class to represent an operation on structured data.
 * This class is primarily intended for implementing the constituent
 * operations of a data::patch.
 */
class operation {
public:
	/** Destroy operation. */
	virtual ~operation() = default;

	/** Apply operation to structured data value (in-place).
	 * @param root the root of the structured data value
	 */
	virtual void operator()(data::any& root) const = 0;

	/** Make an operation from a specification.
	 * @param spec the specification
	 * @return the resulting operation
	 */
	static std::unique_ptr<operation> make_unique(const data::object& spec);
};

/** A mixin class for structured data operations with a "path" attribute. */
class path_attribute {
private:
	/** The path attribute. */
	data::pointer _path;
public:
	/** Construct path attribute.
	 * @param spec a specification for the operation to be performed
	 */
	path_attribute(const data::object& spec);

	/** Get the path attribute.
	 * @return the path attribute
	 */
	const data::pointer& path() const {
		return _path;
	}
};

/** A mixin class for structured data operations with a "from" attribute. */
class from_attribute {
private:
	/** The from attribute. */
	data::pointer _from;
public:
	/** Construct from attribute.
	 * @param spec a specification for the operation to be performed
	 */
	from_attribute(const data::object& spec);

	/** Get the from attribute.
	 * @return the from attribute
	 */
	const data::pointer& from() const {
		return _from;
	}
};

/** A mixin class for structured data operations with a "value" attribute. */
class value_attribute {
private:
	/** The value attribute. */
	data::any _value;
public:
	/** Construct value attribute.
	 * @param spec a specification for the operation to be performed
	 */
	value_attribute(const data::object& spec);

	/** Get the value attribute.
	 * @return the value attribute
	 */
	const data::any& value() const {
		return _value;
	}
};

}

#endif
