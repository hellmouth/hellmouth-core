// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_STRING
#define HELLMOUTH_DATA_STRING

#include <string>
#include <unordered_set>
#include <algorithm>
#include <mutex>
#include <atomic>

namespace hellmouth::data {

class shared_string_index;
class string;

/** A class for holding the reference-counted content of a data::string.
 * The substantive content of a shared string (the character sequence to
 * which it refers) is immutable.
 *
 * Each shared string also has a reference count. This is not considered to
 * be part of the substantive content, and is not taken into account when
 * shared strings are compared or hashed.
 *
 * Shared strings should normally be created and destroyed only via the
 * global shared_string_index maintained by data::string.
 */
class shared_string {
	friend shared_string_index;
private:
	/** The content, as a std::string. */
	std::string _content;

	/** The number of live references to this content. */
	mutable std::atomic<size_t> _use_count = 0;
public:
	/** Construct shared string.
	 * The reference count defaults to zero.
	 *
	 * @param content the required content
	 */
	shared_string(std::string&& content):
		_content(std::move(content)) {}

	shared_string(const shared_string&) = delete;

	~shared_string() = default;

	shared_string& operator=(const shared_string&) = delete;

	/** Access content as a std::string.
	 * @return the underlying content
	 */
	operator const std::string&() const {
		return _content;
	}

	/** Access content as a std::string_view.
	 * @return the underlying content
	 */
	operator std::string_view() const {
		return _content;
	}

	/** Compare for equality.
	 * @param that the value against which to compare
	 * @return true if equal, otherwise false
	 */
	auto operator==(const shared_string& that) const {
		// It is not safe to assume that shared strings are globally
		// unique here, since one side of the comparison might be a
		// temporary created whilst emplacing into the global index.
		// A full comparison by value is therefore required.
		return this->_content == that._content;
	}

	/** Perform three-way comparison.
	 * @param that the value against which to compare.
	 * @return the result of the comparison
	 */
	auto operator<=>(const shared_string& that) const {
		return this->_content <=> that._content;
	}

	/** Get the number of live references to this content.
	 * In the case of the empty string, in addition to any references
	 * owned by data::string instances, the result will include the
	 * reference which is automatically created by the global index.
	 *
	 * @return the number of live references
	 */
	size_t use_count() const {
		return _use_count;
	}
};

/** A functor for hashing shared strings. */
struct shared_string_hash {
	/** Calculate hash of a shared string.
	 * @param shared the shared string to be hashed
	 * @return the resulting hash
	 */
	auto operator()(const shared_string& shared) const {
		return std::hash<std::string>()(shared);
	}
};

/** A class for making shared strings available for reuse.
 * The data::string class maintains a single, global instance of this
 * class.
 *
 * When a shared string with a particular value is needed, the global
 * index should be asked to provide it. It will do this by either:
 *
 * - returning a pointer to an existing shared string from the index,
 *   incrementing its reference count to record the new usage, or
 * - creating a new shared string and inserting it into the index,
 *   with an initial reference count of one.
 *
 * The global index must then be notified if any additional references
 * to that shared string are created by copying, or if the reference
 * is destroyed.
 *
 * The current implementation of this class is not thread-safe.
 */
class shared_string_index {
private:
	/** Available shared strings, indexed by content. */
	std::unordered_set<shared_string, shared_string_hash> _index;

	/** A mutex for guarding the index of available strings. */
	std::mutex _mutex;

	/** The empty shared string.
	 * As an optimisation, an empty shared string is kept permanently
	 * in the global index due to its use by the default constructor
	 * of data::string. It is accessible either by searching the index
	 * in the normal way, or via this pointer.
	 *
	 * In order to prevent this empty shared string from being deleted,
	 * this pointer is itself considered to be a reference which counts
	 * towards the reference count.
	 */
	const shared_string* _empty;
public:
	/** Construct index. */
	shared_string_index();

	shared_string_index(const shared_string_index&) = delete;

	/** Destroy index. */
	~shared_string_index();

	shared_string_index& operator=(const shared_string_index&) = delete;

	/** Duplicate a reference to a shared_string.
	 * This function should be called whenever a new reference is
	 * created from an existing reference to a shared string, and has
	 * the effect of incrementing the reference count.
	 *
	 * It should not be called when a reference is obtained by calling
	 * the shared_string_index::find, since that implicitly performs any
	 * required adjustment to the reference count.
	 *
	 * @param shared the shared string to be duplicated
	 * @return the shared string which was duplicated
	 */
	const shared_string* dup(const shared_string* shared) {
		// The current thread has presumed exclusive possession of
		// one reference, therefore it can safely create another
		// reference.
		++shared->_use_count;
		return shared;
	}

	/** Destroy a reference to a shared_string.
	 * This function should be called whenever a reference to a shared
	 * string is destroyed, that reference previously having been created
	 * by calling the shared_string_index::dup, find or empty functions.
	 * It has the effect of decrementing the reference count, and if the
	 * reference count is then zero, destroying the shared string after
	 * removing it from the index.
	 *
	 * @param shared the shared string to be destroyed
	 */
	void del(const shared_string* shared) {
		if (--shared->_use_count == 0) {
			// For efficiency, acquire the lock only if the
			// reference count has been decremented to zero,
			// however it is then necessary to allow for the
			// possibility that another thread could have
			// incremented the index from zero before the
			// lock was acquired.
			std::lock_guard lock(_mutex);
			if (shared->_use_count == 0) {
				// There are no live references, and no
				// live references can be created while
				// the lock is held, therefore the string
				// can be safely removed from the index.
				_index.erase(*shared);
			}
		}
	}

	/** Create a reference to a shared_string with given content.
	 * If there is a suitable shared_string already in the index then it
	 * will be reused, otherwise a new shared_string will be created and
	 * inserted into the index.
	 *
	 * On exit, the reference count of the shared string will have been
	 * incremented (from its default value of zero in the case where the
	 * shared string is newly-created).
	 *
	 * @param content the required content
	 * @return a pointer to the resulting shared string
	 */
	const shared_string* find(std::string&& content) {
		// Acquire the lock before searching the index, and do not
		// release it until after the reference count has been
		// incremented. This ensures that no other thread can remove
		// the string from the index.
		std::lock_guard lock(_mutex);
		return dup(&*_index.emplace(std::move(content)).first);
	}

	/** Get a reference to the empty shared string.
	 * On exit, the reference count of the empty shared string will have
	 * been incremented (allowing it to be unreferenced in the same way as
	 * if it had been obtained by calling the find function).
	 *
	 * @return a pointer to the empty shared string
	 */
	const shared_string* empty() {
		return dup(_empty);
	}
};

/** A structured data class to represent a character string.
 * This class provides some of the functionality of a std::string using a
 * similar interface, but it is optimised for the case where:
 *
 * - Many strings will have identical content (and not necessarily as a
 *   result of copying).
 * - One a string has been given a value, that value is unlikely to change.
 *
 * Although this is not an immutable class, most of the functions which
 * would be used for mutating a std::string are not implemented by
 * data::string, and if they were implemented then it is likely their
 * performance would be relatively poor. The recommended approach should
 * such functionality be needed is to perform any required string
 * processing using std::string, then copy or move the result into a
 * data::string when it is ready for long-term storage.
 *
 * This class is intended to be partially thread-safe:
 * - As with most containers, it is not safe for multiple threads to access
 *   the same instance concurrently, except in the case of multiple readers
 *   and no writers.
 * - However, it is safe for multiple threads to access separate instances
 *   which refer to the same reference-counted data.
 */
class string {
public:
	// Types

	/** The type of the underlying content. */
	typedef std::string content_type;

	/** The type of a character. */
	typedef content_type::value_type value_type;

	/** A type to represent the size of a string. */
	typedef content_type::size_type size_type;

	/** A type to represent a size difference of a string. */
	typedef content_type::difference_type difference_type;

	/** The type of a reference to a character. */
	typedef content_type::reference reference;

	/** The type of a const reference to a character. */
	typedef content_type::const_reference const_reference;

	/** The type of a pointer to a character. */
	typedef content_type::pointer pointer;

	/** The type of a const pointer to a character. */
	typedef content_type::const_pointer const_pointer;

	/** A const iterator into a string. */
	typedef content_type::const_iterator const_iterator;

	/** A const reverse iterator into a string. */
	typedef content_type::const_reverse_iterator const_reverse_iterator;
private:
	/** The shared content of this string. */
	const shared_string* _shared;

	/** Get global index of shared content available for reuse. */
	shared_string_index& _index() {
		static shared_string_index index;
		return index;
	}

	/** Access the underlying content (non-mutable).
	 * @return the underlying content
	 */
	const std::string& content() const {
		return *_shared;
	}
public:
	// Construct/copy/destroy/assign

	/** Construct an empty data::string. */
	string():
		_shared(_index().empty()) {}

	/** Copy-construct data::string.
	 * @param that the string to be copied
	 */
	string(const data::string& that):
		_shared(_index().dup(that._shared)) {}

	/** Construct data::string from std::string (copied).
	 * @param content the required content
	 */
	string(const std::string& content):
		_shared(_index().find(std::string(content))) {}

	/** Construct data::string from std::string (moved).
	 * @param content the required content
	 */
	string(std::string&& content):
		_shared(_index().find(std::move(content))) {}

	/** Construct data::string from std::string_view.
	 * @param content the required content
	 */
	string(std::string_view content):
		_shared(_index().find(std::string(content))) {}

	/** Construct data::string from C-style string.
	 * @param content the required content
	 */
	string(const char* content):
		_shared(_index().find(std::string(content))) {}

	/** Destroy data::string. */
	~string() {
		_index().del(_shared);
	}

	/** Copy-assign data::string.
	 * @param that the string to be copied
	 * @return a reference to this
	 */
	data::string& operator=(const data::string& that) {
		const shared_string* shared = _index().dup(that._shared);
		_index().del(_shared);
		_shared = shared;
		return *this;
	}

	/** Assign data::string from std::string (copied).
	 * @param content the required content
	 * @return a reference to this
	 */
	data::string& operator=(const std::string& content) {
		const shared_string* shared = _index().find(std::string(content));
		_index().del(_shared);
		_shared = shared;
		return *this;
	}

	/** Assign data::string from std::string (moved).
	 * @param content the required content
	 * @return a reference to this
	 */
	data::string& operator=(std::string&& content) {
		const shared_string* shared = _index().find(std::move(content));
		_index().del(_shared);
		_shared = shared;
		return *this;
	}

	/** Assign data::string from std::string_view.
	 * @param content the required content
	 * @return a reference to this
	 */
	data::string& operator=(std::string_view content) {
		const shared_string* shared = _index().find(std::string(content));
		_index().del(_shared);
		_shared = shared;
		return *this;
	}

	/** Assign data::string from C-style string.
	 * @param content the required content
	 * @return a reference to this
	 */
	data::string& operator=(const char* content) {
		const shared_string* shared = _index().find(std::string(content));
		_index().del(_shared);
		_shared = shared;
		return *this;
	}

	/** Replace the content of this string with a specified value.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	string& assign(Args&& ...args) {
		std::string content;
		content.assign(std::forward<Args>(args)...);
		*this = content;
		return *this;
	}

	// Conversions

	/** Access content as std::string (non-mutable).
	 * @return the underlying content
	 */
	operator const std::string&() const {
		return content();
	}

	/** Access content as std::string_view.
	 * @return the underlying content
	 */
	operator std::string_view() const {
		return content();
	}

	// Iterator support

	/** Obtain a const iterator for beginning forward iteration.
	 * @return the iterator
	 */
	const_iterator begin() const {
		return content().begin();
	}

	/** Obtain a const iterator for beginning forward iteration.
	 * @return the iterator
	 */
	const_iterator cbegin() const {
		return content().cbegin();
	}

	/** Obtain a const iterator for ending forward iteration.
	 * @return the iterator
	 */
	const_iterator end() const {
		return content().end();
	}

	/** Obtain a const iterator for ending forward iteration.
	 * @return the iterator
	 */
	const_iterator cend() const {
		return content().cend();
	}

	/** Obtain a const reverse iterator for beginning reverse iteration.
	 * @return the iterator
	 */
	const_reverse_iterator rbegin() const {
		return content().rbegin();
	}

	/** Obtain a const reverse iterator for beginning reverse iteration.
	 * @return the iterator
	 */
	const_reverse_iterator crbegin() const {
		return content().crbegin();
	}

	/** Obtain a const reverse iterator for ending reverse iteration.
	 * @return the iterator
	 */
	const_reverse_iterator rend() const {
		return content().rend();
	}

	/** Obtain a const reverse iterator for ending reverse iteration.
	 * @return the iterator
	 */
	const_reverse_iterator crend() const {
		return content().crend();
	}

	// Capacity

	/** Get the size/length of this string.
	 * @return the size/length, in bytes
	 */
	size_type size() const {
		return content().size();
	}

	/** Get the size/length of this string.
	 * @return the size/length, in bytes
	 */
	size_type length() const {
		return content().length();
	}

	/** Get the maximum possible size of a string.
	 * @return the size, in bytes
	 */
	size_type max_size() const {
		return content().max_size();
	}

	/** Replace this string with the empty string. */
	void clear() {
		*this = string();
	}

	/** Test whether this string is empty.
	 * @return true if empty, otherwise false
	 */
	bool empty() const {
		return content().empty();
	}

	// Element access

	/** Get the character at a given position (unchecked).
	 * @param pos the position within the string
	 * @return a reference to the character at that position
	 */
	const_reference operator[](size_type pos) const {
		return content()[pos];
	}

	/** Get the character at a given position (checked).
	 * @param pos the position within the string
	 * @return a reference to the character at that position
	 */
	const_reference at(size_type pos) const {
		return content().at(pos);
	}

	// Modifiers

	/** Swap the content of this string with that of another string.
	 * @param that the string with which which to swap
	 */
	void swap(string& that) {
		std::swap(this->_shared, that._shared);
	}

	/** Copy the content of this string to a supplied buffer.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) copy(Args&& ...args) const {
		return content().copy(std::forward<Args>(args)...);
	}

	// String operations

	/** Access content as C-style string.
	 * @return the underlying content
	 */
	const char* c_str() const {
		return content().c_str();
	}

	/** Access content as a sequence of characters.
	 * @return the underlying content
	 */
	const char* data() const {
		return content().data();
	}

	/** Find first instance of substring, searching forwards.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) find(Args&& ...args) const {
		return content().find(std::forward<Args>(args)...);
	}

	/** Find first instance of substring, searching backwards.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) rfind(Args&& ...args) const {
		return content().rfind(std::forward<Args>(args)...);
	}

	/** Find first matching character from set, searching forwards.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) find_first_of(Args&& ...args) const {
		return content().find_first_of(std::forward<Args>(args)...);
	}

	/** Find first matching character from set, searching backwards.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) find_last_of(Args&& ...args) const {
		return content().find_last_of(std::forward<Args>(args)...);
	}

	/** Find first non-matching character from set, searching forwards.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) find_first_not_of(Args&& ...args) const {
		return content().find_first_not_of(std::forward<Args>(args)...);
	}

	/** Find first non-matching character from set, searching backwards.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) find_last_not_of(Args&& ...args) const {
		return content().find_last_not_of(std::forward<Args>(args)...);
	}

	/** Return a given substring.
	 * Parameters and return types are as for the underlying container.
	 * Note that the return type has intentionally been left as a
	 * std::string, as opposed to being converted to a data::string.
	 */
	template <typename ...Args>
	decltype(auto) substr(Args&& ...args) const {
		return content().substr(std::forward<Args>(args)...);
	}

	/** Compare two strings.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	decltype(auto) compare(Args&& ...args) const {
		return content().compare(std::forward<Args>(args)...);
	}

	// Comparison operators

	/** Compare for equality.
	 * @param that the value against which to compare.
	 * @return true if equal, otherwise false
	 */
	bool operator==(const data::string& that) const {
		return this->_shared == that._shared;
	}

	/** Perform three-way comparison.
	 * @param that the value against which to compare.
	 * @return the result of the comparison
	 */
	auto operator<=>(const data::string& that) const {
		return this->_shared <=> that._shared;
	}

	// Additional functions

	/** Get the number of live references to this content.
	 * In the case of the empty string, in addition to any references
	 * owned by data::string instances, the result will include the
	 * reference which is automatically created by the global index.
	 *
	 * @return the number of live references
	 */
	size_t use_count() const {
		return _shared->use_count();
	}
};

}

#endif
