// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_OBJECT
#define HELLMOUTH_DATA_OBJECT

#include <map>
#include <atomic>

#include "hellmouth/data/string.h"

namespace hellmouth::data {

class any;
class shared_object;

/** A structured data class to represent an object.
 * Although objects are currently implemented as ordered collections, this
 * is not guaranteed to remain the case. For this reason, member functions
 * which would only be appropriate for an ordered collection have not been
 * provided.
 *
 * This class is intended to be partially thread-safe:
 * - As with most containers, it is not safe for multiple threads to access
 *   the same instance concurrently, except in the case of multiple readers
 *   and no writers.
 * - However, it is safe for multiple threads to access separate instances
 *   which refer to the same reference-counted data, provided that:
 *   - suitable memory barriers are employed whenever instances are passed
 *     from one thread to another (to ensure that the recipient sees the
 *     content in a consistent and up-to-date state, and
 *   - non-const operations are presumed to invalidate all iterators,
 *     references and pointers obtained from the instance in question
 *     (since the operation could have triggered a copy-on-write).
 */
class object {
public:
	// Types

	/** The type of the underlying content. */
	typedef std::map<data::string, data::any> content_type;

	/** The type of an object key. */
	typedef content_type::key_type key_type;

	/** The type of an object value. */
	typedef content_type::mapped_type mapped_type;

	/** The type of an object key-value pair. */
	typedef content_type::value_type value_type;

	/** The type of a pointer to an object key-value pair. */
	typedef content_type::pointer pointer;

	/** The type of a const pointer to an object key-value pair. */
	typedef content_type::const_pointer const_pointer;

	/** The type of a reference to an object key-value pair. */
	typedef content_type::reference reference;

	/** The type of a const reference to an object key-value pair. */
	typedef content_type::const_reference const_reference;

	/** A type to represent the size of an object. */
	typedef content_type::size_type size_type;

	/** A type to represent a size difference of an object. */
	typedef content_type::difference_type difference_type;

	/** An iterator into an object. */
	typedef content_type::iterator iterator;

	/** A const iterator into an object. */
	typedef content_type::const_iterator const_iterator;
private:
	/** The shared content of this object. */
	shared_object* _shared;

	/** Access the underlying content (non-const).
	 * This operator will perform a copy-on-write if required.
	 *
	 * @return the underlying content
	 */
	inline content_type& content();

	/** Access the underlying content (const).
	 * @return the underlying content
	 */
	inline const content_type& content() const;
public:
	// Construct/copy/destroy/assign

	/** Construct an empty data::object. */
	inline object();

	/** Copy-construct a data::object.
	 * @param that the object to be copied
	 */
	inline object(const object& that);

	/** Construct data::object.
	 * Parameter types are as for the underlying container.
	 */
	template <typename ...Args>
	inline object(Args&& ...args);

	/** Destroy data::object. */
	inline ~object();

	/** Copy-assign data::object.
	 * @param that the object to be copied
	 * @return a reference to this
	 */
	inline object& operator=(const object& that);

	// Conversions

	/** Access the underlying content (non-const).
	 * This operator will perform a copy-on-write if required.
	 *
	 * @return the underlying content
	 */
	inline operator content_type&();

	/** Access the underlying content (const).
	 * @return the underlying content
	 */
	inline operator const content_type&() const;

	/** Make a const reference to this object.
	 * It is not an error if the object acted upon is already const.
	 *
	 * @return the resulting const reference
	 */
	const object& constant() const {
		return *this;
	}

	// Iterator support

	/** Obtain an iterator for beginning forward iteration.
	 * @return the iterator
	 */
	inline iterator begin();

	/** Obtain a const iterator for beginning forward iteration.
	 * @return the iterator
	 */
	inline const_iterator begin() const;

	/** Obtain a const iterator for beginning forward iteration.
	 * @return the iterator
	 */
	inline const_iterator cbegin() const;

	/** Obtain an iterator for ending forward iteration.
	 * @return the iterator
	 */
	inline iterator end();

	/** Obtain a const iterator for ending forward iteration.
	 * @return the iterator
	 */
	inline const_iterator end() const;

	/** Obtain a const iterator for ending forward iteration.
	 * @return the iterator
	 */
	inline const_iterator cend() const;

	// Capacity

	/** Test whether this object is empty.
	 * @return true if empty, otherwise false
	 */
	inline decltype(auto) empty() const;

	/** Get the number of elements in this object.
	 * @return the number of elements
	 */
	inline decltype(auto) size() const;

	/** Get the maximum number of elements this object could hold.
	 * @return the number of elements
	 */
	inline decltype(auto) max_size() const;

	// Modifiers

	/** Emplace an element into this object.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) emplace(Args&& ...args);

	/** Emplace an element into this object using a given hint.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) emplace_hint(Args&& ...args);

	/** Insert an element into this object.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) insert(Args&& ...args);

	/** Emplace an element into this object if key not present.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) try_emplace(Args&& ...args);

	/** Insert an element into this object, or assign if key present.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) insert_or_assign(Args&& ...args);

	/** Remove an element from this object.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) erase(Args&& ...args);

	/** Swap content with another object.
	 * @param that the object with which to swap
	 */
	inline void swap(object& that);

	/** Clear all elements from this object. */
	inline void clear();

	/** Merge an object into this one.
	 * Syntax and behaviour are analogous to a JSON merge patch,
	 * as defined by RFC 7386.
	 *
	 * @param patch the object to be merged
	 */
	void merge(const data::object& patch);

	// Map operations

	/** Find an element in this object (non-const).
	 * @param key the key to be matched
	 * @return an iterator to the element, or end() if not found
	 */
	template<typename K>
	inline decltype(auto) find(K&& key);

	/** Find an element in this object (const).
	 * @param key the key to be matched
	 * @return an iterator to the element, or end() if not found
	 */
	template<typename K>
	inline decltype(auto) find(K&& key) const;

	/** Count the number of elements matching a given key.
	 * @param key the key to be matched
	 * @return the number of elements
	 */
	template<typename K>
	inline decltype(auto) count(K&& key) const;

	/** Test whether a given key is present in this object.
	 * @param key the key to be matched
	 * @return true if the key is present, otherwise false
	 */
	template<typename K>
	inline decltype(auto) contains(K&& key) const;

	/** Return the range of elements in this object matching a given key.
	 * @param key the key to be matched
	 * @return the matching range
	 */
	template<typename K>
	inline decltype(auto) equal_range(K&& key) const;

	// Element access

	/** Access value with a given key (non-const).
	 * If a member with the required key is not present in the object
	 * then an empty value is inserted.
	 *
	 * @param key the key to be accessed
	 * @return a reference to the value
	 */
	template<typename K>
	inline data::any& operator[](K&& key);

	/** Access value with given key (checked, non-const).
	 * @param key the key to be accessed
	 * @return a reference to the value
	 */
	template<typename K>
	inline data::any& at(K&& key);

	/** Access value with given key (checked, const).
	 * @param key the key to be accessed
	 * @return a reference to the value
	 */
	template<typename K>
	inline const data::any& at(K&& key) const;

	/** Access object element with given key (defaulted).
	 * @param key the ke to be accessed
	 * @param dv the default value if not found
	 * @return a reference to the value
	 */
	template<typename K>
	inline data::any at_or(K&& key, const data::any& dv) const;

	// Comparison functions.

	/** Compare objects.
	 * @param that the object with which to compare
         * @return true if equal, otherwise false
         */
	inline bool operator==(const data::object& that) const;

	// Additional functions

	/** Get the number of live references to this content.
	 * @return the number of live references
	 */
	inline size_t use_count() const;
};

}

#include "hellmouth/data/any.h"

namespace hellmouth::data {

/** A class for holding the reference-counted content of a data::object.
 * The substantive content of a shared object is mutable, but must not be
 * mutated if the reference count is greater than one.
 */
class shared_object {
	friend object;
public:
	/** The type of an object key. */
	typedef data::string key_type;

	/** The type of an object value. */
	typedef data::any mapped_type;

	/** The type of the underlying content. */
	typedef std::map<key_type, mapped_type> content_type;
private:
	/** The underlying content. */
	content_type _content;

	/** The number of live references to this content. */
	std::atomic<size_t> _use_count = 0;
public:
	/** Construct empty shared object.
	 * The reference count defaults to zero.
	 */
	shared_object() = default;

	/** Copy-construct shared object.
	 * The substantive content is copied, but the reference count
	 * receives its default value of zero.
	 *
	 * @param that the shared object to be copied
	 */
	shared_object(const shared_object& that):
		_content(that._content) {}

	/** Construct shared object.
	 * Parameter types are as for the underlying container.
	 */
	template <typename ...Args>
	shared_object(Args&& ...args):
		_content(std::forward<Args>(args)...) {}

	/** Destroy shared object. */
	~shared_object() = default;

	shared_object& operator=(const shared_object&) = delete;

	/** Access the underlying content (non-const).
	 * @return the underlying content
	 */
	operator content_type&() {
		return _content;
	}

	/** Access the underlying content (const).
	 * @return the underlying content
	 */
	operator const content_type&() const {
		return _content;
	}

	/** Get the number of live references to this content.
	 * @return the number of live references
	 */
	size_t use_count() const {
		return _use_count;
	}
};

object::content_type& object::content() {
	if (_shared->_use_count > 1) {
		// Copy-on-write is necessary.
		// This thread has presumed exclusive possession of at
		// least one reference, therefore whilst _use_count might
		// decrement during execution of the following (and
		// perhaps obviate the need for a copy-on-write), it should
		// not fall below one. The existing content is therefore
		// safe to copy.
		shared_object* shared = new shared_object(*_shared);

		// Newly constructed, therefore the current thread has
		// exclusive possession.
		++shared->_use_count;

		// As noted above, this should not have fallen below one,
		// however it might since have become equal to one. There
		// is therefore a chance that the copy-on-write was (with
		// hindsight) unnecessary, in which case one of the new
		// copies should be deleted.
		if (--_shared->_use_count == 0) {
			// Reference count was previously one, therefore
			// this was the only live reference, therefore
			// the current thread has exclusive possession.
			delete _shared;
		}
		_shared = shared;
	}
	return *_shared;
}

const object::content_type& object::content() const {
	return *_shared;
}

object::object():
	_shared(new shared_object) {

	// Shared content is newly-constructed, therefore this is the only
	// live reference, therefore the current thread has exclusive
	// possession.
	++_shared->_use_count;
}

object::object(const object& that):
	_shared(that._shared) {

	// The current thread has presumed exclusive possession of one
	// reference, therefore it can safely create another reference.
	++_shared->_use_count;
}

template <typename ...Args>
object::object(Args&& ...args):
	_shared(new shared_object(std::forward<Args>(args)...)) {

	// Shared content is newly-constructed, therefore this is the only
	// live reference, therefore the current thread has exclusive
	// possession.
	++_shared->_use_count;
}

object::~object() {
	if (--_shared->_use_count == 0) {
		// Reference count was previously one, therefore this
		// was the only live reference, therefore the current
		// thread has exclusive possession.
		delete _shared;
	}
}

object& object::operator=(const object& that) {
	if (this != &that) {
		if (--_shared->_use_count == 0) {
			// Reference count was previously one, therefore
			// this was the only live reference, therefore
			// the current thread has exclusive possession.
			delete _shared;
		}
		_shared = that._shared;
		// This thread already has presumed exclusive possession
		// of at least one reference, therefore the reference count
		// should be non-zero and safe to increment.
		++_shared->_use_count;
	}
	return *this;
}

object::operator content_type&() {
	return content();
}

object::operator const content_type&() const {
	return content();
}

object::iterator object::begin() {
	return content().begin();
}

object::const_iterator object::begin() const {
	return content().begin();
}

object::const_iterator object::cbegin() const {
	return content().cbegin();
}

object::iterator object::end() {
	return content().end();
}

object::const_iterator object::end() const {
	return content().end();
}

object::const_iterator object::cend() const {
	return content().cend();
}

decltype(auto) object::empty() const {
	return content().empty();
}

decltype(auto) object::size() const {
	return content().size();
}

decltype(auto) object::max_size() const {
	return content().max_size();
}

template <typename ...Args>
decltype(auto) object::emplace(Args&& ...args) {
	return content().emplace(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) object::emplace_hint(Args&& ...args) {
	return content().emplace_hint(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) object::insert(Args&& ...args) {
	return content().insert(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) object::try_emplace(Args&& ...args) {
	return content().try_emplace(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) object::insert_or_assign(Args&& ...args) {
	return content().insert_or_assign(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) object::erase(Args&& ...args) {
	return content().erase(std::forward<Args>(args)...);
}

void object::swap(object& that) {
	content_type& this_content = *this;
	content_type& that_content = that;
	this_content.swap(that_content);
}

void object::clear() {
	return content().clear();
}

template<typename K>
decltype(auto) object::find(K&& key) {
	return content().find(std::forward<K>(key));
}

template<typename K>
decltype(auto) object::find(K&& key) const {
	return content().find(std::forward<K>(key));
}

template<typename K>
decltype(auto) object::count(K&& key) const {
	return content().count(std::forward<K>(key));
}

template<typename K>
decltype(auto) object::contains(K&& key) const {
	return content().contains(std::forward<K>(key));
}

template<typename K>
decltype(auto) object::equal_range(K&& key) const {
	return content().equal_range(std::forward<K>(key));
}

template<typename K>
data::any& object::operator[](K&& key) {
	return content()[std::forward<K>(key)];
}

template<typename K>
data::any& object::at(K&& key) {
	return content().at(std::forward<K>(key));
}

template<typename K>
const data::any& object::at(K&& key) const {
	return content().at(std::forward<K>(key));
}

template<typename K>
data::any object::at_or(K&& key, const data::any& dv) const {
	auto f = content().find(std::forward<K>(key));
	if (f != content().end()) {
		return f->second;
	} else {
		return dv;
	}
}

bool object::operator==(const object& that) const {
	return this->content() == that.content();
}

size_t object::use_count() const {
	return _shared->use_count();
}

}

#endif
