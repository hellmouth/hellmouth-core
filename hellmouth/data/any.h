// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_ARRAY
#include "hellmouth/data/array.h"
#endif
#ifndef HELLMOUTH_DATA_OBJECT
#include "hellmouth/data/object.h"
#endif

#ifndef HELLMOUTH_DATA_ANY
#define HELLMOUTH_DATA_ANY

#include <cstdint>
#include <limits>
#include <variant>
#include <stdexcept>
#include <concepts>
#include <algorithm>
#include <utility>
#include <string>
#include <string_view>

#include "hellmouth/data/fwd.h"
#include "hellmouth/data/null.h"
#include "hellmouth/data/string.h"

namespace hellmouth::data {

/** A class to represent any supported structured data type.
 * Currently the only supported types are nulls, booleans and integers,
 * with the latter limited to the range of an int32_t.
 *
 * Conversions to or from any integer type are allowed, but are validated
 * to check that they are value-preserving. If a conversion is attempted
 * which would not preserve the value then an exception is thrown.
 *
 * Implicit conversions are not currently allowed between any of the
 * supported types.
 */
class any {
public:
	// Types

	/** The type of am array index. */
	typedef size_t size_type;
private:
	/** The underlying content. */
	std::variant<
		data::null_t,
		data::boolean,
		data::small_integer,
		data::string,
		data::array,
		data::object> _content;
public:
	// Construct/copy/destroy/assign

	/** Construct data::any with null value. */
	any() = default;

	/** Construct data::any from boolean.
	 * @param content the required underlying content
	 */
	template<typename T>
	requires std::same_as<T, bool>
	any(const T& content) {
		_content = content;
	}

	/** Construct data::any from integer.
	 * @param content the required underlying content
	 */
	template<typename T>
	requires (std::integral<T> && !std::same_as<T, bool>)
	any(const T& content) {
		if (!std::in_range<small_integer>(content)) {
			throw std::domain_error("integer out of range");
		}
		_content = static_cast<small_integer>(content);
	}

	/** Construct data::any from data::string.
	 * @param content the required underlying content
	 */
	any(const data::string& content) {
		_content = content;
	}

	/** Construct data::any from std::string (copied).
	 * @param content the required underlying content
	 */
	any(const std::string& content) {
		_content = data::string(content);
	}

	/** Construct data::any from std::string (moved).
	 * @param content the required underlying content
	 */
	any(std::string&& content) {
		_content = data::string(std::move(content));
	}

	/** Construct data::any from C-style string.
	 * @param content the required underlying content
	 */
	template<class T>
	requires std::same_as<T, char>
	any(const T* content) {
		_content = data::string(content);
	}

	/** Construct data::any from data::array.
	 * @param content the required underlying content
	 */
	any(const data::array& content) {
		_content = content;
	}

	/** Construct data::any from data::object.
	 * @param content the required underlying content
	 */
	any(const data::object& content) {
		_content = content;
	}

	/** Assign data::any from boolean.
	 * @param content the required underlying content
	 */
	template<typename T>
	requires std::same_as<T, bool>
	any& operator=(const T& content) {
		_content = content;
		return *this;
	}

	/** Assign data::any from integer.
	 * @param content the required underlying content
	 */
	template<typename T>
	requires (std::integral<T> && !std::same_as<T, bool>)
	any& operator=(const T& content) {
		if (!std::in_range<small_integer>(content)) {
			throw std::domain_error("integer out of range");
		}
		_content = static_cast<small_integer>(content);
		return *this;
	}

	/** Assign data::any from data::string.
	 * @param content the required underlying content
	 */
	any& operator=(data::string content) {
		_content = content;
		return *this;
	}

	/** Assign data::any from std::string (copied).
	 * @param content the required underlying content
	 */
	any& operator=(const std::string& content) {
		_content = data::string(content);
		return *this;
	}

	/** Assign data::any from std::string (moved).
	 * @param content the required underlying content
	 */
	any& operator=(std::string&& content) {
		_content = data::string(std::move(content));
		return *this;
	}

	/** Assign data::any from C-style string.
	 * @param content the required underlying content
	 */
	template<class T>
	requires std::same_as<T, char>
	any& operator=(const T* content) {
		_content = data::string(content);
		return *this;
	}

	/** Assign data::any from data::array.
	 * @param content the required underlying content
	 */
	any& operator=(const data::array& content) {
		_content = content;
		return *this;
	}

	/** Assign data::any from data::object.
	 * @param content the required underlying content
	 */
	any& operator=(const data::object& content) {
		_content = content;
		return *this;
	}

	// Conversion functions.

	/** Test whether this value is null.
	 * @return true if null, otherwise false
	 */
	bool is_null() const {
		return holds_alternative<null_t>(_content);
	}

	/** Test whether this value is a string.
	 * @return true if a string, otherwise false
	 */
	bool is_string() const {
		return holds_alternative<data::string>(_content);
	}

	/** Test whether this value is an object.
	 * @return true if an object, otherwise false
	 */
	bool is_object() const {
		return holds_alternative<data::object>(_content);
	}

	/** Test whether this value is an array.
	 * @return true if an array, otherwise false
	 */
	bool is_array() const {
		return holds_alternative<data::array>(_content);
	}

	/** Convert data::any to null value.
	 * @return the underlying content
	 */
	operator null_t() const {
		return get<null_t>(_content);
	}

	/** Convert data::any to boolean.
	 * @return the underlying content
	 */
	operator bool() const {
		return get<bool>(_content);
	}

	/** Convert data::any to integer.
	 * @return the underlying content as an integer
	 */
	template<typename T>
	requires (std::integral<T> && !std::same_as<T, bool>)
	operator T() const {
		auto content = get<small_integer>(_content);
		if (!std::in_range<T>(content)) {
			throw std::overflow_error("integer out of range");
		}
		return content;
	}

	/** Access data::any as data::string (non-const).
	 * @return the underlying content
	 */
	operator data::string&() {
		return get<data::string>(_content);
	}

	/** Access data::any as data::string (const).
	 * @return the underlying content
	 */
	operator const data::string&() const {
		return get<data::string>(_content);
	}

	/** Access data::any as std::string (const).
	 * @return the converted content
	 */
	operator const std::string&() const {
		return get<data::string>(_content);
	}

	/** Access data::any as std::string_view.
	 * @return the converted content
	 */
	operator std::string_view() const {
		return get<data::string>(_content);
	}

	/** Access data::any as data::array (non-const).
	 * @return the underlying content
	 */
	operator data::array&() {
		return get<data::array>(_content);
	}

	/** Access data::any as data::array (const).
	 * @return the underlying content
	 */
	operator const data::array&() const {
		return get<data::array>(_content);
	}

	/** Access data::any as data::array (non-const).
	 * If the value is currently null then it will be coerced to become
	 * an empty array.
	 *
	 * @return the underlying content
	 */
	data::array& array() {
		try {
			return get<data::array>(_content);
		} catch (std::bad_variant_access&) {
			if (holds_alternative<data::null_t>(_content)) {
				_content = data::array();
				return get<data::array>(_content);
			}
			throw;
		}
	}

	/** Access data::any as data::array (const).
	 * @return the underlying content
	 */
	const data::array& carray() const {
		return get<data::array>(_content);
	}

	/** Access data::any as data::object (non-const).
	 * If the value is currently null then it will be coerced to become
	 * an empty object.
	 *
	 * @return the underlying content
	 */
	operator data::object&() {
		return get<data::object>(_content);
	}

	/** Access data::any as data::object (const).
	 * @return the underlying content
	 */
	operator const data::object&() const {
		return get<data::object>(_content);
	}

	/** Access data::any as data::object (non-const).
	 * @return the underlying content
	 */
	data::object& object() {
		try {
			return get<data::object>(_content);
		} catch (std::bad_variant_access&) {
			if (holds_alternative<data::null_t>(_content)) {
				_content = data::object();
				return get<data::object>(_content);
			}
			throw;
		}
	}

	/** Access data::any as data::object (const).
	 * @return the underlying content
	 */
	const data::object& cobject() const {
		return get<data::object>(_content);
	}

	/** Access data::any as C-style string (const).
	 * @return the converted content
	 */
	const char* c_str() const {
		return get<data::string>(_content).c_str();
	}

	// Map operations

	/** Test whether an object element with a given key is present.
	 * @param key the key to be matched
	 * @return true if the key is present, otherwise false
	 */
	template<typename K>
	inline bool contains(K&& key) const {
		return cobject().contains(key);
	}

	// Element access

	/** Access array element at given index (checked, non-const).
	 * @param pos the index of the element to be accessed
	 * @return a reference to the element
	 */
	any& at(size_type pos) {
		return array().at(pos);
	}

	/** Access array element at given index (checked, const).
	 * @param pos the index of the element to be accessed
	 * @return a reference to the element
	 */
	const any& at(size_type pos) const {
		return carray().at(pos);
	}

	/** Access array element at given index (unchecked, non-const).
	 * @param pos the index of the element to be accessed
	 * @return a reference to the element
	 */
	any& operator[](size_type pos) {
		return array()[pos];
	}

	/** Access array element at given index (unchecked, const).
	 * @param pos the index of the element to be accessed
	 * @return a reference to the element
	 */
	const any& operator[](size_type pos) const {
		return carray()[pos];
	}

	/** Access object element with given key (checked, non-const).
	 * @param key the key of the element to be accessed
	 * @return a reference to the element
	 */
	any& at(std::string_view key) {
		return object().at(key);
	}

	/** Access object element with given key (checked, const).
	 * @param key the key of the element to be accessed
	 * @return a reference to the element
	 */
	const any& at(std::string_view key) const {
		return cobject().at(key);
	}

	/** Access object element with given key (defaulted).
	 * @param key the key of the element to be accessed
	 * @param dv the default value if not found
	 */
	any at_or(std::string_view key, const data::any& dv) const {
		return cobject().at_or(key, dv);
	}

	/** Access object element with given key (unchecked, non-const).
	 * @param key the key of the element to be accessed
	 * @return a reference to the element
	 */
	any& operator[](std::string_view key) {
		return object()[key];
	}

	/** Access object element with given key (checked, non-const).
	 * @param key the key of the element to be accessed
	 * @return a reference to the element
	 */
	template<class T>
	requires std::same_as<T, char>
	any& at(const T* key) {
		return object().at(key);
	}

	/** Access object element with given key (checked, const).
	 * @param key the key of the element to be accessed
	 * @return a reference to the element
	 */
	template<class T>
	requires std::same_as<T, char>
	const any& at(const T* key) const {
		return cobject().at(key);
	}

	/** Access object element with given key (unchecked, non-const).
	 * @param key the key of the element to be accessed
	 * @return a reference to the element
	 */
	template<class T>
	requires std::same_as<T, char>
	any& operator[](const T* key) {
		return object()[key];
	}

	// Modifiers

	/** Clear the content of this data::any (set to null). */
	void clear() {
		_content = null;
	}

	/** Merge a value into this value.
	 * Syntax and behaviour are analogous to a JSON merge patch,
	 * as defined by RFC 7386.
	 *
	 * @param patch the value to be merged
	 */
	void merge(const data::any& patch);

	/** Swap the content of this data::any with another.
	 * @param that the data::any with which to swap content
	 */
	void swap(data::any& that) {
		std::swap(this->_content, that._content);
	}

	/** Visit the content with a given functor.
	 * The functor should be able to accept any of the types
	 * which _content is permitted to have.
	 *
	 * @param visitor the functor to be applied
	 */
	template<class T>
	decltype(auto) visit(T& visitor) {
		return std::visit(visitor, _content);
	}

	/** Visit the content with a given functor.
	 * The functor should be able to accept any of the types
	 * which _content is permitted to have.
	 *
	 * @param visitor the functor to be applied
	 */
	template<class T>
	decltype(auto) visit(T& visitor) const {
		return std::visit(visitor, _content);
	}

	// Comparison functions.

	bool operator==(const data::any& that) const {
		return this->_content == that._content;
	}
};

inline const std::string& to_string(const data::any& value) {
	return value;
}

inline data::object& to_object(data::any& value) {
	return value;
}

inline const data::object& to_object(const data::any& value) {
	return value;
}

inline data::array& to_array(data::any& value) {
	return value;
}

inline const data::array& to_array(const data::any& value) {
	return value;
}

}

#endif
