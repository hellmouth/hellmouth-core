// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_REMOVER
#define HELLMOUTH_DATA_REMOVER

#include "hellmouth/data/segment.h"

namespace hellmouth::data {

/** A visitor class for removing a member of an object or array. */
class remover {
private:
	/** The key or index of the member to be removed. */
	segment _segment;
public:
	/** Construct remover object.
	 * @param segment a segment to provide the array index or object key
	 */
	remover(const segment& seg):
		_segment(seg) {}

        /** Apply this path segment to an array.
         * @param arr the array to which the segment is applied
         * @return the selected array element
         */
        void operator()(data::array& arr) const {
		arr.erase(arr.begin() + _segment.index(arr.size()));
        }

        /** Apply this path segment to an object.
         * @param obj the object to which the segment is applied
         * @return the selected object member
         */
        void operator()(data::object& obj) const {
		obj.erase(_segment.key());
        }

        /** Apply this path segment to any type of structured data.
         * @param value the value to which the segment is applied
         * @return the selected array element or object member
         */
        void operator()(data::any& value) const {
                value.visit(*this);
        }

        /** Attempt to apply this segment to any other type.
         * This is required so that a pointer segment can act as a
         * visitor.
         */
        template<typename T>
        void operator()(T&& value) const {
                throw std::invalid_argument(
                        "variant type is not indexable");
        }
};

}

#endif
