// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_INSERTER
#define HELLMOUTH_DATA_INSERTER

#include "hellmouth/data/segment.h"

namespace hellmouth::data {

/** A visitor class for inserting a member of an object or array.
 * The normal behaviour of a pointer segment is to navigate to a
 * specified object or array member, however the final segment of
 * a path is required to have different behaviour for most types
 * of patch operation. This class provides the required behaviour
 * for the final segment of an add operation, and can be used as
 * a component part of some other operations.
 */
class inserter {
private:
	/** The key or index for the member to be inserted. */
	segment _segment;

	/** The value to be inserted. */
	data::any _value;
public:
        /** Construct remover object.
         * @param segment a segment to provide the array index or object key
	 * @param value the value to be inserted
         */
	inserter(const segment& seg, const data::any& value):
		_segment(seg),
		_value(value) {}

        /** Insert into an array.
         * @param arr the array to insert into
         */
        void operator()(data::array& arr) const {
		data::array::size_type index = _segment.index(arr.size());
		arr.insert(arr.begin() + index, _value);
	}

        /** Apply insert into an object.
         * @param obj the object to insert into
         */
        void operator()(data::object& obj) const {
		obj[_segment.key()] = _value;
        }

        /** Apply this path segment to any type of structured data.
         * @param value the value to which the segment is applied
         * @return the selected array element or object member
         */
        void operator()(data::any& value) const {
                value.visit(*this);
        }

        /** Attempt to apply this segment to any other type.
         * This is required so that a pointer segment can act as a
         * visitor.
         */
        template<typename T>
        void operator()(T&& value) const {
                throw std::invalid_argument(
                        "variant type is not indexable");
        }
};

}

#endif
