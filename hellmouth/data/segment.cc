// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>
#include <utility>

#include "hellmouth/data/segment.h"

namespace hellmouth::data {

array::size_type segment::index(array::size_type size) const {
	if (_token == "-") {
		return size;
	}
	if (_token.empty()) {
		throw std::invalid_argument("numeric array index expected");
	}
	for (char ch : _token) {
		if (!isdigit(ch)) {
			throw std::invalid_argument("numeric array index expected");
		}
	}

	// Have already established that index cannot be negative,
	// since it can only contain digits.
	unsigned long long index = std::stoll(_token);

	// It is acceptable for index == size under some circumstances,
	// for example when performing an add operation within a patch.
	if (index > size) {
		throw std::out_of_range("numeric array index out of range");
	}
	return index;
}

}
