// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_NULL
#define HELLMOUTH_DATA_NULL

namespace hellmouth::data {

/** A structured data class for representing the null value. */
class null_t {
public:
	explicit null_t() = default;
};

constexpr bool operator==(null_t, null_t) noexcept {
	return true;
}

constexpr std::strong_ordering operator<=>(null_t, null_t) noexcept {
	return std::strong_ordering::equal;
}

/** The null value. */
inline constexpr null_t null;

}

#endif
