// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <functional>

#include "hellmouth/data/operation.h"
#include "hellmouth/data/operations/add.h"
#include "hellmouth/data/operations/remove.h"
#include "hellmouth/data/operations/replace.h"
#include "hellmouth/data/operations/move.h"
#include "hellmouth/data/operations/copy.h"
#include "hellmouth/data/operations/test.h"

namespace hellmouth::data {

std::unique_ptr<operation> operation::make_unique(const data::object& spec) {
	typedef std::unique_ptr<operation> return_type;
	typedef std::function<return_type(const data::object& spec)> handler;

	static std::map<std::string, handler> operations = {
		{"add", [](const data::object& spec)->return_type {
			return std::make_unique<operations::add>(spec); }},
		{"remove", [](const data::object& spec)->return_type {
			return std::make_unique<operations::remove>(spec); }},
		{"replace", [](const data::object& spec)->return_type {
			return std::make_unique<operations::replace>(spec); }},
		{"move", [](const data::object& spec)->return_type {
			return std::make_unique<operations::move>(spec); }},
		{"copy", [](const data::object& spec)->return_type {
			return std::make_unique<operations::copy>(spec); }},
		{"test", [](const data::object& spec)->return_type {
			return std::make_unique<operations::test>(spec); }}};

	const std::string& optype = spec.at("op");
	auto f = operations.find(optype);
	if (f == operations.end()) {
		throw std::invalid_argument("unrecognised patch operation");
	}
	return f->second(spec);
}

path_attribute::path_attribute(const data::object& spec):
	_path(data::pointer(spec.at("path"))) {}

from_attribute::from_attribute(const data::object& spec):
	_from(data::pointer(spec.at("from"))) {}

value_attribute::value_attribute(const data::object& spec):
	_value(spec.at("value")) {}

}
