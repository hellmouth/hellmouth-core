// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/signed_integer.h"

namespace hellmouth::data {

void signed_integer::twos_complement() {
	size_type size = _content.size();
	chunk_type carry = 1;
	for (size_type i = 0; i != size; ++i) {
		double_chunk_type chunk = ~_content[i];
		chunk += carry;
		_content[i] = chunk;
		carry = chunk >> chunk_bits;
	}
	_extra = (~_extra) + carry;
}

void signed_integer::add_signx(chunk_type chunk, chunk_type carry) {
	size_type size = _content.size();
	chunk_type signx = -(chunk >> (unsigned_integer::chunk_bits -1));
	for (size_type i = 0; i != size; ++i) {
		double_chunk_type dchunk = _content[i];
		dchunk += chunk;
		dchunk += carry;
		_content[i] = dchunk;
		carry = dchunk >> chunk_bits;
		chunk = signx;
	}
	_extra += chunk;
	_extra += carry;
}

signed_integer::signed_integer(
	sign_type sign, unsigned_integer&& magnitude):
	_content(std::move(magnitude._content)) {

	if (_content.empty()) {
		_content.push_back(0);
	}
	if (sign) {
		twos_complement();
	}
}

signed_integer::content_type&& signed_integer::magnitude() && {
	if (sign()) {
		twos_complement();
	}
	if (_extra) {
		_content.push_back(_extra);
	}
	return std::move(_content);
}

}
