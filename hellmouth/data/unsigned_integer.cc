// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <algorithm>

#include "hellmouth/data/unsigned_integer.h"
#include "hellmouth/data/signed_integer.h"

namespace hellmouth::data {

conversion_error::conversion_error():
	std::overflow_error("integer outside range of destination type") {}

static_assert(sizeof(unsigned_integer::double_chunk_type) ==
	2 * sizeof(unsigned_integer::chunk_type));

unsigned_integer::unsigned_integer(std::string_view intstr, int base) {
	if (base == 0) {
		if (intstr.starts_with("0x") || intstr.starts_with("0X")) {
			base = 16;
			intstr.remove_prefix(2);
		} else if (intstr.starts_with("0")) {
			base = 8;
			intstr.remove_prefix(1);
		} else {
			base = 10;
		}
	}

	if (base < 2 || base > 36) {
		throw std::domain_error("invalid base");
	}
	if (intstr.empty()) {
		throw std::invalid_argument("digit expected");
	}

	while (!intstr.empty()) {
		(*this) *= chunk_type(base);
		char ch = intstr.front();

		int digit;
		if (isdigit(ch)) {
			digit = ch - '0';
		} else if (isupper(ch)) {
			digit = (ch - 'A') + 10;
		} else if (islower(ch)) {
			digit = (ch - 'a') + 10;
		} else {
			throw std::invalid_argument("digit expected");
		}

		if (digit >= base) {
			throw std::invalid_argument("digit expected");
		}
		(*this) += chunk_type(digit);
		intstr.remove_prefix(1);
	}
}

unsigned_integer& unsigned_integer::operator=(signed_integer&& that) {
	_content = std::move(std::move(that).magnitude());
	canonicalise();
	return *this;
}

void unsigned_integer::canonicalise() {
	while (!_content.empty() && _content.back() == 0) {
		_content.pop_back();
	}
}

unsigned_integer& unsigned_integer::operator+=(chunk_type addend) {
	size_type size = _content.size();

	chunk_type carry = addend;
	for (size_type i = 0; i != size; ++i) {
		double_chunk_type dchunk = _content[i];
		dchunk += carry;
		_content[i] = dchunk;
		carry = dchunk >> chunk_bits;
	}
	if (carry) {
		_content.push_back(carry);
	}
	return *this;
}

unsigned_integer& unsigned_integer::operator*=(chunk_type multiplier) {
	size_type size = _content.size();

	if (multiplier == 0) {
		_content.clear();
		return *this;
	}

	chunk_type carry = 0;
	for (size_type i = 0; i != size; ++i) {
		double_chunk_type dchunk = _content[i];
		dchunk *= multiplier;
		dchunk += carry;
		_content[i] = dchunk;
		carry = dchunk >> chunk_bits;
	}
	if (carry) {
		_content.push_back(carry);
	}
	return *this;
}

unsigned_integer& unsigned_integer::operator/=(chunk_type divisor) {
	if (divisor == 0) {
		throw std::domain_error("divison by zero");
	}
	size_type size = _content.size();

	chunk_type remainder = 0;
	for (size_t i = size - 1; i != npos; --i) {
		double_chunk_type dchunk = remainder;
		dchunk <<= chunk_bits;
		dchunk += _content[i];
		remainder = dchunk % divisor;
		dchunk /= divisor;
		_content[i] = dchunk;
	}
	canonicalise();
	return *this;
}

unsigned_integer::chunk_type unsigned_integer::operator%(
	chunk_type divisor) const {

	if (divisor == 0) {
		throw std::domain_error("divison by zero");
	}
	size_type size = _content.size();

	chunk_type remainder = 0;
	for (size_t i = size - 1; i != npos; --i) {
		double_chunk_type dchunk = remainder;
		dchunk <<= chunk_bits;
		dchunk += _content[i];
		remainder = dchunk % divisor;
	}
	return remainder;
}

std::strong_ordering unsigned_integer::operator<=>(
	const unsigned_integer& that) const {

	const content_type& content = that._content;
	if (_content.size() != content.size()) {
		return _content.size() <=> content.size();
	}
	size_type size = _content.size();
	for (size_type i = size - 1; i != npos; --i) {
		if (_content[i] != content[i]) {
			return _content[i] <=> content[i];
		}
	}
	return std::strong_ordering::equal;
}

std::string to_string(unsigned_integer value, int base) {
	if (base < 2 || base > 36) {
		throw std::domain_error("invalid base");
	}

	std::string result;
	while (!value.empty()) {
		int digit = value % base;
		value /= base;
		if (digit < 10) {
			result.push_back('0' + digit);
		} else {
			result.push_back('a' + digit - 10);
		}
	}
	if (result.empty()) {
		result.push_back('0');
	}
	reverse(result.begin(), result.end());
	return result;
}

}
