// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>

#include "hellmouth/data/pointer.h"

namespace hellmouth::data {

static std::string unescape(const std::string& escaped) {
	auto f = escaped.find('~');
	if (f == std::string::npos) {
		return escaped;
	}

	std::string unescaped;
	size_t i = 0;
	while (f != std::string::npos) {
		unescaped.append(escaped, i, f);
		if (f + 1 >= escaped.length()) {
			throw std::invalid_argument(
				"invalid escape sequence in structured data pointer");
		}
		if (escaped[f + 1] == '0') {
			unescaped.push_back('~');
		} else if (escaped[f + 1] == '1') {
			unescaped.push_back('/');
		} else {
			throw std::invalid_argument(
				"invalid escape sequence in structured data pointer");
		}
		i = f + 2;
		f = escaped.find('~', i);
	}
	unescaped.append(escaped, i);
	return unescaped;
}

static std::string escape(const std::string& unescaped) {
	auto f = unescaped.find_first_of("/~");
	if (f == std::string::npos) {
		return unescaped;
	}

	std::string escaped;
	size_t i = 0;
	while (f != std::string::npos) {
		escaped.append(unescaped, i, f - i);
		escaped.push_back('~');
		if (unescaped[f] == '~') {
			escaped.push_back('0');
		} else {
			escaped.push_back('1');
		}
		i = f + 1;
		f = unescaped.find_first_of("/~", i);
	}
	escaped.append(unescaped, i);
	return escaped;
}

pointer::pointer(std::string_view ptrstr) {
	while (!ptrstr.empty()) {
		if (ptrstr.front() != '/') {
			throw std::invalid_argument(
				"missing forward slash in structured data pointer");
		}
		ptrstr.remove_prefix(1);

		size_t f = ptrstr.find('/');
		std::string token(ptrstr, 0, f);
		_segments.push_back(segment(unescape(token)));
		ptrstr.remove_prefix(token.length());
	}
}

pointer::operator std::string() const {
	// The estimated length will be incorrect if any characters are escaped,
	// but this is acceptable since it is used only to optimise the string
	// capacity of the result. Since escaped characters are expected to be
	// rare, it is likely that an exact calculation would consume more time
	// than it saved.
	size_t length = 0;
	for (const std::string& segstr : _segments) {
		length += segstr.length() + 1;
	}

	std::string ptrstr;
	ptrstr.reserve(length);
	for (const std::string& segstr : _segments) {
		ptrstr.push_back('/');
		ptrstr.append(escape(segstr));
	}
	return ptrstr;
}

void pointer::push_back(segment&& segment) {
	_segments.push_back(segment);
}

}
