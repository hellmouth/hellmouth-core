// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_INTEGER
#define HELLMOUTH_DATA_INTEGER

#include <string_view>
#include <type_traits>
#include <limits>
#include <bit>
#include <atomic>

#include "hellmouth/data/unsigned_integer.h"

namespace hellmouth::data {

class shared_integer;

/** A structured data class to represent an arbitrary-size integer.
 * This class is intended to be partially thread-safe:
 * - As with most containers, it is not safe for multiple threads to access
 *   the same instance concurrently, except in the case of multiple readers
 *   and no writers.
 * - However, it is safe for multiple threads to access separate instances
 *   which refer to the same reference-counted data, provided that:
 *   - suitable memory barriers are employed whenever instances are passed
 *     from one thread to another (to ensure that the recipient sees the
 *     content in a consistent and up-to-date state, and
 *   - non-const operations are presumed to invalidate all iterators,
 *     references and pointers obtained from the instance in question
 *     (since the operation could have triggered a copy-on-write).
 */
class integer {
public:
	/** A type representing one chunk of the magnitude. */
	typedef unsigned_integer::chunk_type chunk_type;

	/** A signed type corresponding to one chunk of the magnitude. */
	typedef std::make_signed_t<chunk_type> signed_chunk_type;

	/** A type for representing the full value of the magnitude. */
	typedef unsigned_integer magnitude_type;

	/** A type for representing the sign. */
	typedef bool sign_type;
private:
	/** The shared content of this integer. */
	shared_integer* _shared;

	/** Ensure value is not shared with another instance. */
	inline void unshare();
public:
	// Construct/copy/destroy/assign

	/** Construct data::integer with default value of zero. */
	inline integer();

	/** Copy-construct data::integer.
	 * @param that the integer to be copied
	 */
	inline integer(const integer& that);

	/** Construct from sign and magnitude.
	 * @param sign the required sign
	 * @param magnitude the required magnitude
	 */
	inline integer(sign_type sign, magnitude_type&& magnitude);

	/** Construct from any integer type.
	 * @param value the required value
	 */
	template<typename T>
	requires std::integral<T>
	inline integer(const T& value);

	/** Construct from character string.
	 * @param intstr the character string to be parsed
	 * @param base the base of the integer
	 */
	integer(std::string_view intstr, int base = 10);

	/** Destroy data::integer. */
	inline ~integer();

	/** Copy-assign data::integer.
	 * @param that the integer to be copied
	 * @return a reference to this
	 */
	inline integer& operator=(const integer& that);

	/** Assign from any integer type.
	 * @param value the required value
	 */
	template<typename T>
	requires std::integral<T>
	inline integer& operator=(const T& value);

	// Conversions

	/** Convert to bool.
	 * @return false if zero, otherwise true
	 */
	inline operator bool() const;

	/** Convert to any integer type.
	 * @return the converted value
	 * @throws std::overflow_error if the value cannot be represented
	 *  by the specified type
	 */
	template<typename T>
	requires (std::integral<T> && !std::same_as<T, bool>)
	inline operator T() const;

	// Arithmetic operations

	/** Negate this integer.
	 * @return the negated value
	 */
	integer operator-() const;

	/** Add a signed single-chunk integer.
	 * @param addend the value to be added
	 * @return a reference to this, containing the sum
	 */
	integer& operator+=(signed_chunk_type addend);

	/** Subtract a signed single-chunk integer.
	 * @param subtrahend the value to be subtracted
	 * @return a reference to this, containing the difference
	 */
	integer& operator-=(signed_chunk_type subtrahend);

	/** Multiply by a single-chunk integer.
	 * @param multiplier the value by which to multiply
	 * @return a reference to this, containing the product
	 */
	integer& operator*=(signed_chunk_type multiplier);

	/** Divide by a single-chunk integer.
	 * @param divisor the value by which to divide
	 * @return a reference to this, containing the quoitant
	 */
	integer& operator/=(signed_chunk_type divisor);

	/** Reduce modulo a single-chunk integer.
	 * @param divisor the value by which to divide
	 * @return a reference to this, now containing the remainder
	 */
	integer& operator%=(signed_chunk_type divisor) {
		*this = *this % divisor;
		return *this;
	}

	/** Add a signed single-chunk integer.
	 * @param addend the value to be added
	 * @return the sum
	 */
	integer operator+(signed_chunk_type addend) {
		integer sum = *this;
		sum += addend;
		return sum;
	}

	/** Subtract a signed single-chunk integer.
	 * @param subtrahend the value to be subtracted
	 * @return the difference
	 */
	integer operator-(signed_chunk_type subtrahend) {
		integer difference = *this;
		difference -= subtrahend;
		return difference;
	}

	/** Multiply by a single-chunk integer.
	 * @param multiplier the value by which to multiply
	 * @return the product
	 */
	integer operator*(signed_chunk_type multiplier) {
		integer product = *this;
		product *= multiplier;
		return product;
	}

	/** Divide by a single-chunk integer.
	 * @param divisor the value by which to divide
	 * @return the quoitant
	 */
	integer operator/(signed_chunk_type divisor) {
		integer quoitant = *this;
		quoitant /= divisor;
		return quoitant;
	}

	/** Reduce modulo a single-chunk integer.
	 * @param divisor the value by which to divide
	 * @return the remainder
	 */
	integer operator%(signed_chunk_type divisor) const;

	// Modifiers

	/** Swap value with another integer.
	 * @param that the integer with which to swap
	 */
	inline void swap(integer& that);

	// Comparison operations

	/** Compare for equality with another integer.
	 * @return true if the two values are equal, otherwise false
	 */
	bool operator==(const integer& that) const;

	/** Perform a three-way comparison with another shared integer.
	 * @return the relative order of the two values
	 */
	std::strong_ordering operator<=>(const integer& that) const;

	// Additional functions

	/** Test whether this integer is negative.
	 * @return true if negative, otherwise false
	 */
	inline bool negative() const;

	/** Get the number of live references to this content.
	 * @return the number of live references
	 */
	inline size_t use_count() const;

	/** Dump the raw content of this integer as hex.
	 * @param out the output stream to dump to
	 */
	void dump(std::ostream& out) const;
};

/** A class for holding the reference-counted content of a data::integer.
 * The substantive content of a shared integer is mutable, but must not be
 * mutated if the reference count is greater than one.
 */
class shared_integer {
	friend integer;
private:
	/** A type for representing the mangitude. */
	typedef unsigned_integer magnitude_type;

	/** A type for representing the sign. */
	typedef integer::sign_type sign_type;

	/** The magnitude of the integer. */
	magnitude_type _magnitude;

	/** The sign of the integer. */
	sign_type _sign = false;

	/** The number of live references to this content. */
	std::atomic<size_t> _use_count = 0;
public:
	/** Construct shared integer with default value of zero.
	 * The reference count also defaults to zero.
	 */
	shared_integer() = default;

	/** Copy-construct a shared integer.
	 * The substantive content is copied, but the reference count
	 * receives its default value of zero.
	 *
	 * @param that the shared integer to be copied
	 */
	shared_integer(const shared_integer& that):
		_magnitude(that._magnitude),
		_sign(that._sign) {}

	/** Construct from sign and magnitude.
	 * @param sign the required sign
	 * @param magnitude the required magnitude
	 */
	shared_integer(sign_type sign, magnitude_type&& magnitude):
		_magnitude(std::move(magnitude)),
		_sign(sign) {}

	/** Destroy shared integer. */
	~shared_integer() = default;

	shared_integer& operator=(const shared_integer&) = delete;

	/** Get the magnitude of this integer (non-const).
	 * @return the magnitude
	 */
	magnitude_type& magnitude() {
		return _magnitude;
	}

	/** Get the magnitude of this integer (const).
	 * @return the magnitude
	 */
	const magnitude_type& magnitude() const {
		return _magnitude;
	}

	/** Get the sign of this integer.
	 * @return true if negative, otherwise false.
	 */
	sign_type sign() const {
		return _sign;
	}

	/** Canonicalise this integer. */
	void canonicalise() {
		if (_magnitude.empty()) {
			_sign = false;
		}
	};

	/** Get the number of live references to this content.
	 * @return the number of live references
	 */
	size_t use_count() const {
		return _use_count;
	}
};

void integer::unshare() {
	if (_shared->_use_count > 1) {
		// Copy-on-write is necessary.
		// This thread has presumed exclusive possession of at
		// least one reference, therefore whilst _use_count might
		// decrement during execution of the following (and
		// perhaps obviate the need for a copy-on-write), it should
		// not fall below one. The existing content is therefore
		// safe to copy.
		shared_integer* shared = new shared_integer(*_shared);

		// Newly constructed, therefore the current thread has
		// exclusive possession.
		++shared->_use_count;

		// As noted above, this should not have fallen below one,
		// however it might since have become equal to one. There
		// is therefore a chance that the copy-on-write was (with
		// hindsight) unnecessary, in which case one of the new
		// copies should be deleted.
		if (--_shared->_use_count == 0) {
			// Reference count was previously one, therefore
			// this was the only live reference, therefore
			// the current thread has exclusive possession.
			delete _shared;
		}
		_shared = shared;
	}
}

integer::integer():
	_shared(new shared_integer) {

	// Shared content is newly-constructed, therefore this is the only
	// live reference, therefore the current thread has exclusive
	// possession.
	++_shared->_use_count;
}

integer::integer(const integer& that):
	_shared(that._shared) {

	// The current thread has presumed exclusive possession of one
	// reference, therefore it can safely create another reference.
	++_shared->_use_count;
}

integer::integer(sign_type sign, magnitude_type&& magnitude):
	_shared(new shared_integer(sign, std::move(magnitude))) {

	++_shared->_use_count;
}

template<typename T>
requires std::integral<T>
integer::integer(const T& value) {
	typedef std::make_unsigned_t<T> U;
	U uvalue = value;
	sign_type sign = false;
	if (value < 0) {
		sign = true;
		uvalue = -uvalue;
	}
	magnitude_type magnitude(uvalue);

	_shared = new shared_integer(sign, std::move(magnitude));
	++_shared->_use_count;
}

integer::~integer() {
	if (--_shared->_use_count == 0) {
		// Reference count was previously one, therefore this
		// was the only live reference, therefore the current
		// thread has exclusive possession.
		delete _shared;
	}
}

integer& integer::operator=(const integer& that) {
	if (this != &that) {
		if (--_shared->_use_count == 0) {
			// Reference count was previously one, therefore
			// this was the only live reference, therefore
			// the current thread has exclusive possession.
			delete _shared;
		}
		_shared = that._shared;
		// This thread already has presumed exclusive possession
		// of at least one reference, therefore the reference count
		// should be non-zero and safe to increment.
		++_shared->_use_count;
	}
	return *this;
}

template<typename T>
requires std::integral<T>
integer& integer::operator=(const T& value) {
	integer result(value);
	return (*this = result);
}

integer::operator bool() const {
	return !_shared->magnitude().empty();
}

template<typename T>
requires (std::integral<T> && !std::same_as<T, bool>)
integer::operator T() const {
	// Attempt to convert the magnitude to the destination type
	// if it is unsigned, or to the corresponding unsigned type
	// if it is signed. Note that:
	//
	// - for unsigned results, this will not catch overflows due to
	//   the value being negative;
	// - for signed results, this will not catch overflows due to
	//   the smaller range of magnitudes allowed by the signed type.
	typedef std::make_unsigned_t<T> U;
	U acc = _shared->magnitude();

	// Handle negative values:
	//
	// - for unsigned results this is an overflow (and the only type
	//   of overflow not already handled);
	// - for signed types, calculate what the twos complement value
	//   would be if the result fits in the destination type (but
	//   without regard to whether it actually does).
	if (_shared->sign()) {
		if (!std::is_signed_v<T>) {
			throw conversion_error();
		}
		acc = -acc;
	}

	// Convert to the required destination type.
	// For unsigned types this operation has no effect on what is
	// already the correct result.
	//
	// For signed types this will give the correct result if the
	// destination type is capable of representing the correct result.
	// If that is not the case then result will be incorrect, and
	// specifically, is certain to have the wrong sign.
	T result = std::bit_cast<T>(acc);

	// Detect the case where the conversion above gave a signed
	// result with the wrong sign.
	if ((result < 0) ^ _shared->sign()) {
		throw conversion_error();
	}

	return result;
}

void integer::swap(integer& that) {
	std::swap(_shared, that._shared);
}

bool integer::negative() const {
	return _shared->_sign;
}

size_t integer::use_count() const {
	return _shared->use_count();
}

/** Convert an integer to a decimal string.
 * @param value the integer to be converted
 * @return the resulting string
 */
std::string to_string(integer value);

}

#endif
