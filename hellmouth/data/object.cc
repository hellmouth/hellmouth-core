// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/object.h"

namespace hellmouth::data {

void object::merge(const data::object& patch) {
	for (const auto& [key, value]: patch) {
		if (value.is_null()) {
			this->erase(key);
		} else {
			(*this)[key].merge(value);
		}
	}
}

}
