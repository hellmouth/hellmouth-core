// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_SIGNED_INTEGER
#define HELLMOUTH_DATA_SIGNED_INTEGER

#include "hellmouth/data/unsigned_integer.h"

#include <vector>
#include <limits>
#include <type_traits>

namespace hellmouth::data {

class unsigned_integer;

/** A class to represent an arbitrary-sized signed integer.
 * This class is intended for temporarily converting the content of a
 * data::integer into twos-complement format in order to more conveniently
 * perform additive operations.
 *
 * The value is encoded as a sequence of chunks, which concatenate in
 * little-endian order to form a twos-complement value. The sequence
 * consists of two parts:
 *
 * - a variable-length section, borrowed from the unsigned_integer which
 *   previously held the magnitude.
 * - an extension chunk, which is stored separately and is used to
 *   capture any overflow at the most significant end of the value.
 *
 * The value is not required to be in any particular canonical form,
 * however there must be at least one chunk in addition to the
 * extension chunk.
 */
class signed_integer {
	friend unsigned_integer;
public:
	/** A type for representing one chunk of the integer. */
	typedef unsigned_integer::chunk_type chunk_type;

	/** A signed type corresponding to one chunk of the magnitude. */
	typedef std::make_signed_t<chunk_type> signed_chunk_type;

	/** A type for representing two adjacent chunks of the integer. */
	typedef unsigned_integer::double_chunk_type double_chunk_type;

	/** The number of bits in a chunk. */
	static constexpr auto chunk_bits =
		std::numeric_limits<chunk_type>::digits;

	/** The highest possible chunk value. */
	static constexpr chunk_type chunk_max =
		std::numeric_limits<chunk_type>::max();

	/** A mask corresponding to the sign bit if a chunk is
	 * interpreted as twos-complement. */
	static constexpr chunk_type chunk_sign_mask = (chunk_max >> 1) + 1;

	/** A type for representing the integer as a sequence of chunks. */
	typedef std::vector<chunk_type> content_type;

	/** A type for indexing into the sequence of chunks. */
	typedef content_type::size_type size_type;

	/** A type for representing the sign.
	 * This is true for negative values, false for non-negative. */
	typedef bool sign_type;
private:
	/** A little-endian sequence of chunks which, when combined with the
	 * extension chunk, correspond to the value of the integer in
	 * twos-complement form. */
	content_type _content;

	/** The extension chunk, which acts as an extra member of the
	 * content located at the most significant end. */
	chunk_type _extra = 0;

	/** Replace the value with its twos-complement.
	 * Note that this is an unconditional operation, performed
	 * without regard to the current sign.
	 */
	void twos_complement();

	/** Add a sign-extended chunk.
	 * @param chunk the chunk to be added
	 * @param carry a single-bit value to be carried in to the addition
	 */
	void add_signx(chunk_type chunk, chunk_type carry);
public:
	/** Construct from sign and magnitude (moved).
	 * @param sign the required sign
	 * @param magnitude the required magnitude
	 */
	signed_integer(sign_type sign, unsigned_integer&& magnitude);

	/** Get the sign.
	 * @return true if negative, otherwise false
	 */
	sign_type sign() const {
		return _extra & chunk_sign_mask;
	}

	/** Get the magnitude (destructively).
	 * @return the magnitude of this integer
	 */
	content_type&& magnitude() &&;

	/** Add a signed chunk-sized value
	 * @param addend the value to be added
	 * @return a reference to this, now containing the sum
	 */
	signed_integer& operator+=(signed_chunk_type addend) {
		add_signx(addend, 0);
		return *this;
	}

	/** Subtract a signed chunk-sized value
	 * @param subtrahend the value to be subtracted
	 * @return a reference to this, now containing the difference
	 */
	signed_integer& operator-=(signed_chunk_type subtrahend) {
		add_signx(~chunk_type(subtrahend), 1);
		return *this;
	}
};

}

#endif
