// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/pointer.h"
#include "hellmouth/data/patch.h"

namespace hellmouth::data {

patch::patch(const data::array& spec) {
	for (const auto& opspec : spec) {
		push_back(opspec);
	}
}

void patch::operator()(data::any& root) const {
	for (const auto& operation : _operations) {
		(*operation)(root);
	}
}

}
