// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_UNSIGNED_INTEGER
#define HELLMOUTH_DATA_UNSIGNED_INTEGER

#include <cstdint>
#include <stdexcept>
#include <vector>
#include <string>
#include <string_view>
#include <limits>
#include <concepts>

namespace hellmouth::data {

/** Report that a value is not representable in the required type. */
class conversion_error:
	public std::overflow_error {
public:
	/** Construct conversion error. */
	conversion_error();
};

class signed_integer;

/** A class to represent an arbitrary-sized unsigned integer.
 * The value is encoded as a sequence of chunks, which concatenate in
 * little-endian order to form a pure binary value.
 *
 * The canonical representation uses a minimal number of chunks (and
 * consequently, no chunks in the case where the value is zero).
 * Unless otherwise documented, public member functions of this class:
 *
 * - Have unspecified behaviour if they encounter any class instances which
 *   are not in canonical form on entry.
 * - Will otherwise ensure that any class instances produced or modified
 *   are in canonical form on exit.
 */
class unsigned_integer {
	friend signed_integer;
public:
	/** A type for representing one chunk of the integer. */
	typedef uint32_t chunk_type;

	/** A type for representing two adjacent chunks of the integer. */
	typedef uint64_t double_chunk_type;

	/** The number of bits in a chunk. */
	static constexpr auto chunk_bits =
		std::numeric_limits<chunk_type>::digits;

	/** A type for representing the value as a sequence of chunks. */
	typedef std::vector<chunk_type> content_type;

	/** A type for indexing into the sequence of chunks. */
	typedef content_type::size_type size_type;

	/** The value which numerically precedes an index of zero. */
	static constexpr size_type npos = -1;
private:
	/** The value as a little-endian sequence of chunks. */
	content_type _content;
public:
	/** Construct with default value of zero. */
	unsigned_integer() = default;

	/** Construct from any unsigned integer type.
	 * @param value the required value
	 */
	template<typename T>
	requires std::unsigned_integral<T>
	unsigned_integer(const T& value);

	/** Construct from character sequence.
	 * @param intstr the sequence of characters to be parsed
	 * @param base the required base, or 0 to determine from prefix
	 */
	unsigned_integer(std::string_view intstr, int base = 0);

	/** Assign from a data::signed_integer (move).
	 * If the argument is negative, its absolute value is taken.
	 * If there is a need for the sign of the argument to be preserved,
	 * that must be done before this function is called.
	 *
	 * @param that the value to be assigned
	 */
	unsigned_integer& operator=(signed_integer&& that);

	/** Get the underlying content.
	 * The content is not required to be in canonical form.
	 *
	 * @return the underlying content
	 */
	operator const content_type&() const {
		return _content;
	}

	/** Convert to any unsigned integer type.
	 * @return the converted value
	 * @throws std::overflow_error if the value cannot be represented
	 *  by the specified type
	 */
	template<typename T>
	requires (std::unsigned_integral<T> && !std::same_as<T, bool>)
	operator T() const;

	/** Determine whether the sequence of chunks is empty.
	 * Provided that the value is in canonical form, this is
	 * equivalent to testing for equality with zero. *
	 * @return true if no chunks, otherwise false
	 */
	size_type empty() const {
		return _content.empty();
	}

	/* Append a chunk to this unsigned integer.
	 * The content is not required to be in canonical form on entry,
	 * nor is it guaranteed to be in that form on exit.
	 *
	 * @param chunk the chunk to be appended
	 */
	void push_back(const chunk_type& chunk) {
		_content.push_back(chunk);
	}

	/* Add a single-chunk value.
	 * @param addend the value to be added
	 * @return a reference to this, now containing the sum
	 */
	unsigned_integer& operator+=(chunk_type addend);

	/* Multiply by a single-chunk value.
	 * @param multiplier the value by which to multiply
	 * @return a reference to this, now containing the product
	 */
	unsigned_integer& operator*=(chunk_type multiplier);

	/** Divide by a single-chunk value.
	 * The result is truncated towards zero.
	 *
	 * @param divisor the value by which to divide
	 * @return a reference to this, now containing the quoitant
	 */
	unsigned_integer& operator/=(chunk_type divisor);

	/** Reduce modulo a single-chunk value.
	 * @param divisor the value by which to divide
	 * @return a reference to this, now containing the remainder
	 */
	unsigned_integer& operator%=(chunk_type divisor) {
		*this = *this % divisor;
		return *this;
	}

	/* Add a single-chunk value.
	 * @param addend the value to be added
	 * @return the sum
	 */
	unsigned_integer operator+(chunk_type addend) const {
		unsigned_integer sum = *this;
		sum += addend;
		return sum;
	}

	/* Multiply by a single-chunk value.
	 * @param multiplier the value by which to multiply
	 * @return a reference to this, now containing the product
	 */
	unsigned_integer operator*(chunk_type multiplier) const {
		unsigned_integer product = *this;
		product *= multiplier;
		return product;
	}

	/** Divide by a single-chunk value.
	 * The result is truncated towards zero.
	 *
	 * @param divisor the value by which to divide
	 * @return a reference to this, now containing the quoitant
	 */
	unsigned_integer operator/(chunk_type divisor) const {
		unsigned_integer quoitant = *this;
		quoitant /= divisor;
		return quoitant;
	}

	/** Calculate the remainder, modulo a single-chunk value.
	 * @param divisor the value by which to divide
	 * @return the remainder
	 */
	chunk_type operator%(chunk_type divisor) const;

	/** Compare for equality with another unsigned integer.
	 * @return true if the two values are equal, otherwise false
	 */
	bool operator==(const unsigned_integer& that) const = default;

	/** Perform a three-way comparison with another unsigned integer.
	 * @return the relative order of the two magnitudes
	 */
	std::strong_ordering operator<=>(const unsigned_integer& that) const;

	/** Canonicalise this integer.
	 * The content is not required to be in canonical form on entry,
	 * but will be on exit.
	 */
	void canonicalise();
};

template<typename T>
requires std::unsigned_integral<T>
unsigned_integer::unsigned_integer(const T& value) {
	if (sizeof(T) > sizeof(chunk_type)) {
		T remainder = value;
		while (remainder != 0) {
			_content.push_back(remainder);
			remainder >>= chunk_bits;
		}
	} else if (value != 0) {
		// Handled separately to avoid shifting by
		// more than the width of chunk_type.
		_content.push_back(value);
	}
}

template<typename T>
requires (std::unsigned_integral<T> && !std::same_as<T, bool>)
unsigned_integer::operator T() const {
	T acc = 0;
	if (sizeof(T) > sizeof(chunk_type)) {
		size_type size = _content.size();
		for (size_type i = size - 1; i != npos; --i) {
			T before = acc;
			acc <<= chunk_bits;
			if ((acc >> chunk_bits) != before) {
				throw conversion_error();
			}
			acc |= _content[i];
		}
	} else {
		switch (_content.size()) {
		case 0:
			break;
		case 1:
			acc = _content.front();
			if (acc != _content.front()) {
				throw conversion_error();
			}
			break;
		default:
			throw conversion_error();
		}
	}
	return acc;
}

/** Convert an unsigned integer to a string.
 * @param value the unsigned integer to be converted
 * @param base the required base
 * @return the resulting string
 */
std::string to_string(unsigned_integer value, int base = 10);

}

#endif
