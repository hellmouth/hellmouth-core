// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_ARRAY
#define HELLMOUTH_DATA_ARRAY

#include <vector>
#include <atomic>

namespace hellmouth::data {

class any;
class shared_array;

/** A structured data class to represent an array.
 * This class is intended to be partially thread-safe:
 * - As with most containers, it is not safe for multiple threads to access
 *   the same instance concurrently, except in the case of multiple readers
 *   and no writers.
 * - However, it is safe for multiple threads to access separate instances
 *   which refer to the same reference-counted data, provided that:
 *   - suitable memory barriers are employed whenever instances are passed
 *     from one thread to another (to ensure that the recipient sees the
 *     content in a consistent and up-to-date state, and
 *   - non-const operations are presumed to invalidate all iterators,
 *     references and pointers obtained from the instance in question
 *     (since the operation could have triggered a copy-on-write).
 */
class array {
public:
	// Types

	/** The type of the underlying content. */
	typedef std::vector<data::any> content_type;

	/** The type of an array element. */
	typedef content_type::value_type value_type;

	/** The type of a pointer to an array element. */
	typedef content_type::pointer pointer;

	/** The type of a const pointer to an array element. */
	typedef content_type::const_pointer const_pointer;

	/** The type of a reference to an array element. */
	typedef content_type::reference reference;

	/** The type of a const reference to an array element. */
	typedef content_type::const_reference const_reference;

	/** The type of an array index. */
	typedef content_type::size_type size_type;

	/** The type of the difference between two array indices. */
	typedef content_type::difference_type difference_type;

	/** An iterator into an array. */
	typedef content_type::iterator iterator;

	/** A const iterator into an array. */
	typedef content_type::const_iterator const_iterator;

	/** A reverse iterator into an array. */
	typedef content_type::reverse_iterator reverse_iterator;

	/** A const reverse iterator into an array. */
	typedef content_type::const_reverse_iterator const_reverse_iterator;
private:
	/** The shared content of this array. */
	shared_array* _shared;

	/** Access the underlying content (non-const)
	 * This operator will perform a copy-on-write if required.
	 *
	 * @return the underlying content
	 */
	inline content_type& content();

	/** Access the underlying content (const).
	 * @return the underlying content
	 */
	inline const content_type& content() const;
public:
	// Construct/copy/destroy/assign

	/** Construct empty data::array. */
	inline array();

	/** Copy-construct data::array.
	 * @param that the array to be copied
	 */
	inline array(const array& that);

	/** Construct data::array.
	 * Parameter types are as for the underlying container.
	 */
	template <typename ...Args>
	inline array(Args&& ...args);

	/** Destroy data::array. */
	inline ~array();

	/** Copy-assign data::array.
	 * @param that the array to be copied
	 * @return a reference to this
	 */
	inline array& operator=(const array& that);

	/** Assign to data::array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline array& assign(Args&& ...args);

	// Conversions

	/** Access the underluing content (non-const).
	 * This operator will perform a copy-on-write if required.
	 *
	 * @return the underlying content
	 */
	inline operator content_type&();

	/** Access the underlying content (const).
	 * @return the underlying content
	 */
	inline operator const content_type&() const;

	/** Make a const reference to this array.
	 * It is not an error if the array acted upon is already const.
	 *
	 * @return the resulting const reference
	 */
	const array& constant() const {
		return *this;
	}

	// Iterator support

	/** Obtain an iterator for beginning forward iteration.
	 * @return the iterator
	 */
	inline iterator begin();

	/** Obtain a const iterator for beginning forward iteration.
	 * @return the iterator
	 */
	inline const_iterator begin() const;

	/** Obtain a const iterator for beginning forward iteration.
	 * @return the iterator
	 */
	inline const_iterator cbegin() const;

	/** Obtain an iterator for ending forward iteration.
	 * @return the iterator
	 */
	inline iterator end();

	/** Obtain a const iterator for ending forward iteration.
	 * @return the iterator
	 */
	inline const_iterator end() const;

	/** Obtain a const iterator for ending forward iteration.
	 * @return the iterator
	 */
	inline const_iterator cend() const;

	/** Obtain a reverse iterator for beginning reverse iteration.
	 * @return the iterator
	 */
	inline reverse_iterator rbegin();

	/** Obtain a const reverse iterator for beginning reverse iteration.
	 * @return the iterator
	 */
	inline const_reverse_iterator rbegin() const;

	/** Obtain a const reverse iterator for beginning reverse iteration.
	 * @return the iterator
	 */
	inline const_reverse_iterator crbegin() const;

	/** Obtain a reverse iterator for ending reverse iteration.
	 * @return the iterator
	 */
	inline reverse_iterator rend();

	/** Obtain a const reverse iterator for ending reverse iteration.
	 * @return the iterator
	 */
	inline const_reverse_iterator rend() const;

	/** Obtain a const reverse iterator for ending reverse iteration.
	 * @return the iterator
	 */
	inline const_reverse_iterator crend() const;

	// Capacity

	/** Test whether this array is empty.
	 * @return true if empty, otherwise false
	 */
	inline decltype(auto) empty() const;

	/** Get the number of elements in this array.
	 * @return the number of elements
	 */
	inline decltype(auto) size() const;

	/** Get the maximum number of elements this array could hold.
	 * @return the number of elements
	 */
	inline decltype(auto) max_size() const;

	/** Get the current reserved capacity of this array.
	 * @return the number of elements
	 */
	inline decltype(auto) capacity() const;

	/** Resize this array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) resize(Args&& ...args);

	/** Reserve capacity within this array.
	 * @param n the number of elements
	 */
	inline void reserve(size_type n);

	/** Release any unused capacity from this array.
	 * Note that since this function triggers a copy-on-write, it could
	 * potentially cause memory usage to increase.
	 */
	inline void shrink_to_fit();

	// Element access

	/** Access element at given index (unchecked, non-const).
	 * @param n the index of the element to be accessed
	 * @return a reference to the element
	 */
	inline data::any& operator[](size_type n);

	/** Access element at given index (unchecked, const).
	 * @param n the index of the element to be accessed
	 * @return a reference to the element
	 */
	inline const data::any& operator[](size_type n) const;

	/** Access element at given index (checked, non-const).
	 * @param n the index of the element to be accessed
	 * @return a reference to the element
	 */
	inline data::any& at(size_type n);

	/** Access element at given index (checked, const).
	 * @param n the index of the element to be accessed
	 * @return a reference to the element
	 */
	inline const data::any& at(size_type n) const;

	/** Access first element (non-const).
	 * @return a reference to the element
	 */
	inline data::any& front();

	/** Access first element (const).
	 * @return a reference to the element
	 */
	inline const data::any& front() const;

	/** Access last element (non-const).
	 * @return a reference to the element
	 */
	inline data::any& back();

	/** Access last element (const).
	 * @return a reference to the element
	 */
	inline const data::any& back() const;

	// Modifiers

	/** Emplace an element at the end of this array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) emplace_back(Args&& ...args);

	/** Append an element to this array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) push_back(Args&& ...args);

	/** Remove the last element from this array. */
	inline decltype(auto) pop_back();

	/** Emplace an element into this array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) emplace(Args&& ...args);

	/** Insert an element into this array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) insert(Args&& ...args);

	/** Remove an element from this array.
	 * Parameters and return types are as for the underlying container.
	 */
	template <typename ...Args>
	inline decltype(auto) erase(Args&& ...args);

	/** Swap content with another array.
	 * @param that the array with which to swap
	 */
	inline void swap(array& that);

	/** Clear all elements from this array. */
	inline void clear();

	// Additional functions

	/** Get the number of live references to this content.
	 * @return the number of live references
	 */
	inline size_t use_count() const;

	// Comparison functions.

	/** Compare arrays.
	 * @param that the object with which to compare
	 * @return true if equal, otherwise false
	 */
	inline bool operator==(const data::array& that) const;
};

}

#include "hellmouth/data/any.h"

namespace hellmouth::data {

/** A class for holding the reference-counted content of a data::array.
 * The substantive content of a shared array is mutable, but must not be
 * mutated if the reference count is greater than one.
 */
class shared_array {
	friend array;
public:
	/** The type of an array element. */
	typedef data::any value_type;

	/** The type of the underlying content. */
	typedef std::vector<value_type> content_type;
private:
	/** The underlying content. */
	content_type _content;

	/** The number of live references to this content. */
	std::atomic<size_t> _use_count = 0;
public:
	/** Construct empty shared array.
	 * The reference count defaults to zero.
	 */
	shared_array() = default;

	/** Copy-construct a shared array.
	 * The substantive content is copied, but the reference count
	 * receives its default value of zero.
	 *
	 * @param that the shared array to be copied
	 */
	shared_array(const shared_array& that):
		_content(that._content) {}

	/** Construct a shared array.
	 * Parameters are as for the underlying container.
	 */
	template <typename ...Args>
	shared_array(Args&& ...args):
		_content(std::forward<Args>(args)...) {}

	/** Destroy shared array. */
	~shared_array() = default;

	shared_array& operator=(const shared_array&) = delete;

	/** Access the underlying content (non-const).
	 * @return the underlying content
	 */
	operator content_type&() {
		return _content;
	}

	/** Access the underlying content (const).
	 * @return the underlying content
	 */
	operator const content_type&() const {
		return _content;
	}

	/** Get the number of live references to this content.
	 * @return the number of live references
	 */
	size_t use_count() const {
		return _use_count;
	}
};

array::content_type& array::content() {
	if (_shared->_use_count > 1) {
		// Copy-on-write is necessary.
		// This thread has presumed exclusive possession of at
		// least one reference, therefore whilst _use_count might
		// decrement during execution of the following (and
		// perhaps obviate the need for a copy-on-write), it should
		// not fall below one. The existing content is therefore
		// safe to copy.
		shared_array* shared = new shared_array(*_shared);

		// Newly constructed, therefore the current thread has
		// exclusive possession.
		++shared->_use_count;

		// As noted above, this should not have fallen below one,
		// however it might since have become equal to one. There
		// is therefore a chance that the copy-on-write was (with
		// hindsight) unnecessary, in which case one of the new
		// copies should be deleted.
		if (--_shared->_use_count == 0) {
			// Reference count was previously one, therefore
			// this was the only live reference, therefore
			// the current thread has exclusive possession.
			delete _shared;
		}
		_shared = shared;
	}
	return *_shared;
}

const array::content_type& array::content() const {
	return *_shared;
}

array::array():
	_shared(new shared_array) {

	// Shared content is newly-constructed, therefore this is the only
	// live reference, therefore the current thread has exclusive
	// possession.
	++_shared->_use_count;
}

array::array(const array& that):
	_shared(that._shared) {

	// The current thread has presumed exclusive possession of one
	// reference, therefore it can safely create another reference.
	++_shared->_use_count;
}

template <typename ...Args>
array::array(Args&& ...args):
	_shared(new shared_array(std::forward<Args>(args)...)) {

	// Shared content is newly-constructed, therefore this is the only
	// live reference, therefore the current thread has exclusive
	// possession.
	++_shared->_use_count;
}

array::~array() {
	if (--_shared->_use_count == 0) {
		// Reference count was previously one, therefore this
		// was the only live reference, therefore the current
		// thread has exclusive possession.
		delete _shared;
	}
}

array& array::operator=(const array& that) {
	if (this != &that) {
		if (--_shared->_use_count == 0) {
			// Reference count was previously one, therefore
			// this was the only live reference, therefore
			// the current thread has exclusive possession.
			delete _shared;
		}
		_shared = that._shared;
		// This thread already has presumed exclusive possession
		// of at least one reference, therefore the reference count
		// should be non-zero and safe to increment.
		++_shared->_use_count;
	}
	return *this;
}

template <typename ...Args>
array& array::assign(Args&& ...args) {
	content().assign(std::forward<Args>(args)...);
	return *this;
}

array::operator content_type&() {
	return content();
}

array::operator const content_type&() const {
	return content();
}

array::iterator array::begin() {
	return content().begin();

}

array::const_iterator array::begin() const {
	return content().begin();
}

array::const_iterator array::cbegin() const {
	return content().cbegin();
}

array::iterator array::end() {
	return content().end();
}

array::const_iterator array::end() const {
	return content().end();
}

array::const_iterator array::cend() const {
	return content().cend();
}

array::reverse_iterator array::rbegin() {
	return content().rbegin();
}

array::const_reverse_iterator array::rbegin() const {
	return content().rbegin();
}

array::const_reverse_iterator array::crbegin() const {
	return content().crbegin();
}

array::reverse_iterator array::rend() {
	return content().rend();
}

array::const_reverse_iterator array::rend() const {
	return content().rend();
}

array::const_reverse_iterator array::crend() const {
	return content().crend();
}

decltype(auto) array::empty() const {
	return content().empty();
}

decltype(auto) array::size() const {
	return content().size();
}

decltype(auto) array::max_size() const {
	return content().max_size();
}

decltype(auto) array::capacity() const {
	return content().capacity();
}

template <typename ...Args>
decltype(auto) array::resize(Args&& ...args) {
	return content().resize(std::forward<Args>(args)...);
}

void array::reserve(size_type n) {
	content().reserve(n);
}

void array::shrink_to_fit() {
	content().shrink_to_fit();
}

data::any& array::operator[](size_type n) {
	return content()[n];
}

const data::any& array::operator[](size_type n) const {
	return content()[n];
}

data::any& array::at(size_type n) {
	return content().at(n);
}

const data::any& array::at(size_type n) const {
	return content().at(n);
}

data::any& array::front() {
	return content().front();
}

const data::any& array::front() const {
	return content().front();
}

data::any& array::back() {
	return content().back();
}

const data::any& array::back() const {
	return content().back();
}

template <typename ...Args>
decltype(auto) array::emplace_back(Args&& ...args) {
	return content().emplace_back(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) array::push_back(Args&& ...args) {
	return content().push_back(std::forward<Args>(args)...);
}

decltype(auto) array::pop_back() {
	content().pop_back();
}

template <typename ...Args>
decltype(auto) array::emplace(Args&& ...args) {
	return content().emplace(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) array::insert(Args&& ...args) {
	return content().insert(std::forward<Args>(args)...);
}

template <typename ...Args>
decltype(auto) array::erase(Args&& ...args) {
	return content().erase(std::forward<Args>(args)...);
}

void array::swap(array& that) {
	content_type& this_content = *this;
	content_type& that_content = that;
	this_content.swap(that_content);
}

void array::clear() {
	content().clear();
}

size_t array::use_count() const {
	return _shared->use_count();
}

bool array::operator==(const data::array& that) const {
	return this->content() == that.content();
}

}

#endif
