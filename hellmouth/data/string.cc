// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/string.h"

namespace hellmouth::data {

shared_string_index::shared_string_index() {
	_empty = find("");
}

shared_string_index::~shared_string_index() {
	del(_empty);
}

}
