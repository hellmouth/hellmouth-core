// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_POINTER
#define HELLMOUTH_DATA_POINTER

#include <vector>
#include <string>
#include <string_view>
#include <ranges>

#include "hellmouth/data/array.h"
#include "hellmouth/data/object.h"
#include "hellmouth/data/any.h"
#include "hellmouth/data/segment.h"

namespace hellmouth::data {

/** A class to represent a pointer into a structured data value.
 * Syntax and behaviour are analogous to a JSON pointer, as defined by
 * RFC 6901.
 *
 * The current implementation supports indexing of objects but not of arrays.
 */
class pointer {
private:
	/** The type used to store the underlying content. */
	typedef std::vector<segment> content_type;
public:
	/** The type of a const iterator into a structured data pointer. */
	typedef content_type::const_iterator const_iterator;
private:
	/** The pointer, as a sequence of reference tokens.
	 * The tokens are recorded in unprefixed and unescaped form.
	 */
	content_type _segments;
public:
	/** Construct pointer to root of document. */
	pointer() = default;

	/** Construct structured data pointer from string view.
	 * Tokens are prefixed by forward slash characters.
	 * Any tilde or forward slash characters within tokens must be escaped.
	 *
	 * @param ptrstr the pointer as a string view
	 */
	explicit pointer(std::string_view ptrstr);

	/** Convert pointer to string.
	 * The constituent tokens are escaped, then prefixed, then concatenated.
	 *
	 * @return the pointer as a string
	 */
	operator std::string() const;

	/** Check whether this pointer is empty.
	 * An empty pointer refers to the root of the structured data.
	 *
	 * @return true if empty, otherwise false
	 */
	bool empty() const {
		return _segments.empty();
	}

	/** Get an iterator for the beginning of the sequence of segments.
	 * @return the resulting iterator
	 */
	const_iterator begin() const {
		return _segments.begin();
	}

	/** Get an iterator for the end of the sequence of segments.
	 * @return the resulting iterator
	 */
	const_iterator end() const {
		return _segments.end();
	}

	/** Get the last segment of this pointer.
	 * The behaviour of this function is undefined if the pointer does
	 * not contain any segments.
	 *
	 * @return the last segment
	 */
	const segment& back() const {
		return _segments.back();
	}

	/** Navigate to the parent of a structured data value.
	 *
	 * @param root the root of the structured data value
	 * @return the parent of the member referred to by this pointer
	 */
	pointer parent() const {
		pointer result;
		for (const segment& seg : std::ranges::subrange(
			_segments.begin(), _segments.end() - 1)) {

			result.push_back(segment(seg));
		}
		return result;
	}

	/** Append a segment to this pointer.
	 * @param segment the segment to be appended
	 */
	void push_back(segment&& token);

	/** Use this pointer to navigate into structured data.
	 * @param root the root of the structured data
	 * @return the member referred to by this pointer
	 */
	any& operator()(any& root) const {
		any* ptr = &root;
		for (const segment& seg : _segments) {
			ptr = &seg(*ptr);
		}
		return *ptr;
	}

	/** Use this pointer to navigate into structured data.
	 * @param root the root of the structured data
	 * @return the member referred to by this pointer
	 */
	const any& operator()(const any& root) const {
		const any* ptr = &root;
		for (const segment& seg : _segments) {
			ptr = &seg(*ptr);
		}
		return *ptr;
	}
};

}

#endif
