// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <algorithm>

#include "hellmouth/data/integer.h"
#include "hellmouth/data/signed_integer.h"

namespace hellmouth::data {

integer::integer(std::string_view intstr, int base) {
	sign_type sign = false;
	switch (intstr.front()) {
	case '-':
		sign = true;
		[[fallthrough]];
	case '+':
		intstr.remove_prefix(1);
	}
	unsigned_integer magnitude(intstr);

	_shared = new shared_integer(sign, std::move(magnitude));
	++_shared->_use_count;
}

integer integer::operator-() const {
	sign_type sign = !_shared->sign();
	magnitude_type magnitude = _shared->magnitude();
	if (magnitude.empty()) {
		sign = false;
	}
	return integer(sign, std::move(magnitude));
}

integer& integer::operator+=(signed_chunk_type addend) {
	unshare();
	signed_integer sum(_shared->_sign,
		std::move(_shared->_magnitude));
	sum += addend;
	_shared->_sign = sum.sign();
	_shared->_magnitude = std::move(sum);
	return *this;
}

integer& integer::operator-=(signed_chunk_type subtrahend) {
	unshare();
	signed_integer difference(_shared->_sign,
		std::move(_shared->_magnitude));
	difference -= subtrahend;
	_shared->_sign = difference.sign();
	_shared->_magnitude = std::move(difference);
	return *this;
}

integer& integer::operator*=(signed_chunk_type multiplier) {
	unshare();
	chunk_type umultiplier = multiplier;
	if (multiplier < 0) {
		_shared->_sign ^= true;
		umultiplier = -umultiplier;
	}
	_shared->_magnitude *= umultiplier;
	_shared->canonicalise();
	return *this;
}

integer& integer::operator/=(signed_chunk_type divisor) {
	unshare();
	chunk_type udivisor = divisor;
	if (divisor < 0) {
		_shared->_sign ^= true;
		udivisor = -udivisor;
	}
	_shared->_magnitude /= udivisor;
	_shared->canonicalise();
	return *this;
}

integer integer::operator%(signed_chunk_type divisor) const {
	chunk_type udivisor = divisor;
	if (divisor < 0) {
		udivisor = -udivisor;
	}

	unsigned_integer magnitude = _shared->_magnitude % udivisor;
	sign_type sign = _shared->_sign;
	if (magnitude.empty()) {
		sign = false;
	}
	return integer(sign, std::move(magnitude));
}

bool integer::operator==(const integer& that) const {
	return (_shared->_sign == that._shared->_sign) &&
		(_shared->_magnitude == that._shared->_magnitude);
}

std::strong_ordering integer::operator<=>(const integer& that) const {
	if (_shared->_sign != that._shared->_sign) {
		return that._shared->_sign <=> _shared->_sign;
	}
	if (_shared->_sign) {
		return that._shared->_magnitude <=> _shared->_magnitude;
	} else {
		return _shared->_magnitude <=> that._shared->_magnitude;
	}
}

std::string to_string(integer value) {
	bool negative = value.negative();
	if (negative) {
		value = -value;
	}
	std::string result;

	while (value) {
		int digit = value % 10;
		value /= 10;
		result.push_back('0' + digit);
	}
	if (result.empty()) {
		result.push_back('0');
	}
	if (negative) {
		result.push_back('-');
	}
	reverse(result.begin(), result.end());
	return result;
}

}
