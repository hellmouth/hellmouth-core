// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_SEGMENT
#define HELLMOUTH_DATA_SEGMENT

#include <string>

#include "hellmouth/data/array.h"
#include "hellmouth/data/object.h"
#include "hellmouth/data/any.h"

namespace hellmouth::data {

/** A class to represent a segment of a structured data pointer.
 * Each segment provides the index or key required to descend one level
 * into a structured data array or object.
 *
 * Used as a visitor to a std::any it is capable of descending into either
 * of those types, iterpreting its string value as an index or key as
 * appropriate.
 *
 * When using a pointer navigationally, this is the appropriate behaviour
 * for all of its segments. When using a pointer to add or remove data,
 * this is the appropriate behaviour for all segments except for the final
 * one (which should be converted into an inserter or a remover).
 *
 * Segments have two alternative string representations:
 * - encoded (escaped and prefixed)
 * - unencoded (unescaped and unprefixed)
 *
 * Encoding is required only when they are concatenated as part of a
 * pointer, therefore this class uses the unencoded representation,
 * with encoding and decoding handled by the pointer class.
 */
class segment {
private:
	/** The unencoded string representation of this segment. */
	std::string _token;
public:
	/** Construct pointer segment.
	 * @param token the unencoded string representation of this segment
	 */
	segment(const std::string& token):
		_token(token) {}

	/** Covert this segment to an unencoded string.
	 * @return the resulting string
	 */
	operator const std::string&() const {
		return _token;
	}

	/** Convert this segment into an array index.
	 * @param size the size of the array to be indexed
	 * @return the resulting array index
	 */
	array::size_type index(array::size_type size) const;

	/** Convert this segment into an object key.
	 * @return the resulting object key
	 */
	const std::string& key() const {
		return _token;
	}

	/** Apply this path segment to an array (non-const).
	 * @param arr the array to which the segment is applied
	 * @return the selected array element
	 */
	data::any& operator()(data::array& arr) const {
		return arr.at(index(arr.size()));
	}

	/** Apply this path segment to an array (const).
	 * @param arr the array to which the segment is applied
	 * @return the selected array element
	 */
	const data::any& operator()(const data::array& arr) const {
		return arr.at(index(arr.size()));
	}

	/** Apply this path segment to an object (non-const).
	 * @param obj the object to which the segment is applied
	 * @return the selected object member
	 */
	data::any& operator()(data::object& obj) const {
		return obj.at(key());
	}

	/** Apply this path segment to an object (const).
	 * @param obj the object to which the segment is applied
	 * @return the selected object member
	 */
	const data::any& operator()(const data::object& obj) const {
		return obj.at(key());
	}

	/** Apply this path segment to any type of structured data.
	 * @param value the value to which the segment is applied
	 * @return the selected array element or object member
	 */
	data::any& operator()(data::any& value) const {
		return value.visit(*this);
	}

	/** Apply this path segment to any type of structured data.
	 * @param value the value to which the segment is applied
	 * @return the selected array element or object member
	 */
	const data::any& operator()(const data::any& value) const {
		return value.visit(*this);
	}

	/** Attempt to apply this segment to any other type (non-const).
	 * This is required so that a pointer segment can act as a
	 * visitor.
	 */
	template<typename T>
	data::any& operator()(T& value) const {
		throw std::invalid_argument(
			"variant type is not indexable");
	}

	/** Attempt to apply this segment to any other type (const).
	 * This is required so that a pointer segment can act as a
	 * visitor.
	 */
	template<typename T>
	const data::any& operator()(const T& value) const {
		throw std::invalid_argument(
			"variant type is not indexable");
	}
};

}

#endif
