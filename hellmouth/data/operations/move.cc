// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/remover.h"
#include "hellmouth/data/inserter.h"
#include "hellmouth/data/operations/move.h"

namespace hellmouth::data::operations {

move::move(const data::object& spec):
	path_attribute(spec),
	from_attribute(spec) {}

void move::operator()(data::any& root) const {
	if (path().empty()) {
		// The behaviour of this operation is required to be equivalent
		// to remove plus add, and current behaviour is to fail if
		// attempting to remove the root path. Following this for
		// consistency, do not change unless also revising the behaviour
		// of the remove operation.
		throw std::invalid_argument("cannot move from root");
	}

	const data::any& croot = root;
	data::any value = from()(croot);
	data::any& parent = from().parent()(root);
	remover(from().back())(parent);

	// As per documentation, no rollback if a failure occurs.
	if (path().empty()) {
		root = value;
	} else {
		data::any& parent = path().parent()(root);
		inserter(path().back(), value)(parent);
	}
}

}
