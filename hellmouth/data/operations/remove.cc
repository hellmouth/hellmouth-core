// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/remover.h"
#include "hellmouth/data/operations/remove.h"

namespace hellmouth::data::operations {

remove::remove(const data::object& spec):
	path_attribute(spec) {}

void remove::operator()(data::any& root) const {
	if (path().empty()) {
		// It would be possible to replace the root with
		// a null value, but that is not semantically
		// equivalent to removing it, so current behaviour
		// is to throw an error if this is attempted.
		//
		// If this behaviour is changed in the future, the
		// replace and move operations should be revised to
		// match.
		throw std::invalid_argument("cannot remove root");
	}

	data::any& parent = path().parent()(root);
	remover(path().back())(parent);
}

}
