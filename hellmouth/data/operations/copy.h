// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_OPERATIONS_COPY
#define HELLMOUTH_DATA_OPERATIONS_COPY

#include "hellmouth/data/operation.h"

namespace hellmouth::data::operations {

/** A structured data operation for copying a given path. */
class copy:
	public operation,
	public path_attribute,
	public from_attribute {
public:
	/** Construct a copy operation.
	 * The "op" member is not validated.
	 *
	 * @param spec a specification for the operation to be performed
	 */
	copy(const data::object& spec);

	void operator()(data::any& root) const override;
};

}

#endif
