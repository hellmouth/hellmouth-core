// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/inserter.h"
#include "hellmouth/data/operations/copy.h"

namespace hellmouth::data::operations {

copy::copy(const data::object& spec):
	path_attribute(spec),
	from_attribute(spec) {}

void copy::operator()(data::any& root) const {
	const data::any& croot = root;
	data::any value = from()(croot);
	if (path().empty()) {
		root = value;
	} else {
		data::any& parent = path().parent()(root);
		inserter(path().back(), value)(parent);
	}
}

}
