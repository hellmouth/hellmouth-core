// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/operations/test.h"

namespace hellmouth::data::operations {

test::test(const data::object& spec):
	path_attribute(spec),
	value_attribute(spec) {}

void test::operator()(data::any& root) const {
	const data::any& croot = root;
	if (path()(croot) != value()) {
		throw std::invalid_argument(
			"test operation failed in patch");
	}
}

}
