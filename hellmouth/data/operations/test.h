// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_OPERATIONS_TEST
#define HELLMOUTH_DATA_OPERATIONS_TEST

#include "hellmouth/data/operation.h"

namespace hellmouth::data::operations {

/** A structured data operation for testing the value of a given path. */
class test:
	public operation,
	public path_attribute,
	public value_attribute {
public:
	/** Construct a test operation.
	 * The "op" member is not validated.
	 *
	 * @param spec a specification for the operation to be performed
	 */
	test(const data::object& spec);

	void operator()(data::any& root) const override;
};

}

#endif
