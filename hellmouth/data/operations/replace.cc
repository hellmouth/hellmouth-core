// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/remover.h"
#include "hellmouth/data/inserter.h"
#include "hellmouth/data/operations/replace.h"

namespace hellmouth::data::operations {

replace::replace(const data::object& spec):
	path_attribute(spec),
	value_attribute(spec) {}

void replace::operator()(data::any& root) const {
	if (path().empty()) {
		// The behaviour of this operation is required to be equivalent
		// to remove plus add, and current behaviour is to fail if
		// attempting to remove the root path. Following this for
		// consistency, do not change unless also revising the behaviour
		// of the remove operation.
		throw std::invalid_argument("cannot replace root");
	}

	// The following literally implements as a removal followed by an
	// insertion, which is correct per the specification, but unecessarily
	// inefficient and non-atomic.
	data::any& parent = path().parent()(root);
	remover(path().back())(parent);
	inserter(path().back(), value())(parent);
}

}
