// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/inserter.h"
#include "hellmouth/data/operations/add.h"

namespace hellmouth::data::operations {

add::add(const data::object& spec):
	path_attribute(spec),
	value_attribute(spec) {}

void add::operator()(data::any& root) const {
	if (path().empty()) {
		// Performing an add operation on the document root
		// is defined as a special case in the RFC.
		root = value();
	} else {
		data::any& parent = path().parent()(root);
		inserter(path().back(), value())(parent);
	}
}

}
