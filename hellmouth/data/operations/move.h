// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_OPERATIONS_MOVE
#define HELLMOUTH_DATA_OPERATIONS_MOVE

#include "hellmouth/data/operation.h"

namespace hellmouth::data::operations {

/** A structured data operation for moving a given path.
 * Note that the effect of this class is not atomic: if a failure occurs
 * then the data will not be restored its original state.
 */
class move:
	public operation,
	public path_attribute,
	public from_attribute {
public:
	/** Construct a move operation.
	 * The "op" member is not validated.
	 *
	 * @param spec a specification for the operation to be performed
	 */
	move(const data::object& spec);

	void operator()(data::any& root) const override;
};

}

#endif
