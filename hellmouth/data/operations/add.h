// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_DATA_OPERATIONS_ADD
#define HELLMOUTH_DATA_OPERATIONS_ADD

#include "hellmouth/data/operation.h"

namespace hellmouth::data::operations {

/** A structured data operation for inserting a given path. */
class add:
	public operation,
	public path_attribute,
	public value_attribute {
public:
	/** Construct an add operation.
	 * The "op" member is not validated.
	 *
	 * @param spec a specification for the operation to be performed
	 */
	add(const data::object& spec);

	void operator()(data::any& root) const override;
};

}

#endif
