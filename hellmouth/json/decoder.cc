// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cctype>
#include <stdexcept>
#include <type_traits>
#include <string>

#include "hellmouth/data/fwd.h"
#include "hellmouth/utf8/encoder.h"
#include "hellmouth/json/decoder.h"

namespace hellmouth::json {

class parse_error:
	public std::runtime_error {
public:
	parse_error(const char* msg):
		std::runtime_error(msg) {}
};

void decoder::skip_ws() {
	char ch = _in->get();
	while (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r') {
		ch = _in->get();
	}
	_in->unget();
}

data::any decoder::decode_literal() {
	skip_ws();
	std::string literal;
	char ch = _in->get();
	while (isalpha(ch)) {
		literal.push_back(ch);
		ch = _in->get();
	}
	_in->unget();

	if (literal == "null") {
		return data::any();
	} else if (literal == "true") {
		return data::any(true);
	} else if (literal == "false") {
		return data::any(false);
	} else if (!literal.empty()) {
		throw parse_error("unrecognised JSON literal");
	} else {
		throw parse_error("invalid JSON expression");
	}
}

data::any decoder::decode_number() {
	bool negative = false;
	std::string intpart;
	std::string fracpart;
	std::string exppart;

	skip_ws();
	char ch = _in->get();
	if (ch == '-') {
		negative = true;
		ch = _in->get();
	}
	if (ch == '0') {
		intpart.push_back(ch);
		ch = _in->get();
	} else {
		if (!isdigit(ch)) {
			throw parse_error("digit expected");
		}
		while (isdigit(ch)) {
			intpart.push_back(ch);
			ch = _in->get();
		}
	}
	if (ch == '.') {
		ch = _in->get();
		if (!isdigit(ch)) {
			throw parse_error("digit expected");
		}
		while (isdigit(ch)) {
			fracpart.push_back(ch);
			ch = _in->get();
		}
	}
	if (ch == 'E' || ch =='e') {
		ch = _in->get();
		if (ch == '+') {
			ch = _in->get();
		} else if (ch == '-') {
			exppart.push_back(ch);
			ch = _in->get();
		}
		if (!isdigit(ch)) {
			throw parse_error("digit expected");
		}
		while (isdigit(ch)) {
			exppart.push_back(ch);
			ch = _in->get();
		}
	}
	_in->unget();

	if (!fracpart.empty()) {
		throw parse_error(
			"fractional numeric JSON value not supported");
	}
	if (!exppart.empty()) {
		throw parse_error(
			"exponential numeric JSON value not supported");
	}

	using small_unsigned_integer =
		std::make_unsigned_t<data::small_integer>;
	const small_unsigned_integer ulimit =
		std::numeric_limits<data::small_integer>::max();
	small_unsigned_integer uvalue = 0;
	for (char ch : intpart) {
		if (uvalue > ulimit / 10) {
			uvalue = ulimit + 2;
		} else {
			uvalue *= 10;
		}
		uvalue += ch - '0';
	}
	if (uvalue <= ulimit) {
		data::small_integer value = uvalue;
		if (negative) {
			return data::any(-value);
		} else {
			return data::any(value);
		}
	} else if (negative && (uvalue == ulimit + 1)) {
		data::small_integer value = uvalue - 1;
		return data::any(-value - 1);
	} else {
		throw parse_error(
			"numeric JSON value outside supported range");
	}
}

std::string decoder::decode_unicode() {
	uint16_t code = 0;
	for (unsigned int i = 0; i != 4; ++i) {
		code <<= 4;
		char ch = _in->get();
		if (isdigit(ch)) {
			code += ch - '0';
		} else if (isxdigit(ch)) {
			code += toupper(ch) - 'A' + 10;
		} else {
			throw parse_error("hex digit expected");
		}
	}
	return utf8::encode(code);
}

std::string decoder::decode_escape() {
	char ch = _in->get();
	switch (ch) {
	case '"':
	case '\\':
	case '/':
	case 'b':
	case 'f':
	case 'n':
	case 'r':
	case 't':
		return std::string(1, ch);
	case 'u':
		return decode_unicode();
	default:
		throw parse_error("invalid escape sequence");
	}
}

data::string decoder::decode_string() {
	skip_ws();
	char ch = _in->get();
	if (ch != '"') {
		throw parse_error("quotation mark expected");
	}
	ch = _in->get();

	std::string content;
	while (ch != '"') {
		if (ch == '\\') {
			content.append(decode_escape());
		} else {
			content.push_back(ch);
		}
		ch = _in->get();
	}
	return content;
}

data::array decoder::decode_array() {
	skip_ws();
	char ch = _in->get();
	if (ch != '[') {
		throw parse_error("left square bracket expected");
	}

	bool done = false;
	skip_ws();
	ch = _in->get();
	if (ch == ']') {
		done = true;
	} else {
		_in->unget();
	}

	data::array result;
	while (!done) {
		result.push_back(operator()());
		skip_ws();
		ch = _in->get();
		switch (ch) {
		case ',':
			break;
		case ']':
			done = true;
			break;
		default:
			throw parse_error("comma or right square bracket expected");
		}
	}
	return result;
}

data::object decoder::decode_object() {
	char ch = _in->get();
	if (ch != '{') {
		throw parse_error("left curly bracket expected");
	}

	bool done = false;
	skip_ws();
	ch = _in->get();
	if (ch == '}') {
		done = true;
	} else {
		_in->unget();
	}

	data::object result;
	while (!done) {
		data::string key = decode_string();
		skip_ws();
		ch = _in->get();
		if (ch != ':') {
			throw parse_error("colon expected");
		}
		data::any value = operator()();
		result[key] = value;
		skip_ws();
		ch = _in->get();
		switch (ch) {
		case ',':
			break;
		case '}':
			done = true;
			break;
		default:
			throw parse_error("comma or right curly bracket expected");
		}
	}
	return result;
}

data::any decoder::operator()() {
	skip_ws();
	switch (_in->peek()) {
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	case '-':
		return decode_number();
	case '"':
		return decode_string();
	case '[':
		return decode_array();
	case '{':
		return decode_object();
	default:
		return decode_literal();
	}
}

bool decoder::eof(bool required) {
	int ch = _in->get();
	if (required && ch != EOF) {
		throw parse_error("end of data expected");
	}
	return ch == EOF;
}

}
