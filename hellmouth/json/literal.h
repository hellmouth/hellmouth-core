// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_JSON_LITERAL
#define HELLMOUTH_JSON_LITERAL

#include "hellmouth/data/any.h"

namespace hellmouth {

data::any operator""_json(const char* str, size_t len);

}

#endif
