// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_JSON_DECODER
#define HELLMOUTH_JSON_DECODER

#include <istream>

#include "hellmouth/data/string.h"
#include "hellmouth/data/array.h"
#include "hellmouth/data/object.h"
#include "hellmouth/data/any.h"

namespace hellmouth::json {

/** A class for decoding data using JSON.
 * The current implementation places no limits on the amount of
 * memory that can be consumed during decoding.
 */
class decoder {
private:
	/** An input stream for providing the encoded JSON. */
	std::istream* _in;
public:
	/** Construct decoder.
	 * The input stream must outlive the decoder.
	 *
	 * @param an input stream for providing the encoded JSON
	 */
	decoder(std::istream& in):
		_in(&in) {}
private:
	/** Skip whitespace. */
	void skip_ws();

	/** Decode a literal value.
	 * Supported literals are the values null, true and false.
	 * @return the decoded literal
	 */
	data::any decode_literal();

	/** Decode a number.
	 * @return the decoded number
	 */
	data::any decode_number();

	/** Decode a hexadecimal Unicode character code.
	 * @return the character as a UTF-8 string
	 */
	std::string decode_unicode();

	/** Decode an escape sequence.
	 * @return the decoded escape sequence as a UTF-8 string
	 */
	std::string decode_escape();

	/** Decode a character string.
	 * @return the decoded string
	 */
	data::string decode_string();

	/** Decode an array.
	 * @return the decoded array
	 */
	data::array decode_array();

	/** Decode an object.
	 * @return the decoded object
	 */
	data::object decode_object();
public:
	/** Decode any JSON value.
	 * @return the decoded value
	 */
	data::any operator()();

	/** Test for end of file.
	 * @param true if required, otherwise false
	 */
	bool eof(bool required = false);
};

}

#endif
