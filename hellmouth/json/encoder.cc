
// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/array.h"
#include "hellmouth/data/object.h"
#include "hellmouth/json/encoder.h"

namespace hellmouth::json {

void encoder::operator()(const data::null_t&) {
	*_out << "null";
}

void encoder::operator()(data::boolean value) {
	*_out << ((value) ? "true" : "false");
}

void encoder::operator()(data::small_integer value) {
	*_out << std::dec << value;
}

void encoder::operator()(const data::string& value) {
	std::string escaped;
	escaped.reserve(value.length());
	for (char ch : value) {
		switch (ch) {
		case '"':
		case '\\':
			escaped.push_back('\\');
			escaped.push_back(ch);
			break;
		case 0x00:
		case 0x01:
		case 0x02:
		case 0x03:
		case 0x04:
		case 0x05:
		case 0x06:
		case 0x07:
		case 0x08:
		case 0x09:
		case 0x0a:
		case 0x0b:
		case 0x0c:
		case 0x0d:
		case 0x0e:
		case 0x0f:
		case 0x10:
		case 0x11:
		case 0x12:
		case 0x13:
		case 0x14:
		case 0x15:
		case 0x16:
		case 0x17:
		case 0x18:
		case 0x19:
		case 0x1a:
		case 0x1b:
		case 0x1c:
		case 0x1d:
		case 0x1e:
		case 0x1f:
			char buffer[7];
			snprintf(buffer, sizeof(buffer), "\\u%04x", ch);
			escaped.append(buffer);
			break;
		default:
			escaped.push_back(ch);
		}
	}
	*_out << "\"" << escaped << "\"";
}

void encoder::operator()(const data::array& value) {
	*_out << "[";
	bool first = true;
	for (const auto& v : value) {
		if (first) {
			first = false;
		} else {
			*_out << ",";
		}
		operator()(v);
	}
	*_out << "]";
}

void encoder::operator()(const data::object& value) {
	*_out << "{";
	bool first = true;
	for (const auto& [k, v] : value) {
		if (first) {
			first = false;
		} else {
			*_out << ",";
		}
		operator()(k);
		*_out << ":";
		operator()(v);
	}
	*_out << "}";
}

void encoder::operator()(const data::any& value) {
	value.visit(*this);
}

}
