// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_JSON_ENCODER
#define HELLMOUTH_JSON_ENCODER

#include <ostream>

#include "hellmouth/data/fwd.h"

namespace hellmouth::json {

/** A class for encoding data using JSON.
 * Encoding is performed by invoking an instance of this class as a
 * functor, with the data to be encoded as its argument.
 *
 * Current behaviour is to:
 * - escape only those characters which must be escaped
 *   (quotation mark, reverse solidus, and C0 control codes); and
 * - encode without any unnecessary whitespace.
 *
 * Both of the above are subject to change in future implementations.
 */
class encoder {
public:
	/** An output stream for receiving the encoded JSON. */
	std::ostream* _out;
public:
	/** Construct JSON encoder.
	 * The output stream must outlive the encoder.
	 *
	 * @param out an output stream for receiving the encoded JSON
	 */
	encoder(std::ostream& out):
		_out(&out) {}

	/** Encode a null value.
	 * @param value the value to be encoded
	 */
	void operator()(const data::null_t& value);

	/** Encode a boolean.
	 * @param value the value to be encoded
	 */
	void operator()(data::boolean value);

	/** Encode a signed 32-bit integer.
	 * @param value the value to be encoded
	 */
	void operator()(data::small_integer value);

	/** Encode a data::string.
	 * @param value the string to be encoded
	 */
	void operator()(const data::string& value);

	/** Encode a data::array.
	 * @param value the array to be encoded
	 */
	void operator()(const data::array& value);

	/** Encode a data::object.
	 * @param value the object to be encoded
	 */
	void operator()(const data::object& value);

	/** Encode a data::any.
	 * @param value the value to be encoded
	 */
	void operator()(const data::any& value);
};

}

#endif
