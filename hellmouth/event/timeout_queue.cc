// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <time.h>

#include "hellmouth/os/utility.h"
#include "hellmouth/event/timeout_queue.h"

namespace hellmouth::event {

int timeout_queue::remaining() const {
	if (!_timeouts.empty()) {
		timeout& ev = **_timeouts.begin();
		return ev.remaining();
	}
	return -1;
}

void timeout_queue::operator()() {
	if (!_timeouts.empty()) {
		timeout& ev = **_timeouts.begin();
		if (ev.remaining() == 0) {
			ev.clear();
			ev();
		}
	}
}

}
