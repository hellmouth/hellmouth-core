// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_EVENT_NOTIFICATION
#define HELLMOUTH_OS_EVENT_NOTIFICATION

#include "hellmouth/os/event_descriptor.h"
#include "hellmouth/event/file_descriptor.h"

namespace hellmouth::event {

/** An event class triggered by calling a notification function. */
class notification {
private:
	/** An event file descriptor for communicating notifications. */
	os::event_descriptor _efd;

	/** An event handler for acting upon notifications. */
	event::file_descriptor _ev;
public:
	/** Construct notification event.
	 * @param mgr the event manager with which to register
	 * @param action the action to be performed when notified
	 */
	notification(manager& mgr, const event::base::action_type& action):
		_efd(0, 0),
		_ev(mgr, [this,action]{ _efd.wait(); action();}, _efd) {}

	notification(const notification&) = delete;
	notification& operator=(const notification&) = delete;

	/** Trigger this event. */
	void notify() {
		_efd.notify();
	}
};

}

#endif
