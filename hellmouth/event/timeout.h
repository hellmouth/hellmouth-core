// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_EVENT_TIMEOUT
#define HELLMOUTH_OS_EVENT_TIMEOUT

#include <functional>

#include <time.h>

namespace hellmouth::event {

class manager;

/** An event class triggered by the passage of time. */
class timeout {
public:
	/** The type of an event action. */
	typedef std::function<void()> action_type;
private:
	/** The event manager with which this event should register,
	 * or 0 if none. */
	event::manager* _em = 0;

	/** The action to be performed when this event is triggered. */
	action_type _action;

	/** The time when this event should occur, or zero if inactive. */
	timespec _expiry = {0};

	/** Register this event, if not currently registered. */
	void _add();

	/** Deregister this event, if currently registered. */
	void _remove();
public:
	/** Default construct with no event manager, action, or expiry. */
	timeout() = default;

	/** Construct timeout event.
	 * @param mgr the event manager with which this event should register
	 * @param action the action to be performed when this event occurs
	 * @param expiry the time when this timeout should expire
	 */
	timeout(event::manager& mgr, action_type action, timespec expiry={0}):
		_em(&mgr),
		_action(std::move(action)) {

		reset(expiry);
	}

	/** Construct timeout event.
	 * @param mgr the event manager with which this event should register
	 * @param action the action to be performed when this event occurs
	 * @param duration the number of milliseconds after which this timeout
	 *  should expire
	 */
	timeout(event::manager& mgr, action_type action, long duration):
		_em(&mgr),
		_action(std::move(action)) {

		reset(duration);
	}

	timeout(const timeout&) = delete;

	/** Move-construct timeout event.
	 * @param that the event to be moved
	 */
	timeout(timeout&& that):
		_em(that._em),
		_action(std::move(that._action)),
		_expiry(std::move(that._expiry)) {

		that._remove();
		that._em = 0;
		that._expiry = timespec{0};
		_add();
	}

	/** Destroy timeout event. */
	~timeout() {
		_remove();
	}

	timeout& operator=(const timeout&) = delete;

	/** Move-assign timeout event.
	 * @param ev the event to be moved
	 * @return a reference to this
	 */
	timeout& operator=(timeout&& that) {
		if (this != &that) {
			_remove();
			_em = that._em;
			_action = std::move(that._action);
			_expiry = std::move(that._expiry);

			that._remove();
			that._em = 0;
			that._expiry = timespec{0};
			_add();
		}
		return *this;
	}

	/** Get the event manager used by this event.
	 * @return a pointer to the event manager, or 0 if none
	 */
	event::manager* manager() const {
		return _em;
	}

	/** Test whether this timeout event is active.
	 * @return true if active, otherwise false
	 */
	operator bool() const {
		return (_expiry.tv_sec != 0) || (_expiry.tv_nsec != 0);
	}

	/** Get the current expiry time.
	 * @return the expiry time, or 0 if inactive
	 */
	const timespec& expiry() const {
		return _expiry;
	}

	/** Get the time remaining.
	 * The result is rounded upwards to a whole number of milliseconds.
	 * There are two reasons for this:
	 *
	 * - The requested expiry time is best interpreted as a minimum rather
	 *   than a maximum, since it would not be possible to guarantee the
	 *   latter.
	 * - It ensures that when the event is invoked, all methods of testing
	 *   whether it has reached its expiry time will agree that it has.
	 *
	 * If the result is too large to be represented as an int then it
	 * saturates to the maximum value which can.
	 *
	 * @return the time remaining in milliseconds, or 0 if the event is
	 *  either inactive or expired
	 */
	int remaining() const;

	/** Inactivate this timeout. */
	void clear();

	/** Reset the expiry time.
	 * The event will become active or inactive as appropriate.
	 *
	 * @param expiry the required expiry time
	 */
	void reset(timespec expiry);

	/** Reset the expiry time to a given number of milliseconds from now.
	 * The event will become active.
	 *
	 * @param duration the number of milliseconds after which this timeout
	 *  should expire
	 */
	void reset(long duration);

	/** Invoke this event. */
	void operator()() const;
};

}

#endif
