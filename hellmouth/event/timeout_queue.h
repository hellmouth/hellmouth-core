// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_EVENT_TIMEOUT_QUEUE
#define HELLMOUTH_EVENT_TIMEOUT_QUEUE

#include <functional>
#include <set>

#include "hellmouth/event/timeout.h"

namespace hellmouth::event {

class timeout;

/** A class for maintaining a queue of timeout events in order of expiry.
 * Events should be queued only while active, and their expiry times should
 * not be permitted to change while present in the queue.
 */
class timeout_queue {
private:
	/** Compare two timeout objects.
	 * The comparison is performed first by expiry time, then in the event
	 * of a tie by comparing the pointers.
	 *
	 * @param lhs the left-hand side
	 * @param rhs the right-hand side
	 */
	static bool cmp(timeout* lhs, timeout* rhs) {
		if (lhs->expiry().tv_sec != rhs->expiry().tv_sec) {
			return lhs->expiry().tv_sec < rhs->expiry().tv_sec;
		}
		if (lhs->expiry().tv_nsec != rhs->expiry().tv_nsec) {
			return lhs->expiry().tv_nsec < rhs->expiry().tv_nsec;
		}
		return std::less()(lhs, rhs);
	}

	/** The queued timeouts, indexed by expiry time. */
	std::set<timeout*, decltype(&cmp)> _timeouts;
public:
	/** Construct empty timeout queue. */
	timeout_queue():
		_timeouts(&cmp) {}

	/** Insert a timeout event into the queue.
	 * @param ev the timeout event to be registered
	 */
	void insert(timeout& ev) {
		_timeouts.insert(&ev);
	}

	/** Remove a timeout event from the queue.
	 * @param ev the timeout event to be registered
	 */
	void remove(timeout& ev) {
		_timeouts.erase(&ev);
	}

	/** Get time remaining before next timeout.
	 * The value returned is suitable for passing into epoll_wait:
	 *
	 * - If the queue is empty then -1 is returned, causing epoll_wait to
	 *   wait indefinitely until a file descriptor becomes ready.
	 * - If there are any expired timeouts in the queue then 0 is returned,
	 *   causing epoll_wait to return immediately. This does not necessarily
	 *   result in those timeouts being invoked immediately (or ever), since
	 *   file descriptors take priority, however it does ensure that an
	 *   event of some description is invoked immediately.
	 *
	 * Rounding and saturation of the result are as specified for the
	 * function timeout::remaining.
	 *
	 * @param the time remaining, in milliseconds
	 */
	int remaining() const;

	/** Remove then invoke the expired timeout at the head of the queue.
	 * Removal occurs first because the event handler may want to reset the
	 * event with a new expiry time, causing it to be re-inserted into the
	 * queue.
	 *
	 * If there are no expired events then this function has no effect.
	 */
	void operator()();
};

}

#endif
