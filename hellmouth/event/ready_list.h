// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_READY_LIST
#define HELLMOUTH_OS_READY_LIST

#include "hellmouth/os/epoll_descriptor.h"
#include "hellmouth/event/file_descriptor.h"

namespace hellmouth::event {

/** A class for listing file descriptors which are ready. */
class ready_list {
private:
	/** The underlying buffer.
	 * Note that it is not sufficient for the capacity of this vector to be
	 * equal to the required number of slots: it must also be sized to that
	 * number, since it cannot be known in advance how many slots will be
	 * populated by epoll_wait.
	 */
	std::vector<epoll_event> _events;

	/** The index of the next buffer slot to be processed.
	 * This should be set to zero when not processing the buffer.
	 */
	size_t _nextslot = 0;

	/** The occupied size of the buffer. */
	size_t _size = 0;
public:
	/** Construct ready list.
	 * @param capacity the required number of slots
	 */
	explicit ready_list(size_t capacity=16);

	/** Get the capacity of this ready list.
	 * Note that (unlike a standard container) all slots up to the specified
	 * capacity are available for immediate use by epoll_wait.
	 *
	 * @return the number of available slots
	 */
	size_t capacity() const {
		return _events.size();
	}

	/** Remove any reference to a given fd event from the buffer.
	 * @param ev the file descriptor event to be removed
	 */
	void remove(const file_descriptor& ev);

	/** Get a pointer to the underlying buffer.
	 * The result is suitable for passing to epoll_wait.
	 *
	 * @return the buffer
	 */
	operator epoll_event*() {
		return &_events.front();
	}

	/** Process a given number of slots.
	 * @param count the number of slots to process
	 */
	void operator()(size_t count);
};

}

#endif
