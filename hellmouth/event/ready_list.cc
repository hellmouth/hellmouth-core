// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/event/ready_list.h"

namespace hellmouth::event {

ready_list::ready_list(size_t capacity):
	_events(capacity) {}

void ready_list::remove(const file_descriptor& ev) {
	// It is not necessary to teat _nextslot == _size == 0 as a
	// special case.
	for (size_t i = _nextslot; i != _size; ++i) {
		epoll_event& epev = _events[i];
		if (epev.data.ptr == &ev) {
			epev.data.ptr = 0;
		}
	}
}

void ready_list::operator()(size_t count) {
	_size = count;
	_nextslot = 0;
	for (size_t i = 0; i != count; ++i) {
		_nextslot = i + 1;
		epoll_event& epev = _events[i];
		file_descriptor& ev = *static_cast<file_descriptor*>(epev.data.ptr);
		ev(epev.events);
	}
	_nextslot = 0;
	_size = 0;
}

}
