// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/event/file_descriptor.h"
#include "hellmouth/event/manager.h"

namespace hellmouth::event {

void file_descriptor::_add() {
	if (_em && (_fd != -1)) {
		_em->add(*this);
	}
}

void file_descriptor::_remove() {
	if (_em && (_fd != -1)) {
		_em->remove(*this);
	}
}

void file_descriptor::clear() {
	_remove();
	_fd = -1;
	_events = 0;
	_revents = 0;
}

void file_descriptor::reset(int fd, int events) {
	_remove();
	_fd = fd;
	_events = events;
	_revents = 0;
	_add();
}

void file_descriptor::operator()(int revents) {
	// First make a copy of the action, in case the handler wishes
	// to modify the event.
	action_type action = _action;
	_revents = revents;
	action();
}

}
