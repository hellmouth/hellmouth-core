// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_MANAGER
#define HELLMOUTH_OS_MANAGER

#include <vector>

#include "hellmouth/os/epoll_descriptor.h"
#include "hellmouth/event/timeout.h"
#include "hellmouth/event/timeout_queue.h"
#include "hellmouth/event/file_descriptor.h"
#include "hellmouth/event/ready_list.h"

namespace hellmouth::event {

/** A class for dispatching events. */
class manager {
	friend timeout;
	friend file_descriptor;
private:
	/** A list of registered timeout events. */
	timeout_queue _timeouts;

	/** An epoll_descriptor for detecting file descriptor events. */
	os::epoll_descriptor _epfd;

	/** A list of triggered file descriptor events. */
	ready_list _ready;

	/** Register a timeout.
	 * @param ev the timeout event to be registered
	 */
	void add(timeout& ev) {
		_timeouts.insert(ev);
	}

	/** Deregister a timeout.
	 * @param ev the timeout event to be deregistered.
	 */
	void remove(timeout& ev) {
		_timeouts.remove(ev);
	}

	/** Register a file descriptor event.
	 * @param ev the file descriptor event to be registered
	 */
	void add(file_descriptor& ev);

	/** Deregister a file descriptor event.
	 * @param ev the file descriptor event to be deregistered.
	 */
	void remove(file_descriptor& ev);
public:
	/** Construct event manager.
	 * @param maxevents the maximum number of events to process as a batch
	 */
	manager(size_t maxevents=16);

	/** Poll for events.
	 * This function might dispatch more than one event, but will only
	 * poll for events once.
	 */
	void poll();
};

}

#endif
