// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <poll.h>

#include "hellmouth/event/manager.h"

namespace hellmouth::event {

void manager::add(file_descriptor& ev) {
	epoll_event epev{0};
	epev.events = ev.events();
	epev.data.ptr = &ev;
	_epfd.add(ev.fd(), epev);
}

void manager::remove(file_descriptor& ev) {
	_epfd.del(ev.fd());
	_ready.remove(ev);
}

manager::manager(size_t maxevents):
	_ready(maxevents) {}

void manager::poll() {
	size_t count = _epfd.wait(_ready, _ready.capacity(),
		_timeouts.remaining());
	if (count != 0) {
		_ready(count);
	} else {
		_timeouts();
	}
}

}
