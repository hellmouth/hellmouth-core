// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cstdint>
#include <limits>
#include <utility>

#include "hellmouth/os/utility.h"
#include "hellmouth/event/timeout.h"
#include "hellmouth/event/manager.h"

namespace hellmouth::event {

/** The number of milliseconds per second. */
static constexpr long msec_per_sec = 1000;

/** The number of nanoseconds per millisecond. */
static constexpr long nsec_per_msec = 1000000;

/** The number of nanoseconds per second. */
static constexpr long nsec_per_sec = 1000000000;

void timeout::_add() {
	if (_em && bool(*this)) {
		_em->add(*this);
	}
}

void timeout::_remove() {
	if (_em && bool(*this)) {
		_em->remove(*this);
	}
}

int timeout::remaining() const {
	if (!*this) {
		return 0;
	}

	timespec now;
	if (clock_gettime(CLOCK_MONOTONIC, &now) == -1) {
		os::throw_errno();
	}

	int64_t remaining = _expiry.tv_sec - now.tv_sec;
	remaining *= nsec_per_sec;
	remaining += _expiry.tv_nsec - now.tv_nsec;
	if (remaining < 0) {
		remaining = 0;
	}
	remaining = (remaining + nsec_per_msec - 1) / nsec_per_msec;
	if (!std::in_range<int>(remaining)) {
		remaining = std::numeric_limits<int>::max();
	}
	return remaining;
}

void timeout::clear() {
	_remove();
	_expiry = timespec{0};
}

void timeout::reset(timespec expiry) {
	_remove();
	_expiry = expiry;
	_add();
}

void timeout::reset(long duration) {
	timespec _duration {
		duration / msec_per_sec,
		(duration % msec_per_sec) * nsec_per_msec};

	if (clock_gettime(CLOCK_MONOTONIC, &_expiry) == -1) {
		os::throw_errno();
	}
	_expiry.tv_sec += _duration.tv_sec;
	_expiry.tv_nsec += _duration.tv_nsec;
	if (_expiry.tv_nsec > nsec_per_sec) {
		_expiry.tv_nsec -= nsec_per_sec;
		_expiry.tv_sec += 1;
	}
	_add();
}

void timeout::operator()() const {
	// First make a copy of the action, in case the handler wishes
	// to modify the event.
	action_type action = _action;
	action();
}

}
