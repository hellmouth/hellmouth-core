// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_EVENT_FILE_DESCRIPTOR
#define HELLMOUTH_OS_EVENT_FILE_DESCRIPTOR

#include <functional>

#include <sys/epoll.h>

namespace hellmouth::event {

class manager;

/** An event class triggered by the status of a file descriptor.
 * The current implementation is limited to detecting whether the file
 * descriptor is ready for reading, and does not allow multiple events
 * to monitor the same descriptor.
 *
 * Event types are passed and stored using the same format as
 * epoll_event::events struct member (a bitwise-OR of applicable constants
 * such as EPOLLIN and EPOLLOUT).
 */
class file_descriptor {
public:
	/** The type of an event action. */
	typedef std::function<void()> action_type;
private:
	/** The event manager with which this event should register,
	 * or 0 if none. */
	event::manager* _em = 0;

	/** The action to be performed when this event is triggered. */
	action_type _action;

	/** The file descriptor monitored by this event, or -1 if none. */
	int _fd = -1;

	/** The event types monitored by this event. */
	int _events;

	/** The event types returned when most recently triggered,
	 * or 0 if the event has not yet triggered
	 */
	int _revents;

	/** Register this event, if not currently registered. */
	void _add();

	/** Deregister this event, if currently registered. */
	void _remove();
public:
	/** Default construct with no event manager, action, or
	 * file descriptor. */
	file_descriptor() = default;

	/** Construct file descriptor event.
	 * @param mgr the event manager with which this event should register
	 * @param action the action to be performed when this event occurs
	 * @param fd the file descriptor to be monitored
	 * @param the event types to be monitored
	 */
	file_descriptor(event::manager& mgr, action_type action, int fd=-1,
		int events=EPOLLIN):
		_em(&mgr),
		_action(std::move(action)),
		_fd(fd),
		_events(events) {

		_add();
	}

	file_descriptor(const file_descriptor&) = delete;

	/** Move-construct file descriptor event.
	 * @param that the event to be moved
	 */
	file_descriptor(file_descriptor&& that):
		_em(that._em),
		_action(std::move(that._action)),
		_fd(that._fd) {

		that._remove();
		that._em = 0;
		that._fd = -1;
		that._events = 0;
		_add();
	}

	/** Destroy file descriptor event. */
	~file_descriptor() {
		_remove();
	}

	file_descriptor& operator=(const file_descriptor&) = delete;

	/** Move-assign file descriptor event.
	 * @param ev the event to be moved
	 * @return a reference to this
	 */
	file_descriptor& operator=(file_descriptor&& that) {
		if (this != &that) {
			_remove();
			_em = that._em;
			_action = std::move(that._action);
			_fd = that._fd;
			_events = that._events;

			that._remove();
			that._em = 0;
			that._fd = -1;
			that._events = 0;
			_add();
		}
		return *this;
	}

	/** Get the event manager used by this event.
	 * @return a pointer to the event manager, or 0 if none
	 */
	event::manager* manager() const {
		return _em;
	}

	/** Get the file descriptor monitored by this event object. */
	int fd() const {
		return _fd;
	}

	/** Get the event types monitored by this event object.
	 * @return the event types
	 */
	int events() const {
		return _events;
	}

	/** Get the event types returned when this event last triggered.
	 * @return the event types
	 */
	int revents() const {
		return _revents;
	}

	/** Inactivate this event. */
	void clear();

	/** Reset this event with new monitoring parameters.
	 * @param fd the file descriptor to be monitored
	 * @param the event types to be monitored
	 */
	void reset(int fd=-1, int events=EPOLLIN);

	/** Invoke this event.
	 * @param revents the event types which occurred.
	 */
	void operator()(int revents);
};

}

#endif
