// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_ENCODING
#define HELLMOUTH_ASN1_ENCODING

namespace hellmouth::asn1 {

/** A type to represent an ASN.1 encoding method. */
enum encoding {
	/** Indicate use of the Basic Encoding Rules. */
	encoding_ber,
	/** Indicate use of the Canonical Encoding Rules. */
	encoding_cer,
	/** Indicate use of the Distinguished Encoding Rules. */
	encoding_der,
	/** Indicate use of the Packed Encoding Rules. */
	encoding_per,
	/** Indicate use of the XML Encoding Rules. */
	encoding_xer
};

}

#endif
