// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_TAG
#define HELLMOUTH_ASN1_TAG

namespace hellmouth::asn1 {

/** A type to represent the numeric component of an ASN.1 tag.
 * Tag numbers can in principle be arbitrarily large, however
 * we accept the limitation here that they be within the range
 * of an unsigned integer.
 */
typedef unsigned tag_number;

/** A type to represent the class component of an ASN.1 tag. */
enum class_number {
	class_univ = 0,
	class_app = 1,
	class_ctx = 2,
	class_priv = 3
};

/** A structure to represent an ASN.1 tag. */
struct tag {
	/** The class number for this tag. */
	class_number cls;

	/** The tag number for this tag. */
	tag_number num;

	/** Construct tag.
	 * @param cls the required class number
	 * @param num the required tag number
	 */
	constexpr tag(class_number cls, tag_number num):
		cls(cls), num(num) {}

	bool operator==(const tag&) const = default;
};

/** Make an ASN.1 universal tag.
 * @param num the required tag number
 */
constexpr tag univ(tag_number num) {
	return tag(class_univ, num);
}

/** Make an ASN.1 application tag.
 * @param num the required tag number
 */
constexpr tag app(tag_number num) {
	return tag(class_app, num);
}

/** Make an ASN.1 contextual tag.
 * @param num the required tag number
 */
constexpr tag ctx(tag_number num) {
	return tag(class_ctx, num);
}

/** Make an ASN.1 private tag.
 * @param num the required tag number
 */
constexpr tag priv(tag_number num) {
	return tag(class_priv, num);
}

}

#endif
