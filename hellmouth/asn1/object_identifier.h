// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_OBJECT_IDENTIFIER
#define HELLMOUTH_ASN1_OBJECT_IDENTIFIER

#include "hellmouth/asn1/ber/encoder.h"
#include "hellmouth/asn1/ber/decoder.h"

namespace hellmouth::asn1 {

/** A class to represent an object identifier. */
class object_identifier {
public:
	/** A type to represent a subidentifier within an OID. */
	typedef uint32_t subidentifier;

	/** A type to represent a sequence of subidentifiers. */
	typedef std::vector<subidentifier> subidentifier_list;

	/** A type to represent a number of subidentifiers. */
	typedef subidentifier_list::size_type size_type;

	/** A type for iterating through a sequence of subidentifiers. */
	typedef subidentifier_list::const_iterator iterator;
private:
	/** The sequence of subidentifiers contained within this OID. */
	subidentifier_list _subidentifiers;
public:
	/** Construct empty OID.
	 * Note that the resulting value cannot be encoded, since valid OIDs
	 * are required to contain at least two subidentifiers.
	 */
	object_identifier() = default;

	/** Construct OID from sequence of subidentifiers.
	 * @param subidentifiers the sequence of subidentifiers
	 */
	object_identifier(std::initializer_list<subidentifier> subidentifiers):
		_subidentifiers(subidentifiers) {}

	/** Construct OID from sequence of subidentifiers.
	 * @param begin the beginning of the sequence
	 * @param end the end of the sequence
	 */
	template<class It>
	object_identifier(It begin, It end):
		_subidentifiers(begin, end) {}

	/** Get the length of this OID.
	 * @return the number of subidentifiers
	 */
	size_type length() const {
		return _subidentifiers.size();
	}

	/** Access a subidentifier within this OID.
	 * @param index the subidentifier index
	 * @return the corresponding subidentifier
	 */
	subidentifier at(size_type index) const {
		return _subidentifiers.at(index);
	}

	/** Get beginning of sequence of subidentifiers.
	 * @return the beginning of the sequence
	 */
	iterator begin() const {
		return _subidentifiers.begin();
	}

	/** Get end of sequence of subidentifiers.
	 * @return the end of the sequence
	 */
	iterator end() const {
		return _subidentifiers.end();
	}

	bool operator==(const object_identifier&) const = default;
};

/** Encode object identifier.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
ber::encoder& operator<<(ber::encoder& enc, const object_identifier& oid);

/** Decode an object identifier.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
ber::decoder& operator>>(ber::decoder& dec, object_identifier& oid);

}

#endif
