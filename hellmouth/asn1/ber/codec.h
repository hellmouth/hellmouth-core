// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_BER_CODEC
#define HELLMOUTH_ASN1_BER_CODEC

#include "hellmouth/asn1/encoding.h"
#include "hellmouth/asn1/tag.h"

namespace hellmouth::asn1::ber {

/** A base class providing functionality common to encoders and decoders. */
class codec {
public:
	/** A type to represent an encoded octet. */
	typedef unsigned char value_type;

	/** A type for pointing to a mutable encoded octet. */
	typedef value_type* pointer;

	/** A type for pointing to an immutable encoded octet. */
	typedef const value_type* const_pointer;

	/** A type to represent a number of octets. */
	typedef size_t size_type;
private:
	/** The encoding to be enforced (BER, CER or DER). */
	asn1::encoding _encoding = encoding_ber;

	/** The default tag for encoding or decoding a std::string. */
	tag _default_string_tag = asn1::univ(12);
public:
	/** Get the encoding that is being enforced.
	 * @return the encoding (BER, CER or DER)
	 */
	asn1::encoding encoding() const {
		return _encoding;
	}

	/** Set the encoding to be enforced.
	 * @param encoding the required encoding (BER, CER or DER)
	 */
	void encoding(asn1::encoding encoding);

	/** Get the default tag for encoding or decoding a std::string.
	 * @return the tag
	 */
	tag default_string_tag() const {
		return _default_string_tag;
	}

	/** Set the default tag for encoding or decoding a std::string.
	 * @param the tag
	 */
	void default_string_tag(tag tg) {
		_default_string_tag = tg;
	}

	/** Check whether the encoding is BER.
	 * @return true if BER, otherwise false
	 */
	bool ber() const {
		return _encoding == encoding_ber;
	}

	/** Check whether the encoding is CER.
	 * @return true if CER, otherwise false
	 */
	bool cer() const {
		return _encoding == encoding_cer;
	}

	/** Check whether the encoding is DER.
	 * @return true if DER, otherwise false
	 */
	bool der() const {
		return _encoding == encoding_der;
	}
};

}

#endif
