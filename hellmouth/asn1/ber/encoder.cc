// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cmath>
#include <utility>

#include "hellmouth/octet/string.h"
#include "hellmouth/os/utility.h"
#include "hellmouth/asn1/ber/encoder.h"

namespace hellmouth::asn1::ber {

encoder::encoder(size_t capacity) {
	_base = static_cast<pointer>(malloc(capacity));
	if (!_base) {
		os::throw_errno();
	}
	_ptr = _base;
	_eptr = _base + capacity;
}

encoder::encoder(const encoder& config, size_t capacity):
	codec(config) {

	_base = static_cast<pointer>(malloc(capacity));
	if (!_base) {
		os::throw_errno();
	}
	_ptr = _base;
	_eptr = _base + capacity;
}

encoder::~encoder() {
	free(_base);
}

void encoder::reserve(size_type capacity) {
	size_type index = tell();
	if (index > capacity) {
		index = capacity;
	}

	pointer base = static_cast<pointer>(realloc(_base, capacity));
	if (!base) {
		os::throw_errno();
	}

	_base = base;
	_ptr = _base + index;
	_eptr = _base + capacity;
}

void encoder::write_base128(uint64_t value) {
	size_t length = 0;
	uint64_t temp = value;
	while (temp != 0) {
		length += 1;
		temp >>= 7;
	}
	if (length == 0) {
		length = 1;
	}
	ensure(length);

	uint8_t flag = 0;
	for (size_t i = length; i != 0; --i) {
		_ptr[i - 1] = (value & 0x7f) | flag;
		value >>= 7;
		flag = 0x80;
	}
	_ptr += length;
}

void encoder::write_identifier(tag tg, bool cons) {
	unsigned char id0 = tg.cls << 6;
	if (cons) {
		id0 |= 0x20;
	}

	if (tg.num > 30) {
		id0 |= 0x1f;
	} else {
		id0 |= tg.num;
	}
	ensure(1);
	*_ptr++ = id0;

	if (tg.num > 30) {
		write_base128(tg.num);
	}
}

encoder::pointer encoder::write_content(size_type length) {
	size_t extra = 0;
	if (length >= 0x80) {
		size_t buffer = length;
		while (buffer != 0) {
			extra += 1;
			buffer >>= 8;
		}
	}

	size_t count = length + 1 + extra;
	ensure(count);

	if (length >= 0x80) {
		*_ptr = 0x80 + extra;
		size_t buffer = length;
		for (size_t i = extra; i != 0; --i) {
			_ptr[i] = buffer;
			buffer >>= 8;
		}
		_ptr += 1 + extra;
	} else {
		*_ptr++ = length;
	}
	pointer buffer = _ptr;
	_ptr += length;
	return buffer;
}

void encoder::write_content(octet::string_view content) {
	pointer dest = write_content(content.length());
	content.copy(dest, content.length());
}

void encoder::begin_content() {
	ensure(1);
	*_ptr++ = 0x80;
}

void encoder::end_content() {
	ensure(2);
	*_ptr++ = 0x00;
	*_ptr++ = 0x00;
}

encoder& operator<<(encoder& enc, double value) {
	enc.write_identifier(asn1::univ(9), false);
	switch (std::fpclassify(value)) {
	case FP_ZERO:
		if (std::signbit(value)) {
			enc.write_content(octet::string{0x43});
		} else {
			enc.write_content(octet::string());
		}
		break;
	case FP_NORMAL:
	case FP_SUBNORMAL:
		{
			uint8_t content0 = 0x80;
			if (std::signbit(value)) {
				content0 |= 0x40;
				value = abs(value);
			}

			int exponent = 0;
			uint64_t mantissa = ldexp(frexp(value, &exponent), 64);
			exponent -= 64;
			if (mantissa == 0) {
				// This should not happen, but if it did then an
				// infinite loop would follow if not caught.
				throw std::runtime_error(
					"internal error encoding real value");
			}
			// [TODO]: a more efficient algorithm could be used here
			while ((mantissa & 1) == 0) {
				mantissa >>= 1;
				exponent += 1;
			}
			if (!std::in_range<int8_t>(exponent)) {
				content0 |= 0x01;
			}
			uint16_t uexponent = exponent;

			octet::string content{content0};
			if (content0 & 0x01) {
				content.push_back(uexponent >> 8);
			}
			content.push_back(uexponent);
			while ((mantissa >> 56) == 0) {
				mantissa <<= 8;
			}
			while (mantissa != 0) {
				content.push_back(mantissa >> 56);
				mantissa <<= 8;
			}
			enc.write_content(content);
		}
		break;
	case FP_INFINITE:
		if (std::signbit(value)) {
			enc.write_content(octet::string{0x41});
		} else {
			enc.write_content(octet::string{0x40});
		}
		break;
	case FP_NAN:
		enc.write_content(octet::string{0x42});
		break;
	default:
		throw std::runtime_error("failed to classify real number");
	}
	return enc;
}

encoder& operator<<(encoder& enc, octet::string_view value) {
	if (enc.cer() && value.length() > 1000) {
		enc.write_identifier(asn1::univ(4), true);
		enc.begin_content();
		while (!value.empty()) {
			encoder::size_type count = value.length();
			if (count > 1000) {
				count = 1000;
			}
			enc.write_identifier(asn1::univ(4), false);
			enc.write_content(value.substr(0, count));
			value.remove_prefix(count);
		}
		enc.end_content();
	} else {
		enc.write_identifier(asn1::univ(4), false);
		enc.write_content(value);
	}
	return enc;
}

encoder& operator<<(encoder& enc, std::string_view value) {
	if (enc.cer() && value.length() > 1000) {
		enc.write_identifier(enc.default_string_tag(), true);
		enc.begin_content();
		while (!value.empty()) {
			encoder::size_type count = value.length();
			if (count > 1000) {
				count = 1000;
			}
			octet::string_view segment(
				reinterpret_cast<const unsigned char*>(value.data()),
				count);
			value.remove_prefix(count);
			enc.write_identifier(enc.default_string_tag(), false);
			enc.write_content(segment);
		}
		enc.end_content();
	} else {
		octet::string_view segment(
			reinterpret_cast<const unsigned char*>(value.data()),
			value.length());
		enc.write_identifier(enc.default_string_tag(), false);
		enc.write_content(segment);
	}
	return enc;
}

/** A class to represent part of a std::vector<bool>.
 * Functionality analogous to std::span, but restricted to the
 * needs of asn1::ber::encoder.
 */
class bitstring_view {
private:
	const std::vector<bool>* _bitstring;
	size_t _offset = 0;
	size_t _size;
public:
	bitstring_view(const std::vector<bool>& bitstring):
		_bitstring(&bitstring),
		_size(bitstring.size()) {}

	bitstring_view(bitstring_view view, size_t offset, size_t size):
		_bitstring(view._bitstring),
		_offset(view._offset + offset),
		_size(size) {}

	size_t size() const {
		return _size;
	}

	bool empty() const {
		return _size == 0;
	}

	auto begin() const {
		return _bitstring->begin() + _offset;
	}

	auto end() const {
		return _bitstring->begin() + _offset + _size;
	}

	bool operator[](size_t index) const {
		return (*_bitstring)[_offset + index];
	}

	bitstring_view first(size_t count) {
		return bitstring_view(*_bitstring, _offset, count);
	}

	bitstring_view last(size_t count) {
		return bitstring_view(*_bitstring, _offset + _size - count, count);
	}
};

encoder& operator<<(encoder& enc, bitstring_view value) {
	enc.write_identifier(asn1::univ(3), false);
	size_t bcount = value.size();
	size_t padbits = 7 - ((bcount + 7) % 8);
	encoder::size_type ocount = (bcount + padbits) / 8;

	auto ptr = enc.write_content(ocount + 1);
	*ptr++ = padbits;
	unsigned acc = 1;
	for (bool bit : value) {
		acc <<= 1;
		acc |= bit;
		if (acc >= 0x100) {
			*ptr++ = acc & 0xff;
			acc = 1;
		}
	}
	if (acc != 1) {
		while (acc <= 0x100) {
			acc <<= 1;
		}
		*ptr++ = acc & 0xff;
	}
	return enc;
}

encoder& operator<<(encoder& enc, const std::vector<bool>& value) {
	if (enc.cer() && value.size() > 7992) {
		enc.write_identifier(asn1::univ(3), true);
		enc.begin_content();
		bitstring_view remaining(value);
		while (!remaining.empty()) {
			size_t bcount = remaining.size();
			if (bcount > 7992) {
				bcount = 7992;
			}
			enc << remaining.first(bcount);
			remaining = remaining.last(remaining.size() - bcount);
		}
		enc.end_content();
	} else {
		enc << bitstring_view(value);
	}
	return enc;
}

template encoder& operator<<<bool>(encoder&, bool);

template encoder& operator<<<uint8_t>(encoder&, uint8_t);
template encoder& operator<<<uint16_t>(encoder&, uint16_t);
template encoder& operator<<<uint32_t>(encoder&, uint32_t);
template encoder& operator<<<uint64_t>(encoder&, uint64_t);

template encoder& operator<<<int8_t>(encoder&, int8_t);
template encoder& operator<<<int16_t>(encoder&, int16_t);
template encoder& operator<<<int32_t>(encoder&, int32_t);
template encoder& operator<<<int64_t>(encoder&, int64_t);

}
