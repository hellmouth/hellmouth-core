// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_BER_DECODER
#define HELLMOUTH_ASN1_BER_DECODER

#include <cstdint>
#include <optional>
#include <tuple>
#include <vector>
#include <string>
#include <concepts>

#include "hellmouth/octet/string.h"
#include "hellmouth/octet/string_view.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/concepts.h"
#include "hellmouth/asn1/ber/codec.h"

namespace hellmouth::asn1::ber {

/** A class for decoding from ASN.1 using BER, CER or DER. */
class decoder:
	public codec {
private:
	/** A pointer to the next octet to be decoded. */
	const_pointer _ptr;

	/** A pointer to one past the end of the encoded data. */
	const_pointer _eptr;

	/** The cached ident, or empty if not cached. */
	std::optional<std::tuple<tag, bool>> _ident;

	/** Report that the end of the data was reached unexpectedly. */
	void _end_of_data() const;
public:
	/** Construct new decoder.
	 * The content passed to this function must remain valid until
	 * decoding has been completed.
	 *
	 * @param data the data to be decoded
	 */
	decoder(octet::string_view data);

	/** Construct new decoder with copied config.
	 * The content passed to this function must remain valid until
	 * decoding has been completed.
	 *
	 * @param config an existing decoder with the required configuration
	 * @param data the data to be decoded
	 */
	decoder(const decoder& config, octet::string_view data);

	/** Check whether the end of the sequence has been reached.
	 * @return true if no octets remaining, otherwise false
	 */
	bool empty() const {
		return _ptr == _eptr;
	}

	/** Get the number of octets remaining.
	 * @return the number of octets
	 */
	size_type remaining() const {
		return _eptr - _ptr;
	}

	/** Ensure that a given number of octets are remaining.
	 * @param count the required number of octets
	 */
	void ensure(size_type count) const {
		if (remaining() < count) {
			_end_of_data();
		}
	}

	/** Read a single octet.
	 * @return the octet
	 */
	unsigned char read_octet() {
		ensure(1);
		return *_ptr++;
	}

	/** Read a sequence of octets.
	 * @param count the number of octets to be read
	 * @return the octets
	 */
	octet::string_view read_octets(size_type count) {
		ensure(count);
		octet::string_view seq(_ptr, count);
		_ptr += count;
		return seq;
	}

	/** Read a base-128 value.
	 * @return the value read
	 */
	uint64_t read_base128();

	/** Peek at the identifier octets of the next ASN.1 object.
	 * The identifier octets are consumed, but cached to prevent
	 * them from being read twice.
	 *
	 * @return a tuple containing the tag, and a flag equal to
	 *  true if constructed or false if primitive
	 */
	std::tuple<tag, bool> peek_identifier();

	/** Read the identifier octets of the next ASN.1 object.
	 * The identifier octets are consumed if they have not
	 * already been read and cached.
	 *
	 * @return a tuple containing the tag, and a flag equal to
	 *  true if constructed or false if primitive
	 */
	std::tuple<tag, bool> read_identifier() {
		auto ident = peek_identifier();
		_ident.reset();
		return ident;
	}

	/** Detect identifier octets for a given tag.
	 * @param expected_tag the expected tag
	 * @param expected_cons true to require a constructed object,
	 *  or false to require a primitive object
	 * @return true if observed, otherwise false
	 */
	bool detect(tag expected_tag, bool expected_cons) {
		return peek_identifier() == std::tuple{expected_tag, expected_cons};
	}

	/** Expect identifier octets for a given tag.
	 * @param expected_tag the expected tag
	 * @param expected_cons true to require a constructed object,
	 *  or false to require a primitive object
	 */
	void expect(tag expected_tag, bool expected_cons);

	/** Expect identifier octets for a given tag.
	 * @param expected_tag the expected tag
	 * @return true if constructed, false if primitive
	 */
	bool expect(tag expected_tag);

	/** Check for the end of indefinite-length content.
	 * If the end marker has been reached then it is consumed.
	 *
	 * @param required true if end of content is required here,
	 *  false if optional
	 * @return true if end has been reached, otherwise false
	 */
	bool end_content(bool required = false);

	/** Read the length and content of an ASN.1 object.
	 * The indefinite flag should be set according to the object type,
	 * without regard to the encoding. Restrictions arisng from the
	 * latter are applied automatically:
	 *
	 * - Indefinite-length is always forbidden by DER.
	 * - Definite-length is forbidden by CER if indefinite-length
	 *   would be permitted.
	 *
	 * @param indefinite true to allow indefinite-length content,
	 *  or false to disallow
	 * @return either the content of the object, or nothing if the
	 *  object is of indefinite length
	 */
	std::optional<octet::string_view> read_content(bool indefinite = true);

	/** Visit a member of a structure.
	 * @param name the name of the member
	 * @param value the member
	 */
	template<typename T>
	void operator()(const char* name, T& value) {
		*this >> value;
	}

	/** Visit a tagged member of a structure.
	 * @param name the name of the member
	 * @param value the member
	 * @param tg the applicable tag
	 */
	template<typename T, typename... Args>
	void operator()(const char* name, T& value, tag tg, Args...) {
		expect(tg, true);
		auto content = read_content(true);
		if (content) {
			decoder mdec(*content);
			mdec >> value;
		} else {
			(*this) >> value;
			end_content(true);
		}
	}

	/** Visit an optional member of a structure.
	 * Note that optional members must be tagged.
	 *
	 * @param name the name of the member
	 * @param value the member
	 * @param tg the applicable tag
	 */
	template<typename T, typename... Args>
	void operator()(const char* name, std::optional<T>& value,
		tag tg, Args... args) {
		if (detect(tg, true)) {
			T ovalue;
			(*this)(name, ovalue, tg, args...);
			value = std::move(ovalue);
		} else {
			value.reset();
		}
	}

	/** Visit a member of a structure with an argument of unrecognised type.
	 * Unrecognised arguments are disregarded, but do not prevent subsequent
	 * recognised arguments from being processed.
	 *
	 * @param name the name of the member
	 * @param value the member
	 */
	template<typename T, typename U, typename... Args>
	void operator()(const char* name, T& value, const U&, Args... args) {
		(*this)(name, value, args...);
	}
};

/** Decode boolean.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
template<typename T>
requires std::same_as<T, bool>
inline decoder& operator>>(decoder& dec, T& value) {
	dec.expect(asn1::univ(1), false);
	octet::string_view content = *dec.read_content(false);
	if (content.length() != 1) {
		throw std::invalid_argument(
			"expected content of length 1 when decoding boolean");
	}
	if (!dec.ber() && ((content[0] + 1) & 0xfe) != 0) {
		throw std::invalid_argument(
			"disallowed value when decoding boolean");
	}
	value = content[0];
	return dec;
}

/** Decode any unsigned integer type except for boolean.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
template<typename T>
requires (std::unsigned_integral<T> && !std::same_as<T, bool>)
inline decoder& operator>>(decoder& dec, T& value) {
	dec.expect(asn1::univ(2), false);
	octet::string_view content = *dec.read_content(false);
	if (content.empty()) {
		throw std::invalid_argument(
			"expected non-empty object when decoding unsigned integer");
	}
	if (content[0] >= 0x80) {
		throw std::invalid_argument(
			"expected non-negative value when decoding unsigned integer");
	}
	size_t leading = 0;
	while (content.length() > 1 && content[0] == 0) {
		leading += 1;
		content.remove_prefix(1);
	}
	if (leading > 1 || (leading == 1 && content[0] < 0x80)) {
		throw std::invalid_argument(
			"unnecessary leading zeros when decoding unsigned integer");
	}
	if (content.length() > sizeof(T)) {
		throw std::invalid_argument(
			"value out of range when decoding unsigned integer");
	}

	T acc = 0;
	for (auto b : content) {
		acc <<= 8;
		acc |= b;
	}
	value = acc;
	return dec;
}

/** Obtain the octet required to sign-extend a given octet.
 * @param value the octet to be sign-extended
 * @return the required sign-extension octet
 */
inline uint8_t ext(uint8_t value) {
	return ~((value >> 7) - 1);
}

/** Decode any signed integer type.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
template<typename T>
requires std::signed_integral<T>
inline decoder& operator>>(decoder& dec, T& value) {
	dec.expect(asn1::univ(2), false);
	octet::string_view content = *dec.read_content(false);
	if (content.empty()) {
		throw std::invalid_argument(
			"expected non-zero length when decoding signed integer");
	}
	if (content.length() > 1 && content[0] == ext(content[1])) {
		throw std::invalid_argument(
			"unnecessary leading bits when decoding signed integer");
	}
	if (content.length() > sizeof(T)) {
		throw std::invalid_argument(
			"value out of range when decoding signed integer");
	}

	std::make_unsigned_t<T> acc = (content[0] < 0x80) ? 0 : -1;
	for (auto b : content) {
		acc <<= 8;
		acc |= b;
	}
	value = std::bit_cast<T>(acc);
	return dec;
}

/** Decode double-precision real value.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
decoder& operator>>(decoder& dec, double& value);

/** Decode octet string.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
decoder& operator>>(decoder& dec, octet::string& value);

/** Decode character string.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
decoder& operator>>(decoder& dec, std::string& value);

/** Decode any visitable structure.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
template<visitable T>
decoder& operator>>(decoder& dec, T& value) {
	dec.expect(asn1::univ(16), true);
	auto content = dec.read_content();
	if (content) {
		decoder mdec(dec, *content);
		value.visit(mdec);
	} else {
		value.visit(dec);
		dec.end_content(true);
	}
	return dec;
}

/** Decode a sequence of any type.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
template<class T>
decoder& operator>>(decoder& dec, std::vector<T>& value) {
	dec.expect(asn1::univ(16), true);
	auto content = dec.read_content();
	if (content) {
		decoder mdec(*content);
		value.clear();
		while (!mdec.empty()) {
			T item;
			mdec >> item;
			value.push_back(std::move(item));
		}
	} else {
		value.clear();
		while (!dec.end_content()) {
			T item;
			dec >> item;
			value.push_back(std::move(item));
		}
	}
	return dec;
}

/** Decode a bit string.
 * @param dec a decoder to provide the encoded data
 * @param value a buffer to hold the decoded value
 * @return the decoder
 */
decoder& operator>>(decoder& dec, std::vector<bool>& value);

extern template decoder& operator>><bool>(decoder&, bool&);

extern template decoder& operator>><uint8_t>(decoder&, uint8_t&);
extern template decoder& operator>><uint16_t>(decoder&, uint16_t&);
extern template decoder& operator>><uint32_t>(decoder&, uint32_t&);
extern template decoder& operator>><uint64_t>(decoder&, uint64_t&);

extern template decoder& operator>><int8_t>(decoder&, int8_t&);
extern template decoder& operator>><int16_t>(decoder&, int16_t&);
extern template decoder& operator>><int32_t>(decoder&, int32_t&);
extern template decoder& operator>><int64_t>(decoder&, int64_t&);

}

#endif
