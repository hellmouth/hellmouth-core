// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cmath>
#include <stdexcept>
#include <utility>
#include <bit>
#include <regex>

#include "hellmouth/asn1/ber/decoder.h"

namespace hellmouth::asn1::ber {

decoder::decoder(octet::string_view data):
	_ptr(data.data()),
	_eptr(_ptr + data.length()) {}

decoder::decoder(const decoder& config, octet::string_view data):
	codec(config),
	_ptr(data.data()),
	_eptr(_ptr + data.length()) {}

void decoder::_end_of_data() const {
	throw std::out_of_range("unexpected end of data");
}

uint64_t decoder::read_base128() {
	uint64_t acc = 0;
	bool done = false;
	while (!done) {
		if ((acc >> 57) != 0) {
			throw std::invalid_argument(
				"base-128 value outside supported range");
		}
		acc <<= 7;
		unsigned char octet = read_octet();
		acc |= octet & 0x7f;
		done = !(octet & 0x80);
	}
	return acc;
}

std::tuple<tag, bool> decoder::peek_identifier() {
	if (_ident) {
		return *_ident;
	}

	unsigned char id0 = read_octet();
	bool cons = id0 & 0x20;
	class_number cls = static_cast<class_number>(id0 >> 6);
	tag_number num = id0 & 0x1f;

	if (num == 0x1f) {
		uint64_t value = read_base128();
		if (!std::in_range<tag_number>(value)) {
			throw std::invalid_argument(
				"tag number outside supported range");
		}
		num = value;
	}

	_ident = std::tuple<tag, bool>{tag(cls, num), cons};
	return *_ident;
}

void decoder::expect(tag expected_tag, bool expected_cons) {
	auto [observed_tag, observed_cons] = read_identifier();
	if (observed_tag != expected_tag) {
		throw std::invalid_argument("unexpected tag");
	}
	if (observed_cons != expected_cons) {
		if (expected_cons) {
			throw std::invalid_argument("expected constructed object");
		} else {
			throw std::invalid_argument("expected primitive object");
		}
	}
}

bool decoder::expect(tag expected_tag) {
	auto [observed_tag, observed_cons] = read_identifier();
	if (observed_tag != expected_tag) {
		throw std::invalid_argument("unexpected tag");
	}
	return observed_cons;
}

bool decoder::end_content(bool required) {
	ensure(2);
	if (_ptr[0] == 0 && _ptr[1] == 0) {
		_ptr += 2;
		return true;
	} else if (required) {
		throw std::invalid_argument("end of content marker expected");
	} else {
		return false;
	}
}

std::optional<octet::string_view> decoder::read_content(bool indefinite) {
	size_t length = read_octet();
	if (length & 0x80) {
		if (length == 0x80) {
			if (!indefinite) {
				// Indefinite-length forbidden by all encodings.
				// This error takes priority where applicable.
				throw std::invalid_argument(
					"definite-length object expected");
			} else if (der()) {
				// Indefinite-length forbidden by DER.
				// Only attribute the error to DER when indefinite-length
				// would have been permitted by other encodings.
				throw std::invalid_argument(
					"indefinite-length encoding forbidden by DER");
			} else {
				return std::optional<octet::string_view>();
			}
		}

		octet::string_view lenseq = read_octets(length & 0x7f);
		size_t acc = 0;
		while (!lenseq.empty()) {
			size_t before = acc;
			acc <<= 8;
			if ((acc >> 8) != before) {
				throw std::invalid_argument("long-form length out of range");
			}
			acc |= lenseq.front();
			lenseq.remove_prefix(1);
		}
		length = acc;
	}

	if (indefinite && cer()) {
		// Definite-length is forbidden by CER if indefinite-length
		// would be permitted, but note that the latter does not apply
		// to primitive objects and therefore definite-length encoding
		// must be allowed in that case.
		throw std::invalid_argument(
			"definite-length encoding forbidden by CER");
	}

	return read_octets(length);
}

decoder& operator>>(decoder& dec, double& value) {
	dec.expect(asn1::univ(9), false);
	octet::string_view content = *dec.read_content(false);
	if (content.empty()) {
		value = 0.0;
	} else if (content[0] & 0x80) {
		uint8_t content0 = content[0];
		content.remove_prefix(1);
		size_t explen = (content0 & 0x03) + 1;
		if (explen == 4) {
			explen = content.at(0);
			content.remove_prefix(1);
			if (explen == 0) {
				throw std::invalid_argument(
					"exponent length must be non-zero");
			}
		}

		int32_t exponent = std::bit_cast<int8_t>(content.at(0));
		content.remove_prefix(1);
		while (--explen) {
			exponent <<= 8; // Requires C++20
			exponent |= content.at(0);
			content.remove_prefix(1);
			if (exponent > 2047) {
				exponent = 2047;
			} else if (exponent < -2048) {
				exponent = -2048;
			}
		}

		uint64_t mantissa = 0;
		while (!content.empty()) {
			if ((mantissa >> 56) == 0) {
				mantissa <<= 8;
				mantissa |= content[0];
				content.remove_prefix(1);
			} else {
				exponent += 8;
				if (exponent > 2047) {
					exponent = 2047;
				}
			}
		}

		value = ldexp(mantissa, exponent);
		if (content0 & 0x40) {
			value = -value;
		}
	} else if (content[0] & 0x40) {
		switch (content[0]) {
		case 0x40:
			value = INFINITY;
			break;
		case 0x41:
			value = -INFINITY;
			break;
		case 0x42:
			value = NAN;
			break;
		case 0x43:
			value = -0.0;
			break;
		default:
			throw std::invalid_argument(
				"failed to decode unrecognised special real value");
		}
	} else {
		std::string ccontent(
			reinterpret_cast<const char*>(content.data() + 1),
			content.length() - 1);
		unsigned format = content[0] & 0x3f;
		if (format == 1) {
			static const std::regex nr1_pattern(
				R"( *[-+]?[0-9]+)");
			if (!std::regex_match(ccontent, nr1_pattern)) {
				throw std::invalid_argument("invalid NR1-format real number");
			}
		} else if (format == 2) {
			static const std::regex nr2_pattern(
				R"( *[-+]?[0-9]*([0-9][.,]|[.,][0-9])[0-9]*)");
			if (!std::regex_match(ccontent, nr2_pattern)) {
				throw std::invalid_argument("invalid NR2-format real number");
			}
		} else if (format == 3) {
			static const std::regex nr3_pattern(
				R"( *[-+ ][0-9]*([0-9][.,]|[.,][0-9])[0-9]*E[-+][0-9]+)");
			if (!std::regex_match(ccontent, nr3_pattern)) {
				throw std::invalid_argument("invalid NR3-format real number");
			}
		}
		auto f = ccontent.find(',');
		if (f != ccontent.npos) {
			ccontent[f] = '.';
		}
		value = std::stod(ccontent);
	}
	return dec;
}

decoder& operator>>(decoder& dec, octet::string& value) {
	bool cons = dec.expect(asn1::univ(4));
	if (cons) {
		value = octet::string();
		std::optional<octet::string_view> content = dec.read_content(true);
		if (content) {
			decoder mdec(dec, *content);
			while (!mdec.empty()) {
				octet::string substring;
				mdec >> substring;
				value.append(substring);
			}
		} else {
			while (!dec.end_content()) {
				octet::string substring;
				dec >> substring;
				value.append(substring);
			}
		}
	} else {
		value = *dec.read_content(false);
	}
	return dec;
}

decoder& operator>>(decoder& dec, std::string& value) {
	bool cons = dec.expect(dec.default_string_tag());
	if (cons) {
		value = std::string();
		std::optional<octet::string_view> content = dec.read_content(true);
		if (content) {
			decoder mdec(dec, *content);
			while (!mdec.empty()) {
				std::string substring;
				mdec >> substring;
				value.append(substring);
			}
		} else {
			while (!dec.end_content()) {
				std::string substring;
				dec >> substring;
				value.append(substring);
			}
		}
	} else {
		octet::string_view content = *dec.read_content(false);
		value.resize(content.size());
		content.copy(
			reinterpret_cast<unsigned char*>(value.data()),
			value.size());
	}
	return dec;
}

decoder& operator>>(decoder& dec, std::vector<bool>& value) {
	bool cons = dec.expect(asn1::univ(3));
	if (cons) {
		value.clear();
		std::optional<octet::string_view> content = dec.read_content(true);
		if (content) {
			decoder mdec(dec, *content);
			while (!mdec.empty()) {
				std::vector<bool> substring;
				mdec >> substring;
				value.insert(value.end(), substring.begin(), substring.end());
			}
		} else {
			while (!dec.end_content()) {
				std::vector<bool> substring;
				dec >> substring;
				value.insert(value.end(), substring.begin(), substring.end());
			}
		}
	} else {
		octet::string_view content = *dec.read_content(false);
		if (content.empty()) {
			throw std::invalid_argument("missing padding length octet in bit string");
		}
		size_t padlen = content[0];
		if (padlen > 7) {
			throw std::invalid_argument("invalid padding length in bit string");
		}
		content.remove_prefix(1);
		if (content.empty() && padlen != 0) {
			throw std::invalid_argument("missing content in bit string");
		}

		size_t bcount = content.length() * 8 - padlen;
		value.resize(bcount);
		for (size_t i = 0; i != bcount; ++i) {
			value[i] = bool((content[i / 8] << (i % 8)) & 0x80);
		}
		if (dec.cer() || dec.der()) {
			size_t padbits = 7 - ((bcount + 7) % 8);
			if (padbits != 0) {
				uint8_t padmask = (1 << padbits) - 1;
				if (content[bcount / 8] & padmask) {
					throw std::invalid_argument(
						"non-zero padding in bit string");
				}
			}
		}
	}
	return dec;
}

template decoder& operator>><bool>(decoder&, bool&);

template decoder& operator>><uint8_t>(decoder&, uint8_t&);
template decoder& operator>><uint16_t>(decoder&, uint16_t&);
template decoder& operator>><uint32_t>(decoder&, uint32_t&);
template decoder& operator>><uint64_t>(decoder&, uint64_t&);

template decoder& operator>><int8_t>(decoder&, int8_t&);
template decoder& operator>><int16_t>(decoder&, int16_t&);
template decoder& operator>><int32_t>(decoder&, int32_t&);
template decoder& operator>><int64_t>(decoder&, int64_t&);

}
