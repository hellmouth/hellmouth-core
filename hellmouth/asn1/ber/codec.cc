// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/asn1/ber/codec.h"

namespace hellmouth::asn1::ber {

void codec::encoding(asn1::encoding encoding) {
	if (_encoding != encoding_ber &&
		_encoding != encoding_cer &&
		_encoding != encoding_der) {

		throw std::invalid_argument("invalid encoding method");
	}
	_encoding = encoding;
}

}
