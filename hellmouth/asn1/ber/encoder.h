// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_BER_ENCODER
#define HELLMOUTH_ASN1_BER_ENCODER

#include <cstdint>
#include <concepts>
#include <optional>
#include <vector>
#include <string_view>

#include "hellmouth/octet/string_view.h"
#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/concepts.h"
#include "hellmouth/asn1/ber/codec.h"

namespace hellmouth::asn1::ber {

/** A class for encoding to ASN.1 using BER or DER. */
class encoder:
	public codec {
private:
	/** A pointer to the start of the encoded data. */
	pointer _base;

	/** A pointer to where the next octet should be encoded. */
	pointer _ptr;

	/** A pointer to one past the end of the buffer. */
	pointer _eptr;
public:
	/** Construct new encoder.
	 * @param capacity the required initial capacity, in octets
	 */
	explicit encoder(size_type capacity = 0x100);

	/** Construct new encoder with copied configuration.
	 * @param config an exiting encoder with the required configuration
	 * @param capacity the required initial capacity, in octets
	 */
	explicit encoder(const encoder& config, size_type capacity = 0x100);

	/** Destroy encoder. */
	~encoder();

	encoder& operator=(const encoder&) = delete;

	/** Get the total buffer capacity.
	 * @return the capacity, in octets
	 */
	size_type capacity() const {
		return _eptr - _base;
	}

	/** Reserve a given total buffer capacity.
	 * @param capacity the required capacity
	 */
	void reserve(size_type capacity);

	/** Get the current location within the buffer.
	 * @return the current location
	 */
	size_type tell() {
		return _ptr - _base;
	}

	/** Get the number of free octets remaining in the buffer.
	 * @return the number of free octets remaining
	 */
	size_type remaining() const {
		return _eptr - _ptr;
	}

	/** Ensure that a given number of octets can be written.
	 * @param count the required number of octets
	 */
	void ensure(size_type count) {
		if (count > remaining()) {
			reserve(tell() + count);
		}
	}

	/** Write a base-128 value.
	 * @param value the value to be written
	 */
	void write_base128(uint64_t value);

	/** Write identifier octets.
	 * @param tg the required tag
	 * @param cons true for a constructed type,
	 *  or false for primitive
	 */
	void write_identifier(tag tg, bool cons);

	/** Write the length and content of a definite-length object.
	 * @param count the length of the content to be written
	 */
	pointer write_content(size_type count);

	/** Write the length and content of a definite-length object.
	 * @param content the content to be written
	 */
	void write_content(octet::string_view content);

	/** Mark the start of a region of indefinite content. */
	void begin_content();

	/** Mark the end of a region of indefinite content. */
	void end_content();

	/** Get the encoded output.
	 * @return the encoded output
	 */
	octet::string_view data() const {
		return octet::string_view(_base, _ptr - _base);
	}

	/** Visit a member of a structure.
	 * @param name the name of the member
	 * @param value the member
	 */
	template<typename T>
	void operator()(const char* name, const T& value) {
		*this << value;
	}

	/** Visit a tagged member of a structure.
	 * @param name the name of the member
	 * @param value the member
	 * @param tg the applicable tag
	 */
	template<typename T, typename... Args>
	void operator()(const char* name, const T& value, tag tg, Args...) {
		write_identifier(tg, true);
		if (der()) {
			encoder menc(*this);
			menc << value;
			write_content(menc.data());
		} else {
			begin_content();
			*this << value;
			end_content();
		}
	}

	/** Visit an optional member of a structure.
	 * Note that optional members must be tagged.
	 *
	 * @param name the name of the member
	 * @param value the member
	 * @param tg the applicable tag
	 */
	template<typename T, typename... Args>
	void operator()(const char* name, const std::optional<T>& value,
		tag tg, Args...) {

		if (value) {
			(*this)(name, *value, tg);
		}
	}

	template<typename T, typename U, typename... Args>
	void operator()(const char* name, const T& value, const U&,
		Args... args) {

		(*this)(name, value, args...);
	}
};

/** Encode boolean.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
template<typename T>
requires std::same_as<T, bool>
inline encoder& operator<<(encoder& enc, T value) {
	enc.write_identifier(asn1::univ(1), false);
	encoder::pointer content = enc.write_content(1);
	*content = (value) ? 0xff : 0x00;
	return enc;
}

/** Encode any unsigned integer type except for boolean.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
template<typename T>
requires (std::unsigned_integral<T> && !std::same_as<T, bool>)
inline encoder& operator<<(encoder& enc, T value) {
	T temp = value;
	encoder::size_type length = 1;
	temp >>= 7;
	while (temp != 0) {
		length += 1;
		temp >>= 8;
	}

	enc.write_identifier(asn1::univ(2), false);
	encoder::pointer content = enc.write_content(length);
	for (encoder::size_type i = length; i != 0; --i) {
		content[i - 1] = value;
		value >>= 8;
	}
	return enc;
}

/** Encode any signed integer type.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
template<typename T>
requires std::signed_integral<T>
inline encoder& operator<<(encoder& enc, T value) {
	std::make_unsigned_t<T> temp = value;
	if (value < 0) {
		temp = ~temp;
	}
	encoder::size_type length = 1;
	temp >>= 7;
	while (temp != 0) {
		length += 1;
		temp >>= 8;
	}

	enc.write_identifier(asn1::univ(2), false);
	encoder::pointer content = enc.write_content(length);
	for (encoder::size_type i = length; i != 0; --i) {
		content[i - 1] = value;
		value >>= 8; // Relying on C++20 behaviour.
	}
	return enc;
}

/** Encode a double-precision real number.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
encoder& operator<<(encoder& enc, double value);

/** Encode a sequence of octets.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
encoder& operator<<(encoder& enc, octet::string_view value);

/** Encode a character string.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
encoder& operator<<(encoder& enc, std::string_view value);

/** Encode any visitable structure.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
template<visitable T>
encoder& operator<<(encoder& enc, T& value) {
	enc.write_identifier(asn1::univ(16), true);
	if (enc.der()) {
		encoder menc(enc);
		value.visit(menc);
		enc.write_content(menc.data());
	} else {
		enc.begin_content();
		value.visit(enc);
		enc.end_content();
	}
	return enc;
}

/** Encode a sequence of any type.
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
template<class T>
encoder& operator<<(encoder& enc, const std::vector<T>& value) {
	enc.write_identifier(asn1::univ(16), true);
	if (enc.der()) {
		encoder menc(enc);
		for (const auto& item : value) {
			menc << item;
		}
		enc.write_content(menc.data());
	} else {
		enc.begin_content();
		for (const auto& item : value) {
			enc << item;
		}
		enc.end_content();
	}
    return enc;
}

/** Encode a bit string.
 * The current interpretation is that:
 *
 * - Content octets include the initial octet, that containing
 *   the number of unused bits.
 * - When using CER, segments are limited to 1000 content octets.
 * - CER segments are consequently limited to 7992 substantive bits.
 *
 * @param enc an encoder to receive the encoded data
 * @param value the value to be encoded
 * @return the encoder
 */
encoder& operator<<(encoder& enc, const std::vector<bool>& value);

extern template encoder& operator<<<bool>(encoder&, bool);

extern template encoder& operator<<<uint8_t>(encoder&, uint8_t);
extern template encoder& operator<<<uint16_t>(encoder&, uint16_t);
extern template encoder& operator<<<uint32_t>(encoder&, uint32_t);
extern template encoder& operator<<<uint64_t>(encoder&, uint64_t);

extern template encoder& operator<<<int8_t>(encoder&, int8_t);
extern template encoder& operator<<<int16_t>(encoder&, int16_t);
extern template encoder& operator<<<int32_t>(encoder&, int32_t);
extern template encoder& operator<<<int64_t>(encoder&, int64_t);

}

#endif
