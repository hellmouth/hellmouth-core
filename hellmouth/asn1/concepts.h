// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_ASN1_CONCEPTS
#define HELLMOUTH_ASN1_CONCEPTS

namespace hellmouth::asn1 {

/** A concept for selecting data structures which can be visited.
 * Visitable structures are identified by the fact that they provide a
 * member function named "visit". This should be a template which
 * accepts a single callable visitor object, and which invokes that
 * vistor once for each member of the structure. The first two arguments
 * passed to the vistor object should be the name of the member and the
 * member itself. Any number of additional arguments, of any type, may
 * optionally be appended. It must be possible to visit both const and
 * non-const instances of the structure, and if non-const then the visitor
 * must be able to mutate the structure members.
 */
template<typename S>
concept visitable = requires(S value) {
	value.visit([](S&){});
};

}

#endif
