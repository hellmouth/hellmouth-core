// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <ranges>
#include <utility>

#include "hellmouth/asn1/tag.h"
#include "hellmouth/asn1/object_identifier.h"

namespace hellmouth::asn1 {

ber::encoder& operator<<(ber::encoder& enc, const object_identifier& oid) {
	ber::encoder menc(enc);
	uint64_t combined = uint64_t(oid.at(0) * 40) + oid.at(1);
	menc.write_base128(combined);
	for (auto subid : oid | std::views::drop(2)) {
		menc.write_base128(subid);
	}
	enc.write_identifier(asn1::univ(6), false);
	enc.write_content(menc.data());
	return enc;
}

ber::decoder& operator>>(ber::decoder& dec, object_identifier& oid) {
	dec.expect(asn1::univ(6));
	octet::string_view content = *dec.read_content(false);
	ber::decoder mdec(dec, content);

	std::vector<uint32_t> acc;
	uint64_t combined= mdec.read_base128();
	uint32_t subid0 = (combined < 80) ? combined / 40 : 2;
	uint64_t subid1 = combined - subid0 * 40;
	if (!std::in_range<uint32_t>(subid1)) {
		throw std::invalid_argument("OID subidentifier out of range");
	}
	acc.push_back(subid0);
	acc.push_back(subid1);

	while (!mdec.empty()) {
		uint32_t subid = mdec.read_base128();
		if (!std::in_range<uint32_t>(subid)) {
			throw std::invalid_argument("OID subidentifier out of range");
		}
		acc.push_back(subid);
	}
	oid = object_identifier(acc.begin(), acc.end());
	return dec;
}

}
