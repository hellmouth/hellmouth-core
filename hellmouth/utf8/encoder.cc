// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/utf8/encoder.h"

namespace hellmouth::utf8 {

std::string encode(char32_t value) {
	std::string result;
	if (value <= 0x7f) {
		result.push_back(value);
	} else if (value <= 0x7ff) {
		result.push_back(0xc0 | (value >> 6));
		result.push_back(0x80 | (value & 0x3f));
	} else if (value <= 0xffff) {
		result.push_back(0xe0 | (value >> 12));
		result.push_back(0x80 | ((value >> 6) & 0x3f));
		result.push_back(0x80 | (value & 0x3f));
	} else if (value <= 0x10ffff) {
		result.push_back(0xf0 | (value >> 18));
		result.push_back(0x80 | ((value >> 12) & 0x3f));
		result.push_back(0x80 | ((value >> 6) & 0x3f));
		result.push_back(0x80 | (value & 0x3f));
	} else {
		throw std::domain_error("invalid Unicode code point");
	}
	return result;
}

}
