// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_UTF8_ENCODER
#define HELLMOUTH_UTF8_ENCODER

#include <string>

namespace hellmouth::utf8 {

std::string encode(char32_t value);

}

#endif
