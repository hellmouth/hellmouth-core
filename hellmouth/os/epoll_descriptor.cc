// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <sys/epoll.h>

#include "hellmouth/os/utility.h"
#include "hellmouth/os/epoll_descriptor.h"

namespace hellmouth::os {

epoll_descriptor::epoll_descriptor(int flags):
	file_descriptor(::epoll_create1(flags)) {

	if (!*this) {
		throw_errno();
	}
}

void epoll_descriptor::add(int fd, epoll_event& epev) {
	if (::epoll_ctl(*this, EPOLL_CTL_ADD, fd, &epev) == -1) {
		throw_errno();
	}
}

void epoll_descriptor::mod(int fd, epoll_event& epev) {
	if (::epoll_ctl(*this, EPOLL_CTL_MOD, fd, &epev) == -1) {
		throw_errno();
	}
}

void epoll_descriptor::del(int fd) {
	if (::epoll_ctl(*this, EPOLL_CTL_DEL, fd, 0) == -1) {
		throw_errno();
	}
}

size_t epoll_descriptor::wait(epoll_event* events, int maxevents, int timeout) {
	int count = ::epoll_wait(*this, events, maxevents, timeout);
	if (count == -1) {
		throw_errno();
	}
	return count;
}

}
