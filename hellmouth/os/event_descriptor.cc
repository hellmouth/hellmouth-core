// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>
#include <sys/eventfd.h>

#include "hellmouth/os/event_descriptor.h"
#include "hellmouth/os/utility.h"

namespace hellmouth::os {

event_descriptor::event_descriptor(unsigned int initval, int flags):
	file_descriptor(eventfd(initval, flags)) {

	if (!*this) {
		throw_errno();
	}
}

void event_descriptor::notify(uint64_t count) {
	if (::write(*this, &count, sizeof(count)) == -1) {
		throw_errno();
	}
}

uint64_t event_descriptor::wait() {
	uint64_t count;
	if (::read(*this, &count, sizeof(count)) == -1) {
		throw_errno();
	}
	return count;
}

}
