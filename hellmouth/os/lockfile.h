// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_LOCKFILE
#define HELLMOUTH_OS_LOCKFILE

#include <filesystem>

#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A class to represent a lockfile.
 * This class attempts to acquire an advisory lock on the file at the given
 * pathname, creating it if necessary. The lock is an exclusive one and is
 * created using flock.
 *
 * While an instance of this class exists, no other process will be able to
 * acquire a lock for the same pathname using the same method. If the
 * instance is destroyed then the lock will be released and the lockfile
 * deleted (whether or not that instance was responsible for creating it).
 * Alternatively, if the process which created the instance terminates
 * without first destroying it then the lock will be released. The lockfile
 * will not necessarily be deleted under these circumstances, however that
 * does not prevent a future process from acquiring the same lock.
 *
 * Lockfiles are movable but not copyable.
 */
class lockfile {
private:
	/** The pathname of the lockfile. */
	std::filesystem::path _pathname;

	/** The file descriptor for the lockfile. */
	file_descriptor _fd;
public:
	/** Report that a lock could not be acquired. */
	class lockfile_error;

	/** Create lockfile.
	 * By default this constructor will block until the lock can be
	 * acquired.
	 *
	 * @param pathname the lockfile pathname
	 * @param noblock true to throw an exception if the lockfile cannot be
	 *  acquired immediately, or false to block until it can be acquired.
	 */
	lockfile(std::filesystem::path pathname, bool noblock = false);

	/** Remove lockfile. */
	~lockfile();

	/** Write PID.
	 * Truncate the lockfile, then write the PID of the current process as a
	 * decimal number followed by a linefeed.
	 */
	void write_pid();
};

}

#endif
