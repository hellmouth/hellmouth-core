// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_FILE_DESCRIPTOR
#define HELLMOUTH_OS_FILE_DESCRIPTOR

#include <cstddef>
#include <filesystem>

#include <sys/types.h>

namespace hellmouth::os {

/** A class to represent a file descriptor. */
class file_descriptor {
private:
	/** The raw file descriptor. */
	int _fd;
public:
	/** Create empty file descriptor object. */
	file_descriptor():
		_fd(-1) {}

	/** Wrap raw file descriptor.
	 * The resulting class instance takes ownership of the raw file
	 * descriptor passed to it.
	 *
	 * If a value of -1 is passed in then this constructor will not
	 * overwrite the value of errno.
	 *
	 * @param fd the raw file descriptor, or -1 for an empty file
	 *  descriptor object
	 */
	explicit file_descriptor(int fd):
		_fd(fd) {}

	/** Open file.
	 * The resulting file descriptor object will be non-empty, or if
	 * that is not possible an exception will be thrown.
	 *
	 * @param pathname the file pathname
	 * @param flags flags suitable for passing to open
	 * @param mode the required file permission bits if new file created
	 *  (subject to umask)
	 */
	file_descriptor(const std::filesystem::path& pathname,
		int flags, mode_t mode = 0666);

	/** Close file descriptor. */
	virtual ~file_descriptor();

	/** Copy-construct file descriptor.
	 * This causes the raw file descriptor to be duplicated, therefore the
	 * copy will have a different numerical value.
	 *
	 * @param that the file descriptor to be copied
	 */
	file_descriptor(const file_descriptor& that);

	/** Move-construct file descriptor.
	 * @param that the file descriptor to be moved
	 */
	file_descriptor(file_descriptor&& that) {
		this->_fd = that._fd;
		that._fd = -1;
	}

	/** Copy-assign file descriptor.
	 * This causes the raw file descriptor to be duplicated, therefore the
	 * copy will have a different numerical value.
	 *
	 * @param that the file descriptor to be copied
	 */
	file_descriptor& operator=(const file_descriptor& that);

	/** Move-assign file descriptor.
	 * @param that the file descriptor to be moved
	 */
	file_descriptor& operator=(file_descriptor&& that) {
		std::swap(this->_fd, that._fd);
		return *this;
	}

	/** Test whether file descriptor object is non-empty.
	 * @return true if non-empty, otherwise false
	 */
	operator bool() const {
		return _fd != -1;
	}

	/** Get raw file descriptor.
	 * @return the raw file descriptor, or -1 if empty
	 */
	operator int() const {
		return _fd;
	}

	/** Close this file descriptor.
	 * If the file descriptor is already closed then this has no effect.
	 */
	void close();

	/** Duplicate this file descriptor to a specific descriptor number.
	 * @param fd the required file descriptor number of the duplicate
	 */
	void dup(int fd) const;

	/** Receive data into buffer.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing an exception. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to read into
	 * @param nbyte the maximum number of bytes to be read
	 * @return the number of bytes actually read
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t read(void* buf, size_t nbyte);

	/** Write data from buffer.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing an exception. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to write from
	 * @param nbyte the maximum number of bytes to be written
	 * @return the number of bytes actually written
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t write(const void* buf, size_t nbyte);

	/** Get the size of this file.
	 * @return the size, in octets
	 */
	off_t size() const;

	/** Seek to a given offset.
	 * @param offset the required offset
	 * @param whence the offset reference (SEEK_SET, SEEK_CUR or SEEK_END)
	 * @return the resulting offset in bytes, from the start of the file
	 */
	off_t seek(off_t offset, int whence = SEEK_SET);

	/** Truncate file to given length.
	 * If the file is currently shorter then the requested length then it
	 * will be extended by padding with zeros.
	 * @param length the required length
	 */
	void truncate(off_t length);

	/** Get file status flags.
	 * @return the file status flags
	 */
	int getfl() const;

	/** Set file status flags.
	 * @param flags the required file status flags
	 */
	void setfl(int flags);

	/** Set non-blocking mode.
	 * @param nb true for non-blocking, or false for blocking
	 */
	void nonblock(bool nb);

	/** Flush data and metadata to durable storage. */
	void fsync();

	/** Flush data and metadata to durable storage. */
	void fdatasync();

	/** Establish an exclusive file lock.
	 * @param noblock true to throw an exception if the lock cannot be
	 *  acquired immediately, or false to block until it can be acquired
	 */
	void lock(bool noblock = false);

	/** Release file lock. */
	void unlock();
};

}

#endif
