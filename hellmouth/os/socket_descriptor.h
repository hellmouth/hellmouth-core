// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_SOCKET_DESCRIPTOR
#define HELLMOUTH_OS_SOCKET_DESCRIPTOR

#include <stdexcept>
#include <netinet/in.h>
#include <sys/socket.h>

#include "hellmouth/net/socket_address.h"
#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A class to represent a socket descriptor. */
class socket_descriptor:
	public file_descriptor {
public:
	/** Wrap raw socket descriptor.
	 * The resulting class instance takes ownership of the raw socket
	 * descriptor passed to it.
	 *
	 * If a value of -1 is passed in then this constructor will not
	 * overwrite the value of errno.
	 *
	 * @param sd the raw socket descriptor, or -1 for an empty socket
	 *  descriptor object
	 */
	explicit socket_descriptor(int sd):
		file_descriptor(sd) {}

	/** Create new socket descriptor object.
	 * @param the address family
	 * @param the socket type
	 * @param the protocol
	 */
	socket_descriptor(int domain, int type, int protocol);

	/** Get the address to which this socket is bound.
	 * @param addr a buffer to receive the address
	 * @param addrlen the capacity of the socket address buffer
	 *  on entry, and the length of the socket address on exit
	 */
	void getsockname(struct sockaddr* addr, socklen_t* addrlen);

	/** Get the address to which this socket is bound.
	 * @return the socket address
	 */
	net::socket_address getsockname();

	/** Bind this socket to a socket address.
	 * @param addr the socket address to be bound
	 * @param addrlen the length of the socket address
	 */
	void bind(const struct sockaddr* addr, socklen_t addrlen);

	/** Bind this socket to a socket address.
	 * @param addr the socket address to be bound
	 */
	void bind(const net::socket_address& addr) {
		bind(&static_cast<const sockaddr&>(addr), addr.length());
	}

	/** Listen for connections on this socket.
	 * @param the number of pending connections to queue
	 */
	void listen(int backlog = 5);

	/** Accept a connection on this socket.
	 * @return a socket descriptor for the accepted connection
	 */
	socket_descriptor accept();

	/** Connect this socket to a socket address.
	 * @param addr the socket address to connect to
	 * @param addrlen the length of the socket address
	 */
	void connect(const struct sockaddr* addr, socklen_t addrlen);

	/** Connect this socket to a socket address.
	 * @param addr the socket address to connect to
	 */
	void connect(const net::socket_address& addr) {
		connect(&static_cast<const sockaddr&>(addr), addr.length());
	}

	/** Receive data into buffer.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing a system error. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to receive into
	 * @param nbyte the maximum number of bytes to be received
	 * @param flags flags for specifying the type of reception
	 * @return the number of bytes actually received
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t recv(void* buf, size_t nbyte, int flags = 0);

	/** Receive data into buffer, recording source address.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing a system error. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to receive into
	 * @param nbyte the maximum number of bytes to be received
	 * @param flags flags for specifying the type of reception
	 * @param addr a buffer to hold the source socket address
	 * @param addrlen the capacity of the source socket address buffer
	 *  on entry, and the length of the source socket address on exit
	 * @return the number of bytes actually received
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t recvfrom(void* buf, size_t nbyte, int flags,
		struct sockaddr* addr, socklen_t* addrlen);

	/** Receive data into buffer, recording source address.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing a system error. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to receive into
	 * @param nbyte the maximum number of bytes to be received
	 * @param flags flags for specifying the type of reception
	 * @param addr a buffer to hold the source socket address
	 *  on entry, and the length of the source socket address on exit
	 * @return the number of bytes actually received
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t recvfrom(void* buf, size_t nbyte, int flags,
		net::socket_address& addr);

	/** Send data from buffer.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing a system error. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to send from
	 * @param nbyte the maximum number of bytes to be sent
	 * @param flags flags for specifying the type of transmission
	 * @return the number of bytes actually sent
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t send(const void* buf, size_t nbyte, int flags = 0);

	/** Send data from buffer to given address.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing a system error. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to send from
	 * @param nbyte the maximum number of bytes to be sent
	 * @param flags flags for specifying the type of transmission
	 * @param addr the socket address to send the data to
	 * @param addrlen the length of the socket address
	 * @return the number of bytes actually sent
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t sendto(const void* buf, size_t nbyte, int flags,
		const struct sockaddr* addr, socklen_t addrlen);

	/** Send data from buffer to given address.
	 * The operation is repeated if it fails with EINTR. Other failures
	 * are reported by throwing a system error. Behaviour is otherwise as
	 * per the POSIX function of the same name.
	 *
	 * @param buf the buffer to send from
	 * @param nbyte the maximum number of bytes to be sent
	 * @param flags flags for specifying the type of transmission
	 * @param addr the socket address to send the data to
	 * @return the number of bytes actually sent
	 * @throws std::system_error on failure other than EINTR
	 */
	size_t sendto(const void* buf, size_t nbyte, int flags,
		const net::socket_address& addr) {

		return sendto(buf, nbyte, flags,
			&static_cast<const sockaddr&>(addr), addr.length());
	}

	/** Set socket option.
	 * @param level the protocol level
	 * @param optname the option
	 * @param optval the value
	 * @param optlen the value length, in octets
	 */
	void setsockopt(int level, int optname, const void* optval,
		socklen_t optlen);

	/** Set socket option.
	 * @param level the protocol level
	 * @param optname the option
	 * @param optval the value
	 */
	template<typename T>
	void setsockopt(int level, int optname, const T& optval) {
		setsockopt(level, optname, &optval, sizeof(optval));
	}

	/** Add multicast group.
	 * @param addr the multicast address to be added
	 */
	void add_membership(const struct in_addr& addr);
};

}

#endif
