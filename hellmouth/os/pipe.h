// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_PIPE
#define HELLMOUTH_OS_PIPE

#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A structure to represent a pipe.
 * The lifetime of the underlying pipe can be extended beyond the lifetime
 * of the class instance by moving, copying or duplicating the two file
 * descriptors prior to destruction.
 */
struct pipe {
private:
	/** A class for building new pipes. */
	class builder;

	/** Construct new pipe using pipe builder. */
	explicit pipe(const builder& pb);
public:
	/** A file descriptor for reading output from the pipe. */
	file_descriptor readfd;

	/** A file descriptor for writing input to the pipe. */
	file_descriptor writefd;

	/** Construct new pipe. */
	pipe();
};

}

#endif
