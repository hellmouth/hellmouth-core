// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_EPOLL_DESCRIPTOR
#define HELLMOUTH_OS_EPOLL_DESCRIPTOR

#include <sys/epoll.h>

#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A class to represent an epoll file descriptor. */
class epoll_descriptor:
	public file_descriptor {
public:
	/** Create new epoll descriptor object.
	 * @param flags the bitwise OR of any required flags
	 */
	explicit epoll_descriptor(int flags=0);

	/** Add a file descriptor for monitoring.
	 * @param fd the file descriptor to be added
	 * @param epev the events to be monitored and opaque data returned
	 */
	void add(int fd, epoll_event& epev);

	/** Update the monitoring of a file descriptor.
	 * @param fd the file descriptor to be updated
	 * @param epev the events to be monitored and opaque data returned
	 */
	void mod(int fd, epoll_event& epev);

	/** Remove a file descriptor from monitoring.
	 * @param fd the file descriptor to be removed
	 */
	void del(int fd);

	/** Wait for an event to occur.
	 * @param events an array of epoll_event structures to hold the results
	 * @param maxevents the number of epoll_event structures in the array
	 * @param timeout a timeout in milliseconds, or -1 to wait indefinitely
	 * @return the number of epoll_event slots populated
	 */
	size_t wait(epoll_event* events, int maxevents, int timeout=-1);
};

}

#endif
