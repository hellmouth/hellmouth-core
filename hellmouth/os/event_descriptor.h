// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_EVENT_DESCRIPTOR
#define HELLMOUTH_OS_EVENT_DESCRIPTOR

#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A class to represent an event file descriptor. */
class event_descriptor:
	public file_descriptor {
public:
	/** Wrap raw event descriptor.
	 * The resulting class instance takes ownership of the raw event
	 * descriptor passed to it.
	 *
	 * If a value of -1 is passed in then this constructor will not
	 * overwrite the value of errno.
	 *
	 * @param fd the raw event descriptor, or -1 for an empty event
	 *  descriptor object
	 */
	explicit event_descriptor(int fd):
		file_descriptor(fd) {}

	/** Create new event descriptor object.
	 * Permitted flags include EFD_CLOEXEC, EFD_NONBLOCK and
	 * EFD_SEMAPHORE.
	 *
	 * @param initval the required initial value
	 * @param flags the bitwise OR of any required flags
	 */
	event_descriptor(unsigned int initval, int flags);

	/** Report that one or more events have occurred.
	 * @param count the number of events
	 */
	void notify(uint64_t count = 1);

	/** Wait for an event to occur.
	 * If semaphore semantics are enabled then the number of events
	 * reported will always be equal to one.
	 *
	 * @return the number of events reported
	 */
	uint64_t wait();
};

}

#endif
