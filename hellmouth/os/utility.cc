// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <system_error>

#include "hellmouth/os/utility.h"

namespace hellmouth::os {

void throw_errno() {
	throw std::system_error(errno, std::system_category());
}

std::filesystem::path make_lockname(const std::filesystem::path pathname) {
	std::string filename = std::string(".");
	filename += pathname.filename();
	filename += ".lock";
	return pathname.parent_path() / filename;
}

std::filesystem::path make_tempname(const std::filesystem::path pathname) {
	std::string filename = std::string(".");
	filename += pathname.filename();
	filename += ".tmp";
	return pathname.parent_path() / filename;
}

}
