// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/wait.h>

#include "hellmouth/os/process_descriptor.h"
#include "hellmouth/os/utility.h"

namespace hellmouth::os {

static int pidfd_open(pid_t pid, int flags) {
	return syscall(SYS_pidfd_open, pid, flags);
}

process_descriptor::process_descriptor(pid_t pid, int flags):
	file_descriptor(pidfd_open(pid, flags)) {

	if (!*this) {
		throw_errno();
	}
}

void process_descriptor::kill(int sig) {
	int pidfd = *this;
	siginfo_t* info = 0;
	if (syscall(SYS_pidfd_send_signal, pidfd, sig, info, 0) == -1) {
		throw_errno();
	}
}

siginfo_t process_descriptor::wait(int options) {
	int pidfd = *this;
	siginfo_t info = {0};
	if (waitid(P_PIDFD, pidfd, &info, options) == -1) {
		throw_errno();
	}
	return info;
}

}
