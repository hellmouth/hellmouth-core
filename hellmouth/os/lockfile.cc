// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include <unistd.h>
#include <fcntl.h>

#include "hellmouth/os/lockfile.h"

namespace hellmouth::os {

static bool wouldblock(const std::error_code& errcode) {
	return errcode == std::errc::operation_would_block ||
		errcode == std::errc::resource_unavailable_try_again;
}

class lockfile::lockfile_error:
	public std::runtime_error {
public:
	lockfile_error():
		std::runtime_error("cannot acquire lockfile") {}
};

lockfile::lockfile(std::filesystem::path pathname, bool noblock):
	_pathname(std::move(pathname)),
	_fd(_pathname, O_RDWR|O_CREAT, 0666) {

	try {
		_fd.lock(noblock);
	} catch (std::system_error& ex) {
		if (wouldblock(ex.code())) {
			throw lockfile_error();
		}
		throw;
	}
}

lockfile::~lockfile() {
	try {
		_fd.unlock();
	} catch (std::exception& ex) {
		// No action
	}
	try {
		remove(_pathname.c_str());
	} catch (std::exception& ex) {
		// No action
	}
}

void lockfile::write_pid() {
	std::string pidstr = std::to_string(getpid()) + "\n";
	_fd.truncate(0);
	_fd.write(pidstr.data(), pidstr.length());
}

}
