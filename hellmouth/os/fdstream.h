// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_FDSTREAM
#define HELLMOUTH_OS_FDSTREAM

#include <iostream>

#include "hellmouth/os/fdbuf.h"

namespace hellmouth::os {

/** A stream class for accessing a file or socket descriptor. */
template<class CharT, class Traits = std::char_traits<CharT>>
class basic_fdstream:
	public std::basic_iostream<CharT, Traits> {
private:
	using std::basic_iostream<CharT, Traits>::rdbuf;

	/** The underlying file descriptor buffer. */
	basic_fdbuf<CharT, Traits> _fdbuf;
public:
	/** Construct stream from file descriptor (copied).
	 * This will cause the underlying file descriptor to be
	 * duplicated, therefore there is no requirement for the
	 * original to outive the fdbuf.
	 *
	 * @param fd the underlying file descriptor
	 */
	basic_fdstream(const file_descriptor& fd):
		_fdbuf(fd) {

		rdbuf(&_fdbuf);
	}

	/** Construct stream from file descriptor (moved).
	 * @param fd the underlying file descriptor
	 */
	basic_fdstream(file_descriptor&& fd):
		_fdbuf(std::move(fd)) {

		rdbuf(&_fdbuf);
	}
};

/** A stream class for accessing a file or socket descriptor. */
typedef basic_fdstream<char> fdstream;

}

#endif
