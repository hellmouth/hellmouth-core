// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <cerrno>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/file.h>

#include "hellmouth/os/utility.h"
#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

file_descriptor::file_descriptor(const std::filesystem::path& pathname,
	int flags, mode_t mode):
	_fd(::open(pathname.c_str(), flags, mode)) {

	if (_fd == -1) {
		throw_errno();
	}
}

file_descriptor::~file_descriptor() {
	if (_fd != -1) {
		::close(_fd);
	}
}

file_descriptor::file_descriptor(const file_descriptor& that):
	_fd(-1) {

	if (that._fd != -1) {
		_fd = ::dup(that._fd);
		if (_fd == -1) {
			throw_errno();
		}
	}
}

file_descriptor& file_descriptor::operator=(const file_descriptor& that) {
	if (this != &that) {
		if (_fd != -1) {
			::close(_fd);
			_fd = -1;
		}
		if (that._fd != -1) {
			_fd = ::dup(that._fd);
			if (_fd == -1) {
				throw_errno();
			}
		}
	}
	return *this;
}

void file_descriptor::close() {
	if (_fd != -1) {
		if (::close(_fd) == -1) {
			throw_errno();
		}
		_fd = -1;
	}
}

void file_descriptor::dup(int fd) const {
	if (dup2(_fd, fd) == -1) {
		throw_errno();
	}
}

int file_descriptor::getfl() const {
	return ::fcntl(_fd, F_GETFL);
}


void file_descriptor::setfl(int flags) {
	if (::fcntl(_fd, F_SETFL, flags) == -1) {
		throw_errno();
	}
}

void file_descriptor::nonblock(bool nb) {
	int flags = getfl();
	if (nb) {
		flags |= O_NONBLOCK;
	} else {
		flags &= ~O_NONBLOCK;
	}
	setfl(flags);
}

size_t file_descriptor::read(void* buf, size_t nbyte) {
	while (true) {
		ssize_t count = ::read(*this, buf, nbyte);
		if (count == -1) {
			if (errno != EINTR) {
				throw_errno();
			}
		} else {
			return count;
		}
	}
}

size_t file_descriptor::write(const void* buf, size_t nbyte) {
	while (true) {
		ssize_t count = ::write(*this, buf, nbyte);
		if (count == -1) {
			if (errno != EINTR) {
				throw_errno();
			}
		} else {
			return count;
		}
	}
}

off_t file_descriptor::size() const {
	struct stat statbuf;
	if (::fstat(_fd, &statbuf) == -1) {
		throw_errno();
	}
	return statbuf.st_size;
}

off_t file_descriptor::seek(off_t offset, int whence) {
	offset = lseek(_fd, offset, whence);
	if (offset == -1) {
		throw_errno();
	}
	return offset;
}

void file_descriptor::truncate(off_t length) {
	if (::ftruncate(_fd, length) == -1) {
		throw_errno();
	}
}

void file_descriptor::fsync() {
	if (::fsync(_fd) == -1) {
		throw_errno();
	}
}

void file_descriptor::fdatasync() {
	if (::fdatasync(_fd) == -1) {
		throw_errno();
	}
}

void file_descriptor::lock(bool noblock) {
	int operation = LOCK_EX;
	if (noblock) {
		operation |= LOCK_NB;
	}
	if (::flock(_fd, operation) == -1) {
		throw_errno();
	}
}

void file_descriptor::unlock() {
	if (::flock(_fd, LOCK_UN) == -1) {
		throw_errno();
	}
}

}
