// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_COMMAND
#define HELLMOUTH_OS_COMMAND

#include <vector>
#include <string>
#include <filesystem>

namespace hellmouth::os {

/** A class for executing commands. */
class command {
private:
	/** The pathname of the executable. */
	std::filesystem::path _pathname;

	/** The argument list.
	 * This should begin with the argument that will be placed in
	 * argv[0]. It should not end with a terminator.
	 */
	std::vector<std::string> _arguments;

	/** The working directory for the command. */
	std::filesystem::path _cwd;
public:
	/** Construct command with default first argument.
	 * The first argument will be the leafname of the specified pathname.
	 * @param pathname the pathname of the executable
	 */
	explicit command(std::filesystem::path pathname);

	/** Construct command with given first argument.
	 * @param pathname the pathname of the executable
	 * @param arg0 the first argument
	 */
	command(std::filesystem::path pathname, std::string arg0);

	/** Append argument.
	 * @param arg the argument to be appended
	 * @return a reference to this
	 */
	command& append(std::string arg);

	/** Change working directory.
	 * The specified working directory may be absolute, or relative to the
	 * previous value (which defaults to ".").
	 * @param pathname the new working directory
	 * @return a reference to this
	 */
	command& chdir(std::filesystem::path pathname);

	/** Execute this command synchronously.
	 * @param fork true for fork-and-exec, false for plain exec
	 * @return the exit status
	 */
	int exec(bool fork = true) const;
};

}

#endif
