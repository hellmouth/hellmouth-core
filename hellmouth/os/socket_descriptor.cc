// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <arpa/inet.h>
#include <netinet/ip.h>

#include "hellmouth/os/socket_descriptor.h"
#include "hellmouth/os/utility.h"

namespace hellmouth::os {

socket_descriptor::socket_descriptor(int domain, int type, int protocol):
	file_descriptor(::socket(domain, type, protocol)) {

	if (!*this) {
		throw_errno();
	}
}

void socket_descriptor::getsockname(struct sockaddr* addr, socklen_t* addrlen) {
	if (::getsockname(*this, addr, addrlen) == -1) {
		throw_errno();
	}
}

net::socket_address socket_descriptor::getsockname() {
	net::socket_address addr;
	socklen_t addrlen = addr.capacity();
	getsockname(&static_cast<sockaddr&>(addr), &addrlen);
	addr.resize(addrlen);
	return addr;
}

void socket_descriptor::bind(const struct sockaddr* addr, socklen_t addrlen) {
	if (::bind(*this, addr, addrlen) == -1) {
		throw_errno();
	}
}

void socket_descriptor::listen(int backlog) {
	if (::listen(*this, backlog) == -1) {
		throw_errno();
	}
}

socket_descriptor socket_descriptor::accept() {
	int sd = ::accept(*this, 0, 0);
	if (sd == -1) {
		throw_errno();
	}
	return socket_descriptor(sd);
}

void socket_descriptor::connect(const struct sockaddr* addr, socklen_t addrlen) {
	if (::connect(*this, addr, addrlen) == -1) {
		throw_errno();
	}
}

size_t socket_descriptor::recv(void* buf, size_t nbyte, int flags) {
	while (true) {
		ssize_t count = ::recv(*this, buf, nbyte, flags);
		if (count == -1) {
			if (errno != EINTR) {
				throw_errno();
			}
		} else {
			return count;
		}
	}
}

size_t socket_descriptor::recvfrom(void* buf, size_t nbyte, int flags,
	struct sockaddr* addr, socklen_t* addrlen) {

	while (true) {
		ssize_t count = ::recvfrom(*this, buf, nbyte, flags, addr, addrlen);
		if (count == -1) {
			if (errno != EINTR) {
				throw_errno();
			}
		} else {
			return count;
		}
	}
}

size_t socket_descriptor::recvfrom(void* buf, size_t nbyte, int flags,
	net::socket_address& addr) {

	socklen_t addrlen = addr.capacity();
	size_t count = recvfrom(buf, nbyte, flags,
		&static_cast<sockaddr&>(addr), &addrlen);
	addr.resize(addrlen);
	return count;
}

size_t socket_descriptor::send(const void* buf, size_t nbyte, int flags) {
	while (true) {
		ssize_t count = ::send(*this, buf, nbyte, flags);
		if (count == -1) {
			if (errno != EINTR) {
				throw_errno();
			}
		} else {
			return count;
		}
	}
}

size_t socket_descriptor::sendto(const void* buf, size_t nbyte, int flags,
	const struct sockaddr* addr, socklen_t addrlen) {

	while (true) {
		ssize_t count = ::sendto(*this, buf, nbyte, flags, addr, addrlen);
		if (count == -1) {
			if (errno != EINTR) {
				throw_errno();
			}
		} else {
			return count;
		}
	}
}

void socket_descriptor::setsockopt(int level, int optname, const void* optval,
	socklen_t optlen) {

	if (::setsockopt(*this, level, optname, optval, optlen) == -1) {
		throw_errno();
	}
}

void socket_descriptor::add_membership(const struct in_addr& addr) {
	struct ip_mreqn mreq = {0};
	mreq.imr_multiaddr = addr;
	mreq.imr_address.s_addr = htonl(INADDR_ANY);
	setsockopt(IPPROTO_IP, IP_ADD_MEMBERSHIP, mreq);
}

}
