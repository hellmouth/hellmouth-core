// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>
#include <sys/wait.h>
#include <linux/close_range.h>

#include "hellmouth/os/utility.h"
#include "hellmouth/os/command.h"

namespace hellmouth::os {

command::command(std::filesystem::path pathname):
	_pathname(std::move(pathname)),
	_cwd(".") {

	_arguments.push_back(_pathname.filename());
}

command::command(std::filesystem::path pathname, std::string arg0):
	_pathname(std::move(pathname)),
	 _cwd(".") {

	_arguments.push_back(std::move(arg0));
}

command& command::append(std::string arg) {
	_arguments.push_back(std::move(arg));
	return *this;
}

command& command::chdir(std::filesystem::path pathname) {
	_cwd /= pathname;
	return *this;
}

int command::exec(bool fork) const {
	if (!fork) {
		if (close_range(3, ~0U, CLOSE_RANGE_UNSHARE) == -1) {
			_exit(127);
		}
		if (::chdir(_cwd.c_str()) == -1) {
			_exit(127);
		}
		std::vector<char*> argv;
		for (auto& arg : _arguments) {
			argv.push_back(const_cast<char*>(arg.c_str()));
		}
		argv.push_back(0);
		std::vector<char*> envp;
		envp.push_back(0);
		execve(_pathname.c_str(), &argv[0], &envp[0]);
		_exit(127);
	}

	pid_t pid = ::fork();
	if (pid == -1) {
		throw_errno();
	} else if (pid == 0) {
		exec(false);
	}

	int wstatus = 0;
	if (waitpid(pid, &wstatus, 0) == -1) {
		throw_errno();
	}
	return WEXITSTATUS(wstatus);
}

}
