// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_FDBUF
#define HELLMOUTH_OS_FDBUF

#include <string>
#include <streambuf>

#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A class for providing the buffers for a basic_fdbuf. */
template<class CharT>
struct basic_fdbuf_buffers {
	/** The inbound buffer. */
	CharT _gbuf[0x1000];

	/** The outbound buffer. */
	CharT _pbuf[0x1000];
};

/** A stream buffer template class for reading from and writing to
 * a file or socket descriptor. */
template<class CharT, class Traits = std::char_traits<CharT>>
class basic_fdbuf:
	public basic_fdbuf_buffers<CharT>,
	public std::basic_streambuf<CharT, Traits> {
private:
	using basic_fdbuf_buffers<CharT>::_gbuf;
	using basic_fdbuf_buffers<CharT>::_pbuf;
	using std::basic_streambuf<CharT, Traits>::pbase;
	using std::basic_streambuf<CharT, Traits>::pptr;
	using std::basic_streambuf<CharT, Traits>::epptr;
	using std::basic_streambuf<CharT, Traits>::pbump;
	using std::basic_streambuf<CharT, Traits>::setp;
	using std::basic_streambuf<CharT, Traits>::setg;

	/** The underlying file descriptor. */
	file_descriptor _fd;
public:
	/** Construct buffer from file or socket descriptor (copied).
	 * This will cause the underlying file descriptor to be
	 * duplicated, therefore there is no requirement for the
	 * original to outive the fdbuf.
	 *
	 * @param fd the underlying file descriptor
	 */
	basic_fdbuf(const file_descriptor& fd):
		_fd(fd) {}

	/** Construct buffer from file or socket descriptor (moved).
	 * @param fd the underlying file descriptor
	 */
	basic_fdbuf(file_descriptor&& fd):
		_fd(std::move(fd)) {}
protected:
	Traits::int_type underflow() override {
		size_t count = _fd.read(_gbuf, sizeof(_gbuf));
		setg(_gbuf, _gbuf, _gbuf + count);
		return (count != 0) ? _gbuf[0] : Traits::eof();
	}

	Traits::int_type overflow(Traits::int_type ch) override {
		if (sync() == -1) {
			return Traits::eof();
		}
		setp(_pbuf, _pbuf + sizeof(_pbuf));
		if (ch != Traits::eof()) {
			*pptr() = ch;
			pbump(1);
		}
		return 0;
	}

	int sync() override {
		if (!pptr()) {
			return 0;
		}
		try {
			size_t nbyte = pptr() - pbase();
			CharT* ptr = pbase();
			while (nbyte != 0) {
				size_t count = _fd.write(ptr, nbyte);
				ptr += count;
				nbyte -= count;
			}
			setp(pbase(), epptr());
			return 0;
		} catch (std::system_error&) {
			setp(0, 0);
			return -1;
		}
	}
};

/** A stream buffer class for reading and writing chars using
 * a file or socket descriptor. */
typedef basic_fdbuf<char> fdbuf;

}

#endif
