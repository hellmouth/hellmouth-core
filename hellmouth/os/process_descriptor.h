// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_PROCESS_DESCRIPTOR
#define HELLMOUTH_OS_PROCESS_DESCRIPTOR

#include <signal.h>

#include "hellmouth/os/file_descriptor.h"

namespace hellmouth::os {

/** A class to represent a PID file descriptor. */
class process_descriptor:
	public file_descriptor {
public:
	/** Create empty process descriptor object. */
	process_descriptor() = default;

	/** Create new process descriptor object.
	 *
	 * @param pid the PID of the target process
	 * @param flags the bitwise OR of any required flags
	 */
	process_descriptor(pid_t pid, int flags);

	/** Send a signal to the target process.
	 * @param sig the signal number
	 */
	void kill(int sig);

	/** Wait for process to terminate.
	 * Permitted options include WEXITED, WSTOPPED, WCONTINUED,
	 * WHOHANG and WNOWAIT.
	 *
	 * @param options the bitwise OR of any required options
	 * @return the reason for termination
	 */
	siginfo_t wait(int options);
};

}

#endif
