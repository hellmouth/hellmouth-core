// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <unistd.h>

#include "hellmouth/os/utility.h"
#include "hellmouth/os/pipe.h"

namespace hellmouth::os {

struct pipe::builder {
	int pipefd[2];
	builder();
};

pipe::builder::builder() {
	if (::pipe(pipefd) == -1) {
		throw_errno();
	}
}

pipe::pipe(const builder& pb):
	readfd(pb.pipefd[0]),
	writefd(pb.pipefd[1]) {}

pipe::pipe():
	pipe(builder()) {}

}
