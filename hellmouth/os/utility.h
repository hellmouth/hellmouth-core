// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_OS_UTILITY
#define HELLMOUTH_OS_UTILITY

#include <filesystem>

namespace hellmouth::os {

/** Throw a std::system_error based on the value of errno. */
void throw_errno();

/** Make a pathname for a lock file for locking a given pathname.
 * @param pathname the pathname to be locked
 * @return the corresponding lock file pathname
 */
std::filesystem::path make_lockname(const std::filesystem::path pathname);

/** Make a name for a temporary file corresponding to a given pathname.
 * @param pathname the permanent pathname
 * @return the corresponding temporary pathname
 */
std::filesystem::path make_tempname(const std::filesystem::path pathname);

}

#endif
