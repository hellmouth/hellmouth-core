// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CBOR_ENCODER
#define HELLMOUTH_CBOR_ENCODER

#include <ostream>

#include "hellmouth/data/fwd.h"

namespace hellmouth::cbor {

/** A class for encoding a CBOR data stream. */
class encoder {
private:
	/** An output stream for receiving the encoded CBOR. */
	std::ostream* _out;

	/** Write encoded data.
	 * @param buffer a buffer containing the data to be written
	 * @param nbyte the number of bytes to be written
	 */
	template<typename T>
	void _write(const T& buffer, ssize_t nbyte = sizeof(T)) {
		_out->write(reinterpret_cast<const char*>(&buffer), nbyte);
	}

	/** Encode an 8-bit value.
	 * @param value the value to be encoded
	 */
	void _encode_uint8(uint8_t value) {
		_write(value);
	}

	/** Encode a 16-bit value.
	 * @param value the value to be encoded
	 */
	void _encode_uint16(uint16_t value) {
		unsigned char buffer[2];
		buffer[0] = value >> 8;
		buffer[1] = value;
		_write(buffer);
	}

	/** Encode a 32-bit value.
	 * @param value the value to be encoded
	 */
	void _encode_uint32(uint32_t value) {
		unsigned char buffer[4];
		buffer[0] = value >> 24;
		buffer[1] = value >> 16;
		buffer[2] = value >> 8;
		buffer[3] = value;
		_write(buffer);
	}

	/** Encode a 64-bit value.
	 * @param value the value to be encoded
	 */
	void _encode_uint64(uint64_t value) {
		unsigned char buffer[8];
		buffer[0] = value >> 56;
		buffer[1] = value >> 48;
		buffer[2] = value >> 40;
		buffer[3] = value >> 32;
		buffer[4] = value >> 24;
		buffer[5] = value >> 16;
		buffer[6] = value >> 8;
		buffer[7] = value;
		_write(buffer);
	}

	/** Encode prefix containing major type and argument.
	 * The base value is obtained by shifting the major type left by
	 * five bits.
	 *
	 * Where there are multiple valid options for encoding, the
	 * shortest one will be chosen.
	 *
	 * @param base a base value equal to the shifted major type
	 * @param arg a 64-bit unsigned argument to be encoded
	 */
	void _encode_prefix(uint8_t base, uint64_t arg);
public:
	/** Construct CBOR encoder.
	 * The output stream must outlive the encoder.
	 *
	 * @param out an output stream for receiving the encoded CBOR
	 */
	encoder(std::ostream& out):
		_out(&out) {}

	virtual ~encoder() = default;

	/** Encode a null value.
	 * @param value the value to be encoded
	 */
	void operator()(const data::null_t& value);

	/** Encode a boolean.
	 * @param value the value to be encoded
	 */
	void operator()(data::boolean value);

	/** Encode a signed 32-bit integer.
	 * @param value the value to be encoded
	 */
	void operator()(data::small_integer value);

	/** Encode a data::string.
	 * @param value the string to be encoded
	 */
	void operator()(const data::string& value);

	/** Encode a data::array.
	 * @param value the array to be encoded
	 */
	void operator()(const data::array& value);

	/** Encode a data::object.
	 * @param value the object to be encoded
	 */
	void operator()(const data::object& value);

	/** Encode a data::any.
	 * @param value the value to be encoded
	 */
	void operator()(const data::any& value);
};

}

#endif
