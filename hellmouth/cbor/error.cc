// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdio.h>
#include <stdarg.h>

#include "hellmouth/cbor/error.h"

namespace hellmouth::cbor {

static std::string format(const char* format, ...) {
	va_list vlist;
	va_start(vlist, format);

	char buffer[256];
	vsnprintf(buffer, sizeof(buffer), format, vlist);
	return buffer;
}

reserved_cbor_error::reserved_cbor_error(unsigned char code):
	cbor_error(format("reserved CBOR code 0x%02x", code)) {}

unsupported_cbor_error::unsupported_cbor_error(unsigned char code):
	cbor_error(format("unsupported CBOR code 0x%02x", code)) {}

disallowed_cbor_error::disallowed_cbor_error(unsigned char code):
	cbor_error(format("disallowed CBOR code 0x%02x", code)) {}

}

