// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CBOR_ERROR
#define HELLMOUTH_CBOR_ERROR

#include <stdexcept>
#include <string>

namespace hellmouth::cbor {

/** A base class for reporting CBOR-related errors. */
class cbor_error:
	public std::runtime_error {
public:
	/** Construct generic CBOR error.
	 * @param errmsg a human-readable message describing the error
	 */
	cbor_error(const std::string& errmsg):
		std::runtime_error(errmsg) {}
};

/** An error for reporting input reserved by the CBOR specification
 * for future use.
 */
class reserved_cbor_error:
	public cbor_error {
public:
	/** Construct reserved CBOR error.
	 * @param code the byte which triggered the error
	 */
	reserved_cbor_error(unsigned char code);
};

/** An error for reporting input allowed by the CBOR specification,
 * but not supported by this implementation.
 */
class unsupported_cbor_error:
	public cbor_error {
public:
	/** Construct unsupported CBOR error.
	 * @param code the byte which triggered the error
	 */
	unsupported_cbor_error(unsigned char code);
};

/** An error for reporting input allowed by the CBOR specification,
 * but not in the context in which it was found.
 *
 * For example, an indefinite-length character string must contain
 * a sequence of definite-lenfth character strings terminated by a
 * break code. Other values are forbidden, even if they would have
 * been allowed in a stand-alone context.
 */
class disallowed_cbor_error:
	public cbor_error {
public:
	/** Construct disallowed CBOR error.
	 * @param code the byte which triggered the error
	 */
	disallowed_cbor_error(unsigned char code);
};

}

#endif
