// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include "hellmouth/data/string.h"
#include "hellmouth/data/array.h"
#include "hellmouth/data/object.h"
#include "hellmouth/cbor/encoder.h"

namespace hellmouth::cbor {

void encoder::_encode_prefix(uint8_t base, uint64_t arg) {
	if (arg <= 0x17) {
		_encode_uint8(base + arg);
	} else if (arg <= 0xff) {
		_encode_uint8(base + 0x18);
		_encode_uint8(arg);
	} else if (arg <= 0xffff) {
		_encode_uint8(base + 0x19);
		_encode_uint16(arg);
	} else if (arg <= 0xffffffff) {
		_encode_uint8(base + 0x1a);
		_encode_uint32(arg);
	} else {
		_encode_uint8(base + 0x1b);
		_encode_uint64(arg);
	}
}

void encoder::operator()(const data::null_t&) {
	_encode_uint8(0xf6);
}

void encoder::operator()(data::boolean value) {
	_encode_uint8(value ? 0xf5 : 0xf4);
}

void encoder::operator()(data::small_integer value) {
	if (value >= 0) {
		_encode_prefix(0x00, value);
	} else {
		_encode_prefix(0x20, -1 - value);
	}
}

void encoder::operator()(const data::string& value) {
	_encode_prefix(0x60, value.length());
	_write(*value.data(), value.length());
}

void encoder::operator()(const data::array& value) {
	_encode_prefix(0x80, value.size());
	for (auto& v : value) {
		(*this)(v);
	}
}

void encoder::operator()(const data::object& value) {
	_encode_prefix(0xa0, value.size());
	for (auto& [k, v] : value) {
		(*this)(k);
		(*this)(v);
	}
}

void encoder::operator()(const data::any& value) {
	value.visit(*this);
}

}
