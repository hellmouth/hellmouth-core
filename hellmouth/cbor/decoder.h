// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_CBOR_DECODER
#define HELLMOUTH_CBOR_DECODER

#include <istream>

#include "hellmouth/data/any.h"

namespace hellmouth::cbor {

/** A class for decoding a CBOR data stream.
 * In the current implementation there are no limits on the amount of
 * memory that can be consumed during decoding.
 */
class decoder {
private:
	/** A enumeration for classifying initial byte values. */
	enum mode_type {
		/** Allowed in a stand-alone value. */
		mode_standalone = 0x01,

		/** Allowed in a breakable array or map. */
		mode_struct = 0x02,

		/** Allowed in a breakable character string. */
		mode_string = 0x08,

		/** Defined by CBOR specification, but not supported
		 * by this implementation. */
		mode_unsupported = 0x40,

		/** Reserved for future extension by CBOR specification. */
		mode_reserved = 0x80
	};

	/** An input stream for providing the encoded CBOR. */
	std::istream* _in;

	/** Read encoded data.
	 * @param buffer a buffer to hold the data to be read
	 * @param nbyte the number of bytes to be written
	 */
	template<typename T>
	void _read(T& buffer, ssize_t nbyte = sizeof(T)) {
		_in->read(reinterpret_cast<char*>(&buffer), nbyte);
	}

	/** Decode an 8-bit value.
	 * @return the decoded value
	 */
	uint8_t _decode_uint8() {
		unsigned char buffer[1];
		_read(buffer);
		return buffer[0];
	}

	/** Decode an 16-bit value.
	 * @return the decoded value
	 */
	uint16_t _decode_uint16() {
		unsigned char buffer[2];
		_read(buffer);
		uint64_t result = buffer[0];
		result <<= 8;
		result |= buffer[1];
		return result;
	}

	/** Decode an 32-bit value.
	 * @return the decoded value
	 */
	uint32_t _decode_uint32() {
		unsigned char buffer[4];
		_read(buffer);
		uint64_t result = buffer[0];
		result <<= 8;
		result |= buffer[1];
		result <<= 8;
		result |= buffer[2];
		result <<= 8;
		result |= buffer[3];
		return result;
	}

	/** Decode an 64-bit value.
	 * @return the decoded value
	 */
	uint64_t _decode_uint64() {
		unsigned char buffer[8];
		_read(buffer);
		uint64_t result = buffer[0];
		result <<= 8;
		result |= buffer[1];
		result <<= 8;
		result |= buffer[2];
		result <<= 8;
		result |= buffer[3];
		result <<= 8;
		result |= buffer[4];
		result <<= 8;
		result |= buffer[5];
		result <<= 8;
		result |= buffer[6];
		result <<= 8;
		result |= buffer[7];
		return result;
	}

	/** A class to represent a CBOR break value. */
	class break_type {};
public:
	/** Construct decoder.
	 * The input stream must outlive the decoder.
	 *
	 * @param in an input stream for providing the encoded CBOR
	 */
	decoder(std::istream& in):
		_in(&in) {}

	virtual ~decoder() = default;
private:
	/** Decode a non-breakable character string.
	 * @param size the number of bytes in the string
	 */
	data::string _decode_string(uint64_t size);

	/** Decode a breakable character string. */
	data::string _decode_string_breakable();

	/** Decode a CBOR array with a given number of values.
	 * @param size the number of entries in the array, or -1 if breakable
	 * @param mode a mode selecting the permitted initial byte values
	 */
	data::array _decode_array(
		uint64_t size, mode_type mode = mode_standalone);

	/** Decode a CBOR map with a given number of values.
	 * @param size the number of entries in the map, or -1 if breakable
	 * @param mode a mode selecting the permitted initial byte values
	 */
	data::object _decode_map(
		uint64_t size, mode_type mode = mode_standalone);
public:
	/** Decode any CBOR value.
	 * @param mode a mode selecting the permitted initial byte values
	 * @return the decoded value
	 */
	data::any operator()(mode_type mode = mode_standalone);
};

}

#endif
