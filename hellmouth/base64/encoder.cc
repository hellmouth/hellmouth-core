// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/base64/encoder.h"

namespace hellmouth::base64 {

static const char alphabet[] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

std::string encoder::operator()(octet::string_view data, bool finalise) {
	std::string output;
	output.reserve(((data.length() + (finalise ? 4 : 2)) / 3) * 4);

	for (auto b : data) {
		_buffer <<= 8;
		_buffer |= b;
		if (_buffer >= 0x01000000) {
			for (unsigned i = 0; i != 4; ++i) {
				output.push_back(alphabet[(_buffer >> 18) & 0x3f]);
				_buffer <<= 6;
			}
			_buffer = 1;
		}
	}

	if (finalise && _buffer != 1) {
		unsigned padidx = 4;
		while (_buffer < 0x01000000) {
			_buffer <<= 8;
			padidx -= 1;
		}
		for (unsigned i = 0; i != 4; ++i) {
			if (i >= padidx) {
				output.push_back(alphabet[0x40]);
			} else {
				output.push_back(alphabet[(_buffer >> 18) & 0x3f]);
			}
			_buffer <<= 6;
		}
		_buffer = 1;
	}

	return output;
}

}
