// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#include <stdexcept>

#include "hellmouth/base64/decoder.h"

namespace hellmouth::base64 {

/** A constant for indicating that a character is invalid. */
static const unsigned char nv = -1;

/** A constant for identifying the padding character. */
static const unsigned char pd = -2;

static const unsigned char table[] = {
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, 62, nv, nv, nv, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, nv, nv, nv, pd, nv, nv,
	nv,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, nv, nv, nv, nv, nv,
	nv, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv,
	nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv, nv};

octet::string decoder::operator()(std::string_view str, bool finalise) {
	octet::string output;
	output.reserve(((str.length() + 3) / 4) * 3);

	for (char ch : str) {
		unsigned char value = table[unsigned(ch)];
		if (value < 64) {
			if (_padding != 0) {
				throw std::invalid_argument(
					"base64 padding character expected");
			}
			_buffer <<= 6;
			_buffer |= value;
		} else if (value == pd) {
			_buffer <<= 6;
			_padding += 1;
		} else {
			throw std::invalid_argument("invalid base64 character");
		}
		if (_buffer >= 0x01000000) {
			if (_padding > 2) {
				_buffer = 1;
				_padding = 0;
				throw std::invalid_argument("invalid base64 padding");
			}
			for (unsigned i = 0; i != 3 - _padding; ++i) {
				output.push_back((_buffer >> 16) & 0xff);
				_buffer <<= 8;
			}
			_buffer = 1;
			_padding = 0;
		}
	}

	if (finalise && _buffer != 1) {
		throw std::invalid_argument("incomplete base64 block");
	}

	return output;
}

}
