// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_BASE64_DECODER
#define HELLMOUTH_BASE64_DECODER

#include <cstdint>
#include <string_view>

#include "hellmouth/octet/string.h"

namespace hellmouth::base64 {

/** A class for decoding data using base64. */
class decoder {
private:
	/** A buffer for storing partially-decoded blocks.
	 * This is equal to the value which would be obtained by:
	 *
	 * - converting any undecoded characters to their numerical values,
	 * - concatenating those values as 6-bit unsigned integers,
	 * - prepending a 6-bit unsigned integer with a value of 1,
	 * - prepending with binary zeros until the length is 32 bits, then
	 * - interpreting that sequence as an unsigned integer
	 *   in network byte order.
	 */
	uint32_t _buffer = 1;

	/** The number of bytes of undecoded padding. */
	unsigned int _padding = 0;
public:
	/** Decode a block of data.
	 * Output is returned in units of 24 bits (four encoded characters
	 * mapped to three unencoded octets), therefore the output from any
	 * given call to this function may contain more or less information
	 * than the input.
	 *
	 * Finalising does not cause any additional data to be returned, but
	 * will throw an exception if the final block has not been completed.
	 *
	 * @param str the string to be decoded
	 * @param finalise true to finalise the decoding, otherwise false
	 * @return any completed blocks of decoded output
	 */
	octet::string operator()(std::string_view str, bool finalise = true);
};

}

#endif
