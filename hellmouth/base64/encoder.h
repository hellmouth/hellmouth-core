// This file is part of the HELLMOUTH cluster system.
// Copyright 2024-25 Graham Shaw.
// Distribution and modification are permitted within the terms of the
// GNU General Public License (version 3 or any later version).

#ifndef HELLMOUTH_BASE64_ENCODER
#define HELLMOUTH_BASE64_ENCODER

#include <cstdint>
#include <string>

#include "hellmouth/octet/string_view.h"

namespace hellmouth::base64 {

/** A class for encoding data using base64. */
class encoder {
private:
	/** A buffer for storing partially-encoded blocks.
	 * This is equal to the value which would be obtained by:
	 *
	 * - concatenating any unencoded octets,
	 * - prepending an 8-bit unsigned integer with a value of 1,
	 * - prepending binary zeros until the length is 32 bits, then
	 * - interpreting the result as an unsigned integer
	 *   in network byte order.
	 */
	uint32_t _buffer = 1;
public:
	/** Encode a block of data.
	 * Output is returned in units of 24 bits (three unencoded octets
	 * mapped to four encoded characters), therefore the output from any
	 * given call to this function may contain more or less information
	 * than the input.
	 *
	 * @param data the data to be encoded
	 * @param finalise true to finalise the encoding, otherwise false
	 * @return any completed blocks of encoded output
	 */
	std::string operator()(octet::string_view data, bool finalise = true);
};

}

#endif
